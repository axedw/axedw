SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=Konsumentpris per PL Hemk�p; ClientUser=Administrator;' For Session;


create volatile table ZZNB00, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	Sales_Org_Id	VARCHAR(50), 
	Company_Cd	VARCHAR(50), 
	Calendar_Dt	DATE, 
	KonsPrisExkl	FLOAT, 
	KonsPrisInkl	FLOAT)
primary index (Cost_Price_List_Cd, Article_Seq_Num, Sales_Org_Id, Company_Cd, Calendar_Dt) on commit preserve rows

;insert into ZZNB00 
select	a12.Cost_Price_List_Cd  Cost_Price_List_Cd,
	a11.Article_Seq_Num  Article_Seq_Num,
	a13.Sales_Org_Id  Sales_Org_Id,
	a14.Company_Cd  Company_Cd,
	a11.Calendar_Dt  Calendar_Dt,
	avg(a11.Retail_Price_Amt_Excl_VAT)  KonsPrisExkl,
	avg(a11.Retail_Price_Amt_Incl_VAT)  KonsPrisInkl
from	ITSemCMNVOUT.ARTICLE_RETAIL_PRICE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Location_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.SALES_ORGANIZATION_D	a13
	  on 	(a12.Sales_Org_Seq_Num = a13.Sales_Org_Seq_Num)
	join	ITSemCMNVOUT.COMPANY_D	a14
	  on 	(a13.Company_Seq_Num = a14.Company_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a15
	  on 	(a11.Article_Seq_Num = a15.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Calendar_Dt = a18.Calendar_Dt)
where	(a13.Sales_Org_Id in ('S005')
 and a17.Art_Hier_Lvl_2_Id in ('13')
 and a18.Calendar_Week_Id = 201228)
group by	a12.Cost_Price_List_Cd,
	a11.Article_Seq_Num,
	a13.Sales_Org_Id,
	a14.Company_Cd,
	a11.Calendar_Dt

create volatile table ZZMB01, no fallback, no log(
	Cost_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	Sales_Org_Id	VARCHAR(50), 
	Company_Cd	VARCHAR(50), 
	WJXBFS1	DATE)
primary index (Cost_Price_List_Cd, Article_Seq_Num, Sales_Org_Id, Company_Cd) on commit preserve rows

;insert into ZZMB01 
select	pc11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	pc11.Sales_Org_Id  Sales_Org_Id,
	pc11.Company_Cd  Company_Cd,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
group by	pc11.Cost_Price_List_Cd,
	pc11.Article_Seq_Num,
	pc11.Sales_Org_Id,
	pc11.Company_Cd

select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(a15.Article_Id)  Article_Id,
	max(a15.Article_Desc)  Article_Desc,
	pa11.Company_Cd  Company_Cd,
	max(a13.Company_Name)  Company_Name,
	pa11.Sales_Org_Id  Sales_Org_Id,
	max(a14.Sales_Org_Name)  Sales_Org_Name,
	pa11.Cost_Price_List_Cd  Cost_Price_List_Cd,
	max(a16.Price_List_Name)  Price_List_Name,
	max(pa11.KonsPrisExkl)  WJXBFS1,
	max(pa11.KonsPrisInkl)  WJXBFS2
from	ZZNB00	pa11
	join	ZZMB01	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Company_Cd = pa12.Company_Cd and 
	pa11.Cost_Price_List_Cd = pa12.Cost_Price_List_Cd and 
	pa11.Sales_Org_Id = pa12.Sales_Org_Id)
	join	ITSemCMNVOUT.COMPANY_D	a13
	  on 	(pa11.Company_Cd = a13.Company_Cd)
	join	ITSemCMNVOUT.SALES_ORGANIZATION_D	a14
	  on 	(pa11.Sales_Org_Id = a14.Sales_Org_Id)
	join	ITSemCMNVOUT.ARTICLE_D	a15
	  on 	(pa11.Article_Seq_Num = a15.Article_Seq_Num)
	join	ITSemCMNVOUT.PRICE_LIST_D	a16
	  on 	(pa11.Cost_Price_List_Cd = a16.Price_List_Cd)
group by	pa11.Article_Seq_Num,
	pa11.Company_Cd,
	pa11.Sales_Org_Id,
	pa11.Cost_Price_List_Cd


SET QUERY_BAND = NONE For Session;


drop table ZZNB00

drop table ZZMB01