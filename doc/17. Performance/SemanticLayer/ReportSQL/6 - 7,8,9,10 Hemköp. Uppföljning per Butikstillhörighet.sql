SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=6 - MA204b - Uppföljning av bonusnivåer och medlemstyper per hembutik; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Store_Id	INTEGER, 
	Loyalty_Tier_Class_Cd	VARCHAR(80), 
	AntMedl	FLOAT, 
	AntAktivMedl	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Member_Type, Store_Id, Loyalty_Tier_Class_Cd) on commit preserve rows

;insert into ZZMD00 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a14.Loyalty_Member_Type  Loyalty_Member_Type,
	a15.Store_Id  Store_Id,
	a13.Loyalty_Tier_Level_Id  Loyalty_Tier_Class_Cd,
	sum((Case when a11.Member_Ind > 0 then a11.Member_Sum_Cnt else NULL end))  AntMedl,
	sum((Case when a11.Active_Member_Ind > 0 then a11.Member_Sum_Cnt else NULL end))  AntAktivMedl
from	ITSemCMNVOUT.LOYALTY_MEMBER_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TL_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_Month_Id and 
	a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_TIER_LEVEL_D	a13
	  on 	(a12.Member_Tier_Lvl_Seq_Num = a13.Loyalty_Tier_Level_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a14
	  on 	(a11.Member_Account_Seq_Num = a14.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(a14.Home_Store_Seq_Num = a15.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a16
	  on 	(a14.Loyalty_Program_Seq_Num = a16.Loyalty_Program_Seq_Num)
where	(a15.Concept_Cd in ('HEM', 'HEA')
 and a16.Loyalty_Program_Id in ('1-MQ7V')
 and a11.Calendar_Month_Id in (201306)
 and (a11.Member_Ind > 0
 or a11.Active_Member_Ind > 0))
group by	a11.Calendar_Month_Id,
	a14.Loyalty_Member_Type,
	a15.Store_Id,
	a13.Loyalty_Tier_Level_Id

create volatile table ZZMD01, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Store_Id	INTEGER, 
	Loyalty_Tier_Class_Cd	VARCHAR(80), 
	AntMedlMedKop	INTEGER)
primary index (Calendar_Month_Id, Loyalty_Member_Type, Store_Id, Loyalty_Tier_Class_Cd) on commit preserve rows

;insert into ZZMD01 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a16.Loyalty_Member_Type  Loyalty_Member_Type,
	a17.Store_Id  Store_Id,
	a15.Loyalty_Tier_Level_Id  Loyalty_Tier_Class_Cd,
	count(distinct a12.Member_Account_Seq_Num)  AntMedlMedKop
from	ITSemCMNVOUT.SALES_LOYALTY_TRANSACTION_F	a11
	join	ITSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a12
	  on 	(a11.Contact_Account_Seq_Num = a12.Contact_Account_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TL_MONTH_D	a14
	  on 	(a12.Member_Account_Seq_Num = a14.Member_Account_Seq_Num and 
	a13.Calendar_Month_Id = a14.Calendar_Month_Id)
	join	ITSemCMNVOUT.LOYALTY_TIER_LEVEL_D	a15
	  on 	(a14.Member_Tier_Lvl_Seq_Num = a15.Loyalty_Tier_Level_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a16
	  on 	(a12.Member_Account_Seq_Num = a16.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a17
	  on 	(a16.Home_Store_Seq_Num = a17.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a18
	  on 	(a16.Loyalty_Program_Seq_Num = a18.Loyalty_Program_Seq_Num)
where	(a17.Concept_Cd in ('HEM', 'HEA')
 and a18.Loyalty_Program_Id in ('1-MQ7V')
 and a13.Calendar_Month_Id in (201306)
 and a11.Training_Mode_Status in ('N'))
group by	a13.Calendar_Month_Id,
	a16.Loyalty_Member_Type,
	a17.Store_Id,
	a15.Loyalty_Tier_Level_Id

create volatile table ZZMD02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Store_Id	INTEGER, 
	Loyalty_Tier_Class_Cd	VARCHAR(80), 
	AntKvMedl	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Member_Type, Store_Id, Loyalty_Tier_Class_Cd) on commit preserve rows

;insert into ZZMD02 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a17.Loyalty_Member_Type  Loyalty_Member_Type,
	a18.Store_Id  Store_Id,
	a16.Loyalty_Tier_Level_Id  Loyalty_Tier_Class_Cd,
	sum(a11.Receipt_Cnt)  AntKvMedl
from	ITSemCMNVOUT.SALES_TRANSACTION_F	a11
	join	ITSemCMNVOUT.STORE_DEPARTMENT_D	a12
	  on 	(a11.Store_Department_Seq_Num = a12.Store_Department_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
	join	ITSemCMNVOUT.LOYALTY_CONTACT_ACCOUNT_D	a14
	  on 	(a11.Contact_Account_Seq_Num = a14.Contact_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TL_MONTH_D	a15
	  on 	(a13.Calendar_Month_Id = a15.Calendar_Month_Id and 
	a14.Member_Account_Seq_Num = a15.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_TIER_LEVEL_D	a16
	  on 	(a15.Member_Tier_Lvl_Seq_Num = a16.Loyalty_Tier_Level_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a17
	  on 	(a14.Member_Account_Seq_Num = a17.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a18
	  on 	(a17.Home_Store_Seq_Num = a18.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a19
	  on 	(a17.Loyalty_Program_Seq_Num = a19.Loyalty_Program_Seq_Num)
where	(a18.Concept_Cd in ('HEM', 'HEA')
 and a19.Loyalty_Program_Id in ('1-MQ7V')
 and a13.Calendar_Month_Id in (201306)
 and a11.Contact_Account_Seq_Num <> -1
 and a12.Store_Department_Id not in ('__Prestore__'))
group by	a13.Calendar_Month_Id,
	a17.Loyalty_Member_Type,
	a18.Store_Id,
	a16.Loyalty_Tier_Level_Id

create volatile table ZZMD03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Store_Id	INTEGER, 
	Loyalty_Tier_Class_Cd	VARCHAR(80), 
	BonusgrBel	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Member_Type, Store_Id, Loyalty_Tier_Class_Cd) on commit preserve rows

;insert into ZZMD03 
select	a12.Calendar_Month_Id  Calendar_Month_Id,
	a14.Loyalty_Member_Type  Loyalty_Member_Type,
	a15.Store_Id  Store_Id,
	a13.Loyalty_Tier_Level_Id  Loyalty_Tier_Class_Cd,
	sum(a11.Bonus_Applicable_Amt)  BonusgrBel
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TL_MONTH_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_TIER_LEVEL_D	a13
	  on 	(a12.Member_Tier_Lvl_Seq_Num = a13.Loyalty_Tier_Level_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a14
	  on 	(a11.Member_Account_Seq_Num = a14.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(a14.Home_Store_Seq_Num = a15.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a16
	  on 	(a14.Loyalty_Program_Seq_Num = a16.Loyalty_Program_Seq_Num)
where	(a15.Concept_Cd in ('HEM', 'HEA')
 and a16.Loyalty_Program_Id in ('1-MQ7V')
 and a12.Calendar_Month_Id in (201306)
 and a11.Loyalty_Trans_Sub_Type_Cd in ('Purchase'))
group by	a12.Calendar_Month_Id,
	a14.Loyalty_Member_Type,
	a15.Store_Id,
	a13.Loyalty_Tier_Level_Id

create volatile table ZZSP04, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Store_Id	INTEGER, 
	Loyalty_Tier_Class_Cd	VARCHAR(80), 
	PurchaseAmt	FLOAT, 
	AccrualAmt	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Member_Type, Store_Id, Loyalty_Tier_Class_Cd) on commit preserve rows

;insert into ZZSP04 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a14.Loyalty_Member_Type  Loyalty_Member_Type,
	a15.Store_Id  Store_Id,
	a13.Loyalty_Tier_Level_Id  Loyalty_Tier_Class_Cd,
	sum(a11.Purchase_Amt)  PurchaseAmt,
	sum(a11.Accrual_Amt)  AccrualAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TL_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_Month_Id and 
	a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_TIER_LEVEL_D	a13
	  on 	(a12.Member_Tier_Lvl_Seq_Num = a13.Loyalty_Tier_Level_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a14
	  on 	(a11.Member_Account_Seq_Num = a14.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(a14.Home_Store_Seq_Num = a15.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a16
	  on 	(a14.Loyalty_Program_Seq_Num = a16.Loyalty_Program_Seq_Num)
where	(a15.Concept_Cd in ('HEM', 'HEA')
 and a16.Loyalty_Program_Id in ('1-MQ7V')
 and a11.Calendar_Month_Id in (201306))
group by	a11.Calendar_Month_Id,
	a14.Loyalty_Member_Type,
	a15.Store_Id,
	a13.Loyalty_Tier_Level_Id

create volatile table ZZSP05, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Store_Id	INTEGER, 
	Loyalty_Tier_Class_Cd	VARCHAR(80), 
	DowngradedAmt	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Member_Type, Store_Id, Loyalty_Tier_Class_Cd) on commit preserve rows

;insert into ZZSP05 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a14.Loyalty_Member_Type  Loyalty_Member_Type,
	a15.Store_Id  Store_Id,
	a13.Loyalty_Tier_Level_Id  Loyalty_Tier_Class_Cd,
	sum(a11.Downgraded_Amt)  DowngradedAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TL_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_Month_Id and 
	a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_TIER_LEVEL_D	a13
	  on 	(a12.Member_Tier_Lvl_Seq_Num = a13.Loyalty_Tier_Level_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a14
	  on 	(a11.Member_Account_Seq_Num = a14.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a15
	  on 	(a14.Home_Store_Seq_Num = a15.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a16
	  on 	(a14.Loyalty_Program_Seq_Num = a16.Loyalty_Program_Seq_Num)
where	(a15.Concept_Cd in ('HEM', 'HEA')
 and a16.Loyalty_Program_Id in ('1-MQ7V')
 and a11.Calendar_Month_Id in (201306))
group by	a11.Calendar_Month_Id,
	a14.Loyalty_Member_Type,
	a15.Store_Id,
	a13.Loyalty_Tier_Level_Id

create volatile table ZZMD06, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	Store_Id	INTEGER, 
	Loyalty_Tier_Class_Cd	VARCHAR(80), 
	PurchaseAmt	FLOAT, 
	AccrualAmt	FLOAT, 
	DowngradedAmt	FLOAT)
primary index (Calendar_Month_Id, Loyalty_Member_Type, Store_Id, Loyalty_Tier_Class_Cd) on commit preserve rows

;insert into ZZMD06 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Loyalty_Member_Type, pa12.Loyalty_Member_Type)  Loyalty_Member_Type,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	coalesce(pa11.Loyalty_Tier_Class_Cd, pa12.Loyalty_Tier_Class_Cd)  Loyalty_Tier_Class_Cd,
	pa11.PurchaseAmt  PurchaseAmt,
	pa11.AccrualAmt  AccrualAmt,
	pa12.DowngradedAmt  DowngradedAmt
from	ZZSP04	pa11
	full outer join	ZZSP05	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Loyalty_Member_Type = pa12.Loyalty_Member_Type and 
	pa11.Loyalty_Tier_Class_Cd = pa12.Loyalty_Tier_Class_Cd and 
	pa11.Store_Id = pa12.Store_Id)

select	coalesce(pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa16.Store_Id)  Store_Id,
	max(a18.Store_Name)  Store_Name,
	coalesce(pa12.Loyalty_Member_Type, pa13.Loyalty_Member_Type, pa14.Loyalty_Member_Type, pa15.Loyalty_Member_Type, pa16.Loyalty_Member_Type)  Loyalty_Member_Type,
	max(a19.Loyalty_Member_Type_Desc)  Loyalty_Member_Type_Desc,
	coalesce(pa12.Loyalty_Tier_Class_Cd, pa13.Loyalty_Tier_Class_Cd, pa14.Loyalty_Tier_Class_Cd, pa15.Loyalty_Tier_Class_Cd, pa16.Loyalty_Tier_Class_Cd)  Loyalty_Tier_Class_Cd,
	max(a17.Loyalty_Level_Name)  Loyalty_Tier_Class_Desc,
	coalesce(pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id)  Calendar_Month_Id,
	max(pa12.AntMedl)  AntMedl,
	max(pa12.AntAktivMedl)  AntAktivMedl,
	max(pa13.AntMedlMedKop)  AntMedlMedKop,
	max(pa14.AntKvMedl)  AntKvMedl,
	max(pa15.BonusgrBel)  BonusgrBel,
	max(pa16.PurchaseAmt)  PurchaseAmt,
	max(pa16.AccrualAmt)  AccrualAmt,
	max(pa16.DowngradedAmt)  DowngradedAmt
from	ZZMD00	pa12
	full outer join	ZZMD01	pa13
	  on 	(pa12.Calendar_Month_Id = pa13.Calendar_Month_Id and 
	pa12.Loyalty_Member_Type = pa13.Loyalty_Member_Type and 
	pa12.Loyalty_Tier_Class_Cd = pa13.Loyalty_Tier_Class_Cd and 
	pa12.Store_Id = pa13.Store_Id)
	full outer join	ZZMD02	pa14
	  on 	(coalesce(pa12.Calendar_Month_Id, pa13.Calendar_Month_Id) = pa14.Calendar_Month_Id and 
	coalesce(pa12.Loyalty_Member_Type, pa13.Loyalty_Member_Type) = pa14.Loyalty_Member_Type and 
	coalesce(pa12.Loyalty_Tier_Class_Cd, pa13.Loyalty_Tier_Class_Cd) = pa14.Loyalty_Tier_Class_Cd and 
	coalesce(pa12.Store_Id, pa13.Store_Id) = pa14.Store_Id)
	full outer join	ZZMD03	pa15
	  on 	(coalesce(pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id) = pa15.Calendar_Month_Id and 
	coalesce(pa12.Loyalty_Member_Type, pa13.Loyalty_Member_Type, pa14.Loyalty_Member_Type) = pa15.Loyalty_Member_Type and 
	coalesce(pa12.Loyalty_Tier_Class_Cd, pa13.Loyalty_Tier_Class_Cd, pa14.Loyalty_Tier_Class_Cd) = pa15.Loyalty_Tier_Class_Cd and 
	coalesce(pa12.Store_Id, pa13.Store_Id, pa14.Store_Id) = pa15.Store_Id)
	full outer join	ZZMD06	pa16
	  on 	(coalesce(pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id) = pa16.Calendar_Month_Id and 
	coalesce(pa12.Loyalty_Member_Type, pa13.Loyalty_Member_Type, pa14.Loyalty_Member_Type, pa15.Loyalty_Member_Type) = pa16.Loyalty_Member_Type and 
	coalesce(pa12.Loyalty_Tier_Class_Cd, pa13.Loyalty_Tier_Class_Cd, pa14.Loyalty_Tier_Class_Cd, pa15.Loyalty_Tier_Class_Cd) = pa16.Loyalty_Tier_Class_Cd and 
	coalesce(pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id) = pa16.Store_Id)
	join	ITSemCMNVOUT.LOYALTY_TIER_LEVEL_D	a17
	  on 	(coalesce(pa12.Loyalty_Tier_Class_Cd, pa13.Loyalty_Tier_Class_Cd, pa14.Loyalty_Tier_Class_Cd, pa15.Loyalty_Tier_Class_Cd, pa16.Loyalty_Tier_Class_Cd) = a17.Loyalty_Tier_Level_Id)
	join	ITSemCMNVOUT.STORE_D	a18
	  on 	(coalesce(pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa16.Store_Id) = a18.Store_Id)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TYPE_D	a19
	  on 	(coalesce(pa12.Loyalty_Member_Type, pa13.Loyalty_Member_Type, pa14.Loyalty_Member_Type, pa15.Loyalty_Member_Type, pa16.Loyalty_Member_Type) = a19.Loyalty_Member_Type)
group by	coalesce(pa12.Store_Id, pa13.Store_Id, pa14.Store_Id, pa15.Store_Id, pa16.Store_Id),
	coalesce(pa12.Loyalty_Member_Type, pa13.Loyalty_Member_Type, pa14.Loyalty_Member_Type, pa15.Loyalty_Member_Type, pa16.Loyalty_Member_Type),
	coalesce(pa12.Loyalty_Tier_Class_Cd, pa13.Loyalty_Tier_Class_Cd, pa14.Loyalty_Tier_Class_Cd, pa15.Loyalty_Tier_Class_Cd, pa16.Loyalty_Tier_Class_Cd),
	coalesce(pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZMD02

drop table ZZMD03

drop table ZZSP04

drop table ZZSP05

drop table ZZMD06