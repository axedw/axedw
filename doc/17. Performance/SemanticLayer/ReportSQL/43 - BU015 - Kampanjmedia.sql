SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=BU015 - Kampanjmedia; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Preferred_Activity_Cd_Id	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	KampForsBelExBVBer	FLOAT, 
	KampInkopsBelEx	FLOAT)
primary index (Preferred_Activity_Cd_Id, Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD00 
select	a11.Preferred_Activity_Code_Id  Preferred_Activity_Cd_Id,
	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.GP_Unit_Selling_Price_Amt)  KampForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  KampInkopsBelEx
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a18
	  on 	(a11.Tran_Dt = a18.Calendar_Dt)
where	(a18.Calendar_Week_Id in (201318)
 and a17.Concept_Cd in ('HEM')
 and a16.Art_Hier_Lvl_2_Id in ('-1', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '25', '26', '27')
 and a17.Store_Type_Cd in ('CORP')
 and a11.Preferred_Activity_Code_Id in ('C08'))
group by	a11.Preferred_Activity_Code_Id,
	a16.Art_Hier_Lvl_2_Id

create volatile table ZZMD01, no fallback, no log(
	Art_Hier_Lvl_2_Id	CHAR(2), 
	TotInkopsbelExAllaProd	FLOAT, 
	TotForsBelExBVBerAllaProd	FLOAT)
primary index (Art_Hier_Lvl_2_Id) on commit preserve rows

;insert into ZZMD01 
select	a16.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	sum(a11.Unit_Cost_Amt)  TotInkopsbelExAllaProd,
	sum(a11.GP_Unit_Selling_Price_Amt)  TotForsBelExBVBerAllaProd
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.MEASURING_UNIT_D	a13
	  on 	(a12.Measuring_Unit_Seq_Num = a13.Measuring_Unit_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a15.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a17
	  on 	(a11.Store_Seq_Num = a17.Store_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_1_D	a18
	  on 	(a15.Art_Hier_Lvl_1_Seq_Num = a18.Art_Hier_Lvl_1_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a19
	  on 	(a11.Tran_Dt = a19.Calendar_Dt)
where	(a19.Calendar_Week_Id in (201318)
 and a17.Concept_Cd in ('HEM')
 and a17.Store_Type_Cd in ('CORP')
 and a18.Art_Hier_Lvl_1_Id in ('GG')
 and a16.Art_Hier_Lvl_2_Id not in ('24', '28'))
group by	a16.Art_Hier_Lvl_2_Id

select	pa11.Preferred_Activity_Cd_Id  Preferred_Activity_Cd_Id,
	max(a14.Activity_Cd_Desc)  Activity_Cd_Desc,
	pa11.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	max(a13.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	max(pa11.KampForsBelExBVBer)  KampForsBelExBVBer,
	max(pa12.TotInkopsbelExAllaProd)  TotInkopsbelExAllaProd,
	max(pa11.KampInkopsBelEx)  KampInkopsBelEx,
	max(pa12.TotForsBelExBVBerAllaProd)  TotForsBelExBVBerAllaProd
from	ZZMD00	pa11
	left outer join	ZZMD01	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a13
	  on 	(pa11.Art_Hier_Lvl_2_Id = a13.Art_Hier_Lvl_2_Id)
	join	ITSemCMNVOUT.ACTIVITY_CODE_D	a14
	  on 	(pa11.Preferred_Activity_Cd_Id = a14.Activity_Cd_Id)
group by	pa11.Preferred_Activity_Cd_Id,
	pa11.Art_Hier_Lvl_2_Id


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01
