SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=RG004 - Rapportguide F�rs�ljning med svinn; ClientUser=A18354;' For Session;		2	0	0	0	0	

create volatile table TUPBDXLANMQ000,
		no fallback, no log( 	Calendar_Week_Id	INTEGER) primary index (Calendar_Week_Id) 
	on	commit preserve rows	;
	
insert	into TUPBDXLANMQ000  
select	a11.Calendar_Week_Id  Calendar_Week_Id 
from	PRSemCMNVOUT.CALENDAR_DAY_D	a11 
where	a11.Calendar_Dt = DATE '2013-11-13' 
group	by	a11.Calendar_Week_Id	

	COLLECT STATISTICS 	ON	TUPBDXLANMQ000 INDEX (Calendar_Week_Id)		

create volatile table T61V5Y2J3MD001, no fallback, no log( 	Scan_Code_Seq_Num	INTEGER,  	SvinnKr	FLOAT) primary index (Scan_Code_Seq_Num) 
	on	commit preserve rows		
	;
insert	into T61V5Y2J3MD001  
select	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num, 	
sum(a11.Total_Line_Value_Amt)  SvinnKr 
from	PRSemCMNVOUT.ARTICLE_KNOWN_LOSS_F	a11 	
join	PRSemCMNVOUT.CALENDAR_DAY_D	a12 	  
	on		(a11.Adjustment_Dt = a12.Calendar_Dt) 	
	join	TUPBDXLANMQ000	pa13 	  
	on		(a12.Calendar_Week_Id = pa13.Calendar_Week_Id) 	
	join	PRSemCMNVOUT.STORE_D	a14 	  
	on		(a11.Store_Seq_Num = a14.Store_Seq_Num) 	
	join	PRSemCMNVOUT.CONCEPT_D	a15 	  
	on		(a14.Concept_Cd = a15.Concept_Cd) 	
	join	PRSemCMNVOUT.SCAN_CODE_D	a16 	  
	on		(a11.Scan_Code_Seq_Num = a16.Scan_Code_Seq_Num) 	
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17 	  
	on		(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num) 
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18 	  
	on		(a16.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num) 
where	(a14.Store_Type_Cd in ('CORP')  
	and	a18.Art_Hier_Lvl_2_Id in ('05')  
	and	(a15.Concept_Top_Cd in ('HE')  
	or	a15.Concept_Top_Cd in ('PX'))) 
group	by	a11.Scan_Code_Seq_Num		
	
COLLECT STATISTICS 	ON	T61V5Y2J3MD001 INDEX (Scan_Code_Seq_Num);		

	create volatile table TT7N7SRNZMD002, no fallback, no log( 	Scan_Code_Seq_Num	INTEGER,  	ForsBelExSvinn	FLOAT) primary index (Scan_Code_Seq_Num) 
	on	commit preserve rows		
	
;

--
-- DETTA PASS ANV�NDER AJI, MEN INGEN REDUCERING (INGEN AV DIMENSIONERNA SOM DET FINNS WHERE/JOIN P�)
--

insert	into TT7N7SRNZMD002  
select	a11.Scan_Code_Seq_Num  Scan_Code_Seq_Num, 	
sum(a11.Unit_Selling_Price_Amt)  ForsBelExSvinn 
from	PRSemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11 	
join	TUPBDXLANMQ000	pa12 	  
	on		(a11.Calendar_Week_Id = pa12.Calendar_Week_Id) 	
	join	PRSemCMNVOUT.STORE_D	a13 	  
	on		(a11.Store_Seq_Num = a13.Store_Seq_Num) 	
	join	PRSemCMNVOUT.CONCEPT_D	a14 	  
	on		(a13.Concept_Cd = a14.Concept_Cd) 	
	join	PRSemCMNVOUT.SCAN_CODE_D	a15 	  
	on		(a11.Scan_Code_Seq_Num = a15.Scan_Code_Seq_Num) 	
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16 	  
	on		(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num) 	
	join	PRSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17 	  
	on		(a15.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num) 
where	(a13.Store_Type_Cd in ('CORP')  
	and	a17.Art_Hier_Lvl_2_Id in ('05')  
	and	(a14.Concept_Top_Cd in ('HE')  
	or	a14.Concept_Top_Cd in ('PX'))) 
group	by	a11.Scan_Code_Seq_Num		

COLLECT STATISTICS 	ON	TT7N7SRNZMD002 INDEX (Scan_Code_Seq_Num);		

select	coalesce(pa11.Scan_Code_Seq_Num, pa12.Scan_Code_Seq_Num)  
Scan_Code_Seq_Num, 	
a13.Scan_Cd  Scan_Cd, 	
pa11.SvinnKr  SvinnKr, 	
pa12.ForsBelExSvinn  ForsBelExSvinn 
from	T61V5Y2J3MD001	pa11 	
full outer join	TT7N7SRNZMD002	pa12 	  
	on		(pa11.Scan_Code_Seq_Num = pa12.Scan_Code_Seq_Num) 	
	join	PRSemCMNVOUT.SCAN_CODE_D	a13 	  
	on		(coalesce(pa11.Scan_Code_Seq_Num, pa12.Scan_Code_Seq_Num) = a13.Scan_Code_Seq_Num)	
	
