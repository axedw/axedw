SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=13 - MA206b - Statistik utbetalad bonus per hembutik; ClientUser=Administrator;' For Session;


select	a14.Store_Id  Store_Id,
	max(a14.Store_Name)  Store_Name,
	a12.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Remittance_Amt)  Utbetalad_bonus
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.CALENDAR_MONTH_D	a12
	  on 	(a11.Calendar_Month_Id = a12.Calendar_Next_Month_Id)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(a13.Home_Store_Seq_Num = a14.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a15
	  on 	(a13.Loyalty_Program_Seq_Num = a15.Loyalty_Program_Seq_Num)
where	(a15.Loyalty_Program_Id in ('1-MQ7V')
 and a14.Concept_Cd in ('HEM')
 and a12.Calendar_Month_Id in (201306))
group by	a14.Store_Id,
	a12.Calendar_Month_Id


SET QUERY_BAND = NONE For Session;
