SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=PS301 - Rapportguide Prissättning - vecka test; ClientUser=Administrator;' For Session;


create volatile table ZZSP00, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	AntalSalda	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZSP00 
select	a12.Article_Seq_Num  Article_Seq_Num,
	sum(a11.Item_Qty)  AntalSalda
from	ITSemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11
	join	ITSemCMNVOUT.SCAN_CODE_D	a12
	  on 	(a11.Scan_Code_Seq_Num = a12.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a14
	  on 	(a12.Art_Hier_Lvl_2_Seq_Num = a14.Art_Hier_Lvl_2_Seq_Num)
where	(a11.Calendar_Week_Id in (201342)
 and a13.Cost_Price_List_Cd in ('AA')
 and a14.Art_Hier_Lvl_2_Id in ('13'))
group by	a12.Article_Seq_Num

create volatile table ZZNB01, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	BasForPaslag	FLOAT)
primary index (Article_Seq_Num, Calendar_Dt) on commit preserve rows

;insert into ZZNB01 
select	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg(a11.Base_Price_Amt)  BasForPaslag
from	ITSemCMNVOUT.ARTICLE_COST_PRICE_F	a11
	join	ITSemCMNVOUT.ARTICLE_D	a12
	  on 	(a11.Article_Seq_Num = a12.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a13
	  on 	(a12.Art_Hier_Lvl_8_Seq_Num = a13.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a14
	  on 	(a12.Art_Hier_Lvl_2_Seq_Num = a14.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Calendar_Dt = a15.Calendar_Dt)
where	(a15.Calendar_Week_Id in (201342)
 and a11.Cost_Price_List_Cd in ('AA')
 and a14.Art_Hier_Lvl_2_Id in ('13'))
group by	a11.Article_Seq_Num,
	a11.Calendar_Dt

create volatile table ZZMB02, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB02 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB01	pc11
group by	pc11.Article_Seq_Num

create volatile table ZZSP03, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZSP03 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.BasForPaslag)  WJXBFS1
from	ZZNB01	pa11
	join	ZZMB02	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
group by	pa11.Article_Seq_Num

create volatile table ZZMD04, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	AntalSalda	FLOAT, 
	BasForPaslag	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMD04 
select	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num)  Article_Seq_Num,
	pa11.AntalSalda  AntalSalda,
	pa12.WJXBFS1  BasForPaslag
from	ZZSP00	pa11
	full outer join	ZZSP03	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num)

create volatile table ZZNB05, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	Listpris	FLOAT, 
	GODWFLAG6_1	INTEGER, 
	FUREfterskNet	FLOAT, 
	GODWFLAG9_1	INTEGER, 
	FURFakturaNet	FLOAT, 
	GODWFLAGc_1	INTEGER, 
	EstFraktkostn	FLOAT, 
	GODWFLAGf_1	INTEGER, 
	EstKvantrab	FLOAT, 
	GODWFLAG12_1	INTEGER, 
	EstPallrabatt	FLOAT, 
	GODWFLAG15_1	INTEGER, 
	FUREfterskLP	FLOAT, 
	GODWFLAG18_1	INTEGER, 
	FURFaktListpr	FLOAT, 
	GODWFLAG1b_1	INTEGER, 
	FMR	FLOAT, 
	GODWFLAG1e_1	INTEGER, 
	MREfterskott	FLOAT, 
	GODWFLAG21_1	INTEGER, 
	CMRZ145	FLOAT, 
	GODWFLAG24_1	INTEGER, 
	PVServPOS	FLOAT, 
	GODWFLAG27_1	INTEGER, 
	PVServPOS1	FLOAT, 
	GODWFLAG2a_1	INTEGER, 
	PVServPOS2	FLOAT, 
	GODWFLAG2d_1	INTEGER, 
	PVServPlock	FLOAT, 
	GODWFLAG30_1	INTEGER, 
	PVServPOS3	FLOAT, 
	GODWFLAG33_1	INTEGER, 
	NettoNetPris	FLOAT, 
	GODWFLAG36_1	INTEGER, 
	ServEDIst	FLOAT, 
	GODWFLAG39_1	INTEGER, 
	ServKrossSt	FLOAT, 
	GODWFLAG3c_1	INTEGER, 
	ServPlockSt	FLOAT, 
	GODWFLAG3f_1	INTEGER, 
	ServPOSst	FLOAT, 
	GODWFLAG42_1	INTEGER, 
	ServSKst	FLOAT, 
	GODWFLAG45_1	INTEGER, 
	EMVFaktor	FLOAT, 
	GODWFLAG48_1	INTEGER, 
	MarknFakt	FLOAT, 
	GODWFLAG4b_1	INTEGER, 
	Grundpris	FLOAT, 
	GODWFLAG4e_1	INTEGER, 
	LogpaslagBas	FLOAT, 
	GODWFLAG51_1	INTEGER, 
	LogpaslagArt	FLOAT, 
	GODWFLAG54_1	INTEGER, 
	PVServPOS4	FLOAT, 
	GODWFLAG57_1	INTEGER, 
	PVServPOS5	FLOAT, 
	GODWFLAG5a_1	INTEGER, 
	OHPalagg	FLOAT, 
	GODWFLAG5d_1	INTEGER, 
	SIPalagg	FLOAT, 
	GODWFLAG60_1	INTEGER, 
	TransfPris	FLOAT, 
	GODWFLAG63_1	INTEGER, 
	DirektlevPaslag	FLOAT, 
	GODWFLAG66_1	INTEGER)
primary index (Article_Seq_Num, Calendar_Dt) on commit preserve rows

;insert into ZZNB05 
select	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg((Case when a11.Price_Condition_Type_Cd in ('PB00') then a11.Price_Condition_Amt else NULL end))  Listpris,
	max((Case when a11.Price_Condition_Type_Cd in ('PB00') then 1 else 0 end))  GODWFLAG6_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z115') then a11.Price_Condition_Amt else NULL end))  FUREfterskNet,
	max((Case when a11.Price_Condition_Type_Cd in ('Z115') then 1 else 0 end))  GODWFLAG9_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z105') then a11.Price_Condition_Amt else NULL end))  FURFakturaNet,
	max((Case when a11.Price_Condition_Type_Cd in ('Z105') then 1 else 0 end))  GODWFLAGc_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z127') then a11.Price_Condition_Amt else NULL end))  EstFraktkostn,
	max((Case when a11.Price_Condition_Type_Cd in ('Z127') then 1 else 0 end))  GODWFLAGf_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z120', 'Z122') then a11.Price_Condition_Amt else NULL end))  EstKvantrab,
	max((Case when a11.Price_Condition_Type_Cd in ('Z120', 'Z122') then 1 else 0 end))  GODWFLAG12_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z125') then a11.Price_Condition_Amt else NULL end))  EstPallrabatt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z125') then 1 else 0 end))  GODWFLAG15_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z110') then a11.Price_Condition_Amt else NULL end))  FUREfterskLP,
	max((Case when a11.Price_Condition_Type_Cd in ('Z110') then 1 else 0 end))  GODWFLAG18_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z101') then a11.Price_Condition_Amt else NULL end))  FURFaktListpr,
	max((Case when a11.Price_Condition_Type_Cd in ('Z101') then 1 else 0 end))  GODWFLAG1b_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZFMR') then a11.Price_Condition_Amt else NULL end))  FMR,
	max((Case when a11.Price_Condition_Type_Cd in ('ZFMR') then 1 else 0 end))  GODWFLAG1e_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z140', 'Z141') then a11.Price_Condition_Amt else NULL end))  MREfterskott,
	max((Case when a11.Price_Condition_Type_Cd in ('Z140', 'Z141') then 1 else 0 end))  GODWFLAG21_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z145') then a11.Price_Condition_Amt else NULL end))  CMRZ145,
	max((Case when a11.Price_Condition_Type_Cd in ('Z145') then 1 else 0 end))  GODWFLAG24_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z150') then a11.Price_Condition_Amt else NULL end))  PVServPOS,
	max((Case when a11.Price_Condition_Type_Cd in ('Z150') then 1 else 0 end))  GODWFLAG27_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z151') then a11.Price_Condition_Amt else NULL end))  PVServPOS1,
	max((Case when a11.Price_Condition_Type_Cd in ('Z151') then 1 else 0 end))  GODWFLAG2a_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z155') then a11.Price_Condition_Amt else NULL end))  PVServPOS2,
	max((Case when a11.Price_Condition_Type_Cd in ('Z155') then 1 else 0 end))  GODWFLAG2d_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z156') then a11.Price_Condition_Amt else NULL end))  PVServPlock,
	max((Case when a11.Price_Condition_Type_Cd in ('Z156') then 1 else 0 end))  GODWFLAG30_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z157') then a11.Price_Condition_Amt else NULL end))  PVServPOS3,
	max((Case when a11.Price_Condition_Type_Cd in ('Z157') then 1 else 0 end))  GODWFLAG33_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZEKN') then a11.Price_Condition_Amt else NULL end))  NettoNetPris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZEKN') then 1 else 0 end))  GODWFLAG36_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z180') then a11.Price_Condition_Amt else NULL end))  ServEDIst,
	max((Case when a11.Price_Condition_Type_Cd in ('Z180') then 1 else 0 end))  GODWFLAG39_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z185') then a11.Price_Condition_Amt else NULL end))  ServKrossSt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z185') then 1 else 0 end))  GODWFLAG3c_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z189') then a11.Price_Condition_Amt else NULL end))  ServPlockSt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z189') then 1 else 0 end))  GODWFLAG3f_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z183') then a11.Price_Condition_Amt else NULL end))  ServPOSst,
	max((Case when a11.Price_Condition_Type_Cd in ('Z183') then 1 else 0 end))  GODWFLAG42_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z187') then a11.Price_Condition_Amt else NULL end))  ServSKst,
	max((Case when a11.Price_Condition_Type_Cd in ('Z187') then 1 else 0 end))  GODWFLAG45_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z232') then a11.Price_Condition_Amt else NULL end))  EMVFaktor,
	max((Case when a11.Price_Condition_Type_Cd in ('Z232') then 1 else 0 end))  GODWFLAG48_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z230') then a11.Price_Condition_Amt else NULL end))  MarknFakt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z230') then 1 else 0 end))  GODWFLAG4b_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZPB2') then a11.Price_Condition_Amt else NULL end))  Grundpris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZPB2') then 1 else 0 end))  GODWFLAG4e_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z250', 'Z251') then a11.Price_Condition_Amt else NULL end))  LogpaslagBas,
	max((Case when a11.Price_Condition_Type_Cd in ('Z250', 'Z251') then 1 else 0 end))  GODWFLAG51_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z252', 'Z253') then a11.Price_Condition_Amt else NULL end))  LogpaslagArt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z252', 'Z253') then 1 else 0 end))  GODWFLAG54_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z260') then a11.Price_Condition_Amt else NULL end))  PVServPOS4,
	max((Case when a11.Price_Condition_Type_Cd in ('Z260') then 1 else 0 end))  GODWFLAG57_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z261') then a11.Price_Condition_Amt else NULL end))  PVServPOS5,
	max((Case when a11.Price_Condition_Type_Cd in ('Z261') then 1 else 0 end))  GODWFLAG5a_1,
	avg((Case when a11.Price_Condition_Type_Cd = 'Z262' then a11.Price_Condition_Amt else NULL end))  OHPalagg,
	max((Case when a11.Price_Condition_Type_Cd = 'Z262' then 1 else 0 end))  GODWFLAG5d_1,
	avg((Case when (a11.Price_Condition_Type_Cd in ('Z241') or a11.Price_Condition_Type_Cd in ('Z240')) then a11.Price_Condition_Amt else NULL end))  SIPalagg,
	max((Case when (a11.Price_Condition_Type_Cd in ('Z241') or a11.Price_Condition_Type_Cd in ('Z240')) then 1 else 0 end))  GODWFLAG60_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then a11.Price_Condition_Amt else NULL end))  TransfPris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then 1 else 0 end))  GODWFLAG63_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z278') then a11.Price_Condition_Amt else NULL end))  DirektlevPaslag,
	max((Case when a11.Price_Condition_Type_Cd in ('Z278') then 1 else 0 end))  GODWFLAG66_1
from	ITSemCMNVOUT.ARTICLE_COST_PRICE_COND_F	a11
	join	ITSemCMNVOUT.ARTICLE_D	a12
	  on 	(a11.Article_Seq_Num = a12.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a13
	  on 	(a12.Art_Hier_Lvl_8_Seq_Num = a13.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a14
	  on 	(a12.Art_Hier_Lvl_2_Seq_Num = a14.Art_Hier_Lvl_2_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a15
	  on 	(a11.Calendar_Dt = a15.Calendar_Dt)
where	(a15.Calendar_Week_Id in (201342)
 and a11.Cost_Price_List_Cd in ('AA')
 and a14.Art_Hier_Lvl_2_Id in ('13')
 and (a11.Price_Condition_Type_Cd in ('PB00')
 or a11.Price_Condition_Type_Cd in ('Z115')
 or a11.Price_Condition_Type_Cd in ('Z105')
 or a11.Price_Condition_Type_Cd in ('Z127')
 or a11.Price_Condition_Type_Cd in ('Z120', 'Z122')
 or a11.Price_Condition_Type_Cd in ('Z125')
 or a11.Price_Condition_Type_Cd in ('Z110')
 or a11.Price_Condition_Type_Cd in ('Z101')
 or a11.Price_Condition_Type_Cd in ('ZFMR')
 or a11.Price_Condition_Type_Cd in ('Z140', 'Z141')
 or a11.Price_Condition_Type_Cd in ('Z145')
 or a11.Price_Condition_Type_Cd in ('Z150')
 or a11.Price_Condition_Type_Cd in ('Z151')
 or a11.Price_Condition_Type_Cd in ('Z155')
 or a11.Price_Condition_Type_Cd in ('Z156')
 or a11.Price_Condition_Type_Cd in ('Z157')
 or a11.Price_Condition_Type_Cd in ('ZEKN')
 or a11.Price_Condition_Type_Cd in ('Z180')
 or a11.Price_Condition_Type_Cd in ('Z185')
 or a11.Price_Condition_Type_Cd in ('Z189')
 or a11.Price_Condition_Type_Cd in ('Z183')
 or a11.Price_Condition_Type_Cd in ('Z187')
 or a11.Price_Condition_Type_Cd in ('Z232')
 or a11.Price_Condition_Type_Cd in ('Z230')
 or a11.Price_Condition_Type_Cd in ('ZPB2')
 or a11.Price_Condition_Type_Cd in ('Z250', 'Z251')
 or a11.Price_Condition_Type_Cd in ('Z252', 'Z253')
 or a11.Price_Condition_Type_Cd in ('Z260')
 or a11.Price_Condition_Type_Cd in ('Z261')
 or a11.Price_Condition_Type_Cd = 'Z262'
 or (a11.Price_Condition_Type_Cd in ('Z241')
 or a11.Price_Condition_Type_Cd in ('Z240'))
 or a11.Price_Condition_Type_Cd in ('ZKP2')
 or a11.Price_Condition_Type_Cd in ('Z278')))
group by	a11.Article_Seq_Num,
	a11.Calendar_Dt

create volatile table ZZMB06, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB06 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG6_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC07, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC07 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.Listpris)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB06	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG6_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB08, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB08 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG9_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC09, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC09 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FUREfterskNet)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB08	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG9_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0A, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0A 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAGc_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0B, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0B 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FURFakturaNet)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0A	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAGc_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0C, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0C 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAGf_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0D, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0D 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EstFraktkostn)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0C	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAGf_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0E, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0E 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG12_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0F, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0F 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EstKvantrab)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0E	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG12_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0G, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0G 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG15_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0H, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0H 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EstPallrabatt)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0G	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG15_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0I, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0I 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG18_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0J, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0J 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FUREfterskLP)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0I	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG18_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0K, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0K 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG1b_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0L, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0L 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FURFaktListpr)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0K	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG1b_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0M, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0M 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG1e_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0N, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0N 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FMR)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0M	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG1e_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0O, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0O 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG21_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0P, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0P 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.MREfterskott)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0O	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG21_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0Q, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0Q 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG24_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0R, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0R 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.CMRZ145)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0Q	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG24_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0S, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0S 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG27_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0T, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0T 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0S	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG27_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0U, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0U 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG2a_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0V, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0V 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS1)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0U	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG2a_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0W, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0W 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG2d_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0X, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0X 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS2)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0W	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG2d_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0Y, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0Y 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG30_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0Z, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0Z 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPlock)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB0Y	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG30_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB10, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB10 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG33_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC11, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC11 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS3)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB10	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG33_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB12, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB12 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG36_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC13, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC13 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.NettoNetPris)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB12	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG36_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB14, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB14 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG39_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC15, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC15 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServEDIst)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB14	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG39_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB16, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB16 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG3c_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC17, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC17 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServKrossSt)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB16	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG3c_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB18, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB18 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG3f_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC19, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC19 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServPlockSt)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB18	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG3f_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1A, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1A 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG42_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1B, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1B 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServPOSst)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1A	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG42_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1C, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1C 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG45_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1D, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1D 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServSKst)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1C	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG45_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1E, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1E 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG48_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1F, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1F 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EMVFaktor)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1E	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG48_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1G, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1G 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG4b_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1H, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1H 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.MarknFakt)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1G	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG4b_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1I, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1I 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG4e_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1J, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1J 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.Grundpris)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1I	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG4e_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1K, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1K 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG51_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1L, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1L 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.LogpaslagBas)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1K	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG51_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1M, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1M 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG54_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1N, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1N 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.LogpaslagArt)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1M	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG54_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1O, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1O 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG57_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1P, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1P 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS4)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1O	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG57_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1Q, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1Q 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG5a_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1R, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1R 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS5)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1Q	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG5a_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1S, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1S 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG5d_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1T, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1T 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.OHPalagg)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1S	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG5d_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1U, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1U 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG60_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1V, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1V 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.SIPalagg)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1U	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG60_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1W, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1W 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG63_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1X, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1X 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.TransfPris)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1W	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG63_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1Y, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1Y 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB05	pc11
where	pc11.GODWFLAG66_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1Z, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1Z 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.DirektlevPaslag)  WJXBFS1
from	ZZNB05	pa11
	join	ZZMB1Y	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG66_1 = 1
group by	pa11.Article_Seq_Num

select	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num, pa131.Article_Seq_Num, pa132.Article_Seq_Num, pa133.Article_Seq_Num, pa134.Article_Seq_Num)  Article_Seq_Num,
	max(a135.Article_Id)  Article_Id,
	max(a135.Article_Desc)  Article_Desc,
	max(pa11.AntalSalda)  AntalSalda,
	max(pa12.WJXBFS1)  Listpris,
	max(pa13.WJXBFS1)  FUREfterskNet,
	max(pa14.WJXBFS1)  FURFakturaNet,
	max(pa15.WJXBFS1)  EstFraktkostn,
	max(pa16.WJXBFS1)  EstKvantrab,
	max(pa17.WJXBFS1)  EstPallrabatt,
	max(pa18.WJXBFS1)  FUREfterskLP,
	max(pa19.WJXBFS1)  FURFaktListpr,
	max(pa110.WJXBFS1)  FMR,
	(ZEROIFNULL(max(pa111.WJXBFS1)) + ZEROIFNULL(max(pa112.WJXBFS1)))  CMR,
	(ZEROIFNULL(max(pa113.WJXBFS1)) + ZEROIFNULL(max(pa114.WJXBFS1)))  EMK1st,
	((ZEROIFNULL(max(pa115.WJXBFS1)) + ZEROIFNULL(max(pa116.WJXBFS1))) + ZEROIFNULL(max(pa117.WJXBFS1)))  EMK2st,
	max(pa118.WJXBFS1)  NettoNetPris,
	max(pa119.WJXBFS1)  ServEDIst,
	max(pa120.WJXBFS1)  ServKrossSt,
	max(pa121.WJXBFS1)  ServPlockSt,
	max(pa122.WJXBFS1)  ServPOSst,
	max(pa123.WJXBFS1)  ServSKst,
	max(pa124.WJXBFS1)  EMVFaktor,
	max(pa125.WJXBFS1)  MarknFakt,
	max(pa126.WJXBFS1)  Grundpris,
	max(pa11.BasForPaslag)  BasForPaslag,
	max(pa127.WJXBFS1)  LogpaslagBas,
	max(pa128.WJXBFS1)  LogpaslagArt,
	(ZEROIFNULL(max(pa129.WJXBFS1)) + ZEROIFNULL(max(pa130.WJXBFS1)))  Kedjepaslag,
	max(pa131.WJXBFS1)  OHPalagg,
	max(pa132.WJXBFS1)  SIPalagg,
	max(pa133.WJXBFS1)  TransfPris,
	max(pa134.WJXBFS1)  DirektlevPaslag
from	ZZMD04	pa11
	full outer join	ZZNC07	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num)
	full outer join	ZZNC09	pa13
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num) = pa13.Article_Seq_Num)
	full outer join	ZZNC0B	pa14
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num) = pa14.Article_Seq_Num)
	full outer join	ZZNC0D	pa15
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num) = pa15.Article_Seq_Num)
	full outer join	ZZNC0F	pa16
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num) = pa16.Article_Seq_Num)
	full outer join	ZZNC0H	pa17
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num) = pa17.Article_Seq_Num)
	full outer join	ZZNC0J	pa18
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num) = pa18.Article_Seq_Num)
	full outer join	ZZNC0L	pa19
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num) = pa19.Article_Seq_Num)
	full outer join	ZZNC0N	pa110
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num) = pa110.Article_Seq_Num)
	full outer join	ZZNC0P	pa111
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num) = pa111.Article_Seq_Num)
	full outer join	ZZNC0R	pa112
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num) = pa112.Article_Seq_Num)
	full outer join	ZZNC0T	pa113
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num) = pa113.Article_Seq_Num)
	full outer join	ZZNC0V	pa114
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num) = pa114.Article_Seq_Num)
	full outer join	ZZNC0X	pa115
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num) = pa115.Article_Seq_Num)
	full outer join	ZZNC0Z	pa116
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num) = pa116.Article_Seq_Num)
	full outer join	ZZNC11	pa117
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num) = pa117.Article_Seq_Num)
	full outer join	ZZNC13	pa118
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num) = pa118.Article_Seq_Num)
	full outer join	ZZNC15	pa119
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num) = pa119.Article_Seq_Num)
	full outer join	ZZNC17	pa120
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num) = pa120.Article_Seq_Num)
	full outer join	ZZNC19	pa121
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num) = pa121.Article_Seq_Num)
	full outer join	ZZNC1B	pa122
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num) = pa122.Article_Seq_Num)
	full outer join	ZZNC1D	pa123
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num) = pa123.Article_Seq_Num)
	full outer join	ZZNC1F	pa124
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num) = pa124.Article_Seq_Num)
	full outer join	ZZNC1H	pa125
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num) = pa125.Article_Seq_Num)
	full outer join	ZZNC1J	pa126
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num) = pa126.Article_Seq_Num)
	full outer join	ZZNC1L	pa127
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num) = pa127.Article_Seq_Num)
	full outer join	ZZNC1N	pa128
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num) = pa128.Article_Seq_Num)
	full outer join	ZZNC1P	pa129
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num) = pa129.Article_Seq_Num)
	full outer join	ZZNC1R	pa130
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num) = pa130.Article_Seq_Num)
	full outer join	ZZNC1T	pa131
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num) = pa131.Article_Seq_Num)
	full outer join	ZZNC1V	pa132
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num, pa131.Article_Seq_Num) = pa132.Article_Seq_Num)
	full outer join	ZZNC1X	pa133
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num, pa131.Article_Seq_Num, pa132.Article_Seq_Num) = pa133.Article_Seq_Num)
	full outer join	ZZNC1Z	pa134
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num, pa131.Article_Seq_Num, pa132.Article_Seq_Num, pa133.Article_Seq_Num) = pa134.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a135
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num, pa131.Article_Seq_Num, pa132.Article_Seq_Num, pa133.Article_Seq_Num, pa134.Article_Seq_Num) = a135.Article_Seq_Num)
group by	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num, pa131.Article_Seq_Num, pa132.Article_Seq_Num, pa133.Article_Seq_Num, pa134.Article_Seq_Num)


SET QUERY_BAND = NONE For Session;


drop table ZZSP00

drop table ZZNB01

drop table ZZMB02

drop table ZZSP03

drop table ZZMD04

drop table ZZNB05

drop table ZZMB06

drop table ZZNC07

drop table ZZMB08

drop table ZZNC09

drop table ZZMB0A

drop table ZZNC0B

drop table ZZMB0C

drop table ZZNC0D

drop table ZZMB0E

drop table ZZNC0F

drop table ZZMB0G

drop table ZZNC0H

drop table ZZMB0I

drop table ZZNC0J

drop table ZZMB0K

drop table ZZNC0L

drop table ZZMB0M

drop table ZZNC0N

drop table ZZMB0O

drop table ZZNC0P

drop table ZZMB0Q

drop table ZZNC0R

drop table ZZMB0S

drop table ZZNC0T

drop table ZZMB0U

drop table ZZNC0V

drop table ZZMB0W

drop table ZZNC0X

drop table ZZMB0Y

drop table ZZNC0Z

drop table ZZMB10

drop table ZZNC11

drop table ZZMB12

drop table ZZNC13

drop table ZZMB14

drop table ZZNC15

drop table ZZMB16

drop table ZZNC17

drop table ZZMB18

drop table ZZNC19

drop table ZZMB1A

drop table ZZNC1B

drop table ZZMB1C

drop table ZZNC1D

drop table ZZMB1E

drop table ZZNC1F

drop table ZZMB1G

drop table ZZNC1H

drop table ZZMB1I

drop table ZZNC1J

drop table ZZMB1K

drop table ZZNC1L

drop table ZZMB1M

drop table ZZNC1N

drop table ZZMB1O

drop table ZZNC1P

drop table ZZMB1Q

drop table ZZNC1R

drop table ZZMB1S

drop table ZZNC1T

drop table ZZMB1U

drop table ZZNC1V

drop table ZZMB1W

drop table ZZNC1X

drop table ZZMB1Y

drop table ZZNC1Z