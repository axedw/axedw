/* New SQL Lojalitetsrapport 2 månad, 201304 alla Willys koncept */

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=MA002b - Lojalitetsrapport månad 2; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntRekrKontaktAck	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD00 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontaktAck
from	ITSemCMNVOUT.LOY_CONTACT_FIRST_SALE_DAY_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	ITSemCMNVOUT.CALENDAR_MONTH_YTD_D	a13
	  on 	(a12.Calendar_Month_Id = a13.Calendar_Month_YTD_Id)
	join	ITSemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
where	(a13.Calendar_Month_Id in (201304)
 and a14.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Month_Id

create volatile table ZZMD01, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntKvMedl	FLOAT, 
	AntKv	FLOAT, 
	AntKvEjMedl	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD01 
select	a14.Calendar_Month_Id  Calendar_Month_Id,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Receipt_Cnt else NULL end))  AntKvMedl,
	sum(a11.Receipt_Cnt)  AntKv,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Receipt_Cnt else NULL end))  AntKvEjMedl
from	ITSemCMNVOUT.SALES_TRANSACTION_F	a11
	join	ITSemCMNVOUT.STORE_DEPARTMENT_D	a12
	  on 	(a11.Store_Department_Seq_Num = a12.Store_Department_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a14
	  on 	(a11.Tran_Dt = a14.Calendar_Dt)
where	(a14.Calendar_Month_Id in (201304)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a12.Store_Department_Id not in ('__Prestore__'))
group by	a14.Calendar_Month_Id

create volatile table ZZSP02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntRekrKontakt	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZSP02 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontakt
from	ITSemCMNVOUT.LOY_CONTACT_FIRST_SALE_DAY_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Month_Id in (201304)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Month_Id

create volatile table ZZSP03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	ForsBelEx	FLOAT, 
	WJXBFS3	FLOAT, 
	WJXBFS4	FLOAT, 
	ForsBelExMedl	FLOAT, 
	GODWFLAG8_1	INTEGER, 
	KampInkBelExEjMedl	FLOAT, 
	KampForsExBVEjMedl	FLOAT, 
	GODWFLAG18_1	INTEGER, 
	AntalSaldaStEjMedl	FLOAT, 
	GODWFLAG19_1	INTEGER, 
	AntSaldaViktartEjMedl	FLOAT, 
	GODWFLAG1a_1	INTEGER, 
	KampForsExBVMedl	FLOAT, 
	KampInkBelExMedl	FLOAT, 
	GODWFLAG1b_1	INTEGER, 
	AntalSaldaStMedl	FLOAT, 
	GODWFLAG1c_1	INTEGER, 
	AntSaldaViktartMedl	FLOAT, 
	GODWFLAG1d_1	INTEGER, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT, 
	GODWFLAG22_1	INTEGER, 
	ForsBelExBVEjMedl	FLOAT, 
	ForsBelExEjMedl	FLOAT, 
	InkBelExEjMedl	FLOAT, 
	GODWFLAG25_1	INTEGER)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZSP03 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.GP_Unit_Selling_Price_Amt)  WJXBFS1,
	sum(a11.Unit_Cost_Amt)  WJXBFS2,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  WJXBFS3,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Cost_Amt else NULL end))  WJXBFS4,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExMedl,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG8_1,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.Unit_Cost_Amt else NULL end))  KampInkBelExEjMedl,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsExBVEjMedl,
	max((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then 1 else 0 end))  GODWFLAG18_1,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('UNT')) then a11.Item_Qty else NULL end))  AntalSaldaStEjMedl,
	max((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('UNT')) then 1 else 0 end))  GODWFLAG19_1,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('WGH')) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartEjMedl,
	max((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('WGH')) then 1 else 0 end))  GODWFLAG1a_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsExBVMedl,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.Unit_Cost_Amt else NULL end))  KampInkBelExMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then 1 else 0 end))  GODWFLAG1b_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('UNT')) then a11.Item_Qty else NULL end))  AntalSaldaStMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('UNT')) then 1 else 0 end))  GODWFLAG1c_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('WGH')) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('WGH')) then 1 else 0 end))  GODWFLAG1d_1,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer,
	max((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then 1 else 0 end))  GODWFLAG22_1,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVEjMedl,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExEjMedl,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Cost_Amt else NULL end))  InkBelExEjMedl,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAG25_1
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Month_Id in (201304)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Month_Id

create volatile table ZZSP04, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AccrualAmt	FLOAT, 
	PurchaseAmt	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZSP04 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Accrual_Amt)  AccrualAmt,
	sum(a11.Purchase_Amt)  PurchaseAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
where	(a11.Calendar_Month_Id in (201304)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a11.Calendar_Month_Id

create volatile table ZZMD05, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntRekrKontakt	FLOAT, 
	WJXBFS1	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	AccrualAmt	FLOAT, 
	PurchaseAmt	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD05 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id)  Calendar_Month_Id,
	pa11.AntRekrKontakt  AntRekrKontakt,
	(ZEROIFNULL(pa12.WJXBFS1) - ZEROIFNULL(pa12.WJXBFS2))  WJXBFS1,
	pa12.ForsBelEx  ForsBelEx,
	pa12.WJXBFS1  ForsBelExBVBer,
	pa12.WJXBFS2  InkopsBelEx,
	pa13.AccrualAmt  AccrualAmt,
	pa13.PurchaseAmt  PurchaseAmt
from	ZZSP02	pa11
	full outer join	ZZSP03	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)
	full outer join	ZZSP04	pa13
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa13.Calendar_Month_Id)

create volatile table ZZOP06, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP06 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.WJXBFS3  WJXBFS1,
	pa01.WJXBFS4  WJXBFS2,
	pa01.ForsBelExMedl  WJXBFS3
from	ZZSP03	pa01
where	pa01.GODWFLAG8_1 = 1

create volatile table ZZSP07, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	TotMedlRbt	FLOAT, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZSP07 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Tot_Loy_Discount_Amt)  TotMedlRbt,
	sum(a11.Tot_Discount_Amt)  WJXBFS1
from	ITSemCMNVOUT.SALES_TRANSACTION_TOTALS_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Month_Id in (201304)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a11.Contact_Account_Seq_Num <> -1)
group by	a13.Calendar_Month_Id

create volatile table ZZMD08, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	TotMedlRbt	FLOAT, 
	TotRbtExMedlRbt	FLOAT, 
	ForsBelExMedl	FLOAT, 
	InkBelExMedl	FLOAT, 
	ForsBelExBVMedlem	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD08 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	(ZEROIFNULL(pa11.WJXBFS1) - ZEROIFNULL(pa11.WJXBFS2))  WJXBFS1,
	pa12.TotMedlRbt  TotMedlRbt,
	(ZEROIFNULL(pa12.WJXBFS1) - ZEROIFNULL(pa12.TotMedlRbt))  TotRbtExMedlRbt,
	pa11.WJXBFS3  ForsBelExMedl,
	pa11.WJXBFS2  InkBelExMedl,
	pa11.WJXBFS1  ForsBelExBVMedlem
from	ZZOP06	pa11
	full outer join	ZZSP07	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)

create volatile table ZZMD09, no fallback, no log(
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Membership_Num) on commit preserve rows

;insert into ZZMD09 
select	a13.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num <> -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
where	(a11.Calendar_Month_Id in (201304)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Membership_Num

create volatile table ZZMD0A, no fallback, no log(
	AccrualAmtButik	FLOAT) on commit preserve rows

;insert into ZZMD0A 
select	sum(pa11.WJXBFS1)  AccrualAmtButik
from	ZZMD09	pa11

create volatile table ZZSP0B, no fallback, no log(
	Forfallen_bonus	FLOAT) on commit preserve rows

;insert into ZZSP0B 
select	sum(a11.Expired_Amt)  Forfallen_bonus
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Expiration_Dt = a12.Calendar_Dt)
where	a12.Calendar_Month_Id in (201304)

create volatile table ZZSP0C, no fallback, no log(
	DowngradedAmt	FLOAT) on commit preserve rows

;insert into ZZSP0C 
select	sum(a11.Downgraded_Amt)  DowngradedAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
where	a11.Calendar_Month_Id in (201304)

create volatile table ZZMD0D, no fallback, no log(
	Forfallen_bonus	FLOAT, 
	DowngradedAmt	FLOAT) on commit preserve rows

;insert into ZZMD0D 
select	pa11.Forfallen_bonus  Forfallen_bonus,
	pa12.DowngradedAmt  DowngradedAmt
from	ZZSP0B	pa11
	cross join	ZZSP0C	pa12

create volatile table ZZMD0E, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	ForsBelExBVMedlKvitto	FLOAT, 
	InkBelExMedlKvitto	FLOAT, 
	GODWFLAG11_1	INTEGER, 
	ForsBelExBVKvitto	FLOAT, 
	InkBelExKvitto	FLOAT, 
	ForsBelExBVEjMedlKvitto	FLOAT, 
	InkBelExEjMedlKvitto	FLOAT, 
	GODWFLAG26_1	INTEGER)
primary index (Calendar_Month_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZMD0E 
select	a12.Calendar_Month_Id  Calendar_Month_Id,
	a11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	a11.Store_Seq_Num  Store_Seq_Num,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVMedlKvitto,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Cost_Amt else NULL end))  InkBelExMedlKvitto,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG11_1,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVKvitto,
	sum(a11.Unit_Cost_Amt)  InkBelExKvitto,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVEjMedlKvitto,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Cost_Amt else NULL end))  InkBelExEjMedlKvitto,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAG26_1
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
where	(a12.Calendar_Month_Id in (201304)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a12.Calendar_Month_Id,
	a11.Sales_Tran_Seq_Num,
	a11.Store_Seq_Num

create volatile table ZZSP0F, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	BVKrMedlKvitto	FLOAT, 
	GODWFLAG12_1	INTEGER, 
	BVKrEjMedlKvitto	FLOAT, 
	GODWFLAG27_1	INTEGER)
primary index (Calendar_Month_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZSP0F 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	pa11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num  Store_Seq_Num,
	sum((Case when pa11.GODWFLAG11_1 = 1 then (ZEROIFNULL(pa11.ForsBelExBVMedlKvitto) - ZEROIFNULL(pa11.InkBelExMedlKvitto)) else NULL end))  BVKrMedlKvitto,
	max((Case when pa11.GODWFLAG11_1 = 1 then 1 else 0 end))  GODWFLAG12_1,
	sum((Case when pa11.GODWFLAG26_1 = 1 then (ZEROIFNULL(pa11.ForsBelExBVEjMedlKvitto) - ZEROIFNULL(pa11.InkBelExEjMedlKvitto)) else NULL end))  BVKrEjMedlKvitto,
	max((Case when pa11.GODWFLAG26_1 = 1 then 1 else 0 end))  GODWFLAG27_1
from	ZZMD0E	pa11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(pa11.Store_Seq_Num = a12.Store_Seq_Num)
where	(pa11.Calendar_Month_Id in (201304)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and (pa11.GODWFLAG11_1 = 1
 or pa11.GODWFLAG26_1 = 1))
group by	pa11.Calendar_Month_Id,
	pa11.Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num

create volatile table ZZOP0G, no fallback, no log(
	Store_Seq_Num	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Seq_Num, Sales_Tran_Seq_Num, Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0G 
select	pa01.Store_Seq_Num  Store_Seq_Num,
	pa01.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.BVKrMedlKvitto  WJXBFS1
from	ZZSP0F	pa01
where	pa01.GODWFLAG12_1 = 1

create volatile table ZZMD0H, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	BVKrMedlKvitto	FLOAT, 
	ForsBelExBVKvitto	FLOAT, 
	InkBelExKvitto	FLOAT)
primary index (Calendar_Month_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZMD0H 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Sales_Tran_Seq_Num, pa12.Sales_Tran_Seq_Num)  Sales_Tran_Seq_Num,
	coalesce(pa11.Store_Seq_Num, pa12.Store_Seq_Num)  Store_Seq_Num,
	pa11.WJXBFS1  BVKrMedlKvitto,
	pa12.ForsBelExBVKvitto  ForsBelExBVKvitto,
	pa12.InkBelExKvitto  InkBelExKvitto
from	ZZOP0G	pa11
	full outer join	ZZMD0E	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)

create volatile table ZZMD0I, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	BVProcMedlKvitto	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD0I 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	sum(ZEROIFNULL((pa11.BVKrMedlKvitto / NULLIFZERO(pa12.ForsBelExBVMedlKvitto))))  BVProcMedlKvitto
from	ZZMD0H	pa11
	join	ZZMD0E	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(pa11.Store_Seq_Num = a13.Store_Seq_Num)
where	(pa11.Calendar_Month_Id in (201304)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and pa12.GODWFLAG11_1 = 1)
group by	pa11.Calendar_Month_Id

create volatile table ZZMD0J, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	BVKrKvitto	FLOAT)
primary index (Calendar_Month_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZMD0J 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	pa11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num  Store_Seq_Num,
	sum((ZEROIFNULL(pa11.ForsBelExBVKvitto) - ZEROIFNULL(pa11.InkBelExKvitto)))  BVKrKvitto
from	ZZMD0H	pa11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(pa11.Store_Seq_Num = a12.Store_Seq_Num)
where	(pa11.Calendar_Month_Id in (201304)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	pa11.Calendar_Month_Id,
	pa11.Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num

create volatile table ZZMD0K, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	BVProcKvitto	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD0K 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	sum(ZEROIFNULL((pa11.BVKrKvitto / NULLIFZERO(pa12.ForsBelExBVKvitto))))  BVProcKvitto
from	ZZMD0J	pa11
	join	ZZMD0H	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(pa11.Store_Seq_Num = a13.Store_Seq_Num)
where	(pa11.Calendar_Month_Id in (201304)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	pa11.Calendar_Month_Id

create volatile table ZZOP0L, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0L 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.KampInkBelExEjMedl  WJXBFS1,
	pa01.KampForsExBVEjMedl  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG18_1 = 1

create volatile table ZZOP0M, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0M 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntalSaldaStEjMedl  WJXBFS1
from	ZZSP03	pa01
where	pa01.GODWFLAG19_1 = 1

create volatile table ZZOP0N, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0N 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntSaldaViktartEjMedl  WJXBFS1
from	ZZSP03	pa01
where	pa01.GODWFLAG1a_1 = 1

create volatile table ZZOP0O, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0O 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.KampForsExBVMedl  WJXBFS1,
	pa01.KampInkBelExMedl  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG1b_1 = 1

create volatile table ZZOP0P, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0P 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntalSaldaStMedl  WJXBFS1
from	ZZSP03	pa01
where	pa01.GODWFLAG1c_1 = 1

create volatile table ZZOP0Q, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0Q 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntSaldaViktartMedl  WJXBFS1
from	ZZSP03	pa01
where	pa01.GODWFLAG1d_1 = 1

create volatile table ZZMD0R, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id, Membership_Num) on commit preserve rows

;insert into ZZMD0R 
select	a11.Calendar_Month_Id  Calendar_Month_Id,
	a12.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num = -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
where	a11.Calendar_Month_Id in (201304)
group by	a11.Calendar_Month_Id,
	a12.Membership_Num

create volatile table ZZMD0S, no fallback, no log(
	AccrualAmtEjButik	FLOAT) on commit preserve rows

;insert into ZZMD0S 
select	sum(pa11.WJXBFS1)  AccrualAmtEjButik
from	ZZMD0R	pa11
where	pa11.Calendar_Month_Id in (201304)

create volatile table ZZMD0T, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	WJXBFS1	INTEGER, 
	GODWFLAG20_1	INTEGER, 
	WJXBFS2	INTEGER, 
	GODWFLAG23_1	INTEGER)
primary index (Calendar_Month_Id, Sales_Tran_Seq_Num) on commit preserve rows

;insert into ZZMD0T 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	a11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	count(distinct (Case when a11.Contact_Account_Seq_Num = -1 then a11.Scan_Code_Seq_Num else NULL end))  WJXBFS1,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAG20_1,
	count(distinct (Case when a11.Contact_Account_Seq_Num <> -1 then a11.Scan_Code_Seq_Num else NULL end))  WJXBFS2,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG23_1
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Month_Id in (201304)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and (a11.Contact_Account_Seq_Num = -1
 or a11.Contact_Account_Seq_Num <> -1))
group by	a13.Calendar_Month_Id,
	a11.Sales_Tran_Seq_Num

create volatile table ZZMD0U, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntVarorEjMedl	FLOAT, 
	AntVarorMedl	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD0U 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	sum((Case when pa11.GODWFLAG20_1 = 1 then pa11.WJXBFS1 else NULL end))  AntVarorEjMedl,
	sum((Case when pa11.GODWFLAG23_1 = 1 then pa11.WJXBFS2 else NULL end))  AntVarorMedl
from	ZZMD0T	pa11
where	(pa11.Calendar_Month_Id in (201304)
 and (pa11.GODWFLAG20_1 = 1
 or pa11.GODWFLAG23_1 = 1))
group by	pa11.Calendar_Month_Id

create volatile table ZZOP0V, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0V 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.KampInkopsBelEx  WJXBFS1,
	pa01.KampForsBelExBVBer  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG22_1 = 1

create volatile table ZZOP0W, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0W 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.ForsBelExBVEjMedl  WJXBFS1,
	pa01.ForsBelExEjMedl  WJXBFS2,
	pa01.InkBelExEjMedl  WJXBFS3
from	ZZSP03	pa01
where	pa01.GODWFLAG25_1 = 1

create volatile table ZZMD0X, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	BVProcEjMedlKvitto	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD0X 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	sum(ZEROIFNULL((pa11.BVKrEjMedlKvitto / NULLIFZERO(pa12.ForsBelExBVEjMedlKvitto))))  BVProcEjMedlKvitto
from	ZZSP0F	pa11
	join	ZZMD0E	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(pa11.Store_Seq_Num = a13.Store_Seq_Num)
where	(pa11.Calendar_Month_Id in (201304)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and pa12.GODWFLAG26_1 = 1
 and pa11.GODWFLAG27_1 = 1)
group by	pa11.Calendar_Month_Id

select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id, pa115.Calendar_Month_Id, pa116.Calendar_Month_Id, pa118.Calendar_Month_Id, pa119.Calendar_Month_Id)  Calendar_Month_Id,
	pa11.AntRekrKontaktAck  AntRekrKontaktAck,
	pa12.AntKvMedl  AntKvMedl,
	ZEROIFNULL((pa13.AntRekrKontakt / NULLIFZERO((ZEROIFNULL(pa13.AntRekrKontakt) + (ZEROIFNULL(pa12.AntKv) - ZEROIFNULL(pa12.AntKvMedl))))))  RekrKvotMed,
	ZEROIFNULL((pa15.WJXBFS1 / NULLIFZERO(pa13.WJXBFS1)))  AndelBVMedl,
	pa15.TotMedlRbt  TotMedlRbt,
	pa15.TotRbtExMedlRbt  TotRbtExMedlRbt,
	pa13.ForsBelEx  ForsBelEx,
	pa15.ForsBelExMedl  ForsBelExMedl,
	pa12.AntKvEjMedl  AntKvEjMedl,
	pa120.AccrualAmtButik  AccrualAmtButik,
	pa121.Forfallen_bonus  Forfallen_bonus,
	pa17.BVProcMedlKvitto  BVProcMedlKvitto,
	pa18.BVProcKvitto  BVProcKvitto,
	pa19.WJXBFS1  KampInkBelExEjMedl,
	pa13.ForsBelExBVBer  ForsBelExBVBer,
	pa15.InkBelExMedl  InkBelExMedl,
	(ZEROIFNULL(pa110.WJXBFS1) + ZEROIFNULL(pa111.WJXBFS1))  AntStreckkodEjMedl,
	pa112.WJXBFS1  KampForsExBVMedl,
	(ZEROIFNULL(pa113.WJXBFS1) + ZEROIFNULL(pa114.WJXBFS1))  AntStreckkodMedl,
	pa12.AntKv  AntKv,
	pa122.AccrualAmtEjButik  AccrualAmtEjButik,
	pa19.WJXBFS2  KampForsExBVEjMedl,
	pa13.InkopsBelEx  InkopsBelEx,
	pa112.WJXBFS2  KampInkBelExMedl,
	pa115.AntVarorEjMedl  AntVarorEjMedl,
	pa116.WJXBFS1  KampInkopsBelEx,
	ZEROIFNULL(((pa13.AccrualAmt / NULLIFZERO(pa120.AccrualAmtButik)) * pa121.DowngradedAmt))  NedgraderingBonusButik,
	pa13.PurchaseAmt  PurchaseAmt,
	pa13.AccrualAmt  AccrualAmt,
	pa115.AntVarorMedl  AntVarorMedl,
	pa15.ForsBelExBVMedlem  ForsBelExBVMedlem,
	pa118.WJXBFS1  ForsBelExBVEjMedl,
	pa118.WJXBFS2  ForsBelExEjMedl,
	pa116.WJXBFS2  KampForsBelExBVBer,
	pa118.WJXBFS3  InkBelExEjMedl,
	pa119.BVProcEjMedlKvitto  BVProcEjMedlKvitto
from	ZZMD00	pa11
	full outer join	ZZMD01	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)
	full outer join	ZZMD05	pa13
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa13.Calendar_Month_Id)
	full outer join	ZZMD08	pa15
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id) = pa15.Calendar_Month_Id)
	full outer join	ZZMD0I	pa17
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id) = pa17.Calendar_Month_Id)
	full outer join	ZZMD0K	pa18
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id) = pa18.Calendar_Month_Id)
	full outer join	ZZOP0L	pa19
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id) = pa19.Calendar_Month_Id)
	full outer join	ZZOP0M	pa110
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id) = pa110.Calendar_Month_Id)
	full outer join	ZZOP0N	pa111
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id) = pa111.Calendar_Month_Id)
	full outer join	ZZOP0O	pa112
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id) = pa112.Calendar_Month_Id)
	full outer join	ZZOP0P	pa113
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id) = pa113.Calendar_Month_Id)
	full outer join	ZZOP0Q	pa114
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id, pa113.Calendar_Month_Id) = pa114.Calendar_Month_Id)
	full outer join	ZZMD0U	pa115
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id) = pa115.Calendar_Month_Id)
	full outer join	ZZOP0V	pa116
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id, pa115.Calendar_Month_Id) = pa116.Calendar_Month_Id)
	full outer join	ZZOP0W	pa118
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id, pa115.Calendar_Month_Id, pa116.Calendar_Month_Id) = pa118.Calendar_Month_Id)
	full outer join	ZZMD0X	pa119
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa15.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id, pa115.Calendar_Month_Id, pa116.Calendar_Month_Id, pa118.Calendar_Month_Id) = pa119.Calendar_Month_Id)
	cross join	ZZMD0A	pa120
	cross join	ZZMD0D	pa121
	cross join	ZZMD0S	pa122


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZSP02

drop table ZZSP03

drop table ZZSP04

drop table ZZMD05

drop table ZZOP06

drop table ZZSP07

drop table ZZMD08

drop table ZZMD09

drop table ZZMD0A

drop table ZZSP0B

drop table ZZSP0C

drop table ZZMD0D

drop table ZZMD0E

drop table ZZSP0F

drop table ZZOP0G

drop table ZZMD0H

drop table ZZMD0I

drop table ZZMD0J

drop table ZZMD0K

drop table ZZOP0L

drop table ZZOP0M

drop table ZZOP0N

drop table ZZOP0O

drop table ZZOP0P

drop table ZZOP0Q

drop table ZZMD0R

drop table ZZMD0S

drop table ZZMD0T

drop table ZZMD0U

drop table ZZOP0V

drop table ZZOP0W

drop table ZZMD0X