/* 
SALES_TRANSACTION_WEEK_F
SALES_TRAN_LINE_WEEK_F
SALES_TRAN_TOTALS_WEEK_F
*/

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=MA001b - Lojalitetsrapport vecka 2; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntRekrKontaktAck	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD00 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontaktAck
from	UV2SemCMNVOUT.LOY_CONTACT_FIRST_SALE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	UV2SemCMNVOUT.CALENDAR_WEEK_YTD_D	a13
	  on 	(a12.Calendar_Week_Id = a13.Calendar_Week_YTD_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
where	(a13.Calendar_Week_Id in (201318)
 and a14.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Week_Id

create volatile table ZZMD01, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntKvMedl	FLOAT, 
	AntKv	FLOAT, 
	AntKvEjMedl	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD01 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Receipt_Cnt else NULL end))  AntKvMedl,
	sum(a11.Receipt_Cnt)  AntKv,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Receipt_Cnt else NULL end))  AntKvEjMedl
from	UV2SemCMNVOUT.SALES_TRANSACTION_WEEK_F	a11
	join	UV2SemCMNVOUT.STORE_DEPARTMENT_D	a12
	  on 	(a11.Store_Department_Seq_Num = a12.Store_Department_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201318)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a12.Store_Department_Id not in ('__Prestore__'))
group by	a11.Calendar_Week_Id

create volatile table ZZSP02, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntRekrKontakt	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP02 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontakt
from	UV2SemCMNVOUT.LOY_CONTACT_FIRST_SALE_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201318)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Week_Id

create volatile table ZZSP03, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	ForsBelEx	FLOAT, 
	WJXBFS3	FLOAT, 
	WJXBFS4	FLOAT, 
	ForsBelExMedl	FLOAT, 
	GODWFLAG8_1	INTEGER, 
	KampInkBelExEjMedl	FLOAT, 
	KampForsExBVEjMedl	FLOAT, 
	GODWFLAG11_1	INTEGER, 
	KampForsExBVMedl	FLOAT, 
	KampInkBelExMedl	FLOAT, 
	GODWFLAG14_1	INTEGER, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT, 
	GODWFLAG1b_1	INTEGER, 
	ForsBelExBVEjMedl	FLOAT, 
	ForsBelExEjMedl	FLOAT, 
	InkBelExEjMedl	FLOAT, 
	GODWFLAG1e_1	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP03 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.GP_Unit_Selling_Price_Amt)  WJXBFS1,
	sum(a11.Unit_Cost_Amt)  WJXBFS2,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  WJXBFS3,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Cost_Amt else NULL end))  WJXBFS4,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExMedl,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG8_1,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.Unit_Cost_Amt else NULL end))  KampInkBelExEjMedl,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsExBVEjMedl,
	max((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then 1 else 0 end))  GODWFLAG11_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsExBVMedl,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.Unit_Cost_Amt else NULL end))  KampInkBelExMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then 1 else 0 end))  GODWFLAG14_1,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer,
	max((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then 1 else 0 end))  GODWFLAG1b_1,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVEjMedl,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExEjMedl,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Cost_Amt else NULL end))  InkBelExEjMedl,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAG1e_1
from	UV2SemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201318)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a11.Calendar_Week_Id

create volatile table ZZSP04, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AccrualAmt	FLOAT, 
	PurchaseAmt	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP04 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Accrual_Amt)  AccrualAmt,
	sum(a11.Purchase_Amt)  PurchaseAmt
from	UV2SemCMNVOUT.LOYALTY_TRANSACTION_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Processing_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201318)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Week_Id

create volatile table ZZMD05, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntRekrKontakt	FLOAT, 
	WJXBFS1	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT, 
	AccrualAmt	FLOAT, 
	PurchaseAmt	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD05 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id)  Calendar_Week_Id,
	pa11.AntRekrKontakt  AntRekrKontakt,
	(ZEROIFNULL(pa12.WJXBFS1) - ZEROIFNULL(pa12.WJXBFS2))  WJXBFS1,
	pa12.ForsBelEx  ForsBelEx,
	pa12.WJXBFS1  ForsBelExBVBer,
	pa12.WJXBFS2  InkopsBelEx,
	pa13.AccrualAmt  AccrualAmt,
	pa13.PurchaseAmt  PurchaseAmt
from	ZZSP02	pa11
	full outer join	ZZSP03	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	full outer join	ZZSP04	pa13
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id) = pa13.Calendar_Week_Id)

create volatile table ZZOP06, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP06 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.WJXBFS3  WJXBFS1,
	pa01.WJXBFS4  WJXBFS2,
	pa01.ForsBelExMedl  WJXBFS3
from	ZZSP03	pa01
where	pa01.GODWFLAG8_1 = 1

create volatile table ZZSP07, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	TotMedlRbt	FLOAT, 
	WJXBFS1	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZSP07 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	sum(a11.Tot_Loy_Discount_Amt)  TotMedlRbt,
	sum(a11.Tot_Discount_Amt)  WJXBFS1
from	UV2SemCMNVOUT.SALES_TRAN_TOTALS_WEEK_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
where	(a11.Calendar_Week_Id in (201318)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a11.Contact_Account_Seq_Num <> -1)
group by	a11.Calendar_Week_Id

create volatile table ZZMD08, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	TotMedlRbt	FLOAT, 
	TotRbtExMedlRbt	FLOAT, 
	ForsBelExMedl	FLOAT, 
	InkBelExMedl	FLOAT, 
	ForsBelExBVMedlem	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD08 
select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	(ZEROIFNULL(pa11.WJXBFS1) - ZEROIFNULL(pa11.WJXBFS2))  WJXBFS1,
	pa12.TotMedlRbt  TotMedlRbt,
	(ZEROIFNULL(pa12.WJXBFS1) - ZEROIFNULL(pa12.TotMedlRbt))  TotRbtExMedlRbt,
	pa11.WJXBFS3  ForsBelExMedl,
	pa11.WJXBFS2  InkBelExMedl,
	pa11.WJXBFS1  ForsBelExBVMedlem
from	ZZOP06	pa11
	full outer join	ZZSP07	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)

create volatile table ZZMD09, no fallback, no log(
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Membership_Num) on commit preserve rows

;insert into ZZMD09 
select	a13.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num <> -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	UV2SemCMNVOUT.LOYALTY_TRANSACTION_DAY_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a13
	  on 	(a11.Member_Account_Seq_Num = a13.Member_Account_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a14
	  on 	(a11.Processing_Dt = a14.Calendar_Dt)
where	(a14.Calendar_Week_Id in (201318)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Membership_Num

create volatile table ZZMD0A, no fallback, no log(
	AccrualAmtButik	FLOAT) on commit preserve rows

;insert into ZZMD0A 
select	sum(pa11.WJXBFS1)  AccrualAmtButik
from	ZZMD09	pa11

create volatile table ZZSP0B, no fallback, no log(
	Forfallen_bonus	FLOAT) on commit preserve rows

;insert into ZZSP0B 
select	sum(a11.Expired_Amt)  Forfallen_bonus
from	UV2SemCMNVOUT.LOYALTY_TRANSACTION_F	a11
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Expiration_Dt = a12.Calendar_Dt)
where	a12.Calendar_Week_Id in (201318)

create volatile table ZZSP0C, no fallback, no log(
	DowngradedAmt	FLOAT) on commit preserve rows

;insert into ZZSP0C 
select	sum(a11.Downgraded_Amt)  DowngradedAmt
from	UV2SemCMNVOUT.LOYALTY_TRANSACTION_WEEK_F	a11
where	a11.Calendar_Week_Id in (201318)

create volatile table ZZMD0D, no fallback, no log(
	Forfallen_bonus	FLOAT, 
	DowngradedAmt	FLOAT) on commit preserve rows

;insert into ZZMD0D 
select	pa11.Forfallen_bonus  Forfallen_bonus,
	pa12.DowngradedAmt  DowngradedAmt
from	ZZSP0B	pa11
	cross join	ZZSP0C	pa12

create volatile table ZZOP0E, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0E 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.KampInkBelExEjMedl  WJXBFS1,
	pa01.KampForsExBVEjMedl  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG11_1 = 1

create volatile table ZZMD0F, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntalSaldaStEjMedl	FLOAT, 
	AntSaldaViktartEjMedl	FLOAT, 
	AntalSaldaStMedl	FLOAT, 
	AntSaldaViktartMedl	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD0F 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	sum((Case when (a11.UOM_Category_Cd in ('UNT') and a11.Contact_Account_Seq_Num = -1) then a11.Item_Qty else NULL end))  AntalSaldaStEjMedl,
	sum((Case when (a11.UOM_Category_Cd in ('WGH') and a11.Contact_Account_Seq_Num = -1) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartEjMedl,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('UNT')) then a11.Item_Qty else NULL end))  AntalSaldaStMedl,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('WGH')) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartMedl
from	UV2SemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201318)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and ((a11.UOM_Category_Cd in ('UNT')
 and a11.Contact_Account_Seq_Num = -1)
 or (a11.UOM_Category_Cd in ('WGH')
 and a11.Contact_Account_Seq_Num = -1)
 or (a11.Contact_Account_Seq_Num <> -1
 and a11.UOM_Category_Cd in ('UNT'))
 or (a11.Contact_Account_Seq_Num <> -1
 and a11.UOM_Category_Cd in ('WGH'))))
group by	a13.Calendar_Week_Id

create volatile table ZZOP0G, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0G 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.KampForsExBVMedl  WJXBFS1,
	pa01.KampInkBelExMedl  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG14_1 = 1

create volatile table ZZMD0H, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Membership_Num	VARCHAR(80), 
	WJXBFS1	FLOAT)
primary index (Calendar_Week_Id, Membership_Num) on commit preserve rows

;insert into ZZMD0H 
select	a11.Calendar_Week_Id  Calendar_Week_Id,
	a12.Membership_Num  Membership_Num,
	sum(CASE WHEN a11.Store_Seq_Num = -1 THEN a11.accrual_amt ELSE 0 END)  WJXBFS1
from	UV2SemCMNVOUT.LOYALTY_TRANSACTION_WEEK_F	a11
	join	UV2SemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
where	a11.Calendar_Week_Id in (201318)
group by	a11.Calendar_Week_Id,
	a12.Membership_Num

create volatile table ZZMD0I, no fallback, no log(
	AccrualAmtEjButik	FLOAT) on commit preserve rows

;insert into ZZMD0I 
select	sum(pa11.WJXBFS1)  AccrualAmtEjButik
from	ZZMD0H	pa11
where	pa11.Calendar_Week_Id in (201318)

create volatile table ZZMD0J, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	WJXBFS1	INTEGER, 
	GODWFLAG19_1	INTEGER, 
	WJXBFS2	INTEGER, 
	GODWFLAG1c_1	INTEGER)
primary index (Calendar_Week_Id, Sales_Tran_Seq_Num) on commit preserve rows

;insert into ZZMD0J 
select	a13.Calendar_Week_Id  Calendar_Week_Id,
	a11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	count(distinct (Case when a11.Contact_Account_Seq_Num = -1 then a11.Scan_Code_Seq_Num else NULL end))  WJXBFS1,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAG19_1,
	count(distinct (Case when a11.Contact_Account_Seq_Num <> -1 then a11.Scan_Code_Seq_Num else NULL end))  WJXBFS2,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG1c_1
from	UV2SemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	UV2SemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	UV2SemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Week_Id in (201318)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and (a11.Contact_Account_Seq_Num = -1
 or a11.Contact_Account_Seq_Num <> -1))
group by	a13.Calendar_Week_Id,
	a11.Sales_Tran_Seq_Num

create volatile table ZZMD0K, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	AntVarorEjMedl	FLOAT, 
	AntVarorMedl	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMD0K 
select	pa11.Calendar_Week_Id  Calendar_Week_Id,
	sum((Case when pa11.GODWFLAG19_1 = 1 then pa11.WJXBFS1 else NULL end))  AntVarorEjMedl,
	sum((Case when pa11.GODWFLAG1c_1 = 1 then pa11.WJXBFS2 else NULL end))  AntVarorMedl
from	ZZMD0J	pa11
where	(pa11.Calendar_Week_Id in (201318)
 and (pa11.GODWFLAG19_1 = 1
 or pa11.GODWFLAG1c_1 = 1))
group by	pa11.Calendar_Week_Id

create volatile table ZZOP0L, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0L 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.KampInkopsBelEx  WJXBFS1,
	pa01.KampForsBelExBVBer  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG1b_1 = 1

create volatile table ZZOP0M, no fallback, no log(
	Calendar_Week_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZOP0M 
select	pa01.Calendar_Week_Id  Calendar_Week_Id,
	pa01.ForsBelExBVEjMedl  WJXBFS1,
	pa01.ForsBelExEjMedl  WJXBFS2,
	pa01.InkBelExEjMedl  WJXBFS3
from	ZZSP03	pa01
where	pa01.GODWFLAG1e_1 = 1

select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa15.Calendar_Week_Id, pa17.Calendar_Week_Id, pa18.Calendar_Week_Id, pa110.Calendar_Week_Id, pa113.Calendar_Week_Id, pa114.Calendar_Week_Id, pa116.Calendar_Week_Id)  Calendar_Week_Id,
	pa11.AntRekrKontaktAck  AntRekrKontaktAck,
	pa12.AntKvMedl  AntKvMedl,
	ZEROIFNULL((pa13.AntRekrKontakt / NULLIFZERO((ZEROIFNULL(pa13.AntRekrKontakt) + (ZEROIFNULL(pa12.AntKv) - ZEROIFNULL(pa12.AntKvMedl))))))  RekrKvotMed,
	ZEROIFNULL((pa15.WJXBFS1 / NULLIFZERO(pa13.WJXBFS1)))  AndelBVMedl,
	pa15.TotMedlRbt  TotMedlRbt,
	pa15.TotRbtExMedlRbt  TotRbtExMedlRbt,
	pa13.ForsBelEx  ForsBelEx,
	pa15.ForsBelExMedl  ForsBelExMedl,
	pa12.AntKvEjMedl  AntKvEjMedl,
	pa117.AccrualAmtButik  AccrualAmtButik,
	pa118.Forfallen_bonus  Forfallen_bonus,
	pa17.WJXBFS1  KampInkBelExEjMedl,
	pa13.ForsBelExBVBer  ForsBelExBVBer,
	pa15.InkBelExMedl  InkBelExMedl,
	(ZEROIFNULL(pa18.AntalSaldaStEjMedl) + ZEROIFNULL(pa18.AntSaldaViktartEjMedl))  AntStreckkodEjMedl,
	pa110.WJXBFS1  KampForsExBVMedl,
	(ZEROIFNULL(pa18.AntalSaldaStMedl) + ZEROIFNULL(pa18.AntSaldaViktartMedl))  AntStreckkodMedl,
	pa12.AntKv  AntKv,
	pa119.AccrualAmtEjButik  AccrualAmtEjButik,
	pa17.WJXBFS2  KampForsExBVEjMedl,
	pa13.InkopsBelEx  InkopsBelEx,
	pa110.WJXBFS2  KampInkBelExMedl,
	pa113.AntVarorEjMedl  AntVarorEjMedl,
	pa114.WJXBFS1  KampInkopsBelEx,
	ZEROIFNULL(((pa13.AccrualAmt / NULLIFZERO(pa117.AccrualAmtButik)) * pa118.DowngradedAmt))  NedgraderingBonusButik,
	pa13.PurchaseAmt  PurchaseAmt,
	pa13.AccrualAmt  AccrualAmt,
	pa113.AntVarorMedl  AntVarorMedl,
	pa15.ForsBelExBVMedlem  ForsBelExBVMedlem,
	pa116.WJXBFS1  ForsBelExBVEjMedl,
	pa116.WJXBFS2  ForsBelExEjMedl,
	pa114.WJXBFS2  KampForsBelExBVBer,
	pa116.WJXBFS3  InkBelExEjMedl
from	ZZMD00	pa11
	full outer join	ZZMD01	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	full outer join	ZZMD05	pa13
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id) = pa13.Calendar_Week_Id)
	full outer join	ZZMD08	pa15
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id) = pa15.Calendar_Week_Id)
	full outer join	ZZOP0E	pa17
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa15.Calendar_Week_Id) = pa17.Calendar_Week_Id)
	full outer join	ZZMD0F	pa18
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa15.Calendar_Week_Id, pa17.Calendar_Week_Id) = pa18.Calendar_Week_Id)
	full outer join	ZZOP0G	pa110
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa15.Calendar_Week_Id, pa17.Calendar_Week_Id, pa18.Calendar_Week_Id) = pa110.Calendar_Week_Id)
	full outer join	ZZMD0K	pa113
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa15.Calendar_Week_Id, pa17.Calendar_Week_Id, pa18.Calendar_Week_Id, pa110.Calendar_Week_Id) = pa113.Calendar_Week_Id)
	full outer join	ZZOP0L	pa114
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa15.Calendar_Week_Id, pa17.Calendar_Week_Id, pa18.Calendar_Week_Id, pa110.Calendar_Week_Id, pa113.Calendar_Week_Id) = pa114.Calendar_Week_Id)
	full outer join	ZZOP0M	pa116
	  on 	(coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id, pa13.Calendar_Week_Id, pa15.Calendar_Week_Id, pa17.Calendar_Week_Id, pa18.Calendar_Week_Id, pa110.Calendar_Week_Id, pa113.Calendar_Week_Id, pa114.Calendar_Week_Id) = pa116.Calendar_Week_Id)
	cross join	ZZMD0A	pa117
	cross join	ZZMD0D	pa118
	cross join	ZZMD0I	pa119


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

drop table ZZSP02

drop table ZZSP03

drop table ZZSP04

drop table ZZMD05

drop table ZZOP06

drop table ZZSP07

drop table ZZMD08

drop table ZZMD09

drop table ZZMD0A

drop table ZZSP0B

drop table ZZSP0C

drop table ZZMD0D

drop table ZZOP0E

drop table ZZMD0F

drop table ZZOP0G

drop table ZZMD0H

drop table ZZMD0I

drop table ZZMD0J

drop table ZZMD0K

drop table ZZOP0L

drop table ZZOP0M

