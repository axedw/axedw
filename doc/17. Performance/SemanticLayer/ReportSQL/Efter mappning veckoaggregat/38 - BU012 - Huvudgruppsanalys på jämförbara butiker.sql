SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU012 - Huvudgruppsanalys på jämförbara butiker; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	UV2SemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-08-15'
group by	a11.Calendar_Week_Id

create volatile table ZZMD01, no fallback, no log(
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelEx	FLOAT, 
	AntalSalda	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Art_Hier_Lvl_4_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD01 
select	a19.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a13.Concept_Cd  Concept_Cd,
	a19.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a13.Retail_Region_Cd  Retail_Region_Cd,
	a13.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx
from	UV2SemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a19
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a19.Art_Hier_Lvl_2_Seq_Num and 
	a17.Art_Hier_Lvl_4_Seq_Num = a19.Art_Hier_Lvl_4_Seq_Num)
where	(a13.Store_Id in (4104, 4107, 4110, 4111, 4115, 4116, 4118, 4119, 4121, 4123, 4127, 4129, 4131, 4132, 4135, 4136, 4140, 4142, 4146, 4152, 4156, 4160, 4162, 4168, 4203, 4209, 4211, 4213, 4215, 4217, 4221, 4235, 4239, 4245, 4247, 4251, 4255, 4256, 4263, 4273, 4275, 4277, 4283, 4287, 4293, 4297, 4307, 4313, 4335, 4345, 4347, 4349, 4353, 4355, 4357, 4359, 4363)
 and a19.Art_Hier_Lvl_2_Id in ('03'))
group by	a19.Art_Hier_Lvl_4_Id,
	a13.Concept_Cd,
	a19.Art_Hier_Lvl_2_Id,
	a13.Retail_Region_Cd,
	a13.Store_Id

create volatile table ZZMD02, no fallback, no log(
	Art_Hier_Lvl_4_Id	CHAR(6), 
	Concept_Cd	CHAR(3), 
	Art_Hier_Lvl_2_Id	CHAR(2), 
	Retail_Region_Cd	CHAR(6), 
	Store_Id	INTEGER, 
	ForsBelExFg	FLOAT, 
	AntalSaldaFg	FLOAT, 
	InkopsBelExFg	FLOAT, 
	ForsBelExBVBerFg	FLOAT)
primary index (Art_Hier_Lvl_4_Id, Concept_Cd, Art_Hier_Lvl_2_Id, Retail_Region_Cd, Store_Id) on commit preserve rows

;insert into ZZMD02 
select	a110.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	a14.Concept_Cd  Concept_Cd,
	a110.Art_Hier_Lvl_2_Id  Art_Hier_Lvl_2_Id,
	a14.Retail_Region_Cd  Retail_Region_Cd,
	a14.Store_Id  Store_Id,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExFg,
	sum(a11.Item_Qty)  AntalSaldaFg,
	sum(a11.Unit_Cost_Amt)  InkopsBelExFg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBerFg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11
	join	UV2SemCMNVOUT.CALENDAR_WEEK_D	a12
	  on 	(a11.Calendar_Week_Id = a12.Calendar_PYS_Week_Id)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	UV2SemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a15
	  on 	(a11.Scan_Code_Seq_Num = a15.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a16
	  on 	(a15.Measuring_Unit_Seq_Num = a16.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a17
	  on 	(a16.Article_Seq_Num = a17.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a18
	  on 	(a17.Art_Hier_Lvl_8_Seq_Num = a18.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a110
	  on 	(a18.Art_Hier_Lvl_2_Seq_Num = a110.Art_Hier_Lvl_2_Seq_Num and 
	a18.Art_Hier_Lvl_4_Seq_Num = a110.Art_Hier_Lvl_4_Seq_Num)
where	(a14.Store_Id in (4104, 4107, 4110, 4111, 4115, 4116, 4118, 4119, 4121, 4123, 4127, 4129, 4131, 4132, 4135, 4136, 4140, 4142, 4146, 4152, 4156, 4160, 4162, 4168, 4203, 4209, 4211, 4213, 4215, 4217, 4221, 4235, 4239, 4245, 4247, 4251, 4255, 4256, 4263, 4273, 4275, 4277, 4283, 4287, 4293, 4297, 4307, 4313, 4335, 4345, 4347, 4349, 4353, 4355, 4357, 4359, 4363)
 and a110.Art_Hier_Lvl_2_Id in ('03'))
group by	a110.Art_Hier_Lvl_4_Id,
	a14.Concept_Cd,
	a110.Art_Hier_Lvl_2_Id,
	a14.Retail_Region_Cd,
	a14.Store_Id

select	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id)  Art_Hier_Lvl_2_Id,
	max(a15.Art_Hier_Lvl_2_Desc)  Art_Hier_Lvl_2_Desc,
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id)  Art_Hier_Lvl_4_Id,
	max(a17.Art_Hier_Lvl_4_Desc)  Art_Hier_Lvl_4_Desc,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	max(a13.Store_Name)  Store_Name,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	max(a16.Concept_Name)  Concept_Name,
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)  Retail_Region_Cd,
	max(a14.Retail_Region_Name)  Retail_Region_Name,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa12.ForsBelExFg)  ForsBelExFg,
	max(pa11.AntalSalda)  AntalSalda,
	max(pa12.AntalSaldaFg)  AntalSaldaFg,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa12.InkopsBelExFg)  InkopsBelExFg,
	max(pa12.ForsBelExBVBerFg)  ForsBelExBVBerFg
from	ZZMD01	pa11
	full outer join	ZZMD02	pa12
	  on 	(pa11.Art_Hier_Lvl_2_Id = pa12.Art_Hier_Lvl_2_Id and 
	pa11.Art_Hier_Lvl_4_Id = pa12.Art_Hier_Lvl_4_Id and 
	pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Retail_Region_Cd = pa12.Retail_Region_Cd and 
	pa11.Store_Id = pa12.Store_Id)
	join	UV2SemCMNVOUT.STORE_D	a13
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id) = a13.Store_Id)
	join	UV2SemCMNVOUT.RETAIL_REGION_D	a14
	  on 	(coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd) = a14.Retail_Region_Cd)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a15
	  on 	(coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id) = a15.Art_Hier_Lvl_2_Id)
	join	UV2SemCMNVOUT.CONCEPT_D	a16
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = a16.Concept_Cd)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a17
	  on 	(coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id) = a17.Art_Hier_Lvl_4_Id)
group by	coalesce(pa11.Art_Hier_Lvl_2_Id, pa12.Art_Hier_Lvl_2_Id),
	coalesce(pa11.Art_Hier_Lvl_4_Id, pa12.Art_Hier_Lvl_4_Id),
	coalesce(pa11.Store_Id, pa12.Store_Id),
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd),
	coalesce(pa11.Retail_Region_Cd, pa12.Retail_Region_Cd)


SET QUERY_BAND = NONE For Session;


drop table ZZMQ00

drop table ZZMD01

drop table ZZMD02

