/* Lojalitetsrapport 2 m�nad, 201212 alla Willys koncept */

SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW); Action=MA002b - Lojalitetsrapport m�nad 2; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntRekrKontaktAck	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD00 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontaktAck
from	PRSemCMNVOUT.LOY_CONTACT_FIRST_SALE_F	a11
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	PRSemCMNVOUT.CALENDAR_MONTH_YTD_D	a13
	  on 	(a12.Calendar_Month_Id = a13.Calendar_Month_YTD_Id)
	join	PRSemCMNVOUT.STORE_D	a14
	  on 	(a11.Store_Seq_Num = a14.Store_Seq_Num)
where	(a13.Calendar_Month_Id in (201212)
 and a14.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Month_Id

create volatile table ZZSP01, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntKvMedl	FLOAT, 
	GODWFLAG2_1	INTEGER, 
	AntKv	FLOAT, 
	AntKvEjMedl	FLOAT, 
	GODWFLAGa_1	INTEGER)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZSP01 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Receipt_Cnt else NULL end))  AntKvMedl,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG2_1,
	sum(a11.Receipt_Cnt)  AntKv,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Receipt_Cnt else NULL end))  AntKvEjMedl,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAGa_1
from	PRSemCMNVOUT.SALES_TRANSACTION_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Month_Id in (201212)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Month_Id

create volatile table ZZOP02, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP02 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntKvMedl  WJXBFS1
from	ZZSP01	pa01
where	pa01.GODWFLAG2_1 = 1

create volatile table ZZSP03, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	ForsBelExMedl	FLOAT, 
	AntVarorMedl	INTEGER, 
	GODWFLAG3_1	INTEGER, 
	WJXBFS3	FLOAT, 
	WJXBFS4	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVEjMedl	FLOAT, 
	ForsBelExEjMedl	FLOAT, 
	InkBelExEjMedl	FLOAT, 
	AntVarorEjMedl	INTEGER, 
	GODWFLAGb_1	INTEGER, 
	KampInkBelExEjMedl	FLOAT, 
	KampForsExBVEjMedl	FLOAT, 
	GODWFLAG14_1	INTEGER, 
	AntalSaldaStEjMedl	FLOAT, 
	GODWFLAG15_1	INTEGER, 
	AntSaldaViktartEjMedl	FLOAT, 
	GODWFLAG16_1	INTEGER, 
	KampForsExBVMedl	FLOAT, 
	KampInkBelExMedl	FLOAT, 
	GODWFLAG17_1	INTEGER, 
	AntalSaldaStMedl	FLOAT, 
	GODWFLAG18_1	INTEGER, 
	AntSaldaViktartMedl	FLOAT, 
	GODWFLAG19_1	INTEGER, 
	KampInkopsBelEx	FLOAT, 
	KampForsBelExBVBer	FLOAT, 
	GODWFLAG1b_1	INTEGER)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZSP03 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  WJXBFS1,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Cost_Amt else NULL end))  WJXBFS2,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExMedl,
	count(distinct (Case when a11.Contact_Account_Seq_Num <> -1 then a11.Scan_Code_Seq_Num else NULL end))  AntVarorMedl,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAG3_1,
	sum(a11.GP_Unit_Selling_Price_Amt)  WJXBFS3,
	sum(a11.Unit_Cost_Amt)  WJXBFS4,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVEjMedl,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Selling_Price_Amt else NULL end))  ForsBelExEjMedl,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Cost_Amt else NULL end))  InkBelExEjMedl,
	count(distinct (Case when a11.Contact_Account_Seq_Num = -1 then a11.Scan_Code_Seq_Num else NULL end))  AntVarorEjMedl,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAGb_1,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.Unit_Cost_Amt else NULL end))  KampInkBelExEjMedl,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsExBVEjMedl,
	max((Case when (a11.Contact_Account_Seq_Num = -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then 1 else 0 end))  GODWFLAG14_1,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('UNT')) then a11.Item_Qty else NULL end))  AntalSaldaStEjMedl,
	max((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('UNT')) then 1 else 0 end))  GODWFLAG15_1,
	sum((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('WGH')) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartEjMedl,
	max((Case when (a11.Contact_Account_Seq_Num = -1 and a11.UOM_Category_Cd in ('WGH')) then 1 else 0 end))  GODWFLAG16_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsExBVMedl,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then a11.Unit_Cost_Amt else NULL end))  KampInkBelExMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ')) then 1 else 0 end))  GODWFLAG17_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('UNT')) then a11.Item_Qty else NULL end))  AntalSaldaStMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('UNT')) then 1 else 0 end))  GODWFLAG18_1,
	sum((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('WGH')) then a11.Receipt_Line_Cnt else NULL end))  AntSaldaViktartMedl,
	max((Case when (a11.Contact_Account_Seq_Num <> -1 and a11.UOM_Category_Cd in ('WGH')) then 1 else 0 end))  GODWFLAG19_1,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.Unit_Cost_Amt else NULL end))  KampInkopsBelEx,
	sum((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then a11.GP_Unit_Selling_Price_Amt else NULL end))  KampForsBelExBVBer,
	max((Case when a11.Campaign_Sales_Type_Cd in ('C  ', 'L  ') then 1 else 0 end))  GODWFLAG1b_1
from	PRSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Month_Id in (201212)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Month_Id

create volatile table ZZOP04, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT, 
	WJXBFS4	INTEGER)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP04 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.WJXBFS1  WJXBFS1,
	pa01.WJXBFS2  WJXBFS2,
	pa01.ForsBelExMedl  WJXBFS3,
	pa01.AntVarorMedl  WJXBFS4
from	ZZSP03	pa01
where	(pa01.GODWFLAG3_1 = 1
 and pa01.GODWFLAG3_1 = 1)

create volatile table ZZSP05, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	TotMedlRbt	FLOAT, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZSP05 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Tot_Loy_Discount_Amt)  TotMedlRbt,
	sum(a11.Tot_Discount_Amt)  WJXBFS1
from	PRSemCMNVOUT.SALES_TRANSACTION_TOTALS_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Month_Id in (201212)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and a11.Contact_Account_Seq_Num <> -1)
group by	a13.Calendar_Month_Id

create volatile table ZZMD06, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntKvMedl	FLOAT, 
	WJXBFS1	FLOAT, 
	TotMedlRbt	FLOAT, 
	TotRbtExMedlRbt	FLOAT, 
	ForsBelExMedl	FLOAT, 
	InkBelExMedl	FLOAT, 
	ForsBelExBVMedlem	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD06 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id)  Calendar_Month_Id,
	pa11.WJXBFS1  AntKvMedl,
	(ZEROIFNULL(pa12.WJXBFS1) - ZEROIFNULL(pa12.WJXBFS2))  WJXBFS1,
	pa13.TotMedlRbt  TotMedlRbt,
	(ZEROIFNULL(pa13.WJXBFS1) - ZEROIFNULL(pa13.TotMedlRbt))  TotRbtExMedlRbt,
	pa12.WJXBFS3  ForsBelExMedl,
	pa12.WJXBFS2  InkBelExMedl,
	pa12.WJXBFS1  ForsBelExBVMedlem
from	ZZOP02	pa11
	full outer join	ZZOP04	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)
	full outer join	ZZSP05	pa13
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa13.Calendar_Month_Id)

create volatile table ZZSP07, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntRekrKontakt	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZSP07 
select	a13.Calendar_Month_Id  Calendar_Month_Id,
	sum(a11.Recruited_Contact_Cnt)  AntRekrKontakt
from	PRSemCMNVOUT.LOY_CONTACT_FIRST_SALE_F	a11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Month_Id in (201212)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a13.Calendar_Month_Id

create volatile table ZZMD08, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntRekrKontakt	FLOAT, 
	AntKv	FLOAT, 
	WJXBFS1	FLOAT, 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD08 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id)  Calendar_Month_Id,
	pa11.AntRekrKontakt  AntRekrKontakt,
	pa12.AntKv  AntKv,
	(ZEROIFNULL(pa13.WJXBFS3) - ZEROIFNULL(pa13.WJXBFS4))  WJXBFS1,
	pa13.ForsBelEx  ForsBelEx,
	pa13.WJXBFS3  ForsBelExBVBer,
	pa13.WJXBFS4  InkopsBelEx
from	ZZSP07	pa11
	full outer join	ZZSP01	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)
	full outer join	ZZSP03	pa13
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa13.Calendar_Month_Id)

create volatile table ZZOP09, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP09 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntKvEjMedl  WJXBFS1
from	ZZSP01	pa01
where	pa01.GODWFLAGa_1 = 1

create volatile table ZZOP0A, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT, 
	WJXBFS3	FLOAT, 
	WJXBFS4	INTEGER)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0A 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.ForsBelExBVEjMedl  WJXBFS1,
	pa01.ForsBelExEjMedl  WJXBFS2,
	pa01.InkBelExEjMedl  WJXBFS3,
	pa01.AntVarorEjMedl  WJXBFS4
from	ZZSP03	pa01
where	(pa01.GODWFLAGb_1 = 1
 and pa01.GODWFLAGb_1 = 1)

create volatile table ZZMD0B, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	AntKvEjMedl	FLOAT, 
	ForsBelExBVEjMedl	FLOAT, 
	ForsBelExEjMedl	FLOAT, 
	InkBelExEjMedl	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD0B 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	pa11.WJXBFS1  AntKvEjMedl,
	pa12.WJXBFS1  ForsBelExBVEjMedl,
	pa12.WJXBFS2  ForsBelExEjMedl,
	pa12.WJXBFS3  InkBelExEjMedl
from	ZZOP09	pa11
	full outer join	ZZOP0A	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)

create volatile table ZZMD0C, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	ForsBelExBVMedlKvitto	FLOAT, 
	InkBelExMedlKvitto	FLOAT, 
	GODWFLAGd_1	INTEGER, 
	ForsBelExBVKvitto	FLOAT, 
	InkBelExKvitto	FLOAT, 
	ForsBelExBVEjMedlKvitto	FLOAT, 
	InkBelExEjMedlKvitto	FLOAT, 
	GODWFLAG1d_1	INTEGER)
primary index (Calendar_Month_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZMD0C 
select	a12.Calendar_Month_Id  Calendar_Month_Id,
	a11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	a11.Store_Seq_Num  Store_Seq_Num,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVMedlKvitto,
	sum((Case when a11.Contact_Account_Seq_Num <> -1 then a11.Unit_Cost_Amt else NULL end))  InkBelExMedlKvitto,
	max((Case when a11.Contact_Account_Seq_Num <> -1 then 1 else 0 end))  GODWFLAGd_1,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVKvitto,
	sum(a11.Unit_Cost_Amt)  InkBelExKvitto,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.GP_Unit_Selling_Price_Amt else NULL end))  ForsBelExBVEjMedlKvitto,
	sum((Case when a11.Contact_Account_Seq_Num = -1 then a11.Unit_Cost_Amt else NULL end))  InkBelExEjMedlKvitto,
	max((Case when a11.Contact_Account_Seq_Num = -1 then 1 else 0 end))  GODWFLAG1d_1
from	PRSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	PRSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Tran_Dt = a12.Calendar_Dt)
	join	PRSemCMNVOUT.STORE_D	a13
	  on 	(a11.Store_Seq_Num = a13.Store_Seq_Num)
where	(a12.Calendar_Month_Id in (201212)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	a12.Calendar_Month_Id,
	a11.Sales_Tran_Seq_Num,
	a11.Store_Seq_Num

create volatile table ZZSP0D, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	BVKrMedlKvitto	FLOAT, 
	GODWFLAGe_1	INTEGER, 
	BVKrEjMedlKvitto	FLOAT, 
	GODWFLAG1e_1	INTEGER)
primary index (Calendar_Month_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZSP0D 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	pa11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num  Store_Seq_Num,
	sum((Case when pa11.GODWFLAGd_1 = 1 then (ZEROIFNULL(pa11.ForsBelExBVMedlKvitto) - ZEROIFNULL(pa11.InkBelExMedlKvitto)) else NULL end))  BVKrMedlKvitto,
	max((Case when pa11.GODWFLAGd_1 = 1 then 1 else 0 end))  GODWFLAGe_1,
	sum((Case when pa11.GODWFLAG1d_1 = 1 then (ZEROIFNULL(pa11.ForsBelExBVEjMedlKvitto) - ZEROIFNULL(pa11.InkBelExEjMedlKvitto)) else NULL end))  BVKrEjMedlKvitto,
	max((Case when pa11.GODWFLAG1d_1 = 1 then 1 else 0 end))  GODWFLAG1e_1
from	ZZMD0C	pa11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(pa11.Store_Seq_Num = a12.Store_Seq_Num)
where	(pa11.Calendar_Month_Id in (201212)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and (pa11.GODWFLAGd_1 = 1
 or pa11.GODWFLAG1d_1 = 1))
group by	pa11.Calendar_Month_Id,
	pa11.Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num

create volatile table ZZOP0E, no fallback, no log(
	Store_Seq_Num	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Store_Seq_Num, Sales_Tran_Seq_Num, Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0E 
select	pa01.Store_Seq_Num  Store_Seq_Num,
	pa01.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.BVKrMedlKvitto  WJXBFS1
from	ZZSP0D	pa01
where	pa01.GODWFLAGe_1 = 1

create volatile table ZZMD0F, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	BVKrMedlKvitto	FLOAT, 
	ForsBelExBVKvitto	FLOAT, 
	InkBelExKvitto	FLOAT)
primary index (Calendar_Month_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZMD0F 
select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Sales_Tran_Seq_Num, pa12.Sales_Tran_Seq_Num)  Sales_Tran_Seq_Num,
	coalesce(pa11.Store_Seq_Num, pa12.Store_Seq_Num)  Store_Seq_Num,
	pa11.WJXBFS1  BVKrMedlKvitto,
	pa12.ForsBelExBVKvitto  ForsBelExBVKvitto,
	pa12.InkBelExKvitto  InkBelExKvitto
from	ZZOP0E	pa11
	full outer join	ZZMD0C	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)

create volatile table ZZMD0G, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	BVProcMedlKvitto	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD0G 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	sum(ZEROIFNULL((pa11.BVKrMedlKvitto / NULLIFZERO(pa12.ForsBelExBVMedlKvitto))))  BVProcMedlKvitto
from	ZZMD0F	pa11
	join	ZZMD0C	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)
	join	PRSemCMNVOUT.STORE_D	a13
	  on 	(pa11.Store_Seq_Num = a13.Store_Seq_Num)
where	(pa11.Calendar_Month_Id in (201212)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and pa12.GODWFLAGd_1 = 1)
group by	pa11.Calendar_Month_Id

create volatile table ZZMD0H, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	Sales_Tran_Seq_Num	DECIMAL(19, 0), 
	Store_Seq_Num	INTEGER, 
	BVKrKvitto	FLOAT)
primary index (Calendar_Month_Id, Sales_Tran_Seq_Num, Store_Seq_Num) on commit preserve rows

;insert into ZZMD0H 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	pa11.Sales_Tran_Seq_Num  Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num  Store_Seq_Num,
	sum((ZEROIFNULL(pa11.ForsBelExBVKvitto) - ZEROIFNULL(pa11.InkBelExKvitto)))  BVKrKvitto
from	ZZMD0F	pa11
	join	PRSemCMNVOUT.STORE_D	a12
	  on 	(pa11.Store_Seq_Num = a12.Store_Seq_Num)
where	(pa11.Calendar_Month_Id in (201212)
 and a12.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	pa11.Calendar_Month_Id,
	pa11.Sales_Tran_Seq_Num,
	pa11.Store_Seq_Num

create volatile table ZZMD0I, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	BVProcKvitto	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD0I 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	sum(ZEROIFNULL((pa11.BVKrKvitto / NULLIFZERO(pa12.ForsBelExBVKvitto))))  BVProcKvitto
from	ZZMD0H	pa11
	join	ZZMD0F	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)
	join	PRSemCMNVOUT.STORE_D	a13
	  on 	(pa11.Store_Seq_Num = a13.Store_Seq_Num)
where	(pa11.Calendar_Month_Id in (201212)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2'))
group by	pa11.Calendar_Month_Id

create volatile table ZZOP0J, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0J 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.KampInkBelExEjMedl  WJXBFS1,
	pa01.KampForsExBVEjMedl  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG14_1 = 1

create volatile table ZZOP0K, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0K 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntalSaldaStEjMedl  WJXBFS1
from	ZZSP03	pa01
where	pa01.GODWFLAG15_1 = 1

create volatile table ZZOP0L, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0L 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntSaldaViktartEjMedl  WJXBFS1
from	ZZSP03	pa01
where	pa01.GODWFLAG16_1 = 1

create volatile table ZZOP0M, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0M 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.KampForsExBVMedl  WJXBFS1,
	pa01.KampInkBelExMedl  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG17_1 = 1

create volatile table ZZOP0N, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0N 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntalSaldaStMedl  WJXBFS1
from	ZZSP03	pa01
where	pa01.GODWFLAG18_1 = 1

create volatile table ZZOP0O, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0O 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.AntSaldaViktartMedl  WJXBFS1
from	ZZSP03	pa01
where	pa01.GODWFLAG19_1 = 1

create volatile table ZZOP0P, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZOP0P 
select	pa01.Calendar_Month_Id  Calendar_Month_Id,
	pa01.KampInkopsBelEx  WJXBFS1,
	pa01.KampForsBelExBVBer  WJXBFS2
from	ZZSP03	pa01
where	pa01.GODWFLAG1b_1 = 1

create volatile table ZZMD0Q, no fallback, no log(
	Calendar_Month_Id	INTEGER, 
	BVProcEjMedlKvitto	FLOAT)
primary index (Calendar_Month_Id) on commit preserve rows

;insert into ZZMD0Q 
select	pa11.Calendar_Month_Id  Calendar_Month_Id,
	sum(ZEROIFNULL((pa11.BVKrEjMedlKvitto / NULLIFZERO(pa12.ForsBelExBVEjMedlKvitto))))  BVProcEjMedlKvitto
from	ZZSP0D	pa11
	join	ZZMD0C	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Sales_Tran_Seq_Num = pa12.Sales_Tran_Seq_Num and 
	pa11.Store_Seq_Num = pa12.Store_Seq_Num)
	join	PRSemCMNVOUT.STORE_D	a13
	  on 	(pa11.Store_Seq_Num = a13.Store_Seq_Num)
where	(pa11.Calendar_Month_Id in (201212)
 and a13.Concept_Cd in ('WIL', 'WHE', 'WH2')
 and pa12.GODWFLAG1d_1 = 1
 and pa11.GODWFLAG1e_1 = 1)
group by	pa11.Calendar_Month_Id

select	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id, pa115.Calendar_Month_Id, pa116.Calendar_Month_Id)  Calendar_Month_Id,
	pa11.AntRekrKontaktAck  AntRekrKontaktAck,
	pa12.AntKvMedl  AntKvMedl,
	ZEROIFNULL((pa13.AntRekrKontakt / NULLIFZERO((ZEROIFNULL(pa13.AntRekrKontakt) + (ZEROIFNULL(pa13.AntKv) - ZEROIFNULL(pa12.AntKvMedl))))))  RekrKvotMed,
	ZEROIFNULL((pa12.WJXBFS1 / NULLIFZERO(pa13.WJXBFS1)))  AndelBVMedl,
	pa12.TotMedlRbt  TotMedlRbt,
	pa12.TotRbtExMedlRbt  TotRbtExMedlRbt,
	pa13.ForsBelEx  ForsBelEx,
	pa12.ForsBelExMedl  ForsBelExMedl,
	pa14.AntKvEjMedl  AntKvEjMedl,
	pa15.BVProcMedlKvitto  BVProcMedlKvitto,
	pa16.BVProcKvitto  BVProcKvitto,
	pa17.WJXBFS1  KampInkBelExEjMedl,
	pa13.ForsBelExBVBer  ForsBelExBVBer,
	pa12.InkBelExMedl  InkBelExMedl,
	(ZEROIFNULL(pa18.WJXBFS1) + ZEROIFNULL(pa19.WJXBFS1))  AntStreckkodEjMedl,
	pa110.WJXBFS1  KampForsExBVMedl,
	(ZEROIFNULL(pa111.WJXBFS1) + ZEROIFNULL(pa112.WJXBFS1))  AntStreckkodMedl,
	pa13.AntKv  AntKv,
	pa17.WJXBFS2  KampForsExBVEjMedl,
	pa13.InkopsBelEx  InkopsBelEx,
	pa110.WJXBFS2  KampInkBelExMedl,
	pa113.WJXBFS4  AntVarorEjMedl,
	pa114.WJXBFS1  KampInkopsBelEx,
	pa115.WJXBFS4  AntVarorMedl,
	pa12.ForsBelExBVMedlem  ForsBelExBVMedlem,
	pa14.ForsBelExBVEjMedl  ForsBelExBVEjMedl,
	pa14.ForsBelExEjMedl  ForsBelExEjMedl,
	pa114.WJXBFS2  KampForsBelExBVBer,
	pa14.InkBelExEjMedl  InkBelExEjMedl,
	pa116.BVProcEjMedlKvitto  BVProcEjMedlKvitto
from	ZZMD00	pa11
	full outer join	ZZMD06	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id)
	full outer join	ZZMD08	pa13
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id) = pa13.Calendar_Month_Id)
	full outer join	ZZMD0B	pa14
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id) = pa14.Calendar_Month_Id)
	full outer join	ZZMD0G	pa15
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id) = pa15.Calendar_Month_Id)
	full outer join	ZZMD0I	pa16
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id) = pa16.Calendar_Month_Id)
	full outer join	ZZOP0J	pa17
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id) = pa17.Calendar_Month_Id)
	full outer join	ZZOP0K	pa18
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id, pa17.Calendar_Month_Id) = pa18.Calendar_Month_Id)
	full outer join	ZZOP0L	pa19
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id) = pa19.Calendar_Month_Id)
	full outer join	ZZOP0M	pa110
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id) = pa110.Calendar_Month_Id)
	full outer join	ZZOP0N	pa111
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id) = pa111.Calendar_Month_Id)
	full outer join	ZZOP0O	pa112
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id) = pa112.Calendar_Month_Id)
	full outer join	ZZOP0A	pa113
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id) = pa113.Calendar_Month_Id)
	full outer join	ZZOP0P	pa114
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id, pa113.Calendar_Month_Id) = pa114.Calendar_Month_Id)
	full outer join	ZZOP04	pa115
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id) = pa115.Calendar_Month_Id)
	full outer join	ZZMD0Q	pa116
	  on 	(coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id, pa13.Calendar_Month_Id, pa14.Calendar_Month_Id, pa15.Calendar_Month_Id, pa16.Calendar_Month_Id, pa17.Calendar_Month_Id, pa18.Calendar_Month_Id, pa19.Calendar_Month_Id, pa110.Calendar_Month_Id, pa111.Calendar_Month_Id, pa112.Calendar_Month_Id, pa113.Calendar_Month_Id, pa114.Calendar_Month_Id, pa115.Calendar_Month_Id) = pa116.Calendar_Month_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZSP01

drop table ZZOP02

drop table ZZSP03

drop table ZZOP04

drop table ZZSP05

drop table ZZMD06

drop table ZZSP07

drop table ZZMD08

drop table ZZOP09

drop table ZZOP0A

drop table ZZMD0B

drop table ZZMD0C

drop table ZZSP0D

drop table ZZOP0E

drop table ZZMD0F

drop table ZZMD0G

drop table ZZMD0H

drop table ZZMD0I

drop table ZZOP0J

drop table ZZOP0K

drop table ZZOP0L

drop table ZZOP0M

drop table ZZOP0N

drop table ZZOP0O

drop table ZZOP0P

drop table ZZMD0Q

