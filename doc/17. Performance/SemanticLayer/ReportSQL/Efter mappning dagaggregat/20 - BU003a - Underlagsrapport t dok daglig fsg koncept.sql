SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-UV2); Action=BU003a - Underlagsrapport t dok daglig fsg/koncept; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	UV2SemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-08-14'
group by	a11.Calendar_Week_Id

create volatile table ZZMD01, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Day_Of_Week_Num	BYTEINT, 
	Calendar_Week_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	ForsBelEx	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Store_Type_Cd, Day_Of_Week_Num, Calendar_Week_Id, Concept_Cd) on commit preserve rows

;insert into ZZMD01 
select	a18.Store_Type_Cd  Store_Type_Cd,
	a11.Day_Of_Week_Num  Day_Of_Week_Num,
	a11.Calendar_Week_Id  Calendar_Week_Id,
	a18.Concept_Cd  Concept_Cd,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelEx,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	ZZMQ00	pa12
	  on 	(a11.Calendar_Week_Id = pa12.Calendar_Week_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a14
	  on 	(a13.Measuring_Unit_Seq_Num = a14.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a15
	  on 	(a14.Article_Seq_Num = a15.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a16
	  on 	(a15.Art_Hier_Lvl_8_Seq_Num = a16.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a17
	  on 	(a16.Art_Hier_Lvl_2_Seq_Num = a17.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a18
	  on 	(a11.Store_Seq_Num = a18.Store_Seq_Num)
where	(a17.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a18.Concept_Cd in ('HEM', 'PRX', 'SNG', 'WIL', 'WHE', 'WH2'))
group by	a18.Store_Type_Cd,
	a11.Day_Of_Week_Num,
	a11.Calendar_Week_Id,
	a18.Concept_Cd

create volatile table ZZMD02, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Day_Of_Week_Num	BYTEINT, 
	Calendar_Week_Id	INTEGER, 
	Concept_Cd	CHAR(3), 
	ForsBelExFg	FLOAT, 
	InkopsBelExFg	FLOAT, 
	ForsBelExBVBerFg	FLOAT)
primary index (Store_Type_Cd, Day_Of_Week_Num, Calendar_Week_Id, Concept_Cd) on commit preserve rows

;insert into ZZMD02 
select	a19.Store_Type_Cd  Store_Type_Cd,
	a11.Day_Of_Week_Num  Day_Of_Week_Num,
	a12.Calendar_Week_Id  Calendar_Week_Id,
	a19.Concept_Cd  Concept_Cd,
	sum(a11.Unit_Selling_Price_Amt)  ForsBelExFg,
	sum(a11.Unit_Cost_Amt)  InkopsBelExFg,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBerFg
from	UV2SemCMNVOUT.SALES_TRAN_LINE_DAY_F	a11
	join	UV2SemCMNVOUT.CALENDAR_WEEK_D	a12
	  on 	(a11.Calendar_Week_Id = a12.Calendar_PYS_Week_Id)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	UV2SemCMNVOUT.SCAN_CODE_D	a14
	  on 	(a11.Scan_Code_Seq_Num = a14.Scan_Code_Seq_Num)
	join	UV2SemCMNVOUT.MEASURING_UNIT_D	a15
	  on 	(a14.Measuring_Unit_Seq_Num = a15.Measuring_Unit_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_D	a16
	  on 	(a15.Article_Seq_Num = a16.Article_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a17
	  on 	(a16.Art_Hier_Lvl_8_Seq_Num = a17.Art_Hier_Lvl_8_Seq_Num)
	join	UV2SemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a18
	  on 	(a17.Art_Hier_Lvl_2_Seq_Num = a18.Art_Hier_Lvl_2_Seq_Num)
	join	UV2SemCMNVOUT.STORE_D	a19
	  on 	(a11.Store_Seq_Num = a19.Store_Seq_Num)
where	(a18.Art_Hier_Lvl_2_Id not in ('24', '28')
 and a19.Concept_Cd in ('HEM', 'PRX', 'SNG', 'WIL', 'WHE', 'WH2'))
group by	a19.Store_Type_Cd,
	a11.Day_Of_Week_Num,
	a12.Calendar_Week_Id,
	a19.Concept_Cd

select	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id)  Calendar_Week_Id,
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd)  Concept_Cd,
	max(a13.Concept_Name)  Concept_Name,
	coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd)  Store_Type_Cd,
	max(a15.Store_Type_Desc)  Store_Type_Desc,
	coalesce(pa11.Day_Of_Week_Num, pa12.Day_Of_Week_Num)  Day_Of_Week_Num,
	max(a14.Day_Of_Week_Name)  Day_Of_Week_Name,
	max(pa11.ForsBelEx)  ForsBelEx,
	max(pa12.ForsBelExFg)  ForsBelExFg,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa12.InkopsBelExFg)  InkopsBelExFg,
	max(pa12.ForsBelExBVBerFg)  ForsBelExBVBerFg
from	ZZMD01	pa11
	full outer join	ZZMD02	pa12
	  on 	(pa11.Calendar_Week_Id = pa12.Calendar_Week_Id and 
	pa11.Concept_Cd = pa12.Concept_Cd and 
	pa11.Day_Of_Week_Num = pa12.Day_Of_Week_Num and 
	pa11.Store_Type_Cd = pa12.Store_Type_Cd)
	join	UV2SemCMNVOUT.CONCEPT_D	a13
	  on 	(coalesce(pa11.Concept_Cd, pa12.Concept_Cd) = a13.Concept_Cd)
	join	UV2SemCMNVOUT.DAY_OF_WEEK_D	a14
	  on 	(coalesce(pa11.Day_Of_Week_Num, pa12.Day_Of_Week_Num) = a14.Day_Of_Week_Num)
	join	UV2SemCMNVOUT.STORE_TYPE_D	a15
	  on 	(coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd) = a15.Store_Type_Cd)
group by	coalesce(pa11.Calendar_Week_Id, pa12.Calendar_Week_Id),
	coalesce(pa11.Concept_Cd, pa12.Concept_Cd),
	coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd),
	coalesce(pa11.Day_Of_Week_Num, pa12.Day_Of_Week_Num)


SET QUERY_BAND = NONE For Session;
