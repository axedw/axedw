SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=KR06 - Konsumentpriser per Artikel; ClientUser=Administrator;' For Session;


create volatile table ZZMQ00, no fallback, no log(
	Calendar_Week_Id	INTEGER)
primary index (Calendar_Week_Id) on commit preserve rows

;insert into ZZMQ00 
select	a11.Calendar_Week_Id  Calendar_Week_Id
from	ITSemCMNVOUT.CALENDAR_DAY_D	a11
where	a11.Calendar_Dt = DATE '2013-10-29'
group by	a11.Calendar_Week_Id

create volatile table ZZNB01, no fallback, no log(
	Retail_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	KonsPrisExkl	FLOAT, 
	KonsPrisInkl	FLOAT)
primary index (Retail_Price_List_Cd, Article_Seq_Num, Calendar_Dt) on commit preserve rows

;insert into ZZNB01 
select	a11.Retail_Price_List_Cd  Retail_Price_List_Cd,
	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg(a11.Retail_Price_Amt_Excl_VAT)  KonsPrisExkl,
	avg(a11.Retail_Price_Amt_Incl_VAT)  KonsPrisInkl
from	ITSemCMNVOUT.ARTICLE_RETAIL_PRICE_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Calendar_Dt = a12.Calendar_Dt)
	join	ZZMQ00	pa13
	  on 	(a12.Calendar_Week_Id = pa13.Calendar_Week_Id)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a11.Article_Seq_Num = a14.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a15
	  on 	(a14.Art_Hier_Lvl_8_Seq_Num = a15.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_2_D	a16
	  on 	(a14.Art_Hier_Lvl_2_Seq_Num = a16.Art_Hier_Lvl_2_Seq_Num)
where	a16.Art_Hier_Lvl_2_Id in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '21', '22', '23', '27', '28')
group by	a11.Retail_Price_List_Cd,
	a11.Article_Seq_Num,
	a11.Calendar_Dt

create volatile table ZZMB02, no fallback, no log(
	Retail_Price_List_Cd	CHAR(2), 
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Retail_Price_List_Cd, Article_Seq_Num) on commit preserve rows

;insert into ZZMB02 
select	pc11.Retail_Price_List_Cd  Retail_Price_List_Cd,
	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB01	pc11
group by	pc11.Retail_Price_List_Cd,
	pc11.Article_Seq_Num

select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(a13.Article_Id)  Article_Id,
	max(a13.Article_Desc)  Article_Desc,
	pa11.Retail_Price_List_Cd  Retail_Price_List_Cd,
	max(a14.Price_List_Name)  Price_List_Name,
	max(pa11.KonsPrisExkl)  WJXBFS1,
	max(pa11.KonsPrisInkl)  WJXBFS2
from	ZZNB01	pa11
	join	ZZMB02	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1 and 
	pa11.Retail_Price_List_Cd = pa12.Retail_Price_List_Cd)
	join	ITSemCMNVOUT.ARTICLE_D	a13
	  on 	(pa11.Article_Seq_Num = a13.Article_Seq_Num)
	join	ITSemCMNVOUT.PRICE_LIST_D	a14
	  on 	(pa11.Retail_Price_List_Cd = a14.Price_List_Cd)
group by	pa11.Article_Seq_Num,
	pa11.Retail_Price_List_Cd


SET QUERY_BAND = NONE For Session;


drop table ZZMQ00

drop table ZZNB01

drop table ZZMB02
