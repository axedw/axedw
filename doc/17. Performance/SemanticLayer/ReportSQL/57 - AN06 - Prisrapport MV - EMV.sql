SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=AN06 - Prisrapport MV - EMV; ClientUser=Administrator;' For Session;


create volatile table ZZSP00, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	AntalSalda	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZSP00 
select	a13.Article_Seq_Num  Article_Seq_Num,
	sum(a11.Item_Qty)  AntalSalda,
	sum(a11.GP_Unit_Selling_Price_Amt)  ForsBelExBVBer,
	sum(a11.Unit_Cost_Amt)  InkopsBelEx
from	ITSemCMNVOUT.SALES_TRAN_LINE_WEEK_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.SCAN_CODE_D	a13
	  on 	(a11.Scan_Code_Seq_Num = a13.Scan_Code_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a13.Article_Seq_Num = a14.Article_Seq_Num)
where	(a11.Calendar_Week_Id in (201342)
 and a12.Cost_Price_List_Cd in ('AA')
 and a14.Article_Id in ('100365418', '100002676', '101093718', '101188044', '100001150', '100810687', '100027338', '100748997', '100679393', '100736367', '100600609', '101098848', '100225560', '101209552', '101210289', '100520697', '101169980')
 and a12.Concept_Cd in ('HEM'))
group by	a13.Article_Seq_Num

create volatile table ZZNB01, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	BasForPaslag	FLOAT)
primary index (Article_Seq_Num, Calendar_Dt) on commit preserve rows

;insert into ZZNB01 
select	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg(a11.Base_Price_Amt)  BasForPaslag
from	ITSemCMNVOUT.ARTICLE_COST_PRICE_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Calendar_Dt = a12.Calendar_Dt)
	join	ITSemCMNVOUT.ARTICLE_D	a13
	  on 	(a11.Article_Seq_Num = a13.Article_Seq_Num)
where	(a12.Calendar_Week_Id in (201342)
 and a11.Cost_Price_List_Cd in ('AA')
 and a13.Article_Id in ('100365418', '100002676', '101093718', '101188044', '100001150', '100810687', '100027338', '100748997', '100679393', '100736367', '100600609', '101098848', '100225560', '101209552', '101210289', '100520697', '101169980'))
group by	a11.Article_Seq_Num,
	a11.Calendar_Dt

create volatile table ZZMB02, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB02 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB01	pc11
group by	pc11.Article_Seq_Num

create volatile table ZZSP03, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZSP03 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.BasForPaslag)  WJXBFS1
from	ZZNB01	pa11
	join	ZZMB02	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
group by	pa11.Article_Seq_Num

create volatile table ZZNB04, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	KonsPrisExkl	FLOAT, 
	KonsPrisInkl	FLOAT)
primary index (Article_Seq_Num, Calendar_Dt) on commit preserve rows

;insert into ZZNB04 
select	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg(a11.Retail_Price_Amt_Excl_VAT)  KonsPrisExkl,
	avg(a11.Retail_Price_Amt_Incl_VAT)  KonsPrisInkl
from	ITSemCMNVOUT.ARTICLE_RETAIL_PRICE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Location_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Calendar_Dt = a13.Calendar_Dt)
	join	ITSemCMNVOUT.ARTICLE_D	a14
	  on 	(a11.Article_Seq_Num = a14.Article_Seq_Num)
where	(a13.Calendar_Week_Id in (201342)
 and a12.Cost_Price_List_Cd in ('AA')
 and a14.Article_Id in ('100365418', '100002676', '101093718', '101188044', '100001150', '100810687', '100027338', '100748997', '100679393', '100736367', '100600609', '101098848', '100225560', '101209552', '101210289', '100520697', '101169980')
 and a12.Concept_Cd in ('HEM'))
group by	a11.Article_Seq_Num,
	a11.Calendar_Dt

create volatile table ZZMB05, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB05 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB04	pc11
group by	pc11.Article_Seq_Num

create volatile table ZZSP06, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT, 
	WJXBFS2	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZSP06 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.KonsPrisExkl)  WJXBFS1,
	max(pa11.KonsPrisInkl)  WJXBFS2
from	ZZNB04	pa11
	join	ZZMB05	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
group by	pa11.Article_Seq_Num

create volatile table ZZMD07, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	AntalSalda	FLOAT, 
	BasForPaslag	FLOAT, 
	KonsPrisExkl	FLOAT, 
	KonsPrisInkl	FLOAT, 
	ForsBelExBVBer	FLOAT, 
	InkopsBelEx	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMD07 
select	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num)  Article_Seq_Num,
	pa11.AntalSalda  AntalSalda,
	pa12.WJXBFS1  BasForPaslag,
	pa13.WJXBFS1  KonsPrisExkl,
	pa13.WJXBFS2  KonsPrisInkl,
	pa11.ForsBelExBVBer  ForsBelExBVBer,
	pa11.InkopsBelEx  InkopsBelEx
from	ZZSP00	pa11
	full outer join	ZZSP03	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num)
	full outer join	ZZSP06	pa13
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num) = pa13.Article_Seq_Num)

create volatile table ZZNB08, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	Listpris	FLOAT, 
	GODWFLAG9_1	INTEGER, 
	FURFaktListpr	FLOAT, 
	GODWFLAGc_1	INTEGER, 
	EstKvantrab	FLOAT, 
	GODWFLAGf_1	INTEGER, 
	EstPallrabatt	FLOAT, 
	GODWFLAG12_1	INTEGER, 
	EstFraktkostn	FLOAT, 
	GODWFLAG15_1	INTEGER, 
	PVServPOS	FLOAT, 
	GODWFLAG18_1	INTEGER, 
	PVServPOS1	FLOAT, 
	GODWFLAG1b_1	INTEGER, 
	PVServPOS2	FLOAT, 
	GODWFLAG1e_1	INTEGER, 
	PVServPlock	FLOAT, 
	GODWFLAG21_1	INTEGER, 
	PVServPOS3	FLOAT, 
	GODWFLAG24_1	INTEGER, 
	InkPrisGrossist	FLOAT, 
	GODWFLAG27_1	INTEGER, 
	NettoNetPris	FLOAT, 
	GODWFLAG2a_1	INTEGER, 
	EMVFaktor	FLOAT, 
	GODWFLAG2d_1	INTEGER, 
	ServEDIst	FLOAT, 
	GODWFLAG30_1	INTEGER, 
	ServKrossSt	FLOAT, 
	GODWFLAG33_1	INTEGER, 
	ServPlockSt	FLOAT, 
	GODWFLAG36_1	INTEGER, 
	ServPOSst	FLOAT, 
	GODWFLAG39_1	INTEGER, 
	ServSKst	FLOAT, 
	GODWFLAG3c_1	INTEGER, 
	LogpaslagBas	FLOAT, 
	GODWFLAG3f_1	INTEGER, 
	LogpaslagArt	FLOAT, 
	GODWFLAG42_1	INTEGER, 
	SIPalagg	FLOAT, 
	GODWFLAG45_1	INTEGER, 
	PVServPOS4	FLOAT, 
	GODWFLAG48_1	INTEGER, 
	PVServPOS5	FLOAT, 
	GODWFLAG4b_1	INTEGER, 
	TransfPris	FLOAT, 
	GODWFLAG4e_1	INTEGER, 
	DirektlevPaslag	FLOAT, 
	GODWFLAG51_1	INTEGER, 
	FUREfterskLP	FLOAT, 
	GODWFLAG54_1	INTEGER, 
	FUREfterskNet	FLOAT, 
	GODWFLAG57_1	INTEGER, 
	FURFakturaNet	FLOAT, 
	GODWFLAG5a_1	INTEGER, 
	MREfterskott	FLOAT, 
	GODWFLAG5d_1	INTEGER, 
	CMRZ145	FLOAT, 
	GODWFLAG60_1	INTEGER, 
	FMR	FLOAT, 
	GODWFLAG63_1	INTEGER)
primary index (Article_Seq_Num, Calendar_Dt) on commit preserve rows

;insert into ZZNB08 
select	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg((Case when a11.Price_Condition_Type_Cd in ('PB00') then a11.Price_Condition_Amt else NULL end))  Listpris,
	max((Case when a11.Price_Condition_Type_Cd in ('PB00') then 1 else 0 end))  GODWFLAG9_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z101') then a11.Price_Condition_Amt else NULL end))  FURFaktListpr,
	max((Case when a11.Price_Condition_Type_Cd in ('Z101') then 1 else 0 end))  GODWFLAGc_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z120', 'Z122') then a11.Price_Condition_Amt else NULL end))  EstKvantrab,
	max((Case when a11.Price_Condition_Type_Cd in ('Z120', 'Z122') then 1 else 0 end))  GODWFLAGf_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z125') then a11.Price_Condition_Amt else NULL end))  EstPallrabatt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z125') then 1 else 0 end))  GODWFLAG12_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z127') then a11.Price_Condition_Amt else NULL end))  EstFraktkostn,
	max((Case when a11.Price_Condition_Type_Cd in ('Z127') then 1 else 0 end))  GODWFLAG15_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z150') then a11.Price_Condition_Amt else NULL end))  PVServPOS,
	max((Case when a11.Price_Condition_Type_Cd in ('Z150') then 1 else 0 end))  GODWFLAG18_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z151') then a11.Price_Condition_Amt else NULL end))  PVServPOS1,
	max((Case when a11.Price_Condition_Type_Cd in ('Z151') then 1 else 0 end))  GODWFLAG1b_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z155') then a11.Price_Condition_Amt else NULL end))  PVServPOS2,
	max((Case when a11.Price_Condition_Type_Cd in ('Z155') then 1 else 0 end))  GODWFLAG1e_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z156') then a11.Price_Condition_Amt else NULL end))  PVServPlock,
	max((Case when a11.Price_Condition_Type_Cd in ('Z156') then 1 else 0 end))  GODWFLAG21_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z157') then a11.Price_Condition_Amt else NULL end))  PVServPOS3,
	max((Case when a11.Price_Condition_Type_Cd in ('Z157') then 1 else 0 end))  GODWFLAG24_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZSVK') then a11.Price_Condition_Amt else NULL end))  InkPrisGrossist,
	max((Case when a11.Price_Condition_Type_Cd in ('ZSVK') then 1 else 0 end))  GODWFLAG27_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZEKN') then a11.Price_Condition_Amt else NULL end))  NettoNetPris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZEKN') then 1 else 0 end))  GODWFLAG2a_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z232') then a11.Price_Condition_Amt else NULL end))  EMVFaktor,
	max((Case when a11.Price_Condition_Type_Cd in ('Z232') then 1 else 0 end))  GODWFLAG2d_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z180') then a11.Price_Condition_Amt else NULL end))  ServEDIst,
	max((Case when a11.Price_Condition_Type_Cd in ('Z180') then 1 else 0 end))  GODWFLAG30_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z185') then a11.Price_Condition_Amt else NULL end))  ServKrossSt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z185') then 1 else 0 end))  GODWFLAG33_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z189') then a11.Price_Condition_Amt else NULL end))  ServPlockSt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z189') then 1 else 0 end))  GODWFLAG36_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z183') then a11.Price_Condition_Amt else NULL end))  ServPOSst,
	max((Case when a11.Price_Condition_Type_Cd in ('Z183') then 1 else 0 end))  GODWFLAG39_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z187') then a11.Price_Condition_Amt else NULL end))  ServSKst,
	max((Case when a11.Price_Condition_Type_Cd in ('Z187') then 1 else 0 end))  GODWFLAG3c_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z250', 'Z251') then a11.Price_Condition_Amt else NULL end))  LogpaslagBas,
	max((Case when a11.Price_Condition_Type_Cd in ('Z250', 'Z251') then 1 else 0 end))  GODWFLAG3f_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z252', 'Z253') then a11.Price_Condition_Amt else NULL end))  LogpaslagArt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z252', 'Z253') then 1 else 0 end))  GODWFLAG42_1,
	avg((Case when (a11.Price_Condition_Type_Cd in ('Z241') or a11.Price_Condition_Type_Cd in ('Z240')) then a11.Price_Condition_Amt else NULL end))  SIPalagg,
	max((Case when (a11.Price_Condition_Type_Cd in ('Z241') or a11.Price_Condition_Type_Cd in ('Z240')) then 1 else 0 end))  GODWFLAG45_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z260') then a11.Price_Condition_Amt else NULL end))  PVServPOS4,
	max((Case when a11.Price_Condition_Type_Cd in ('Z260') then 1 else 0 end))  GODWFLAG48_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z261') then a11.Price_Condition_Amt else NULL end))  PVServPOS5,
	max((Case when a11.Price_Condition_Type_Cd in ('Z261') then 1 else 0 end))  GODWFLAG4b_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then a11.Price_Condition_Amt else NULL end))  TransfPris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then 1 else 0 end))  GODWFLAG4e_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z278') then a11.Price_Condition_Amt else NULL end))  DirektlevPaslag,
	max((Case when a11.Price_Condition_Type_Cd in ('Z278') then 1 else 0 end))  GODWFLAG51_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z110') then a11.Price_Condition_Amt else NULL end))  FUREfterskLP,
	max((Case when a11.Price_Condition_Type_Cd in ('Z110') then 1 else 0 end))  GODWFLAG54_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z115') then a11.Price_Condition_Amt else NULL end))  FUREfterskNet,
	max((Case when a11.Price_Condition_Type_Cd in ('Z115') then 1 else 0 end))  GODWFLAG57_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z105') then a11.Price_Condition_Amt else NULL end))  FURFakturaNet,
	max((Case when a11.Price_Condition_Type_Cd in ('Z105') then 1 else 0 end))  GODWFLAG5a_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z140', 'Z141') then a11.Price_Condition_Amt else NULL end))  MREfterskott,
	max((Case when a11.Price_Condition_Type_Cd in ('Z140', 'Z141') then 1 else 0 end))  GODWFLAG5d_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z145') then a11.Price_Condition_Amt else NULL end))  CMRZ145,
	max((Case when a11.Price_Condition_Type_Cd in ('Z145') then 1 else 0 end))  GODWFLAG60_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZFMR') then a11.Price_Condition_Amt else NULL end))  FMR,
	max((Case when a11.Price_Condition_Type_Cd in ('ZFMR') then 1 else 0 end))  GODWFLAG63_1
from	ITSemCMNVOUT.ARTICLE_COST_PRICE_COND_F	a11
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a12
	  on 	(a11.Calendar_Dt = a12.Calendar_Dt)
	join	ITSemCMNVOUT.ARTICLE_D	a13
	  on 	(a11.Article_Seq_Num = a13.Article_Seq_Num)
where	(a12.Calendar_Week_Id in (201342)
 and a11.Cost_Price_List_Cd in ('AA')
 and a13.Article_Id in ('100365418', '100002676', '101093718', '101188044', '100001150', '100810687', '100027338', '100748997', '100679393', '100736367', '100600609', '101098848', '100225560', '101209552', '101210289', '100520697', '101169980')
 and (a11.Price_Condition_Type_Cd in ('PB00')
 or a11.Price_Condition_Type_Cd in ('Z101')
 or a11.Price_Condition_Type_Cd in ('Z120', 'Z122')
 or a11.Price_Condition_Type_Cd in ('Z125')
 or a11.Price_Condition_Type_Cd in ('Z127')
 or a11.Price_Condition_Type_Cd in ('Z150')
 or a11.Price_Condition_Type_Cd in ('Z151')
 or a11.Price_Condition_Type_Cd in ('Z155')
 or a11.Price_Condition_Type_Cd in ('Z156')
 or a11.Price_Condition_Type_Cd in ('Z157')
 or a11.Price_Condition_Type_Cd in ('ZSVK')
 or a11.Price_Condition_Type_Cd in ('ZEKN')
 or a11.Price_Condition_Type_Cd in ('Z232')
 or a11.Price_Condition_Type_Cd in ('Z180')
 or a11.Price_Condition_Type_Cd in ('Z185')
 or a11.Price_Condition_Type_Cd in ('Z189')
 or a11.Price_Condition_Type_Cd in ('Z183')
 or a11.Price_Condition_Type_Cd in ('Z187')
 or a11.Price_Condition_Type_Cd in ('Z250', 'Z251')
 or a11.Price_Condition_Type_Cd in ('Z252', 'Z253')
 or (a11.Price_Condition_Type_Cd in ('Z241')
 or a11.Price_Condition_Type_Cd in ('Z240'))
 or a11.Price_Condition_Type_Cd in ('Z260')
 or a11.Price_Condition_Type_Cd in ('Z261')
 or a11.Price_Condition_Type_Cd in ('ZKP2')
 or a11.Price_Condition_Type_Cd in ('Z278')
 or a11.Price_Condition_Type_Cd in ('Z110')
 or a11.Price_Condition_Type_Cd in ('Z115')
 or a11.Price_Condition_Type_Cd in ('Z105')
 or a11.Price_Condition_Type_Cd in ('Z140', 'Z141')
 or a11.Price_Condition_Type_Cd in ('Z145')
 or a11.Price_Condition_Type_Cd in ('ZFMR')))
group by	a11.Article_Seq_Num,
	a11.Calendar_Dt

create volatile table ZZMB09, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB09 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG9_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0A, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0A 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.Listpris)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB09	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG9_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0B, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0B 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAGc_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0C, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0C 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FURFaktListpr)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0B	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAGc_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0D, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0D 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAGf_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0E, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0E 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EstKvantrab)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0D	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAGf_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0F, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0F 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG12_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0G, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0G 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EstPallrabatt)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0F	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG12_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0H, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0H 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG15_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0I, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0I 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EstFraktkostn)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0H	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG15_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0J, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0J 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG18_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0K, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0K 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0J	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG18_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0L, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0L 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG1b_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0M, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0M 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS1)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0L	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG1b_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0N, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0N 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG1e_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0O, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0O 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS2)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0N	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG1e_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0P, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0P 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG21_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0Q, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0Q 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPlock)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0P	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG21_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0R, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0R 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG24_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0S, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0S 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS3)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0R	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG24_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0T, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0T 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG27_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0U, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0U 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.InkPrisGrossist)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0T	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG27_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0V, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0V 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG2a_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0W, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0W 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.NettoNetPris)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0V	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG2a_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0X, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0X 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG2d_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0Y, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0Y 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EMVFaktor)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0X	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG2d_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0Z, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0Z 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG30_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC10, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC10 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServEDIst)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB0Z	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG30_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB11, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB11 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG33_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC12, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC12 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServKrossSt)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB11	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG33_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB13, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB13 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG36_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC14, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC14 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServPlockSt)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB13	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG36_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB15, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB15 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG39_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC16, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC16 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServPOSst)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB15	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG39_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB17, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB17 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG3c_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC18, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC18 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServSKst)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB17	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG3c_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB19, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB19 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG3f_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1A, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1A 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.LogpaslagBas)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB19	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG3f_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1B, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1B 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG42_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1C, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1C 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.LogpaslagArt)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1B	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG42_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1D, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1D 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG45_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1E, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1E 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.SIPalagg)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1D	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG45_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1F, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1F 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG48_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1G, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1G 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS4)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1F	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG48_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1H, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1H 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG4b_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1I, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1I 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS5)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1H	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG4b_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1J, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1J 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG4e_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1K, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1K 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.TransfPris)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1J	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG4e_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1L, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1L 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG51_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1M, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1M 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.DirektlevPaslag)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1L	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG51_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1N, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1N 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG54_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1O, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1O 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FUREfterskLP)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1N	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG54_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1P, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1P 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG57_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1Q, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1Q 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FUREfterskNet)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1P	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG57_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1R, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1R 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG5a_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1S, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1S 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FURFakturaNet)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1R	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG5a_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1T, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1T 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG5d_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1U, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1U 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.MREfterskott)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1T	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG5d_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1V, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1V 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG60_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1W, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1W 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.CMRZ145)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1V	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG60_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1X, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1X 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB08	pc11
where	pc11.GODWFLAG63_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1Y, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1Y 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FMR)  WJXBFS1
from	ZZNB08	pa11
	join	ZZMB1X	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG63_1 = 1
group by	pa11.Article_Seq_Num

select	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num, pa131.Article_Seq_Num, pa132.Article_Seq_Num)  Article_Seq_Num,
	max(a133.Article_Id)  Article_Id,
	max(a133.Article_Desc)  Article_Desc,
	max(pa11.AntalSalda)  AntalSalda,
	max(pa12.WJXBFS1)  Listpris,
	max(pa13.WJXBFS1)  FURFaktListpr,
	max(pa14.WJXBFS1)  EstKvantrab,
	max(pa15.WJXBFS1)  EstPallrabatt,
	max(pa16.WJXBFS1)  EstFraktkostn,
	(ZEROIFNULL(max(pa17.WJXBFS1)) + ZEROIFNULL(max(pa18.WJXBFS1)))  EMK1st,
	((ZEROIFNULL(max(pa19.WJXBFS1)) + ZEROIFNULL(max(pa110.WJXBFS1))) + ZEROIFNULL(max(pa111.WJXBFS1)))  EMK2st,
	max(pa112.WJXBFS1)  InkPrisGrossist,
	max(pa113.WJXBFS1)  NettoNetPris,
	max(pa114.WJXBFS1)  EMVFaktor,
	max(pa115.WJXBFS1)  ServEDIst,
	max(pa116.WJXBFS1)  ServKrossSt,
	max(pa117.WJXBFS1)  ServPlockSt,
	max(pa118.WJXBFS1)  ServPOSst,
	max(pa119.WJXBFS1)  ServSKst,
	max(pa11.BasForPaslag)  BasForPaslag,
	max(pa120.WJXBFS1)  LogpaslagBas,
	max(pa121.WJXBFS1)  LogpaslagArt,
	max(pa122.WJXBFS1)  SIPalagg,
	(ZEROIFNULL(max(pa123.WJXBFS1)) + ZEROIFNULL(max(pa124.WJXBFS1)))  Kedjepaslag,
	max(pa125.WJXBFS1)  TransfPris,
	max(pa11.KonsPrisExkl)  KonsPrisExkl,
	max(pa11.KonsPrisInkl)  KonsPrisInkl,
	max(pa126.WJXBFS1)  DirektlevPaslag,
	max(pa127.WJXBFS1)  FUREfterskLP,
	max(pa128.WJXBFS1)  FUREfterskNet,
	max(pa129.WJXBFS1)  FURFakturaNet,
	(ZEROIFNULL(max(pa130.WJXBFS1)) + ZEROIFNULL(max(pa131.WJXBFS1)))  CMR,
	max(pa11.ForsBelExBVBer)  ForsBelExBVBer,
	max(pa11.InkopsBelEx)  InkopsBelEx,
	max(pa132.WJXBFS1)  FMR
from	ZZMD07	pa11
	full outer join	ZZNC0A	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num)
	full outer join	ZZNC0C	pa13
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num) = pa13.Article_Seq_Num)
	full outer join	ZZNC0E	pa14
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num) = pa14.Article_Seq_Num)
	full outer join	ZZNC0G	pa15
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num) = pa15.Article_Seq_Num)
	full outer join	ZZNC0I	pa16
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num) = pa16.Article_Seq_Num)
	full outer join	ZZNC0K	pa17
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num) = pa17.Article_Seq_Num)
	full outer join	ZZNC0M	pa18
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num) = pa18.Article_Seq_Num)
	full outer join	ZZNC0O	pa19
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num) = pa19.Article_Seq_Num)
	full outer join	ZZNC0Q	pa110
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num) = pa110.Article_Seq_Num)
	full outer join	ZZNC0S	pa111
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num) = pa111.Article_Seq_Num)
	full outer join	ZZNC0U	pa112
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num) = pa112.Article_Seq_Num)
	full outer join	ZZNC0W	pa113
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num) = pa113.Article_Seq_Num)
	full outer join	ZZNC0Y	pa114
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num) = pa114.Article_Seq_Num)
	full outer join	ZZNC10	pa115
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num) = pa115.Article_Seq_Num)
	full outer join	ZZNC12	pa116
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num) = pa116.Article_Seq_Num)
	full outer join	ZZNC14	pa117
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num) = pa117.Article_Seq_Num)
	full outer join	ZZNC16	pa118
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num) = pa118.Article_Seq_Num)
	full outer join	ZZNC18	pa119
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num) = pa119.Article_Seq_Num)
	full outer join	ZZNC1A	pa120
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num) = pa120.Article_Seq_Num)
	full outer join	ZZNC1C	pa121
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num) = pa121.Article_Seq_Num)
	full outer join	ZZNC1E	pa122
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num) = pa122.Article_Seq_Num)
	full outer join	ZZNC1G	pa123
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num) = pa123.Article_Seq_Num)
	full outer join	ZZNC1I	pa124
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num) = pa124.Article_Seq_Num)
	full outer join	ZZNC1K	pa125
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num) = pa125.Article_Seq_Num)
	full outer join	ZZNC1M	pa126
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num) = pa126.Article_Seq_Num)
	full outer join	ZZNC1O	pa127
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num) = pa127.Article_Seq_Num)
	full outer join	ZZNC1Q	pa128
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num) = pa128.Article_Seq_Num)
	full outer join	ZZNC1S	pa129
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num) = pa129.Article_Seq_Num)
	full outer join	ZZNC1U	pa130
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num) = pa130.Article_Seq_Num)
	full outer join	ZZNC1W	pa131
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num) = pa131.Article_Seq_Num)
	full outer join	ZZNC1Y	pa132
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num, pa131.Article_Seq_Num) = pa132.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a133
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num, pa131.Article_Seq_Num, pa132.Article_Seq_Num) = a133.Article_Seq_Num)
group by	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num, pa124.Article_Seq_Num, pa125.Article_Seq_Num, pa126.Article_Seq_Num, pa127.Article_Seq_Num, pa128.Article_Seq_Num, pa129.Article_Seq_Num, pa130.Article_Seq_Num, pa131.Article_Seq_Num, pa132.Article_Seq_Num)


SET QUERY_BAND = NONE For Session;


drop table ZZSP00

drop table ZZNB01

drop table ZZMB02

drop table ZZSP03

drop table ZZNB04

drop table ZZMB05

drop table ZZSP06

drop table ZZMD07

drop table ZZNB08

drop table ZZMB09

drop table ZZNC0A

drop table ZZMB0B

drop table ZZNC0C

drop table ZZMB0D

drop table ZZNC0E

drop table ZZMB0F

drop table ZZNC0G

drop table ZZMB0H

drop table ZZNC0I

drop table ZZMB0J

drop table ZZNC0K

drop table ZZMB0L

drop table ZZNC0M

drop table ZZMB0N

drop table ZZNC0O

drop table ZZMB0P

drop table ZZNC0Q

drop table ZZMB0R

drop table ZZNC0S

drop table ZZMB0T

drop table ZZNC0U

drop table ZZMB0V

drop table ZZNC0W

drop table ZZMB0X

drop table ZZNC0Y

drop table ZZMB0Z

drop table ZZNC10

drop table ZZMB11

drop table ZZNC12

drop table ZZMB13

drop table ZZNC14

drop table ZZMB15

drop table ZZNC16

drop table ZZMB17

drop table ZZNC18

drop table ZZMB19

drop table ZZNC1A

drop table ZZMB1B

drop table ZZNC1C

drop table ZZMB1D

drop table ZZNC1E

drop table ZZMB1F

drop table ZZNC1G

drop table ZZMB1H

drop table ZZNC1I

drop table ZZMB1J

drop table ZZNC1K

drop table ZZMB1L

drop table ZZNC1M

drop table ZZMB1N

drop table ZZNC1O

drop table ZZMB1P

drop table ZZNC1Q

drop table ZZMB1R

drop table ZZNC1S

drop table ZZMB1T

drop table ZZNC1U

drop table ZZMB1V

drop table ZZNC1W

drop table ZZMB1X

drop table ZZNC1Y
