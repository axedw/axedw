SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=17 - MA202 - Extern bonus; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Partner_Id	VARCHAR(50), 
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	BonusgrBel	FLOAT)
primary index (Partner_Id, Calendar_Month_Id, Loyalty_Member_Type) on commit preserve rows

;insert into ZZMD00 
select	a15.Partner_Id  Partner_Id,
	a14.Calendar_Month_Id  Calendar_Month_Id,
	a12.Loyalty_Member_Type  Loyalty_Member_Type,
	sum(a11.Bonus_Applicable_Amt)  BonusgrBel
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a12.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a14
	  on 	(a11.Processing_Dt = a14.Calendar_Dt)
	join	ITSemCMNVOUT.LOYALTY_PARTNER_D	a15
	  on 	(a11.Partner_Seq_Num = a15.Partner_Seq_Num)
where	(a13.Loyalty_Program_Id in ('1-MQ7V')
 and a15.Partner_Id not in ('0001008556')
 and a14.Calendar_Month_Id in (201306)
 and a15.Partner_Id <> -1
 and a11.Loyalty_Trans_Sub_Type_Cd in ('Purchase'))
group by	a15.Partner_Id,
	a14.Calendar_Month_Id,
	a12.Loyalty_Member_Type

create volatile table ZZSP01, no fallback, no log(
	Partner_Id	VARCHAR(50), 
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	PurchaseAmt	FLOAT, 
	AccrualAmt	FLOAT)
primary index (Partner_Id, Calendar_Month_Id, Loyalty_Member_Type) on commit preserve rows

;insert into ZZSP01 
select	a14.Partner_Id  Partner_Id,
	a11.Calendar_Month_Id  Calendar_Month_Id,
	a12.Loyalty_Member_Type  Loyalty_Member_Type,
	sum(a11.Purchase_Amt)  PurchaseAmt,
	sum(a11.Accrual_Amt)  AccrualAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a12.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PARTNER_D	a14
	  on 	(a11.Partner_Seq_Num = a14.Partner_Seq_Num)
where	(a13.Loyalty_Program_Id in ('1-MQ7V')
 and a14.Partner_Id not in ('0001008556')
 and a11.Calendar_Month_Id in (201306)
 and a14.Partner_Id <> -1)
group by	a14.Partner_Id,
	a11.Calendar_Month_Id,
	a12.Loyalty_Member_Type

create volatile table ZZSP02, no fallback, no log(
	Partner_Id	VARCHAR(50), 
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	DowngradedAmt	FLOAT)
primary index (Partner_Id, Calendar_Month_Id, Loyalty_Member_Type) on commit preserve rows

;insert into ZZSP02 
select	a14.Partner_Id  Partner_Id,
	a11.Calendar_Month_Id  Calendar_Month_Id,
	a12.Loyalty_Member_Type  Loyalty_Member_Type,
	sum(a11.Downgraded_Amt)  DowngradedAmt
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_MONTH_F	a11
	join	ITSemCMNVOUT.LOYALTY_MEMBER_ACCOUNT_D	a12
	  on 	(a11.Member_Account_Seq_Num = a12.Member_Account_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PROGRAM_D	a13
	  on 	(a12.Loyalty_Program_Seq_Num = a13.Loyalty_Program_Seq_Num)
	join	ITSemCMNVOUT.LOYALTY_PARTNER_D	a14
	  on 	(a11.Partner_Seq_Num = a14.Partner_Seq_Num)
where	(a13.Loyalty_Program_Id in ('1-MQ7V')
 and a14.Partner_Id not in ('0001008556')
 and a11.Calendar_Month_Id in (201306)
 and a14.Partner_Id <> -1)
group by	a14.Partner_Id,
	a11.Calendar_Month_Id,
	a12.Loyalty_Member_Type

create volatile table ZZMD03, no fallback, no log(
	Partner_Id	VARCHAR(50), 
	Calendar_Month_Id	INTEGER, 
	Loyalty_Member_Type	VARCHAR(10), 
	PurchaseAmt	FLOAT, 
	AccrualAmt	FLOAT, 
	DowngradedAmt	FLOAT)
primary index (Partner_Id, Calendar_Month_Id, Loyalty_Member_Type) on commit preserve rows

;insert into ZZMD03 
select	coalesce(pa11.Partner_Id, pa12.Partner_Id)  Partner_Id,
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	coalesce(pa11.Loyalty_Member_Type, pa12.Loyalty_Member_Type)  Loyalty_Member_Type,
	pa11.PurchaseAmt  PurchaseAmt,
	pa11.AccrualAmt  AccrualAmt,
	pa12.DowngradedAmt  DowngradedAmt
from	ZZSP01	pa11
	full outer join	ZZSP02	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Loyalty_Member_Type = pa12.Loyalty_Member_Type and 
	pa11.Partner_Id = pa12.Partner_Id)

select	coalesce(pa11.Partner_Id, pa12.Partner_Id)  Partner_Id,
	max(a14.Partner_Desc)  Partner_Desc,
	coalesce(pa11.Loyalty_Member_Type, pa12.Loyalty_Member_Type)  Loyalty_Member_Type,
	max(a13.Loyalty_Member_Type_Desc)  Loyalty_Member_Type_Desc,
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	max(pa11.BonusgrBel)  BonusgrBel,
	max(pa12.PurchaseAmt)  PurchaseAmt,
	max(pa12.AccrualAmt)  AccrualAmt,
	max(pa12.DowngradedAmt)  DowngradedAmt
from	ZZMD00	pa11
	full outer join	ZZMD03	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Loyalty_Member_Type = pa12.Loyalty_Member_Type and 
	pa11.Partner_Id = pa12.Partner_Id)
	join	ITSemCMNVOUT.LOYALTY_MEMBER_TYPE_D	a13
	  on 	(coalesce(pa11.Loyalty_Member_Type, pa12.Loyalty_Member_Type) = a13.Loyalty_Member_Type)
	join	ITSemCMNVOUT.LOYALTY_PARTNER_D	a14
	  on 	(coalesce(pa11.Partner_Id, pa12.Partner_Id) = a14.Partner_Id)
group by	coalesce(pa11.Partner_Id, pa12.Partner_Id),
	coalesce(pa11.Loyalty_Member_Type, pa12.Loyalty_Member_Type),
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZSP01

drop table ZZSP02

drop table ZZMD03
