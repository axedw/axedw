SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=52 - MA203 - Ekonomisk uppf�ljning p� transaktioner per butik; ClientUser=Administrator;' For Session;


create volatile table ZZMD00, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	ForsBelInklMedl	FLOAT)
primary index (Store_Type_Cd, Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD00 
select	a12.Store_Type_Cd  Store_Type_Cd,
	a13.Calendar_Month_Id  Calendar_Month_Id,
	a12.Store_Id  Store_Id,
	sum((a11.Unit_Selling_Price_Amt + a11.Tax_Amt))  ForsBelInklMedl
from	ITSemCMNVOUT.SALES_TRANSACTION_LINE_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Tran_Dt = a13.Calendar_Dt)
where	(a13.Calendar_Month_Id in (201304)
 and a12.Concept_Cd in ('HEM')
 and a11.Contact_Account_Seq_Num <> -1)
group by	a12.Store_Type_Cd,
	a13.Calendar_Month_Id,
	a12.Store_Id

create volatile table ZZMD01, no fallback, no log(
	Store_Type_Cd	CHAR(4), 
	Calendar_Month_Id	INTEGER, 
	Store_Id	INTEGER, 
	BonusgrBel	FLOAT)
primary index (Store_Type_Cd, Calendar_Month_Id, Store_Id) on commit preserve rows

;insert into ZZMD01 
select	a12.Store_Type_Cd  Store_Type_Cd,
	a13.Calendar_Month_Id  Calendar_Month_Id,
	a12.Store_Id  Store_Id,
	sum(a11.Bonus_Applicable_Amt)  BonusgrBel
from	ITSemCMNVOUT.LOYALTY_TRANSACTION_F	a11
	join	ITSemCMNVOUT.STORE_D	a12
	  on 	(a11.Store_Seq_Num = a12.Store_Seq_Num)
	join	ITSemCMNVOUT.CALENDAR_DAY_D	a13
	  on 	(a11.Processing_Dt = a13.Calendar_Dt)
	join	ITSemCMNVOUT.LOYALTY_PARTNER_D	a14
	  on 	(a11.Partner_Seq_Num = a14.Partner_Seq_Num)
where	(a13.Calendar_Month_Id in (201304)
 and a14.Partner_Id = -1
 and a12.Concept_Cd in ('HEM')
 and a11.Loyalty_Trans_Sub_Type_Cd in ('Purchase'))
group by	a12.Store_Type_Cd,
	a13.Calendar_Month_Id,
	a12.Store_Id

select	coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd)  Store_Type_Cd,
	max(a14.Store_Type_Desc)  Store_Type_Desc,
	coalesce(pa11.Store_Id, pa12.Store_Id)  Store_Id,
	max(a13.Store_Name)  Store_Name,
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)  Calendar_Month_Id,
	max(pa11.ForsBelInklMedl)  ForsBelInklMedl,
	max(pa12.BonusgrBel)  BonusgrBel
from	ZZMD00	pa11
	full outer join	ZZMD01	pa12
	  on 	(pa11.Calendar_Month_Id = pa12.Calendar_Month_Id and 
	pa11.Store_Id = pa12.Store_Id and 
	pa11.Store_Type_Cd = pa12.Store_Type_Cd)
	join	ITSemCMNVOUT.STORE_D	a13
	  on 	(coalesce(pa11.Store_Id, pa12.Store_Id) = a13.Store_Id)
	join	ITSemCMNVOUT.STORE_TYPE_D	a14
	  on 	(coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd) = a14.Store_Type_Cd)
group by	coalesce(pa11.Store_Type_Cd, pa12.Store_Type_Cd),
	coalesce(pa11.Store_Id, pa12.Store_Id),
	coalesce(pa11.Calendar_Month_Id, pa12.Calendar_Month_Id)


SET QUERY_BAND = NONE For Session;


drop table ZZMD00

drop table ZZMD01

