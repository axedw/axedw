SET QUERY_BAND = 'ApplicationName=MicroStrategy; Source=Detaljhandel(AxEDW-IT)(20130725); Action=Test priskomp l�svikt; ClientUser=Administrator;' For Session;


create volatile table ZZNB00, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	Grundpris	FLOAT, 
	GODWFLAG1_1	INTEGER, 
	Listpris	FLOAT, 
	GODWFLAG4_1	INTEGER, 
	FMR	FLOAT, 
	GODWFLAG7_1	INTEGER, 
	MREfterskott	FLOAT, 
	GODWFLAGa_1	INTEGER, 
	CMRZ145	FLOAT, 
	GODWFLAGd_1	INTEGER, 
	NettoNetPris	FLOAT, 
	GODWFLAG10_1	INTEGER, 
	FURFakturaNet	FLOAT, 
	GODWFLAG13_1	INTEGER, 
	FURFaktListpr	FLOAT, 
	GODWFLAG16_1	INTEGER, 
	FUREfterskNet	FLOAT, 
	GODWFLAG19_1	INTEGER, 
	FUREfterskLP	FLOAT, 
	GODWFLAG1c_1	INTEGER, 
	EstKvantrab	FLOAT, 
	GODWFLAG1f_1	INTEGER, 
	EstPallrabatt	FLOAT, 
	GODWFLAG22_1	INTEGER, 
	EstFraktkostn	FLOAT, 
	GODWFLAG25_1	INTEGER, 
	ServPOSst	FLOAT, 
	GODWFLAG28_1	INTEGER, 
	ServPlockSt	FLOAT, 
	GODWFLAG2b_1	INTEGER, 
	TransfPris	FLOAT, 
	GODWFLAG31_1	INTEGER, 
	PVServPOS	FLOAT, 
	GODWFLAG34_1	INTEGER, 
	PVServPOS1	FLOAT, 
	GODWFLAG37_1	INTEGER, 
	PVServPOS2	FLOAT, 
	GODWFLAG3a_1	INTEGER, 
	PVServPlock	FLOAT, 
	GODWFLAG3d_1	INTEGER, 
	PVServPOS3	FLOAT, 
	GODWFLAG40_1	INTEGER, 
	EMVFaktor	FLOAT, 
	GODWFLAG43_1	INTEGER)
primary index (Article_Seq_Num, Calendar_Dt) on commit preserve rows

;insert into ZZNB00 
select	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZPB2') then a11.Price_Condition_Amt else NULL end))  Grundpris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZPB2') then 1 else 0 end))  GODWFLAG1_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('PB00') then a11.Price_Condition_Amt else NULL end))  Listpris,
	max((Case when a11.Price_Condition_Type_Cd in ('PB00') then 1 else 0 end))  GODWFLAG4_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZFMR') then a11.Price_Condition_Amt else NULL end))  FMR,
	max((Case when a11.Price_Condition_Type_Cd in ('ZFMR') then 1 else 0 end))  GODWFLAG7_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z140', 'Z141') then a11.Price_Condition_Amt else NULL end))  MREfterskott,
	max((Case when a11.Price_Condition_Type_Cd in ('Z140', 'Z141') then 1 else 0 end))  GODWFLAGa_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z145') then a11.Price_Condition_Amt else NULL end))  CMRZ145,
	max((Case when a11.Price_Condition_Type_Cd in ('Z145') then 1 else 0 end))  GODWFLAGd_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZEKN') then a11.Price_Condition_Amt else NULL end))  NettoNetPris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZEKN') then 1 else 0 end))  GODWFLAG10_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z105') then a11.Price_Condition_Amt else NULL end))  FURFakturaNet,
	max((Case when a11.Price_Condition_Type_Cd in ('Z105') then 1 else 0 end))  GODWFLAG13_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z101') then a11.Price_Condition_Amt else NULL end))  FURFaktListpr,
	max((Case when a11.Price_Condition_Type_Cd in ('Z101') then 1 else 0 end))  GODWFLAG16_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z115') then a11.Price_Condition_Amt else NULL end))  FUREfterskNet,
	max((Case when a11.Price_Condition_Type_Cd in ('Z115') then 1 else 0 end))  GODWFLAG19_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z110') then a11.Price_Condition_Amt else NULL end))  FUREfterskLP,
	max((Case when a11.Price_Condition_Type_Cd in ('Z110') then 1 else 0 end))  GODWFLAG1c_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z120', 'Z122') then a11.Price_Condition_Amt else NULL end))  EstKvantrab,
	max((Case when a11.Price_Condition_Type_Cd in ('Z120', 'Z122') then 1 else 0 end))  GODWFLAG1f_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z125') then a11.Price_Condition_Amt else NULL end))  EstPallrabatt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z125') then 1 else 0 end))  GODWFLAG22_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z127') then a11.Price_Condition_Amt else NULL end))  EstFraktkostn,
	max((Case when a11.Price_Condition_Type_Cd in ('Z127') then 1 else 0 end))  GODWFLAG25_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z183') then a11.Price_Condition_Amt else NULL end))  ServPOSst,
	max((Case when a11.Price_Condition_Type_Cd in ('Z183') then 1 else 0 end))  GODWFLAG28_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z189') then a11.Price_Condition_Amt else NULL end))  ServPlockSt,
	max((Case when a11.Price_Condition_Type_Cd in ('Z189') then 1 else 0 end))  GODWFLAG2b_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then a11.Price_Condition_Amt else NULL end))  TransfPris,
	max((Case when a11.Price_Condition_Type_Cd in ('ZKP2') then 1 else 0 end))  GODWFLAG31_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z150') then a11.Price_Condition_Amt else NULL end))  PVServPOS,
	max((Case when a11.Price_Condition_Type_Cd in ('Z150') then 1 else 0 end))  GODWFLAG34_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z151') then a11.Price_Condition_Amt else NULL end))  PVServPOS1,
	max((Case when a11.Price_Condition_Type_Cd in ('Z151') then 1 else 0 end))  GODWFLAG37_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z155') then a11.Price_Condition_Amt else NULL end))  PVServPOS2,
	max((Case when a11.Price_Condition_Type_Cd in ('Z155') then 1 else 0 end))  GODWFLAG3a_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z156') then a11.Price_Condition_Amt else NULL end))  PVServPlock,
	max((Case when a11.Price_Condition_Type_Cd in ('Z156') then 1 else 0 end))  GODWFLAG3d_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z157') then a11.Price_Condition_Amt else NULL end))  PVServPOS3,
	max((Case when a11.Price_Condition_Type_Cd in ('Z157') then 1 else 0 end))  GODWFLAG40_1,
	avg((Case when a11.Price_Condition_Type_Cd in ('Z232') then a11.Price_Condition_Amt else NULL end))  EMVFaktor,
	max((Case when a11.Price_Condition_Type_Cd in ('Z232') then 1 else 0 end))  GODWFLAG43_1
from	ITSemCMNVOUT.ARTICLE_COST_PRICE_COND_F	a11
	join	ITSemCMNVOUT.ARTICLE_D	a12
	  on 	(a11.Article_Seq_Num = a12.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a13
	  on 	(a12.Art_Hier_Lvl_8_Seq_Num = a13.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a14
	  on 	(a12.Art_Hier_Lvl_4_Seq_Num = a14.Art_Hier_Lvl_4_Seq_Num)
where	(a11.Calendar_Dt = DATE '2013-04-18'
 and a14.Art_Hier_Lvl_4_Id in ('140207')
 and (a11.Price_Condition_Type_Cd in ('ZPB2')
 or a11.Price_Condition_Type_Cd in ('PB00')
 or a11.Price_Condition_Type_Cd in ('ZFMR')
 or a11.Price_Condition_Type_Cd in ('Z140', 'Z141')
 or a11.Price_Condition_Type_Cd in ('Z145')
 or a11.Price_Condition_Type_Cd in ('ZEKN')
 or a11.Price_Condition_Type_Cd in ('Z105')
 or a11.Price_Condition_Type_Cd in ('Z101')
 or a11.Price_Condition_Type_Cd in ('Z115')
 or a11.Price_Condition_Type_Cd in ('Z110')
 or a11.Price_Condition_Type_Cd in ('Z120', 'Z122')
 or a11.Price_Condition_Type_Cd in ('Z125')
 or a11.Price_Condition_Type_Cd in ('Z127')
 or a11.Price_Condition_Type_Cd in ('Z183')
 or a11.Price_Condition_Type_Cd in ('Z189')
 or a11.Price_Condition_Type_Cd in ('ZKP2')
 or a11.Price_Condition_Type_Cd in ('Z150')
 or a11.Price_Condition_Type_Cd in ('Z151')
 or a11.Price_Condition_Type_Cd in ('Z155')
 or a11.Price_Condition_Type_Cd in ('Z156')
 or a11.Price_Condition_Type_Cd in ('Z157')
 or a11.Price_Condition_Type_Cd in ('Z232')))
group by	a11.Article_Seq_Num,
	a11.Calendar_Dt

create volatile table ZZMB01, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB01 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG1_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC02, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC02 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.Grundpris)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB01	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG1_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB03, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB03 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG4_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC04, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC04 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.Listpris)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB03	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG4_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB05, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB05 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG7_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC06, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC06 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FMR)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB05	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG7_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB07, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB07 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAGa_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC08, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC08 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.MREfterskott)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB07	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAGa_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB09, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB09 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAGd_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0A, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0A 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.CMRZ145)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB09	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAGd_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0B, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0B 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG10_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0C, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0C 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.NettoNetPris)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0B	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG10_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0D, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0D 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG13_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0E, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0E 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FURFakturaNet)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0D	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG13_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0F, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0F 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG16_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0G, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0G 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FURFaktListpr)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0F	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG16_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0H, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0H 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG19_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0I, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0I 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FUREfterskNet)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0H	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG19_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0J, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0J 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG1c_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0K, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0K 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.FUREfterskLP)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0J	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG1c_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0L, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0L 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG1f_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0M, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0M 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EstKvantrab)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0L	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG1f_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0N, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0N 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG22_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0O, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0O 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EstPallrabatt)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0N	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG22_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0P, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0P 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG25_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0Q, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0Q 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EstFraktkostn)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0P	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG25_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0R, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0R 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG28_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0S, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0S 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServPOSst)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0R	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG28_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB0T, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0T 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG2b_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0U, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0U 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.ServPlockSt)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0T	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG2b_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZNB0V, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	Calendar_Dt	DATE, 
	BasForPaslag	FLOAT)
primary index (Article_Seq_Num, Calendar_Dt) on commit preserve rows

;insert into ZZNB0V 
select	a11.Article_Seq_Num  Article_Seq_Num,
	a11.Calendar_Dt  Calendar_Dt,
	avg(a11.Base_Price_Amt)  BasForPaslag
from	ITSemCMNVOUT.ARTICLE_COST_PRICE_F	a11
	join	ITSemCMNVOUT.ARTICLE_D	a12
	  on 	(a11.Article_Seq_Num = a12.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a13
	  on 	(a12.Art_Hier_Lvl_8_Seq_Num = a13.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a14
	  on 	(a12.Art_Hier_Lvl_4_Seq_Num = a14.Art_Hier_Lvl_4_Seq_Num)
where	(a11.Calendar_Dt = DATE '2013-04-18'
 and a14.Art_Hier_Lvl_4_Id in ('140207'))
group by	a11.Article_Seq_Num,
	a11.Calendar_Dt

create volatile table ZZMB0W, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0W 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB0V	pc11
group by	pc11.Article_Seq_Num

create volatile table ZZNC0X, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0X 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.BasForPaslag)  WJXBFS1
from	ZZNB0V	pa11
	join	ZZMB0W	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
group by	pa11.Article_Seq_Num

create volatile table ZZMB0Y, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB0Y 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG31_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC0Z, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC0Z 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.TransfPris)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB0Y	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG31_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB10, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB10 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG34_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC11, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC11 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB10	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG34_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB12, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB12 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG37_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC13, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC13 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS1)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB12	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG37_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB14, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB14 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG3a_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC15, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC15 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS2)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB14	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG3a_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB16, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB16 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG3d_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC17, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC17 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPlock)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB16	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG3d_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB18, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB18 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG40_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC19, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC19 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.PVServPOS3)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB18	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG40_1 = 1
group by	pa11.Article_Seq_Num

create volatile table ZZMB1A, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	DATE)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZMB1A 
select	pc11.Article_Seq_Num  Article_Seq_Num,
	max(pc11.Calendar_Dt)  WJXBFS1
from	ZZNB00	pc11
where	pc11.GODWFLAG43_1 = 1
group by	pc11.Article_Seq_Num

create volatile table ZZNC1B, no fallback, no log(
	Article_Seq_Num	INTEGER, 
	WJXBFS1	FLOAT)
primary index (Article_Seq_Num) on commit preserve rows

;insert into ZZNC1B 
select	pa11.Article_Seq_Num  Article_Seq_Num,
	max(pa11.EMVFaktor)  WJXBFS1
from	ZZNB00	pa11
	join	ZZMB1A	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num and 
	pa11.Calendar_Dt = pa12.WJXBFS1)
where	pa11.GODWFLAG43_1 = 1
group by	pa11.Article_Seq_Num

select	a126.Art_Hier_Lvl_4_Id  Art_Hier_Lvl_4_Id,
	max(a126.Art_Hier_Lvl_4_Desc)  Art_Hier_Lvl_4_Desc,
	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num)  Article_Seq_Num,
	max(a124.Article_Id)  Article_Id,
	max(a124.Article_Desc)  Article_Desc,
	max(pa11.WJXBFS1)  Grundpris,
	max(pa12.WJXBFS1)  Listpris,
	max(pa13.WJXBFS1)  FMR,
	(ZEROIFNULL(max(pa14.WJXBFS1)) + ZEROIFNULL(max(pa15.WJXBFS1)))  CMR,
	max(pa16.WJXBFS1)  NettoNetPris,
	max(pa17.WJXBFS1)  FURFakturaNet,
	max(pa18.WJXBFS1)  FURFaktListpr,
	max(pa19.WJXBFS1)  FUREfterskNet,
	max(pa110.WJXBFS1)  FUREfterskLP,
	max(pa111.WJXBFS1)  EstKvantrab,
	max(pa112.WJXBFS1)  EstPallrabatt,
	max(pa113.WJXBFS1)  EstFraktkostn,
	max(pa114.WJXBFS1)  ServPOSst,
	max(pa115.WJXBFS1)  ServPlockSt,
	max(pa116.WJXBFS1)  BasForPaslag,
	max(pa117.WJXBFS1)  TransfPris,
	(ZEROIFNULL(max(pa118.WJXBFS1)) + ZEROIFNULL(max(pa119.WJXBFS1)))  EMK1st,
	((ZEROIFNULL(max(pa120.WJXBFS1)) + ZEROIFNULL(max(pa121.WJXBFS1))) + ZEROIFNULL(max(pa122.WJXBFS1)))  EMK2st,
	max(pa123.WJXBFS1)  EMVFaktor
from	ZZNC02	pa11
	full outer join	ZZNC04	pa12
	  on 	(pa11.Article_Seq_Num = pa12.Article_Seq_Num)
	full outer join	ZZNC06	pa13
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num) = pa13.Article_Seq_Num)
	full outer join	ZZNC08	pa14
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num) = pa14.Article_Seq_Num)
	full outer join	ZZNC0A	pa15
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num) = pa15.Article_Seq_Num)
	full outer join	ZZNC0C	pa16
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num) = pa16.Article_Seq_Num)
	full outer join	ZZNC0E	pa17
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num) = pa17.Article_Seq_Num)
	full outer join	ZZNC0G	pa18
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num) = pa18.Article_Seq_Num)
	full outer join	ZZNC0I	pa19
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num) = pa19.Article_Seq_Num)
	full outer join	ZZNC0K	pa110
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num) = pa110.Article_Seq_Num)
	full outer join	ZZNC0M	pa111
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num) = pa111.Article_Seq_Num)
	full outer join	ZZNC0O	pa112
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num) = pa112.Article_Seq_Num)
	full outer join	ZZNC0Q	pa113
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num) = pa113.Article_Seq_Num)
	full outer join	ZZNC0S	pa114
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num) = pa114.Article_Seq_Num)
	full outer join	ZZNC0U	pa115
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num) = pa115.Article_Seq_Num)
	full outer join	ZZNC0X	pa116
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num) = pa116.Article_Seq_Num)
	full outer join	ZZNC0Z	pa117
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num) = pa117.Article_Seq_Num)
	full outer join	ZZNC11	pa118
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num) = pa118.Article_Seq_Num)
	full outer join	ZZNC13	pa119
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num) = pa119.Article_Seq_Num)
	full outer join	ZZNC15	pa120
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num) = pa120.Article_Seq_Num)
	full outer join	ZZNC17	pa121
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num) = pa121.Article_Seq_Num)
	full outer join	ZZNC19	pa122
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num) = pa122.Article_Seq_Num)
	full outer join	ZZNC1B	pa123
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num) = pa123.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_D	a124
	  on 	(coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num) = a124.Article_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_8_D	a125
	  on 	(a124.Art_Hier_Lvl_8_Seq_Num = a125.Art_Hier_Lvl_8_Seq_Num)
	join	ITSemCMNVOUT.ARTICLE_HIERARCHY_LVL_4_D	a126
	  on 	(a124.Art_Hier_Lvl_4_Seq_Num = a126.Art_Hier_Lvl_4_Seq_Num)
group by	a126.Art_Hier_Lvl_4_Id,
	coalesce(pa11.Article_Seq_Num, pa12.Article_Seq_Num, pa13.Article_Seq_Num, pa14.Article_Seq_Num, pa15.Article_Seq_Num, pa16.Article_Seq_Num, pa17.Article_Seq_Num, pa18.Article_Seq_Num, pa19.Article_Seq_Num, pa110.Article_Seq_Num, pa111.Article_Seq_Num, pa112.Article_Seq_Num, pa113.Article_Seq_Num, pa114.Article_Seq_Num, pa115.Article_Seq_Num, pa116.Article_Seq_Num, pa117.Article_Seq_Num, pa118.Article_Seq_Num, pa119.Article_Seq_Num, pa120.Article_Seq_Num, pa121.Article_Seq_Num, pa122.Article_Seq_Num, pa123.Article_Seq_Num)


SET QUERY_BAND = NONE For Session;


drop table ZZNB00

drop table ZZMB01

drop table ZZNC02

drop table ZZMB03

drop table ZZNC04

drop table ZZMB05

drop table ZZNC06

drop table ZZMB07

drop table ZZNC08

drop table ZZMB09

drop table ZZNC0A

drop table ZZMB0B

drop table ZZNC0C

drop table ZZMB0D

drop table ZZNC0E

drop table ZZMB0F

drop table ZZNC0G

drop table ZZMB0H

drop table ZZNC0I

drop table ZZMB0J

drop table ZZNC0K

drop table ZZMB0L

drop table ZZNC0M

drop table ZZMB0N

drop table ZZNC0O

drop table ZZMB0P

drop table ZZNC0Q

drop table ZZMB0R

drop table ZZNC0S

drop table ZZMB0T

drop table ZZNC0U

drop table ZZNB0V

drop table ZZMB0W

drop table ZZNC0X

drop table ZZMB0Y

drop table ZZNC0Z

drop table ZZMB10

drop table ZZNC11

drop table ZZMB12

drop table ZZNC13

drop table ZZMB14

drop table ZZNC15

drop table ZZMB16

drop table ZZNC17

drop table ZZMB18

drop table ZZNC19

drop table ZZMB1A

drop table ZZNC1B
