<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="/jsp/app_error.jsp"%>
<%@ page import="com.teradata.modelmanager.util.HTMLUtil" %>
<%@ page import="com.teradata.modelmanager.common.Messages" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%@ page import="com.teradata.modelmanager.util.DisplayTagUtil" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="disp" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%!
	private static final String LIST_TABLE_ID = "mmListTable";
%>
<%
	String listQueryStr = (String)session.getAttribute(MMAttr.MODEL_LIST_QUERY);
	listQueryStr = (listQueryStr == null ? "" : listQueryStr);
	
	String mmCtx = request.getContextPath();
	String appTitle = Messages.APP_TITLE;
	String pageTitle = Messages.MODELLIST_TITLE;
	String msg = (String)request.getAttribute(MMAttr.MSG);
	String [][] menuLinks = new String [][] {
	        {Messages.MAIN_TITLE, mmCtx + "/main.do"},
	        {Messages.JOBLIST_TITLE, mmCtx + "/joblist.do"},
	};
	int linkIdx;
	
	DisplayTagUtil tagUtil = new DisplayTagUtil(request, LIST_TABLE_ID);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
		  "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%= mmCtx %>/style/mmCommon.css"/>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmCommon.js"></script>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmModelList.js"></script>
	<title><%= pageTitle %></title>
</head>
<body>
	<%@include file="common/titleBar.snippet" %>
	
	<p/>
	
	<div style="text-align:center">
	
	<form name="selectForm"
		  method="post"
		  action="<%= mmCtx %>/modellist.do<%= listQueryStr %>"
		  onSubmit="return mmSubmitModelList(this, '<%= mmCtx %>', 'msgSelectOne', 'msgDelConfirm')">
		  
	<disp:table name="mylist" id="<%= LIST_TABLE_ID %>" class="listrender" pagesize="15" sort="list" defaultsort="3" requestURI="/modellist.do">
		<disp:column property="select" title="<%= tagUtil.getFirstColumnTitle(\"&nbsp;\") %>" style="width:25px;text-align:center"></disp:column>
		<disp:column property="modelid" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELLIST_ID) %>" href="modelproperties.do" paramId="m" style="width:50px;text-align:center"></disp:column>
		<disp:column property="name" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELLIST_NAME) %>" maxLength="15" style="width:150px;text-align:left"></disp:column>
		<disp:column property="description" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELLIST_DESC) %>" maxLength="15" style="width:150px;text-align:left"></disp:column>
		<disp:column property="createdby" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELLIST_USER) %>" maxLength="15" style="width:100px;text-align:left"></disp:column>
		<disp:column property="version" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELLIST_VERSION) %>" style="width:75px;text-align:center"></disp:column>
		<disp:column property="datecreated" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELLIST_PUBLISHED) %>" style="width:100px;text-align:center" decorator="com.teradata.modelmanager.display.DateDecorator"></disp:column>
		<disp:column property="expirationdate" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELLIST_EXPIRE) %>" style="width:100px;text-align:center" decorator="com.teradata.modelmanager.display.DateDecorator"></disp:column>
		<disp:column property="hasads" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELLIST_ADS) %>" style="width:50px;text-align:center"></disp:column>
		<disp:column property="hasscore" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELLIST_SCORE) %>" style="width:50px;text-align:center"></disp:column>
		<disp:column property="lastexecuted" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELLIST_LAST_EXECUTED) %>" maxLength="20" style="width:200px;text-align:left"></disp:column>
	</disp:table>
	
	<disp:table name="joblist" id="hiddenjoblist" style="display:none" pagesize="500" sort="list" requestURI="/modellist.do">
		<disp:column property="select" title="<%= tagUtil.getFirstColumnTitle(\"<input type='checkbox' onclick='mmToggleCheckbox(this, jobForm.mmJob)'/>\") %>" style="width:25px;text-align:center"></disp:column>
		<disp:column property="name" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_JOB) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="publishDB" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PUBLISH_DB) %>" sortable="true" style="width:100px;text-align:center"></disp:column>
		<disp:column property="modelID" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_MODEL) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="refreshADS" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_ADS) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
		<disp:column property="scoreSQL" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_SCORE) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
		<disp:column property="description" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_DESCRIPTION) %>" sortable="true" style="width:200px;text-align:left"></disp:column>
		<disp:column property="dependency" title="Depends On" sortable="true" style="width:200px;text-align:left"></disp:column>
		<disp:column property="previousFireTime" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PREVIOUS) %>" sortable="true" style="width:150px;text-align:center" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
		<disp:column property="nextFireTime" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_NEXT) %>" sortable="true" style="width:150px;text-align:center" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
	</disp:table>
	
	<disp:table name="dependencylist" id="dependencylist" style="display:none" pagesize="500" sort="list" requestURI="joblist.do">
		<disp:column property="modelID" title="<%= tagUtil.getFirstColumnTitle(\"<input type='checkbox' onclick='mmToggleCheckbox(this, jobForm.mmJob)'/>\") %>" style="width:25px;text-align:center"></disp:column>
		<disp:column property="appID" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_JOB) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="appType" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_PUBLISH_DB) %>" sortable="true" style="width:100px;text-align:center"></disp:column>
		<disp:column property="retryDelay" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_MODEL) %>" sortable="true" style="width:50px;text-align:center"></disp:column>
		<disp:column property="retryNum" title="<%= tagUtil.getColumnTitle(Messages.JOBLIST_ADS) %>" sortable="true" style="width:75px;text-align:center"></disp:column>
	</disp:table>
	
	<script type="text/javascript" language="JavaScript">
	// sets all anchor tags under TH block to mmTH class
	if (document.getElementById('<%= LIST_TABLE_ID %>')) {
		mmSetTHAnchorStyle(document.getElementById('<%= LIST_TABLE_ID %>').getElementsByTagName('thead')[0]);
	}
	</script>
	
	<br>
	<br>
	<table border=0 cellspacing=0 cellpadding=0>
	<tr><td>
		<div id="action-shadow"><div id="action-content">
		<input class="mmAction" type="submit" value="<%= Messages.MODELLIST_VIEW %>" onClick="this.form.a.value='View'"/>
		<input class="mmAction" type="submit" value="<%= Messages.MODELLIST_DELETE %>" onClick="this.form.a.value='Delete'"/>
		<input class="mmAction" type="button" value="<%= Messages.MODELLIST_SCHEDULER %>" onClick="mmShowSchedulerByList('<%= LIST_TABLE_ID %>', this.form, 'msgSelectOne')"/>
		<input class="mmAction" type="submit" value="<%= Messages.MODELLIST_REFRESH %>" onClick="this.form.a.value='Refresh'"/>
		</div></div>
	</td></tr>
	</table>
	
	<input type="hidden" name="a" value=""/>
	
	</form>
	
	</div>
	
	<div class="mmDialog" id="msgSelectOne"><%= Messages.MODELLIST_MSG_SEL_ONE %></div>
	<div class="mmDialog" id="msgDelConfirm"><%= Messages.MODELLIST_MSG_DEL_CONFIRM %></div>
	
	<%@include file="popup/scheduler.snippet" %>
</body>
</html>