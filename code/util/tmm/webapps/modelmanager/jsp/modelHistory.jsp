<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="/jsp/app_error.jsp"%>
<%@ page import="com.teradata.modelmanager.common.Messages" %>
<%@ page import="com.teradata.modelmanager.common.MMConstants.MMAttr" %>
<%@ page import="com.teradata.modelmanager.util.DisplayTagUtil" %>
<%@ page import="com.teradata.modelmanager.util.HTMLUtil" %>
<%@ page import="com.teradata.modelmanager.vo.ModelProperties" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="disp" %>
<%!
/*
 * Copyright © 1999-2008 by Teradata Corporation. 
 * All Rights Reserved. 
 * TERADATA CONFIDENTIAL AND TRADE SECRET
 */
%>
<%!
	private static final String ADS_TABLE_ID = "mmADSHist";
	private static final String SCORE_TABLE_ID = "mmScoreHist";
%>
<%
	ModelProperties model = (ModelProperties)session.getAttribute(MMAttr.MODEL_PROP);
	String listQueryStr = (String)session.getAttribute(MMAttr.MODEL_LIST_QUERY);
	listQueryStr = (listQueryStr == null ? "" : listQueryStr);
	
	String mmCtx = request.getContextPath();
	String appTitle = Messages.APP_TITLE;
	String pageTitle = HTMLUtil.escapeHTML(Messages.MODELHIST_TITLE + " - "
										   + model.getName() + " ("
									       + model.getId() + ")"
										   );
	String msg = (String)request.getAttribute(MMAttr.MSG);
	String [][] menuLinks = new String [][] {
	        {Messages.MAIN_TITLE, mmCtx + "/main.do"},
	        {Messages.MODELLIST_TITLE, mmCtx + "/modellist.do" + listQueryStr},
	        {Messages.MODELPROP_TITLE, mmCtx + "/modelproperties.do?m=" + model.getId()},
	};
	int linkIdx;
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
		  "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%= mmCtx %>/style/mmCommon.css"/>
	<style type="text/css">
		table.listrender { margin-left:0; }
		td.section { font-size:12pt; font-weight:bold; text-align:left; }
		td.history { text-align:center; }
	</style>
	<script type="text/javascript" language="JavaScript" src="<%= mmCtx %>/script/mmCommon.js"></script>
	<title><%= pageTitle %></title>
</head>
<body>
	<%@include file="common/titleBar.snippet" %>
	
	<p/>
	
	<div style="text-align:center">
	
	<table border=0 cellspacing=5 cellpadding=0>
<%
	if (model.getAds() != null) {
	    DisplayTagUtil tagUtil = new DisplayTagUtil(request, ADS_TABLE_ID);
%>
	<tr><td class="section"><%= Messages.MODELHIST_ADS_HISTORY %>
	</td></tr>
	<tr><td class="history">
	
	<disp:table name="adsHistory" id="<%= ADS_TABLE_ID %>" class="listrender" pagesize="5" sort="list" requestURI="/modelhistory.do">
		<disp:column property="lastruntime" sortable="true" title="<%= tagUtil.getFirstColumnTitle(Messages.MODELHIST_LAST_REFRESH) %>" style="width:175px;text-align:left" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
		<disp:column property="dbname" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELHIST_DATABASE) %>" style="width:125px;text-align:center"></disp:column>
		<disp:column property="tablename" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELHIST_TABLE) %>" style="width:125px;text-align:center"></disp:column>
		<disp:column property="targetdate" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELHIST_TARGET_DATE) %>" style="width:175px;text-align:center" decorator="com.teradata.modelmanager.display.DateDecorator"></disp:column>
		<disp:column property="output" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELHIST_RESULT) %>" style="width:200px;text-align:left"></disp:column>
	</disp:table>
	
	<script type="text/javascript" language="JavaScript">
	// sets all anchor tags under TH block to mmTH class
	if (document.getElementById('<%= ADS_TABLE_ID %>')) {
		mmSetTHAnchorStyle(document.getElementById('<%= ADS_TABLE_ID %>').getElementsByTagName('thead')[0]);
	}
	</script>
	
	</td></tr>
<%	} %>
	<tr><td>&nbsp;</td></tr>
<%
	if (model.getScore() != null) {
	    DisplayTagUtil tagUtil = new DisplayTagUtil(request, SCORE_TABLE_ID);
%>
	<tr><td class="section"><%= Messages.MODELHIST_SCORE_HISTORY %>
	</td></tr>
	<tr><td class="history">
	
	<disp:table name="scoreHistory" id="<%= SCORE_TABLE_ID %>" class="listrender" pagesize="5" sort="list" requestURI="/modelhistory.do">
		<disp:column property="lastruntime" sortable="true" title="<%= tagUtil.getFirstColumnTitle(Messages.MODELHIST_LAST_RUN) %>" style="width:175px;text-align:left" decorator="com.teradata.modelmanager.display.TimestampDecorator"></disp:column>
		<disp:column property="outputdb" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELHIST_OUT_DATABASE) %>" style="width:125px;text-align:center"></disp:column>
		<disp:column property="outputtable" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELHIST_OUT_TABLE) %>" style="width:125px;text-align:center"></disp:column>
		<disp:column property="inputdb" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELHIST_IN_DATABASE) %>" style="width:125px;text-align:center"></disp:column>
		<disp:column property="inputtable" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELHIST_IN_TABLE) %>" style="width:125px;text-align:center"></disp:column>
		<disp:column property="output" sortable="true" title="<%= tagUtil.getColumnTitle(Messages.MODELHIST_RESULT) %>" style="width:200px;text-align:left"></disp:column>
	</disp:table>
	
	<script type="text/javascript" language="JavaScript">
	// sets all anchor tags under TH block to mmTH class
	if (document.getElementById('<%= SCORE_TABLE_ID %>')) {
		mmSetTHAnchorStyle(document.getElementById('<%= SCORE_TABLE_ID %>').getElementsByTagName('thead')[0]);
	}
	</script>
	
	</td></tr>
<%	} %>
	</table>
	
	</div>
	
</body>
</html>