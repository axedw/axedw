/* This JavaScript is used by the userSettings.snippet JSP page.  Much of the
 * functions in this script are taylored specifically to work with the user
 * management feature of the application.  Watch out for hardcodings of
 * HTML form names and input fields in this script when modifying it.
 */

function mmHideUserSettingsOnEsc(e)
{
	// alert('which: ' + e.which + ' charCode: ' + e.charCode + ' keyCode: ' + e.keyCode);
	var code = (window.event ? window.event.keyCode : e.keyCode);
	if (code == 27) {
		mmHideDiv('divUserOuterPane');
		document.onkeypress = '';
	}
} // mmHideUserSettingsOnEsc

var userReq, listReq;

function mmHandleUserProfile()
{
	var prop, formObj;
	
	if (userReq.readyState == 4 && userReq.status == 200) {
		//alert(userReq.responseText);
		formObj = document.userForm;
		
		// reset user role checkboxes
		if (formObj.mmRole && formObj.mmRole[0]) {
			for (var j = 0; j < formObj.mmRole.length; j++) {
				formObj.mmRole[j].checked = false;
			}
		}
		
		// fill in the form
		items = userReq.responseText.split('\n');
		for (var i = 0; i < items.length; i++) {
			items[i] = trim(items[i]);
			prop = items[i].split('=');
			if (prop[0] == 'id') {
				// do nothing
			} else if (prop[0] == 'email') {
				formObj.mmEmail.value = prop[1];
			} else if (prop[0] == 'role' && formObj.mmRole) {
				if (formObj.mmRole[0]) {
					for (var j = 0; j < formObj.mmRole.length; j++) {
						if (formObj.mmRole[j].value == prop[1]) {
							formObj.mmRole[j].checked = true;
						}
					}
				}
			}
		}
		
		if (document.userForm.mmUserList) {
			listReq = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
			listReq.open('GET', 'users.do', true);
			listReq.onreadystatechange = mmHandleUserList;
			listReq.setRequestHeader('Content-Type', 'text/plain;charset=UTF-8');
			listReq.send(null);
		} else {
			document.getElementById('mmLoading').style.display = 'none';
		}
	}
} // mmHandleUserProfile

function mmHandleUserList()
{
	var prop, formObj;
	var items, optionList;
	
	if (listReq.readyState == 4 && listReq.status == 200) {
		//alert(listReq.responseText);
		formObj = document.userForm;
		formObj.mmUserList.options.length = 0;
		
		// fill in the user list
		items = listReq.responseText.split('\n');
		for (var i = 0; i < items.length; i++) {
			items[i] = trim(items[i]);
			if (items[i] != '') {
				optionList = formObj.mmUserList.options;
				if (items[i] == formObj.mmUserID.value) {
					optionList[optionList.length] = new Option(items[i], items[i], false, true);
				} else {
					optionList[optionList.length] = new Option(items[i], items[i], false, false);
				}
			}
		}
		document.getElementById('mmLoading').style.display = 'none';
	}
} // mmHandleUserList

function mmShowUserSettings()
{
	mmClearSettings(); // reset field values
	
	mmShowDiv('divUserOuterPane');
	
	document.getElementById('mmShowPass').style.display = '';
	document.getElementById('mmPass1').style.display = '';
	document.getElementById('mmPass2').style.display = '';
	if (document.userForm.mmUserList) { // if admin access
		mmShowDiv('mmDivSelName'); // show user ID list
		mmHideDiv('mmDivNewName'); // hide new user entry box
		mmShowSpan('mmDeleteAction'); // show delete action button
	}
	
	var shield = document.getElementById('divUserOuterPane').lastChild;
	var scrollHeight = document.documentElement.scrollHeight;
	var docHeight = document.documentElement.clientHeight;
	while (shield != null && shield.id != 'mmShield') {
		shield = shield.previousSibling;
	}
	mmSetElementStyle(shield, 'height:' + (scrollHeight > docHeight ? scrollHeight : docHeight) + 'px');
	
	var innerPane = document.getElementById('divUserInnerPane');
	var scrollOffset = document.documentElement.scrollTop;
	var viewPort = document.documentElement.clientHeight;
	var divHeight = innerPane.offsetHeight;
	mmSetElementStyle(innerPane, 'top:' + (scrollOffset + (viewPort / 2) - (divHeight / 2)) + 'px');
	
	document.getElementById('mmPass1').style.display = 'none'; // hide password entry box
	document.getElementById('mmPass2').style.display = 'none'; // hide password entry box
	document.userForm.mmEmail.focus();
	
	document.onkeypress = mmHideUserSettingsOnEsc;
	
	mmRefreshUserProfile();
} // mmShowUserSettings

function mmRefreshUserProfile()
{
	document.getElementById('mmLoading').style.display = '';
	
	userReq = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	userReq.open('POST', 'users.do?mmUserID=1', true);
	userReq.onreadystatechange = mmHandleUserProfile;
	userReq.setRequestHeader('Content-Type', 'text/plain;charset=UTF-8');
	userReq.send(document.userForm.mmUserID.value);
} // mmRefreshUserProfile

function mmChangePassword()
{
	document.getElementById('mmShowPass').style.display = 'none';
	document.getElementById('mmPass1').style.display = ''; // this should be table-row, but IE6 does not support it
	document.getElementById('mmPass2').style.display = ''; // this should be table-row, but IE6 does not support it
	document.userForm.mmPassword.value = '';
	document.userForm.mmPassword.focus();
	document.userForm.mmPasswordC.value = '';
} // mmChangePassword

function mmAddNewUser()
{
	mmChangePassword();
	mmHideDiv('mmDivSelName');
	mmShowDiv('mmDivNewName');
	mmHideSpan('mmDeleteAction');
	mmClearSettings();
	document.userForm.newUser.value = '1';
	document.userForm.mmNewUserID.focus();
} // mmAddNewUser

function mmDeleteUser(formObj, confirmDelete)
{
	if (confirm(document.getElementById(confirmDelete).innerHTML)) {
		formObj.a.value = 'Delete';
		formObj.submit();
	}
} // mmDeleteUser

function mmClearSettings()
{
	document.userForm.mmPassword.value = '';
	document.userForm.mmPasswordC.value = '';
	document.userForm.mmEmail.value = '';
	document.userForm.newUser.value = '';
	if (document.userForm.mmUserList) {
		document.userForm.mmNewUserID.value = '';
		document.userForm.mmRole[0].checked = false;
		document.userForm.mmRole[1].checked = true;
	}
} // mmClearSettings

function mmValidateUser(formObj, blankID, blankPasswd, mismatch, invalidEmail)
{
	var isNewUser = (formObj.newUser.value == '1');
	
	if (isNewUser) {
		if (trim(formObj.mmNewUserID.value) == '') {
			alert(document.getElementById(blankID).innerHTML);
			formObj.mmNewUserID.focus();
			return false;
		} else if (trim(formObj.mmPassword.value) == '' && trim(formObj.mmPassword.value) == '') {
			alert(document.getElementById(blankPasswd).innerHTML);
			formObj.mmPassword.focus();
			return false;
		}
	}
	if (trim(formObj.mmPassword.value) != '' || trim(formObj.mmPassword.value) != '') {
		if (formObj.mmPassword.value != formObj.mmPasswordC.value) {
			alert(document.getElementById(mismatch).innerHTML);
			formObj.mmPassword.focus();
			return false;
		}
	} else if (trim(formObj.mmEmail.value) != '' && !mmValidateEmail(formObj.mmEmail.value)) {
		alert(document.getElementById(invalidEmail).innerHTML);
		formObj.mmEmail.focus();
		return false;
	}
	return true;
} // mmValidateUser
