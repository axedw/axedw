@ECHO OFF
REM ----------------------------------------------------------------------------
REM SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
REM ----------------------------------------------------------------------------
REM ID               : $Id  $
REM Last Changed By  : $Author  $
REM Last Change Date : $Date  $
REM Last Revision    : $Revision  $
REM Subversion URL   : $HeadURL  $
REM ----------------------------------------------------------------------------
REM SVN Info END
REM ----------------------------------------------------------------------------

SETLOCAL ENABLEDELAYEDEXPANSION

SET "v_utilpath=%~d0%~p0"
SET "v_infile=%1"
SET "v_listpath=%~d1%~p1"
SET "v_inname=%~n1"
SET "v_option1=%2"
SET "v_option2=%3"

SET "v_dbname=%v_option1%"
SET "v_btqmode=BTET"
SET "v_nosplit="

IF /I "%v_option1%"=="NOSPLIT" (
	SET "v_dbname=%v_option2%"
	SET "v_nosplit=NOSPLIT"
) ELSE IF /I "%v_option2%"=="NOSPLIT" (
	SET "v_nosplit=NOSPLIT"
)
IF /I "%v_option1%"=="BTET" (
	SET "v_dbname=%v_option2%"
	SET "v_btqmode=BTET"
) ELSE IF /I "%v_option1%"=="ANSI" (
	SET "v_dbname=%v_option2%"
	SET "v_btqmode=ANSI"
) ELSE IF /I "%v_option2%"=="ANSI" (
	SET "v_btqmode=ANSI"
)

FOR /F "tokens=1* delims=|" %%a IN ("%v_utilpath:\util\=|%") DO SET "v_croot=%%a"

rem SET "v_headerfile=%v_croot%\util\scm\template\svn_header_sql.txt"
SET "v_outpath=%v_listpath%table\"
SET "v_ext=.btq"
SET "c_SED=%v_utilpath%sed.exe"

SET "v_date=%DATE:-=%"
SET "v_time=%TIME::=%"
SET "v_time=!v_time: =0!"
SET "v_ver=%v_date:~2%%v_time:~0,6%"

SET "v_neq=<>"
SET "v_bb=("
SET "v_be=)"
SET "v_bang=^!"
SET "v_defno=0"
SET "v_dollar=$"
SET v_quote="

SET "v_reptxt1=REFERENCES WITH NO CHECK OPTION"
SET "v_dbnameprefix=${DB_ENV}"
SET "v_dbnamepostfix=T"
SET "v_SVN_Id=!v_dollar!Id:  !v_dollar!"
SET "v_SVN_Author=!v_dollar!Author:  !v_dollar!"
SET "v_SVN_Date=!v_dollar!Date:  !v_dollar!"
SET "v_SVN_Revision=!v_dollar!Revision:  !v_dollar!"
SET "v_SVN_HeadURL=!v_dollar!HeadURL:  !v_dollar!"

SET v_blnklineind=0
SET "v_db="
SET "v_obj="
SET "v_objpath="
SET "v_basename="
SET "v_newobj="
SET "v_line="
SET "v_line0="
SET "v_line1="
SET "v_line2="
SET "v_row="
SET "v_tablekind="
SET "v_ET="
SET "v_ETDTname="
SET "v_ETDTdb="
SET "v_ETDTtab="

IF /I "%v_infile%"=="" EXIT /B 999

FOR /D %%k IN (%v_listpath:~0,-1%) DO SET "v_basename=%%~nk"
IF /I "%v_dbname%"=="" SET "v_dbname=%v_dbnameprefix%%v_basename%%v_dbnamepostfix%"

SET "v_objpath=code/%v_outpath:*\code\=%"
SET "v_objpath=%v_objpath:\=/%"

IF NOT EXIST "%v_outpath%" (
	IF /I "%v_nosplit%" NEQ "NOSPLIT" MKDIR "%v_outpath%">NUL
)

IF EXIST %v_listpath%_runorder.%v_inname%.txt REN %v_listpath%_runorder.%v_inname%.txt _runorder.%v_inname%.txt.!v_ver!

FOR /F "tokens=* delims=" %%x IN ('FIND /v /n "" %v_infile%') DO (
	SET "v_line=%%x"
	SET "v_line=!v_line:*]=!"
	SET "v_line0=!v_line!"
	IF /I "!v_line!"=="" (
		SET /A v_blnklineind+=1
	) ELSE SET v_blnklineind=0
	IF !v_blnklineind! LEQ 1 (
		FOR /F "tokens=1,2,3,4,5,6*" %%a IN ("!v_line0!") DO (
			IF /I "%%a"=="CREATE" (
rem echo !v_line0!
				SET "v_newobj="
				SET "v_line1="
				SET "v_line2="
				SET "v_ETDTname="
				SET "v_ETDTdb="
				SET "v_ETDTtab="
				IF /I "%%e"=="TABLE" (
					SET "v_ET=0"
					SET "v_newobj=%%f"
					SET "v_line1=%%a %%b %%c %%d %%e"
					SET "v_line2=%%g"
				)
				IF /I "%%d"=="TABLE" (
					SET "v_ET=0"
					SET "v_newobj=%%e"
					SET "v_line1=%%a %%b %%c %%d"
					SET "v_line2=%%f %%g"
					IF /I "%%g"=="" SET "v_line2=%%f"
				)
				IF /I "%%c"=="TABLE" (
					SET "v_ET=0"
					IF /I "%%b"=="ERROR" (
						SET "v_ET=1"
						IF /I "%%e"=="FOR" (
							SET "v_ETDTname=%%f"
							SET "v_newobj=%%d"
						) ELSE IF /I "%%d"=="FOR" (
							SET "v_ETDTname=%%e"
						)
						SET "v_ETDTname=!v_ETDTname:;=!"
						FOR /F "tokens=1* delims=." %%k IN ("!v_ETDTname!") DO (
							IF /I "%%l"=="" (
								SET "v_ETDTtab=!%%k!"
								SET "v_ETDTdb=!v_dbname!"
							) ELSE (
								SET "v_ETDTdb=%%k"
								SET "v_ETDTtab=%%l"
							)
						)
						IF /I "!v_newobj!"=="" (
							SET "v_newobj=!v_ETDTdb!.ET_!v_ETDTtab:~0,27!
						)
						SET "v_line1=CREATE ERROR TABLE"
						SET "v_line2=FOR !v_ETDTdb!.!v_ETDTtab!;"
					) ELSE (
						SET "v_newobj=%%d"
						SET "v_line1=%%a %%b %%c"
						SET "v_line2=%%e %%f %%g"
						IF /I "%%g"=="" SET "v_line2=%%e %%f"
						IF /I "%%f"=="" SET "v_line2=%%e"
					)
				)
				IF /I "%%b"=="TABLE" (
					SET "v_ET=0"
					SET "v_newobj=%%c"
					SET "v_line1=%%a %%b"
					SET "v_line2=%%d %%e %%f %%g"
					IF /I "%%g"=="" SET "v_line2=%%d %%e %%f"
					IF /I "%%f"=="" SET "v_line2=%%d %%e"
					IF /I "%%e"=="" SET "v_line2=%%d"
				)
				IF /I "!v_newobj!" NEQ "" (
					IF /I "!v_obj!" NEQ "" (
						IF /I "%v_nosplit%" NEQ "NOSPLIT" (
							ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
							IF /I "%v_btqmode%"=="ANSI" (ECHO.COMMIT;>>%v_outpath%\!v_obj!!v_ext!)&ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
							ECHO.>>%v_outpath%\!v_obj!!v_ext!
							IF /I "!v_tablekind!"=="T" (
								IF /I "%v_btqmode%"=="ANSI" (
									ECHO.CALL !v_dbnameprefix!dbadmin.DBA_NewTabDef_ANSI!v_bb!'!v_db!','!v_obj!','POST',NULL,1!v_be!>>%v_outpath%\!v_obj!!v_ext!
									ECHO.;>>%v_outpath%\!v_obj!!v_ext!
									ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
									ECHO.COMMIT;>>%v_outpath%\!v_obj!!v_ext!
									ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
								) ELSE (
									ECHO.CALL !v_dbnameprefix!dbadmin.DBA_NewTabDef!v_bb!'!v_db!','!v_obj!','POST',NULL,1!v_be!>>%v_outpath%\!v_obj!!v_ext!
									ECHO.;>>%v_outpath%\!v_obj!!v_ext!
									ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
									ECHO.CALL !v_dbnameprefix!dbadmin.DBA_CreLoadReady!v_bb!'!v_db!','!v_obj!','!v_dbnameprefix!CntlLoadReadyT',NULL,'D',1!v_be!>>%v_outpath%\!v_obj!!v_ext!
									ECHO.;>>%v_outpath%\!v_obj!!v_ext!
									ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
								)
								ECHO.>>%v_outpath%\!v_obj!!v_ext!
								ECHO.COMMENT ON !v_db!.!v_obj! IS '!v_SVN_Revision! - !v_SVN_Date! '>>%v_outpath%\!v_obj!!v_ext!
								ECHO.;>>%v_outpath%\!v_obj!!v_ext!
								ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
								IF /I "%v_btqmode%"=="ANSI" (ECHO.COMMIT;>>%v_outpath%\!v_obj!!v_ext!)&ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
								ECHO.>>%v_outpath%\!v_obj!!v_ext!
							)
							ECHO.>>%v_outpath%\!v_obj!!v_ext!
						)
					)
					FOR /F "tokens=1* delims=." %%k IN ("!v_newobj!") DO (
						IF /I "%%l"=="" (
							SET "v_obj=!v_newobj:,=!"
							SET "v_db=!v_dbname!"
						) ELSE (
							SET "v_db=%%k"
							SET "v_obj=%%l"
							SET "v_obj=!v_obj:,=!"
						)
					)
					IF /I "!v_ET!"=="1" (
						SET "v_tablekind=ET"
					) ELSE (
						SET "v_tablekind=T"
					)
					
					IF /I "%v_btqmode%"=="ANSI" (
						ECHO.BTQRUN_ANSI	%v_objpath%!v_obj!!v_ext!>>%v_listpath%_runorder.%v_inname%.txt
					) ELSE ECHO.BTQRUN	%v_objpath%!v_obj!!v_ext!>>%v_listpath%_runorder.%v_inname%.txt
					
					SET /A "v_defno+=1"
					SET "v_line=!v_line1! !v_db!.!v_obj! !v_line2!"
					IF /I "!v_line2!"=="" SET "v_line=!v_line1! !v_db!.!v_obj!"
					
					IF /I "%v_nosplit%" NEQ "NOSPLIT" (
						IF EXIST %v_outpath%\!v_obj!!v_ext! REN %v_outpath%\!v_obj!!v_ext! !v_obj!._!v_ver!>NUL
						ECHO./*>%v_outpath%\!v_obj!!v_ext!
						ECHO.# ---------------------------------------------------------------------------->>%v_outpath%\!v_obj!!v_ext!
						ECHO.# SVN Infostamp             !v_bb!DO NOT EDIT THE NEXT 9 LINES!v_bang!!v_be!>>%v_outpath%\!v_obj!!v_ext!
						ECHO.# ---------------------------------------------------------------------------->>%v_outpath%\!v_obj!!v_ext!
						ECHO.# ID               : !v_SVN_Id!>>%v_outpath%\!v_obj!!v_ext!
						ECHO.# Last Changed By  : !v_SVN_Author!>>%v_outpath%\!v_obj!!v_ext!
						ECHO.# Last Change Date : !v_SVN_Date!>>%v_outpath%\!v_obj!!v_ext!
						ECHO.# Last Revision    : !v_SVN_Revision!>>%v_outpath%\!v_obj!!v_ext!
						ECHO.# Subversion URL   : !v_SVN_HeadURL!>>%v_outpath%\!v_obj!!v_ext!
						ECHO.# -------------------------------------------------------------------------->>%v_outpath%\!v_obj!!v_ext!
						ECHO.# SVN Info END>>%v_outpath%\!v_obj!!v_ext!
						ECHO.# -------------------------------------------------------------------------->>%v_outpath%\!v_obj!!v_ext!
						ECHO.*/>>%v_outpath%\!v_obj!!v_ext!
						
	rem					COPY /Y %v_headerfile% %v_outpath%\!v_obj!!v_ext!>NUL
						ECHO..SET MAXERROR 0;>>%v_outpath%\!v_obj!!v_ext!
						ECHO.>>%v_outpath%\!v_obj!!v_ext!
						ECHO.DATABASE !v_db!;>>%v_outpath%\!v_obj!!v_ext!
	rem					ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
						IF /I "%v_btqmode%"=="ANSI" (ECHO.COMMIT;>>%v_outpath%\!v_obj!!v_ext!)&ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
						ECHO.>>%v_outpath%\!v_obj!!v_ext!
						
						IF /I "%v_btqmode%"=="ANSI" (
							IF /I "!v_tablekind!"=="T" (
								ECHO.CALL !v_dbnameprefix!dbadmin.DBA_NewTabDef_ANSI!v_bb!'!v_db!','!v_obj!','PRE',NULL,1!v_be!>>%v_outpath%\!v_obj!!v_ext!
							) ELSE IF /I "!v_tablekind!"=="ET" (
								ECHO.CALL !v_dbnameprefix!dbadmin.DBA_NewTabDef_ANSI!v_bb!'!v_ETDTdb!','!v_ETDTtab!','PREET',NULL,1!v_be!>>%v_outpath%\!v_obj!!v_ext!
							)
							ECHO.;>>%v_outpath%\!v_obj!!v_ext!
							ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
							ECHO.COMMIT;>>%v_outpath%\!v_obj!!v_ext!
							ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
						) ELSE (
							IF /I "!v_tablekind!"=="T" (
								ECHO.CALL !v_dbnameprefix!dbadmin.DBA_NewTabDef!v_bb!'!v_db!','!v_obj!','PRE',NULL,1!v_be!>>%v_outpath%\!v_obj!!v_ext!
							) ELSE IF /I "!v_tablekind!"=="ET" (
								ECHO.CALL !v_dbnameprefix!dbadmin.DBA_NewTabDef!v_bb!'!v_ETDTdb!','!v_ETDTtab!','PREET',NULL,1!v_be!>>%v_outpath%\!v_obj!!v_ext!
							)
							ECHO.;>>%v_outpath%\!v_obj!!v_ext!
							ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
						)
						ECHO.>>%v_outpath%\!v_obj!!v_ext!
					)
				)
			)
		)
		IF /I "!v_obj!" NEQ "" (
			IF /I "%v_nosplit%" NEQ "NOSPLIT" (
				IF /I "!v_line!" NEQ "!v_line:REFERENCES WITH NO CHECK OPTION=!" (
					ECHO.!v_line!|%c_SED% -e "s/\REFERENCES WITH NO CHECK OPTION /REFERENCES WITH NO CHECK OPTION !v_db!./g">>%v_outpath%\!v_obj!!v_ext!
				) ELSE (
					ECHO.!v_line!>>%v_outpath%\!v_obj!!v_ext!
				)
			)
		)
	)
)
IF /I "!v_obj!" NEQ "" (
	IF /I "%v_nosplit%" NEQ "NOSPLIT" (
		IF !v_blnklineind!==0 ECHO.>>%v_outpath%\!v_obj!!v_ext!
		ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
		IF /I "%v_btqmode%"=="ANSI" (ECHO.COMMIT;>>%v_outpath%\!v_obj!!v_ext!)&ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
		ECHO.>>%v_outpath%\!v_obj!!v_ext!
		IF /I "!v_tablekind!"=="T" (
			IF /I "%v_btqmode%"=="ANSI" (
				ECHO.CALL !v_dbnameprefix!dbadmin.DBA_NewTabDef_ANSI!v_bb!'!v_db!','!v_obj!','POST',NULL,1!v_be!>>%v_outpath%\!v_obj!!v_ext!
				ECHO.;>>%v_outpath%\!v_obj!!v_ext!
				ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
				ECHO.COMMIT;>>%v_outpath%\!v_obj!!v_ext!
				ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
			) ELSE (
				ECHO.CALL !v_dbnameprefix!dbadmin.DBA_NewTabDef!v_bb!'!v_db!','!v_obj!','POST',NULL,1!v_be!>>%v_outpath%\!v_obj!!v_ext!
				ECHO.;>>%v_outpath%\!v_obj!!v_ext!
				ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
				ECHO.CALL !v_dbnameprefix!dbadmin.DBA_CreLoadReady!v_bb!'!v_db!','!v_obj!','!v_dbnameprefix!CntlLoadReadyT',NULL,'D',1!v_be!>>%v_outpath%\!v_obj!!v_ext!
				ECHO.;>>%v_outpath%\!v_obj!!v_ext!
				ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
			)
			ECHO.>>%v_outpath%\!v_obj!!v_ext!
			ECHO.COMMENT ON !v_db!.!v_obj! IS '!v_SVN_Revision! - !v_SVN_Date! '>>%v_outpath%\!v_obj!!v_ext!
			ECHO.;>>%v_outpath%\!v_obj!!v_ext!
			ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
			IF /I "%v_btqmode%"=="ANSI" (ECHO.COMMIT;>>%v_outpath%\!v_obj!!v_ext!)&ECHO..IF ERRORCODE !v_neq! 0 THEN .EXIT ERRORCODE>>%v_outpath%\!v_obj!!v_ext!
			ECHO.>>%v_outpath%\!v_obj!!v_ext!
		)
	)
)

