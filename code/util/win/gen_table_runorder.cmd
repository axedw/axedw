@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET "v_utilpath=%~d0%~p0"
SET "v_listpath=%CD%"
SET "v_listname=%v_listpath%\_runorder.table.txt"
SET "v_srcpath=%v_listpath%\table\"
SET "v_tmppath=%v_listpath%\_runorder_table_tmp\"
SET "v_objpath=code/%v_srcpath:*\code\=%"
SET "v_objpath=%v_objpath:\=/%"
FOR /F "tokens=1* delims=|" %%a IN ("%v_utilpath:\util\=|%") DO SET "v_croot=%%a"

SET "c_SED=%v_utilpath%sed.exe"

SET "v_ext=.btq"
SET "v_dbnameprefix=${DB_ENV}"

SET "v_date=%DATE:-=%"
SET "v_time=%TIME::=%"
SET "v_time=!v_time: =0!"
SET "v_ver=%v_date:~2%%v_time:~0,6%"

SET "v_neq=<>"
SET "v_bb=("
SET "v_be=)"
SET "v_bang=^!"
SET "v_defno=0"
SET v_quote="
SET v_blnkln_ind=0
SET v_queuedepth=0

SET "v_obj="


cls
ECHO.Generating table runorder, please wait...


IF EXIST "%v_tmppath%" RMDIR /S/Q "%v_tmppath%">NUL
MKDIR "%v_tmppath%">NUL

IF EXIST "%v_listname%" MOVE /Y "%v_listname%" "%v_listname%.%v_ver%">NUL
ECHO.------ Start of table definitions (insert new lines between Start and End marker)>%v_listname%

REM Initial pass through list of files in script-folder
FOR %%f IN ("%v_srcpath%*%v_ext%") DO (
	SET "v_obj=%%~nf"
	SET v_iscretab=0
	SET v_refno=0
	SET v_refinlist=0
	rem ECHO.%v_objpath%!v_obj!%v_ext%
	
	REM Check if file contains a create-table statement, else skip file
	FOR /F "usebackq tokens=1*" %%x IN (`FIND /C "CREATE TABLE ${DB_ENV}" %%f`) DO (
				SET "v_line=%%y"
				SET "v_line=!v_line:*: =!"
				IF /I NOT "!v_line!"=="" IF !v_line! GTR 0 SET v_iscretab=1
	)

	IF !v_iscretab!==1 (
		REM Process all references-clauses in script, ignore self-references
		FOR /F "usebackq tokens=* delims=" %%l IN (`FIND /N /I "REFERENCES WITH NO CHECK OPTION ${DB_ENV}" %%f`) DO (
			SET "v_line=%%l"
			SET "v_line=!v_line:REFERENCES WITH NO CHECK OPTION ${DB_ENV}=$!"
			IF /I "!v_line!" NEQ "%%l" FOR /F "tokens=1,2* delims=$." %%m IN ("!v_line!") DO FOR /F "tokens=1 delims=(" %%r IN ("%%o") DO (
				FOR /F "tokens=1" %%x IN ("%%r") DO SET "v_refobj=%%x"
				IF "!v_refobj!" NEQ "!v_obj!" (
					ECHO.!v_refobj!>>%v_tmppath%!v_obj!.tmp
					SET /A v_refno+=1
					REM Check if referenced object is accounted for
					IF EXIST "%v_tmppath%!v_refobj!.done" SET /A v_refinlist+=1
				)
			)
		)
		IF !v_refno!==!v_refinlist! (
			REM All referenced objects accounted for in runlist, add current object to runlist and remove from queue
			ECHO.BTQRUN	%v_objpath%!v_obj!%v_ext%>>%v_listname%
			IF EXIST "%v_tmppath%!v_obj!.tmp" (
				REN "%v_tmppath%!v_obj!.tmp" !v_obj!.done>NUL
			) ELSE ECHO.>"%v_tmppath%!v_obj!.done"
		) ELSE (
			REM One or more referenced objects missing from runlist, keep current object to queue
			REM Put number of references as extention of queue file-name
			IF EXIST "%v_tmppath%!v_obj!.tmp" REN "%v_tmppath%!v_obj!.tmp" !v_obj!.R-!v_refno!>NUL
			SET /A v_queuedepth+=1
		)
	)
)


:L_QUEUE
REM Process queue
SET v_cr_ind=0
SET v_cr_no=0
SET v_iterno=0
FOR /L %%a IN (2,1,1000) DO (

rem ECHO.--- Pass %%a

	SET v_queuered=0
	SET "v_obj="
	REM Process remaining queued objects in order of number of references (increasing order)
	IF EXIST %v_tmppath%*.R-* (
		FOR /F "usebackq tokens=1,2,3 delims=.-" %%f IN (`DIR /B /OE %v_tmppath%*.R-*`) DO (
			SET "v_obj=%%f"
			SET "v_totrefs=%%h"
			SET v_refno=0
			SET v_refinlist=0
			REM Process all referenced objects in queued object
			FOR /F "usebackq tokens=1" %%r IN ("%v_tmppath%!v_obj!.R-!v_totrefs!") DO (
				SET /A v_refno+=1
				REM Check if referenced object is accounted for
				IF EXIST "%v_tmppath%%%r.done" SET /A v_refinlist+=1
			)

	rem echo.!v_obj! - !v_totrefs! - !v_refno! - !v_refinlist!

			IF !v_refno!==!v_refinlist! (
				REM All referenced objects accounted for in runlist, add current object to runlist and remove from queue
				ECHO.BTQRUN	%v_objpath%!v_obj!%v_ext%>>%v_listname%
				IF EXIST "%v_tmppath%!v_obj!.R-!v_totrefs!" REN "%v_tmppath%!v_obj!.R-!v_totrefs!" !v_obj!.done>NUL
				SET /A v_queuered+=1
			) ELSE IF !v_cr_ind!==1 (
				REM Circular reference suspected (no reduction of queue in previous iteration), force current object on list
				ECHO.Circ refs suspected in iter: !v_iterno! obj: !v_obj! refs: !v_refinlist! of !v_totrefs!
				IF !v_cr_no!==1 ECHO.------ Circular reference suspected in one or more of following table-scripts>>%v_listname%
				ECHO.BTQRUN	%v_objpath%!v_obj!%v_ext%>>%v_listname%
				IF EXIST "%v_tmppath%!v_obj!.R-!v_totrefs!" REN "%v_tmppath%!v_obj!.R-!v_totrefs!" !v_obj!.done>NUL
				SET /A v_queuered+=1
				SET v_cr_ind=0
			)
		)
		REM If no reduction of queue indicate suspected Circular reference
		IF !v_queuered!==0 (
			SET v_cr_ind=1
			SET /A v_cr_no+=1
		)
		SET v_iterno=%%a
	)
	IF NOT DEFINED v_obj GOTO L_ENDQUEUE
)
:L_ENDQUEUE

ECHO.------ End of table definitions>>%v_listname%

ECHO.Iterations#: !v_iterno!

:ENDSCR
EXIT /B 999
