Call Main
Sub Main
 sFilePath=InputBox("Input TPT file path which you want to split","TPT file path","")
 iNumberOfSteps=InputBox("Input the number of steps per file","Number of steps","")
 Call SplitTPT(sFilePath,iNumberOfSteps)
End Sub

Sub SplitTPT(ByVal sFilePath,ByVal iNumberOfSteps)
 set oFS=CreateObject("Scripting.FileSystemObject")
 
 if oFS.FileExists(sFilePath)=false then
  msgbox "File do not exists"
  exit sub
 end if
 
 set oFile=oFS.OpenTextFile(sFilePath,1)
 Dim arrFileLines() 
 i = 0 
 Do Until oFile.AtEndOfStream 
  Redim Preserve arrFileLines(i) 
  arrFileLines(i) = oFile.ReadLine
  if i<8 then
   sHeader=sHeader & arrFileLines(i) & vbCrLf
  end if
  i=i+1
 Loop 
 oFile.Close 
 
 'Create first file - before loading
 iFileCnt=0
 iStepCnt=0
 s=""
 
 sFileName=oFS.GetFileName(sFilePath)
 sFolderName=oFS.GetFile(sFilePath).ParentFolder
 sFileExtension=oFS.GetExtensionName(sFilePath)
 For j=0 to i-1
  
  if Left(arrFileLines(j),5)="STEP " or j=i-1 then
   iStepCnt=iStepCnt+1
   s=s & arrFileLines(j) & vbCrLf
   if iStepCnt mod iNumberOfSteps=0 or j=i-1 then
    iFileCnt=iFileCnt+1
    sNewFilePath=sFolderName & "\" & Left(sFileName,Len(sFileName)-Len(sFileExtension)) & iFileCnt & "." & sFileExtension
    set oFile = oFS.CreateTextFile(sNewFilePath,True, False)
    if iFileCnt<>1 then
     oFile.Write(sHeader)
    end if
    oFile.Write(s)
    if j<>i-1 then
     oFile.WriteLine(");")
    end if
    oFile.Close
    set oFile=nothing
    iStepCnt=0
    s=""
   end if
  else
   s=s & arrFileLines(j)  & vbCrLf
  end if 
 next
 msgbox "Done! " & vbCrLf & iFileCnt & " files generated in folder " & vbCrLf & sFolderName
End Sub