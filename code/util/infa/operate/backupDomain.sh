#!/bin/ksh
############################################################################
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: backupDomain.sh 129 2011-09-07 08:23:59Z k9106728 $
# Last Changed By  : $Author:  
# Last Change Date : $Date: 2011-09-07 10:23:59 +0200 (ons, 07 sep 2011) $
# Last Revision    : $Revision: 129 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/infa/operate/backupDomain.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# PURPOSE: Backup Informatica security domain
# 
# REVISION HISTORY
# ==========================================================================
# INITIALS   DATE         COMMENT
# --------------------------------------------------------------------------
# SSU (TD)   2011-05-19   Initial version
#
# OBJECT REFERENCE INFORMATION
# ==========================================================================
# TYPE    ACTION    OBJECT NAME
# --------------------------------------------------------------------------
# 
############################################################################

# load all variables from profile
. $HOME/.infascm_profile

BACKUP_FILE_DIR=$HOME/infa_backup
NOW=$(date +"%Y%m%d_%H%M%S")
BACKUP_FILE=$BACKUP_DIR/${DOMAIN_NAME}_$NOW.xml


$INFASETUP BackupDomain \
    -da "$DOMAIN_DATABASEADRESS" \
    -du "$DOMAIN_DATABASE_USER_NAME" \
    -dp "$DOMAIN_DATABASE_USER_PASSWORD" \
    -dt "$DOMAIN_DATABASE_TYPE" \
    -ds "$DOMAIN_DATABASE_SERVICE_NAME" \
    -ts "$DOMAIN_DATABASE_TABLESPACE" \
    -bf "$DOMAIN_BACKUP_FILE" \
    -dn "$DOMAIN_DOMAIN_NAME" \

gzip $BACKUP_FILE

exit 0