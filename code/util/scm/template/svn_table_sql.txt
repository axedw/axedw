/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id:  $
# Last Changed By  : $Author:  $
# Last Change Date : $Date:  $
# Last Revision    : $Revision:  $
# Subversion URL   : $HeadURL:  $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}<DBNAME>;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}<DBNAME>','<TABLENAME>','PRE','I',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}<DBNAME>.<TABLENAME>
(	
	<Item>_Seq_Num	INTEGER NOT NULL,
	Valid_From_Dttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT TIMESTAMP '0001-01-01 00:00:00.000000' NOT NULL,
	<Item>_Id	VARCHAR(20),
	<Item>_Desc	VARCHAR(250) NOT NULL,
	<ItemFK>_Seq_Num	INTEGER NOT NULL,

<additional column definitions>

	Valid_To_Dttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT TIMESTAMP '9999-12-31 23:59:59.999999' NOT NULL,
	OpenJobRunId	INTEGER,
	CloseJobRunId	INTEGER,
	
	CONSTRAINT PK<TABLENAME> PRIMARY KEY (<Item>_Seq_Num,Valid_From_Dttm),
	CONSTRAINT AK1<TABLENAME> PRIMARY KEY (<Item>_Seq_Num,Valid_To_Dttm),
	CONSTRAINT FK1<TABLENAME> FOREIGN KEY (<ItemFK>_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}<DBNAME>.<FK_TABLENAME> (<ItemFK>_Seq_Num)
)
INDEX S1ARTICLE_D (<ItemFK>_Seq_Num)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}<DBNAME>','<TABLENAME>','POST','I',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}<DBNAME>.<TABLENAME> IS '$Revision:  $ - $Date:  $'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- Define additional/sample Statistics that are not PK or FK or index
COLLECT STATISTICS
COLUMN(<Item>_Id)
ON ${DB_ENV}<DBNAME>.<TABLENAME>
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
