#!/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID                    : $Id: merge2trunk.sh 11463 2013-11-27 15:51:53Z k9105194 $
# Last Changed By       : $Author: k9105194 $
# Last Changed Date     : $Date: 2013-11-27 16:51:53 +0100 (ons, 27 nov 2013) $
# Last Changed Revision : $Revision: 11463 $
# Subversion URL        : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/merge2trunk.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : merge to trunk from project specific branch
# Project     : EDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date          Author          Description
# 2013-11-26    A.Bagge         Initial version
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# merge to trunk from project specific branch
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
# 2.) Initialization
# 3.) Check for correct syntax
# 4.) Verify module name correctness
# 5.) Determine latest revision 
# 6.) Check for uncommitted changes in merge work area and cleanup if required
# 7.) Update merge target work area 
# 8.) Get merge info and perform merge accordingly
# 9.) Cleanup & finish
#
# --------------------------------------------------------------------------
# Open points
# --------------------------------------------------------------------------
# 1.) 
# 2.) 
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialize all variables from ini file and define base functions
#---------------------------------------------------------------------------
THIS_SCRIPT=`basename $0 .sh`
all_args="$THIS_SCRIPT $@"
. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-r <revno>] [-R] [-f <fromURL>] -t <toURL> [-h]"
    echo "Merge from development trunk to system test staging area"
    echo "       [-r <revno>]  : Revision number to merge. Use valid revision number."
    echo "                       If omitted, HEAD revision is used."
    echo "       -f <fromURL>  : URL to merge from"
    echo "       [-t <toURL>]  : URL to merge to (default: http://axprsv01.axfood.se/svn/axedw/trunk/code)"
    echo "       [-w WAPATH]   : Path to Working Area (default: \$HOME/svn/code)"
    echo "       -l <logURL>   : URL to the log folder where to store the list of result"
    echo "       [-R]          : Do a dry-run and save the Report into trunk (default is to do a merge)"
    echo "       [-h]          : Print script syntax help"
    echo ""
    }
# get parameters
REV_NO=""
REVISION=""
REPORT_ONLY="false"
FROM_URL="noURL"
TO_URL="http://axprsv01.axfood.se/svn/axedw/trunk/code"
WA_PATH="$HOME/svn/code"
LOG_URL=""
USAGE="r:f:t:w:l:Rh"
while getopts "$USAGE" opt; do
    case $opt in
        r  ) REV_NO="$OPTARG"
             REVISION="-r$REV_NO"
             ;;
        R  ) REPORT_ONLY="true"
             ;;
        f  ) FROM_URL="$OPTARG"
			 ;;
        t  ) TO_URL="$OPTARG"
			 ;;
        w  ) WA_PATH="$OPTARG"
			 ;;
        l  ) LOG_URL="$OPTARG"
			 ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

if [ "$FROM_URL" = "noURL" ]
then
	print_help
	exit 1
fi

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# re-route all output to screen and logfile simultaneously
tee "$LOG_FILE" >/dev/tty |&
exec 1>&p
exec 2>&1

export SVN=/opt/devutil/svn
echo "Checking input parameters"
cd $WA_PATH || exit 1

echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter -r (Revision No)           : $REV_NO"
echo "Parameter -R (Report Only)           : $REPORT_ONLY"
echo "Parameter -f (fromURL)               : $FROM_URL"
echo "Parameter -t (toURL)                 : $TO_URL"
echo "Parameter -w (WAPATH)                : $WA_PATH"
echo "Parameter -l (logURL)                : $LOG_URL"
echo ""
echo "Local build merge path (WA)          : `pwd`"
echo "Local temp path                      : $BUILD_TMP_PATH"
echo "Local logging path                   : $BUILD_LOG_PATH"
echo "Log file                             : $LOG_FILE"
echo ""


$SVN info "$FROM_URL" | fgrep "Not a valid URL" >/dev/null
stat=$?
if [ $stat -eq 0 ]
then
	echo "Not a valid fromURL: $FROM_URL"
	print_help
	exit 1
fi
$SVN info "$TO_URL" | fgrep "Not a valid URL" >/dev/null
stat=$?
if [ $stat -eq 0 ]
then
	echo "Not a valid toURL: $TO_URL"
	print_help
	exit 1
fi
$SVN info "$LOG_URL" | fgrep "Not a valid URL" >/dev/null
stat=$?
if [ $stat -eq 0 ]
then
	echo "Not a valid logURL: $LOG_URL"
	print_help
	exit 1
fi
$SVN info | fgrep "URL: $TO_URL" >/dev/null
stat=$?
if [ $stat -ne 0 ]
then
	echo "Not a working area for toURL: `pwd` - should be connected to $TO_URL"
	print_help
	exit 1
fi
echo "Input parameters ok"

echo "Update of working area `pwd` from $TO_URL"
$SVN update || exit 1

echo "Start of merge from $FROM_URL to working area `pwd`"

merge_logfile="$BUILD_TMP_PATH/`if [ \"$REPORT_ONLY\" = \"true\" ]; then echo \"dryrun\"; else echo \"merge\"; fi`_$SCRIPT_START_DATE.txt"
echo "Merge from $FROM_URL into $TO_URL" >$merge_logfile
echo "Parameters: $all_args" >>$merge_logfile
echo "Started `date`" >>$merge_logfile
$SVN merge --reintegrate `test "$REPORT_ONLY" = "true" && echo "--dry-run"` --accept postpone "$FROM_URL" >>$merge_logfile || exit 1
echo "Ended `date`" >>$merge_logfile

echo "The result of the `test \"$REPORT_ONLY\" = \"true\" && echo \"dryrun \"`is in the file: $merge_logfile"
echo "Sending to svn repository"

cd "$BUILD_TMP_PATH" || exit 1

$SVN checkout "$LOG_URL" || exit 1

cd "`basename $LOG_URL`" || exit 1
cp "$merge_logfile" . || exit 1

file="`basename $merge_logfile`"

$SVN add "$file" || exit 1
$SVN commit -m "Added resultfile from merge `test \"$REPORT_ONLY\" = \"true\" && echo \"dryrun \"`from $FROM_URL" "$file" || exit 1
cd ..  || exit 1
rm -rf "`basename $LOG_URL`"  || exit 1
echo "NOTE!! do a subversion commit in the working area `pwd`/code to send the merged files into the repository $TO_URL"

echo "logfile: $merge_logfile"

exit 0
