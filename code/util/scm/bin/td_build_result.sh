#!/usr/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: td_build_result.sh 29483 2019-11-15 11:31:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-11-15 12:31:55 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29483 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/td_build_result.sh $
#---------------------------------------------------------------------------
# SVN Info END
#---------------------------------------------------------------------------
#
# Purpose     : update work copy from specified URL
# Project     : Axfood
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History
# --------------------------------------------------------------------------
# Date       Author         Description
# 2012-06-13 S.Sutter       Initial version
#
# --------------------------------------------------------------------------
# Description
# --------------------------------------------------------------------------
# create build results (file lists & scripts) for specific build/release
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -m        master script       yes         yes         master script calling this script (omit
# -e        environment         yes         yes         environment of build/release
# -s        script start date   yes         yes         start date of master script
# -n        new build/rel no.   yes         yes         new build/release number - the 
# -o        old build/rel no.   yes         yes         old build/release number used as reference for delta determination
# -t        target build URL    yes         yes         SVN path of new build/release
# -p        previous rel URL    yes         yes         SVN path of old build/release
# -c        previous count      yes         yes         number of previous builds/releases (0 or 1)
# -l        log file            yes         no          name (and path) of logfile (if empty use default)
# -d        delta mode          no          no          use actual build number instead of 'next' (only relevant for it build)
# -h        print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#   
#
# --------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh


#---------------------------------------------------------------------------
# 1.) initialize variables from ini file & function definitions
#---------------------------------------------------------------------------

#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo ""
    echo "Usage: ${THIS_SCRIPT}.sh -m <master_script> -e <environment> -d <start_date> \ "
    echo "                          -n <new_build_no> -o <old_build_no> -t <target_url> \ "
    echo "                          -p <previous_url> -c <previous_count> \ "
    echo "                          [-l <logfile>] [-h]"
    echo "Create build result files from build or release"
    echo "       -m <master_script>  : master script calling this subscript - omit the .sh ending"
    echo "       -e <environment>    : environment of build/release"
    echo "       -s <start_date>     : script start date"
    echo "       -n <new_build_no>   : new build/release number"
    echo "       -o <old_build_no>   : old build/release number used as reference for delta determination"
    echo "       -t <target_url>     : SVN path of new build/release"
    echo "       -p <previous_url>   : SVN path of old reference build/release"
    echo "       -c <previous_count> : number of previous builds/releases (0 or 1)"
    echo "       [-l <logfile>]      : name (and path) of log file (if empty use default)"
    echo "       [-d <delta_mode>]   : use actual build number instead of 'next' (only relevant for it build)"
    echo "       [-h]                : Print script syntax help"
    echo ""
    }


#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#   Cleanup procedures at exit - in 
#   If var for final log file was not defined yet,
#   then an error early in the script has occured. Left here as template
    if [ ! "$OPT_ISSET_LGF" ] ; then
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$SCRIPT_START_DATE.error
        fi
        # move temp log file to final position
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    fi
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
# trap at_exit EXIT

#---------------------------------------------------------------------------
#  Filter out directories with children from packaging list
#---------------------------------------------------------------------------

filter_package_list ()
    {
    typeset file2scan=$1
    typeset filebasedir=$2
    typeset url2test
    typeset name2scan
    typeset is_dir
    typeset has_child
    typeset search_str
    
    
    # echo "Scanning $file2scan for directories with children"
    # echo "that should thus not be in the tar package input list"
    touch $file2scan.found
    cat $file2scan | while read line2scan
    do
        # scan, if object is directory
        url2test=$filebasedir/`dirname $line2scan`
        # directories have trailing "/"
        name2scan=`basename $line2scan | sed -e 's/$/\//'`
        is_dir=`$SVN list $url2test | grep $name2scan | wc -l`
        # object is file
        if  [ $is_dir -eq 0 ] ; then
            # echo "        FOUND. $line2scan is regular file."
            echo $line2scan >> $file2scan.found
        # object is directory
        else
            # echo "    $line2scan is directory. Doing further checks"
            search_str=$filebasedir/$line2scan
            # do any children exist?
            has_child=`$SVN list $search_str | wc -l`
            # no children -> keep
            if  [ $has_child -eq 0 ] ; then
                # echo  "        FOUND. Directory $line2scan has no children. Keeping."
                echo $line2scan >> $file2scan.found
            # has children -> ignore
            # else
                # echo "    Directory $line2scan has children. Filtering out."
            fi
        fi
    done
    mv $file2scan.found $file2scan
    # echo "... done."
    }


#---------------------------------------------------------------------------
# 2.) Initialize other variables
#---------------------------------------------------------------------------
# set Defaults
SCRIPT_START_DATE=`date +%Y%m%d_%H%M%S`
DELTA_MODE="false"

# echo "$@"
USAGE="m:e:s:n:o:t:p:c:l:dh"
while getopts "$USAGE" opt; do
    case $opt in
        m  ) MASTER_SCRIPT="$OPTARG"
             OPT_ISSET_MSC="true"
             ;;
        e  ) BUILD_ENV="$OPTARG"
             OPT_ISSET_ENV="true"
             ;;
        s  ) SCRIPT_START_DATE="$OPTARG"
             OPT_ISSET_SSD="true"
             ;;
        n  ) NEW_BUILD_NO="$OPTARG"
             OPT_ISSET_NBN="true"
             ;;
        o  ) OLD_BUILD_NO="$OPTARG"
             OPT_ISSET_OBN="true"
             ;;
        t  ) NEW_BUILD_URL="$OPTARG"
             OPT_ISSET_NBU="true"
             ;;
        p  ) OLD_BUILD_URL="$OPTARG"
             OPT_ISSET_OBU="true"
             ;;
        c  ) PREV_CNT="$OPTARG"
             OPT_ISSET_PC="true"
             ;;
        l  ) LOG_FILE="$OPTARG"
             OPT_ISSET_LGF="true"
             ;;
        d  ) DELTA_MODE="true"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))



ALL_TMP_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$NEW_BUILD_NO.$SCRIPT_START_DATE.all_tmp_file.txt
CLEANUP_SCRIPT=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.cleanup.sh
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log_msg.txt
CHANGE_TMP_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$NEW_BUILD_NO.$SCRIPT_START_DATE.changes.txt

touch $LOG_MSG_FILE

# fill cleanup script to delete temporary files
touch $CLEANUP_SCRIPT
chmod +x $CLEANUP_SCRIPT
echo "rm -f $LOG_MSG_FILE"                                     >> $CLEANUP_SCRIPT
echo "rm -f $ALL_TMP_FILE"                                     >> $CLEANUP_SCRIPT
echo "rm -f $CHANGE_TMP_FILE"                                  >> $CLEANUP_SCRIPT


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0
# check, if all necessary arguments are set
if [ ! "$OPT_ISSET_MSC" ] ; then
    echo "Option -m (master script) not specified!"
    ERROR_CODE=1
fi
if [ ! "$OPT_ISSET_ENV" ] ; then
    echo "Option -e (environment) not specified!"
    ERROR_CODE=1
fi
if [ ! "$OPT_ISSET_NBN" ] ; then
    echo "Option -n (new build number) not specified!"
    ERROR_CODE=1
fi
if [ ! "$OPT_ISSET_OBN" ] ; then
    echo "Option -o (old/previous build number) not specified!"
    ERROR_CODE=1
fi
if [ ! "$OPT_ISSET_NBU" ] ; then
    echo "Option -t (new build url / target) not specified!"
    ERROR_CODE=1
fi
if [ ! "$OPT_ISSET_OBU" ] ; then
    echo "Option -p (old/previous build url) not specified!"
    ERROR_CODE=1
fi
if [ ! "$OPT_ISSET_PC" ] ; then
    echo "Option -c (previous build/release count) not specified!"
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi

case $BUILD_ENV in
    it|int )
        if [ "$DELTA_MODE" = "true" ] ; then
            NEXT_BUILD_NO="$NEW_BUILD_NO"
        else
            NEXT_BUILD_NO="next"
        fi
         ;;
    * ) NEXT_BUILD_NO="$NEW_BUILD_NO"
         ;;
esac

if [ ! "$OPT_ISSET_LGF" ] ; then
    LOG_FILE=$BUILD_TMP_PATH/`basename $0 .sh`.$SCRIPT_START_DATE.log
    # re-route all output to screen and logfile simultaneously
    tee $LOG_FILE >/dev/tty |&
    exec 1>&p
    exec 2>&1
fi

echo ""
echo "###############################################################################"
echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter -m (Master script)              : $MASTER_SCRIPT"
echo "Parameter -e (Environment)                : $BUILD_ENV"
echo "Parameter -s (Script Start Date)          : $SCRIPT_START_DATE"
echo "Parameter -n (New build no.)              : $NEW_BUILD_NO"
echo "Parameter -o (Old build no.)              : $OLD_BUILD_NO"
echo "Parameter -t (Target build URL)           : $NEW_BUILD_URL"
echo "Parameter -p (Previous release/build URL) : $OLD_BUILD_URL"
echo "Parameter -c (Previous count)             : $PREV_CNT"
echo "Parameter -l (Log file)                   : $LOG_FILE"
echo "Parameter -d (Delta mode)                 : $DELTA_MODE"
echo ""
echo "Next build no.                            : $NEXT_BUILD_NO"
echo ""
echo "SVN repository URL                        : $SVN_BASE_URL"
echo "Local build path                          : $BUILD_WA_PATH"
echo "Local temp path                           : $BUILD_TMP_PATH"
echo "Local logging path                        : $BUILD_LOG_PATH"
echo ""


#---------------------------------------------------------------------------
#  9.) Generate complete filelist from new build
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Generating complete filelist from new build"
echo "###############################################################################"
echo ""
echo "writing to $ALL_TMP_FILE ..."

# get a recursive list of all files within the build
$SVN list $NEW_BUILD_URL -R > $ALL_TMP_FILE.tmp
check_error $?
# remove trailing slashes indicating a directory
cat $ALL_TMP_FILE.tmp | sed -e 's/\/$//' > $ALL_TMP_FILE
check_error $?
rm -f $ALL_TMP_FILE.tmp
echo "... done"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 12.) Getting changes since previous release
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Getting changes since previous release ..."
echo "###############################################################################"
echo ""

if  [ $PREV_CNT -eq 0 ] ; then
    echo "No previous build/release exists. Using all files"
else
    echo "Getting changes of $NEW_BUILD_URL"
    echo "    since last release $OLD_BUILD_URL ..."
    
    # perform diff and write result to change file
    # diff only within code directory, since other directories were created by the build itself
    $SVN diff --summarize $OLD_BUILD_URL/code $NEW_BUILD_URL/code | sort > $CHANGE_TMP_FILE.tmp
    check_error $?
    
    # "/" in grep or sed parameter must be escaped
    OLD_BUILD_URL_SED=$( echo $OLD_BUILD_URL | sed -e 's/\//\\\//'g )
    #echo "(OLD_BUILD_URL_SED)      : $OLD_BUILD_URL_SED"
    
    # remove the build path - it is not required
    cat $CHANGE_TMP_FILE.tmp | sed -e "s/$OLD_BUILD_URL_SED\///" > $CHANGE_TMP_FILE
    rm $CHANGE_TMP_FILE.tmp
fi
echo ""
echo "... done"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 13.) Create build result directories and add to SVN
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Creating build result directories ..."
echo "###############################################################################"
echo ""

# the build result directories will be located under the build root
# it contains all file lists required for packaging and automatic install scripts
BUILD_RESULT_BASE_DIR=$BUILD_TMP_PATH/$THIS_SCRIPT.$BUILD_ENV.$NEW_BUILD_NO.$SCRIPT_START_DATE
mkdir -p $BUILD_RESULT_BASE_DIR
echo "rm -rf $BUILD_RESULT_BASE_DIR"                                    >> $CLEANUP_SCRIPT

# an empty checkout is sufficient, since we only add new objects
$SVN checkout $NEW_BUILD_URL --depth empty $BUILD_RESULT_BASE_DIR
check_error $?

$SVN mkdir $BUILD_RESULT_BASE_DIR/build
$SVN mkdir $BUILD_RESULT_BASE_DIR/build/results
$SVN mkdir $BUILD_RESULT_BASE_DIR/build/results/filelists
$SVN mkdir $BUILD_RESULT_BASE_DIR/build/results/scripts
BUILD_RESULT_DIR=$BUILD_RESULT_BASE_DIR/build/results

echo "... done"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 14.) Get all changes and write to result files
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get all changes and write to result files ..."
echo "###############################################################################"
echo ""

# Creating many filelists and some installation scripts

# Filelists SVN source
SVN_ALL_FILE=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_all_files.txt
SVN_ADD_FILE=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_add_files.txt
SVN_DEL_FILE=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_del_files.txt
SVN_UPD_FILE=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_upd_files.txt
# echo "SVN source all files     : $SVN_ALL_FILE"
# echo "SVN source added files   : $SVN_ADD_FILE"
# echo "SVN source updated files : $SVN_UPD_FILE"
# echo "SVN source deleted files : $SVN_DEL_FILE"

# Filelists target filesystem
TRG_ALL_FILE=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_all_files.txt
TRG_ADD_FILE=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_add_files.txt
TRG_DEL_FILE=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_del_files.txt
TRG_UPD_FILE=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_upd_files.txt
# echo "Target all files         : $TRG_ALL_FILE"
# echo "Target added files       : $TRG_ADD_FILE"
# echo "Target updated files     : $TRG_UPD_FILE"
# echo "Target deleted files     : $TRG_DEL_FILE"

# Filelists SVN source (DDL only)
SVN_ALL_DDL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_all_ddl.txt
SVN_ADD_DDL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_add_ddl.txt
SVN_DEL_DDL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_del_ddl.txt
SVN_UPD_DDL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_upd_ddl.txt
# echo "SVN source all ddl       : $SVN_ALL_DDL"
# echo "SVN source added ddl     : $SVN_ADD_DDL"
# echo "SVN source updated ddl   : $SVN_UPD_DDL"
# echo "SVN source deleted ddl   : $SVN_DEL_DDL"

# Filelists target filesystem (DDL only)
TRG_ALL_DDL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_all_ddl.txt
TRG_ADD_DDL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_add_ddl.txt
TRG_DEL_DDL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_del_ddl.txt
TRG_UPD_DDL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_upd_ddl.txt
# echo "Target all ddl           : $TRG_ALL_DDL"
# echo "Target added ddl         : $TRG_ADD_DDL"
# echo "Target updated ddl       : $TRG_UPD_DDL"
# echo "Target deleted ddl       : $TRG_DEL_DDL"

# Filelists SVN source (Infa objects only)
SVN_ALL_INFA=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_all_infa.txt
SVN_ADD_INFA=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_add_infa.txt
SVN_DEL_INFA=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_del_infa.txt
SVN_UPD_INFA=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.svn_upd_infa.txt
# echo "SVN source all infa objects       : $SVN_ALL_INFA"
# echo "SVN source added infa objects     : $SVN_ADD_INFA"
# echo "SVN source updated infa objects   : $SVN_UPD_INFA"
# echo "SVN source deleted infa objects   : $SVN_DEL_INFA"

# Filelists target filesystem (Infa objects only)
TRG_ALL_INFA=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_all_infa.txt
TRG_ADD_INFA=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_add_infa.txt
TRG_DEL_INFA=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_del_infa.txt
TRG_UPD_INFA=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_upd_infa.txt
IMPORT_INFA_FILELIST=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.infa_import_filelist.txt
IMPORT_INFA_ALL_LIST=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.infa_import_all_list.txt
# echo "Target all infa objects           : $TRG_ALL_INFA"
# echo "Target added infa objects         : $TRG_ADD_INFA"
# echo "Target updated infa objects       : $TRG_UPD_INFA"
# echo "Target deleted infa objects       : $TRG_DEL_INFA"
# echo "Informatica objects import list   : $IMPORT_INFA_FILELIST"

# Release letter info (ready for copy/paste into release letter)
RL_FILE_INFO=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.releaseletter_file_info.txt
RL_DDL_INFO=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.releaseletter_ddl_info.txt
RL_INFA_INFO=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.releaseletter_infa_info.txt
RL_FILE_ALL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.releaseletter_file_all.txt
RL_DDL_ALL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.releaseletter_ddl_all.txt
RL_INFA_ALL=$BUILD_RESULT_DIR/filelists/$OLD_BUILD_NO.$NEXT_BUILD_NO.releaseletter_infa_all.txt
# echo "Release letter file info : $RL_FILE_INFO"
# echo "Release letter ddl info  : $RL_DDL_INFO"
# echo "Release letter infa info : $RL_DDL_INFO"
# echo "Release letter all files : $RL_FILE_ALL"
# echo "Release letter all ddl   : $RL_DDL_ALL"
# echo "Release letter all infa  : $RL_FILE_ALL"

# Installation scripts
TRG_RENAME_SCRIPT=$BUILD_RESULT_DIR/scripts/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_rename_script.sh
TRG_BACKUP_SCRIPT=$BUILD_RESULT_DIR/scripts/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_backup_script.sh
TRG_RESTORE_SCRIPT=$BUILD_RESULT_DIR/scripts/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_restore_script.sh
TRG_REMOVE_SCRIPT=$BUILD_RESULT_DIR/scripts/$OLD_BUILD_NO.$NEXT_BUILD_NO.trg_remove_script.sh
# echo "Target rename script     : $TRG_RENAME_SCRIPT"
# echo "Target backup script     : $TRG_BACKUP_SCRIPT"
# echo "Target restore script    : $TRG_RESTORE_SCRIPT"
# echo "Target remove script     : $TRG_REMOVE_SCRIPT"

#   Temporary files
ADD_TMP_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$BUILD_ENV.$NEXT_BUILD_NO.$SCRIPT_START_DATE.add_tmp_file.txt
DEL_TMP_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$BUILD_ENV.$NEXT_BUILD_NO.$SCRIPT_START_DATE.del_tmp_file.txt
UPD_TMP_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$BUILD_ENV.$NEXT_BUILD_NO.$SCRIPT_START_DATE.upd_tmp_file.txt
MOD_TMP_LIST=$BUILD_TMP_PATH/$THIS_SCRIPT.$BUILD_ENV.$NEXT_BUILD_NO.$SCRIPT_START_DATE.mod_tmp_list.txt
echo "rm -f $ADD_TMP_FILE"                                     >> $CLEANUP_SCRIPT
echo "rm -f $DEL_TMP_FILE"                                     >> $CLEANUP_SCRIPT
echo "rm -f $UPD_TMP_FILE"                                     >> $CLEANUP_SCRIPT
echo "rm -f $MOD_TMP_LIST"                                     >> $CLEANUP_SCRIPT

# Remove files in case they already exist
rm -f $SVN_ALL_FILE $SVN_ADD_FILE $SVN_DEL_FILE $SVN_UPD_FILE
rm -f $SVN_ALL_DDL $SVN_ADD_DDL $SVN_DEL_DDL $SVN_UPD_DDL
rm -f $SVN_ALL_INFA $SVN_ADD_INFA $SVN_DEL_INFA $SVN_UPD_INFA
rm -f $TRG_ALL_FILE $TRG_ADD_FILE $TRG_DEL_FILE $TRG_UPD_FILE
rm -f $TRG_ALL_DDL $TRG_ADD_DDL $TRG_DEL_DDL $TRG_UPD_DDL
rm -f $TRG_ALL_INFA $TRG_ADD_INFA $TRG_DEL_INFA $TRG_UPD_INFA
rm -f $TRG_REMOVE_SCRIPT $TRG_RENAME_SCRIPT $TRG_BACKUP_SCRIPT $TRG_RESTORE_SCRIPT
rm -f $RL_FILE_INFO $RL_DDL_INFO $RL_INFA_INFO $RL_FILE_ALL $RL_DDL_ALL $RL_INFA_ALL
rm -f $IMPORT_INFA_FILELIST $IMPORT_INFA_ALL_LIST

# Touch files so they won't show error messages during very first release
touch $SVN_ALL_FILE $SVN_ADD_FILE $SVN_DEL_FILE $SVN_UPD_FILE
touch $SVN_ALL_DDL $SVN_ADD_DDL $SVN_DEL_DDL $SVN_UPD_DDL
touch $SVN_ALL_INFA $SVN_ADD_INFA $SVN_DEL_INFA $SVN_UPD_INFA
touch $TRG_ALL_FILE $TRG_ADD_FILE $TRG_DEL_FILE $TRG_UPD_FILE
touch $TRG_ALL_DDL $TRG_ADD_DDL $TRG_DEL_DDL $TRG_UPD_DDL
touch $TRG_ALL_INFA $TRG_ADD_INFA $TRG_DEL_INFA $TRG_UPD_INFA
touch $TRG_REMOVE_SCRIPT $TRG_RENAME_SCRIPT $TRG_BACKUP_SCRIPT $TRG_RESTORE_SCRIPT
touch $RL_FILE_INFO $RL_DDL_INFO $RL_INFA_INFO $RL_FILE_ALL $RL_DDL_ALL $RL_INFA_ALL
touch $IMPORT_INFA_FILELIST $IMPORT_INFA_ALL_LIST

# Read cfg file
echo "Reading $PACKAGE_CONFIG_FILE ..."
echo ""
# Read every entry in config file. 2 fields, separated by ":"
cat $PACKAGE_CONFIG_FILE | while read PATH_TODO
do
#   Field 1: path as found in SVN trunk/branch/tag
    CFG_SOURCE_PATH=`echo $PATH_TODO | cut -f 1 -d ":"`
#   Field 2: path as it will appear on target system
    CFG_TARGET_PATH=`echo $PATH_TODO | cut -f 2 -d ":"`
    # echo "Dealing with SVN source path $CFG_SOURCE_PATH"
    # echo "    which maps to target path : $CFG_TARGET_PATH"
#   Add escape characters to sed search string
    CFG_SOURCE_PATH_SED=$( echo $CFG_SOURCE_PATH | sed -e 's/\//\\\//'g )
    CFG_TARGET_PATH_SED=$( echo $CFG_TARGET_PATH | sed -e 's/\//\\\//'g )
    # echo $CFG_SOURCE_PATH_SED
    # echo $CFG_TARGET_PATH_SED

#   ALL_TEMP file previously created contains all SVN source files for that release
#   grep for specific package config file entry
    cat $ALL_TMP_FILE | grep ^$CFG_SOURCE_PATH_SED >> $SVN_ALL_FILE
    
#   for target file, the SVN source path is replaced by target path (as defined in package config file)
    cat $SVN_ALL_FILE | grep ^$CFG_SOURCE_PATH_SED | sed -e "s/$CFG_SOURCE_PATH_SED/$CFG_TARGET_PATH_SED/g" >> $TRG_ALL_FILE

#   this part is relevant if a previous release already exists
    if  [ $PREV_CNT -ne 0 ] ; then
        # take the result from the svn diff, remove first 9 chars and filter for entry in package config file
        # write into filelists containing new, updated, deleted files
        cat $CHANGE_TMP_FILE | grep ^A | cut -c 9- | grep ^$CFG_SOURCE_PATH_SED > $ADD_TMP_FILE
        cat $CHANGE_TMP_FILE | grep ^D | cut -c 9- | grep ^$CFG_SOURCE_PATH_SED > $DEL_TMP_FILE
        cat $CHANGE_TMP_FILE | grep ^M | cut -c 9- | grep ^$CFG_SOURCE_PATH_SED > $UPD_TMP_FILE

        # Some directories in the file lists have children and would cause trouble
        # during tar file creation. Filter them out.
        filter_package_list $ADD_TMP_FILE $NEW_BUILD_URL
        filter_package_list $DEL_TMP_FILE $NEW_BUILD_URL
        filter_package_list $UPD_TMP_FILE $NEW_BUILD_URL

        # add to files for added, updated, deleted filelists for SVN and target environments
        cat $ADD_TMP_FILE >> $SVN_ADD_FILE
        cat $DEL_TMP_FILE >> $SVN_DEL_FILE
        cat $UPD_TMP_FILE >> $SVN_UPD_FILE
        # for target file, the SVN source path is replaced by target path (as defined in package config file)
        cat $ADD_TMP_FILE | sed -e "s/$CFG_SOURCE_PATH_SED/$CFG_TARGET_PATH_SED/g" >> $TRG_ADD_FILE
        cat $DEL_TMP_FILE | sed -e "s/$CFG_SOURCE_PATH_SED/$CFG_TARGET_PATH_SED/g" >> $TRG_DEL_FILE
        cat $UPD_TMP_FILE | sed -e "s/$CFG_SOURCE_PATH_SED/$CFG_TARGET_PATH_SED/g" >> $TRG_UPD_FILE

        # Create script to delete obsolete files on target system
        # THIS PART NEEDS TO BE ADAPTED TO AXFOOD SPECIFICS !!!!!!!!!!!!!!!!!!!
        cat $DEL_TMP_FILE | sort -r \
            | sed -e "s/$CFG_SOURCE_PATH_SED/$CFG_TARGET_PATH_SED/g" \
            | sed -e "s/ENVUSER_TO_REPLACE/\$ENVUSER_TO_REPLACE/g" \
            | sed -e "s/\(.*\)/rm \/\1/" \
            >> $TRG_REMOVE_SCRIPT

        # Create script to backup deleted and updated files on target system
        # THIS PART NEEDS TO BE ADAPTED TO AXFOOD SPECIFICS !!!!!!!!!!!!!!!!!!!
        cat $DEL_TMP_FILE \
            | sed -e "s/$CFG_SOURCE_PATH_SED/$CFG_TARGET_PATH_SED/g" \
            | sed -e "s/ENVUSER_TO_REPLACE/\$ENVUSER_TO_REPLACE/g" \
            | sed -e "s/\(.*\)/f_copy_with_path \${DEPLOY_TARGET_PATH}\1 \$DEPLOY_BACKUP_BASE_PATH\/\$RELEASE_NO\/\$PACKAGE_NAME\/\1/" \
            >> $TRG_BACKUP_SCRIPT
        cat $UPD_TMP_FILE \
            | sed -e "s/$CFG_SOURCE_PATH_SED/$CFG_TARGET_PATH_SED/g" \
            | sed -e "s/ENVUSER_TO_REPLACE/\$ENVUSER_TO_REPLACE/g" \
            | sed -e "s/\(.*\)/f_copy_with_path \${DEPLOY_TARGET_PATH}\1 \$DEPLOY_BACKUP_BASE_PATH\/\$RELEASE_NO\/\$PACKAGE_NAME\/\1/" \
            >> $TRG_BACKUP_SCRIPT

        # Create script to restore deleted and updated files and delete added files
        # on target system that were backed up before deployment
        # THIS PART NEEDS TO BE ADAPTED TO AXFOOD SPECIFICS !!!!!!!!!!!!!!!!!!!
        cat $ADD_TMP_FILE | sort -r \
            | sed -e "s/$CFG_SOURCE_PATH_SED/$CFG_TARGET_PATH_SED/g" \
            | sed -e "s/ENVUSER_TO_REPLACE/\$ENVUSER_TO_REPLACE/g" \
            | sed -e "s/\(.*\)/rm \/\1/" \
            >> $TRG_RESTORE_SCRIPT
        cat $DEL_TMP_FILE \
            | sed -e "s/$CFG_SOURCE_PATH_SED/$CFG_TARGET_PATH_SED/g" \
            | sed -e "s/ENVUSER_TO_REPLACE/\$ENVUSER_TO_REPLACE/g" \
            | sed -e "s/\(.*\)/cp -p \$DEPLOY_BACKUP_BASE_PATH\/\$RELEASE_NO\/\$PACKAGE_NAME\/\1 \${DEPLOY_TARGET_PATH}\1/" \
            >> $TRG_RESTORE_SCRIPT
        cat $UPD_TMP_FILE \
            | sed -e "s/$CFG_SOURCE_PATH_SED/$CFG_TARGET_PATH_SED/g" \
            | sed -e "s/ENVUSER_TO_REPLACE/\$ENVUSER_TO_REPLACE/g" \
            | sed -e "s/\(.*\)/cp -p \$DEPLOY_BACKUP_BASE_PATH\/\$RELEASE_NO\/\$PACKAGE_NAME\/\1 \${DEPLOY_TARGET_PATH}\1/" \
            >> $TRG_RESTORE_SCRIPT

    fi
done

# If there is no previous release, no updated or deleted files exist.
# In that case take the easy way.
# No backup/restore/remove scripts are created either.
if  [ $PREV_CNT -eq 0 ] ; then
    cat $SVN_ALL_FILE >> $SVN_ADD_FILE
    cat $TRG_ALL_FILE >> $TRG_ADD_FILE
fi


# get potentially changed DDL. For this just grep the result files for ddl directories
DDL_GREP_PARA_A="\/dbadmin\/"

cat $SVN_ALL_FILE | egrep "$DDL_GREP_PARA_A" > $SVN_ALL_DDL
cat $SVN_ADD_FILE | egrep "$DDL_GREP_PARA_A" > $SVN_ADD_DDL
cat $SVN_DEL_FILE | egrep "$DDL_GREP_PARA_A" > $SVN_DEL_DDL
cat $SVN_UPD_FILE | egrep "$DDL_GREP_PARA_A" > $SVN_UPD_DDL
cat $TRG_ALL_FILE | egrep "$DDL_GREP_PARA_A" > $TRG_ALL_DDL
cat $TRG_ADD_FILE | egrep "$DDL_GREP_PARA_A" > $TRG_ADD_DDL
cat $TRG_DEL_FILE | egrep "$DDL_GREP_PARA_A" > $TRG_DEL_DDL
cat $TRG_UPD_FILE | egrep "$DDL_GREP_PARA_A" > $TRG_UPD_DDL


# get potentially changed Informatica objects. For this just grep the result files for infa xml directories
INFA_GREP_PARA="\/infa_xml\/"

cat $SVN_ALL_FILE | egrep "$INFA_GREP_PARA" > $SVN_ALL_INFA
cat $SVN_ADD_FILE | egrep "$INFA_GREP_PARA" > $SVN_ADD_INFA
cat $SVN_DEL_FILE | egrep "$INFA_GREP_PARA" > $SVN_DEL_INFA
cat $SVN_UPD_FILE | egrep "$INFA_GREP_PARA" > $SVN_UPD_INFA
cat $TRG_ALL_FILE | egrep "$INFA_GREP_PARA" > $TRG_ALL_INFA
cat $TRG_ADD_FILE | egrep "$INFA_GREP_PARA" > $TRG_ADD_INFA
cat $TRG_DEL_FILE | egrep "$INFA_GREP_PARA" > $TRG_DEL_INFA
cat $TRG_UPD_FILE | egrep "$INFA_GREP_PARA" > $TRG_UPD_INFA

# create list of new/change Infa objects which will be used by the deployment script in the target environment
cat $TRG_ADD_INFA > $IMPORT_INFA_FILELIST
cat $TRG_UPD_INFA >> $IMPORT_INFA_FILELIST

cat $SVN_ALL_INFA > $IMPORT_INFA_ALL_LIST
f_write_date
echo ""


#---------------------------------------------------------------------------
# 9.) Preparing files for release letter
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Preparing files for release letter ..."
echo "###############################################################################"
echo ""


# Create item & ddl lists - ready to paste into release letter
echo "Creating a list of changed items - ready to paste into release letter ..."
for file4rl in $TRG_ADD_FILE $TRG_UPD_FILE $TRG_DEL_FILE
do
    case $file4rl in
        $TRG_ADD_FILE)  CHANGE_TYPE="new";;
        $TRG_DEL_FILE)  CHANGE_TYPE="deleted";;
        $TRG_UPD_FILE)  CHANGE_TYPE="changed";;
        *)              CHANGE_TYPE="";;
    esac
    cat $file4rl | while read line4rl
    do
        #td_module_value=`$SVN propget td:module $NEW_BUILD_URL/$line4rl`
        echo "`dirname $line4rl`	`basename $line4rl`	$CHANGE_TYPE" >> $RL_FILE_INFO
    done
done
echo "... done"
echo ""

echo "Creating list of all items - ready to paste into release letter ..."
cat $TRG_ALL_FILE | while read line4rl
do
    #td_module_value=`$SVN propget td:module $NEW_BUILD_URL/$line4rl`
    echo "`dirname $line4rl`	`basename $line4rl`	new" >> $RL_FILE_ALL
done
echo "... done"

echo "Creating a list of changed ddl items - ready to paste into release letter ..."
for file4rl in $TRG_ADD_DDL $TRG_UPD_DDL $TRG_DEL_DDL
do
    case $file4rl in
        $TRG_ADD_DDL)  CHANGE_TYPE="new";;
        $TRG_DEL_DDL)  CHANGE_TYPE="deleted";;
        $TRG_UPD_DDL)  CHANGE_TYPE="changed";;
        *)             CHANGE_TYPE="";;
    esac
    cat $file4rl | grep "ddl$" | while read line4rl
    do
        echo "`basename $(dirname $(dirname $line4rl))`	`basename $line4rl .btq`	`basename $(dirname $line4rl)`	$CHANGE_TYPE" >> $RL_DDL_INFO
    done
done
echo "... done"

echo "Creating list of all ddl - ready to paste into release letter ..."
cat $TRG_ALL_DDL | grep "ddl$" | while read line4rl
do
    echo "`basename $(dirname $(dirname $line4rl))`	`basename $line4rl .btq`	`basename $(dirname $line4rl)`	new" >> $RL_DDL_ALL
done
echo "... done"


echo "Creating a list of changed informatica objects - ready to paste into release letter ..."
for file4rl in $TRG_ADD_INFA $TRG_UPD_INFA $TRG_DEL_INFA
do
    case $file4rl in
        $TRG_ADD_INFA)  CHANGE_TYPE="new";;
        $TRG_DEL_INFA)  CHANGE_TYPE="deleted";;
        $TRG_UPD_INFA)  CHANGE_TYPE="changed";;
        *)              CHANGE_TYPE="";;
    esac
    cat $file4rl | grep "xml$" | while read line4rl
    do
        echo "`basename $(dirname $(dirname $line4rl))`	`basename $line4rl .xml`	`basename $(dirname $line4rl)`	$CHANGE_TYPE" >> $RL_INFA_INFO
    done
done
echo "... done"

echo "Creating list of all informatica objects - ready to paste into release letter ..."
cat $TRG_ALL_INFA | grep "xml$" | while read line4rl
do
    echo "`basename $(dirname $(dirname $line4rl))`	`basename $line4rl .xml`	`basename $(dirname $line4rl)`	new" >> $RL_INFA_ALL
done
echo "... done"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 9.) Delete empty files just created and add other files to SVN
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Delete empty files just created and add other files to SVN"
echo "###############################################################################"
echo ""

#   A lot of files have been created, but some will be empty.
#   Delete those files and check other files if they already exists in SVN.
#   If not, then add to SVN.
echo "Removing files without content and keeping the rest ..."
for file2keep in $SVN_ALL_FILE $SVN_ADD_FILE $SVN_DEL_FILE $SVN_UPD_FILE $TRG_ALL_FILE $TRG_ADD_FILE $TRG_DEL_FILE $TRG_UPD_FILE $SVN_ALL_DDL $SVN_ADD_DDL $SVN_DEL_DDL $SVN_UPD_DDL $TRG_ALL_DDL $TRG_ADD_DDL $TRG_DEL_DDL $TRG_UPD_DDL $SVN_ALL_INFA $SVN_ADD_INFA $SVN_DEL_INFA $SVN_UPD_INFA $TRG_ALL_INFA $TRG_ADD_INFA $TRG_DEL_INFA $TRG_UPD_INFA $TRG_REMOVE_SCRIPT $TRG_RENAME_SCRIPT $TRG_BACKUP_SCRIPT $TRG_RESTORE_SCRIPT $RL_FILE_INFO $RL_DDL_INFO $RL_INFA_INFO $RL_FILE_ALL $RL_DDL_ALL $RL_INFA_ALL $IMPORT_INFA_FILELIST $IMPORT_INFA_ALL_LIST
do
#   Check file size. Delete files without contents, add the rest to SVN.
    # echo "$file2keep"
    # echo "filesize is :"
    #ls -l  $file2keep | tr -s ' ' | cut -d' ' -f5
    if [ $(ls -l  $file2keep | tr -s ' ' | cut -d' ' -f5) -eq 0 ]; then
        echo "removing $file2keep"
        rm -f $file2keep 
    else
#       add non-existing files only
        if [ "$($SVN status $file2keep | cut -c1)" = "?" ]; then
            #echo "adding to svn: $file2keep"
            $SVN add $file2keep
#           some files are scripts and need to be executable
            for prop2set in $TRG_REMOVE_SCRIPT $TRG_RENAME_SCRIPT $TRG_BACKUP_SCRIPT $TRG_RESTORE_SCRIPT
            do
                if [ $file2keep = $prop2set ]; then
                    $SVN propset svn:executable ON $file2keep
                fi
            done
#           Add header & footer to deploy target backup script
            if [ $file2keep = $TRG_BACKUP_SCRIPT ]; then
                cat $BUILD_TEMPLATE_PATH/deploy_target_backup_script_header.txt >> $TRG_BACKUP_SCRIPT.tmp
                cat $TRG_BACKUP_SCRIPT >> $TRG_BACKUP_SCRIPT.tmp
                cat $BUILD_TEMPLATE_PATH/deploy_target_backup_script_footer.txt >> $TRG_BACKUP_SCRIPT.tmp
                mv $TRG_BACKUP_SCRIPT.tmp $TRG_BACKUP_SCRIPT
            fi
#           Add header & footer to deploy target restore script
            if [ $file2keep = $TRG_RESTORE_SCRIPT ]; then
                cat $BUILD_TEMPLATE_PATH/deploy_target_restore_script_header.txt >> $TRG_RESTORE_SCRIPT.tmp
                cat $TRG_RESTORE_SCRIPT >> $TRG_RESTORE_SCRIPT.tmp
                cat $BUILD_TEMPLATE_PATH/deploy_target_restore_script_footer.txt >> $TRG_RESTORE_SCRIPT.tmp
                mv $TRG_RESTORE_SCRIPT.tmp $TRG_RESTORE_SCRIPT
            fi
        fi
    fi
done

echo "... done" 
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 15.) Get previous release number and write to info file
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Write previous/current release number to *_releasenumber.txt ..."
echo "###############################################################################"
echo ""

echo "$OLD_BUILD_NO" > $BUILD_RESULT_DIR/filelists/previous_releasenumber.txt
$SVN add $BUILD_RESULT_DIR/filelists/previous_releasenumber.txt
check_error $?
echo "$NEXT_BUILD_NO" > $BUILD_RESULT_DIR/filelists/current_releasenumber.txt
$SVN add $BUILD_RESULT_DIR/filelists/current_releasenumber.txt
check_error $?
echo "... done" 
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 16.) Commit results to integration test branch
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Commit results to build/release branch ..."
echo "###############################################################################"
echo ""

echo "Adding results from new build/release $NEW_BUILD_NO to build/release branch" > $LOG_MSG_FILE
cat $LOG_MSG_FILE
$SVN commit $BUILD_RESULT_BASE_DIR --file $LOG_MSG_FILE
check_error $?
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 17.) Cleanup & finish
#---------------------------------------------------------------------------

# run cleanup script to delete temporary files
# for debugging purpose, just comment the next line
. $CLEANUP_SCRIPT
rm -f $CLEANUP_SCRIPT


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0
