#!/usr/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: td_merge.sh 29483 2019-11-15 11:31:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-11-15 12:31:55 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29483 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/td_merge.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : merge two different branch using several options
# Project     : Axfood
# Subproject  : all
# --------------------------------------------------------------------------
# Change History
# Date       Author         Description
# 2008-08-06 S.Sutter       Initial version 
# 2009-03-31 T.Bachmann     Parameter Assignment changed
# 2011-03-07 S.Sutter       Adapt to Axfood specifics
#
# --------------------------------------------------------------------------
# Description
#   generic script to execute SVN merge
#
# Dependencies
#   parent  : 
#
# Parameters:
#   --scope <single|itemlist|property> : scope (module, single -file or dir recursively, itemlist)  *)
#     --mrgobj     if --scope single   : mergeobj (name of object to be merged & released)
#     --mrglist    if --scope itemlist : name of filelist
#     --propval    if --scope property : mergeobj (name of object to be merged & released)
#   --source $SOURCE_URL               : source URL *)
#   --target $TARGET_URL               : target URL *)
#   --mrgpath $WA_PATH                 : work area path for merge results, must exist and up-to-date! *)
#   --logfile $LOG_FILE                : log file (default, if not provided)
#   --msgfile $MSG_FILE                : file for SVN commit message (default, if not provided)
#   --revision $REV_NO                 : revision number *)
#   --tmppath $BUILD_TMP_PATH          : Temp Path ($BUILD_TMP_PATH, if not provided)
#  *) throw error, if parameter is not provided
#
# Variables (please add any other variables that you may use):
#   --XYZ                              : description
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) Function definition merge_single
#     Perform either copy or merge
# 2.) Initialize other variables
# 3.) Check if all necessary files exist
# 4.) If scope is limited to single object, perform merge of that object
# 5.) If scope is set to itemlist, then we only have to set a variable
# 6.) If scope is set to property, then find out which changes are relevant
#     and then merge those changes
# 7.) cleanup
# --------------------------------------------------------------------------

. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
# 1.) Function definition merge_single
#     Perform either copy or merge
#---------------------------------------------------------------------------

merge_single ()
    {
#       set merge parameters and display them for debugging purpose
        SINGLE_MERGE_OBJ="$1"
        MERGE_SOURCE_URL="$SOURCE_URL/${SINGLE_MERGE_OBJ}${REVISION}"
        MERGE_TARGET_URL="$TARGET_URL/$SINGLE_MERGE_OBJ"
        MERGE_WA_PATH="$WA_PATH/$SINGLE_MERGE_OBJ"

#        echo "Merging $SINGLE_MERGE_OBJ"
#        echo "REVISION_SINGLE : $REVISION_SINGLE"
#        echo "MERGE_SOURCE_URL: $MERGE_SOURCE_URL"
#        echo "MERGE_TARGET_URL: $MERGE_TARGET_URL"
#        echo "MERGE_WA_PATH:    $MERGE_WA_PATH"

#       Only existing files/dirs can be merged.
#       Check if merge target exists, otherwise do a copy.
#       Otherwise do the merge.
        $SVN info "$MERGE_WA_PATH" > /dev/null 2>&1
        if [ $? -ne 0 ] ; then
#           the SVN copy command needs parent dir as target
            MERGE_PARENT="$WA_PATH/`dirname $SINGLE_MERGE_OBJ`"
            echo "Merge object $MERGE_WA_PATH does not exist. Copying to parent dir."
#           perform the copy from repository to the work copy
            $SVN copy \
                "$MERGE_SOURCE_URL" \
                "$MERGE_WA_PATH" \
                --parents \
                2>&1
            check_error $?
        else
            # If the parent directory was copied in a previous iteration,
            # the merge target exists in the work copy but not yet in the target URL.
            # In that case, no action is required.
            if [ $($SVN info "$MERGE_TARGET_URL" 2>&1 | grep "Not a valid URL" | wc -l ) -eq 0 ] ; then
#               Check if there are any changes at all that are worth merging.
#               Of course, the mergeinfo itself is of no interest and is filtered out using egrep.
#               With new SVN versions, it might be necessary to modify the egrep filter.
                if [ $($SVN diff --ignore-properties "$MERGE_SOURCE_URL" "$MERGE_TARGET_URL" \
                       | egrep -v "^$|^____|^Property changes on|svn:mergeinfo|Merged|Reverse-merged" \
                       | wc -l) -ne 0 ] ; then
                    echo "$MERGE_WA_PATH exists. Merging."
#                   And now really perform the merge
                    $SVN merge --accept postpone --ignore-ancestry --allow-mixed-revisions --quiet \
                        "$MERGE_TARGET_URL" \
                        "$MERGE_SOURCE_URL" \
                        "$MERGE_WA_PATH" \
                        2>&1
                    check_error $?
                fi
            fi
        fi

    }

#---------------------------------------------------------------------------
# 2.) Initialize other variables
#---------------------------------------------------------------------------

# set Defaults
SCRIPT_START_DATE=`date +%Y%m%d_%H%M%S`
LOG_FILE="$BUILD_TMP_PATH/`basename $0`.$SCRIPT_START_DATE.log"
MSG_FILE="$BUILD_TMP_PATH/`basename $0`.$SCRIPT_START_DATE.log_msg.txt"
DEFAULT_PROPNAME="td:module"

# get parameters and make them read-only
IFS_TEMP=$IFS
IFS=","
set -A OPT_ARRAY ${0#*/} ${@}
IFS=$IFS_TEMP
i=1
while [ $i -lt "${#OPT_ARRAY[*]}" ]
do
    case "${OPT_ARRAY[$i]}" in
        --scope)    # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_SCOPE" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    SCOPE=${OPT_ARRAY[$i]}
                    OPT_ISSET_SCOPE="true"
                    if [ $SCOPE != "single" -a $SCOPE != "itemlist" -a $SCOPE != "property" ]
                    then
                        echo "Value $SCOPE for Option --scope is invalid. Only single, itemlist or property accepted!"
                        check_error 1
                    fi
                    ;;
        --mrgobj)   # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_MRGOBJ" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    MERGE_OBJ="${OPT_ARRAY[$i]}"
                    OPT_ISSET_MRGOBJ="true"
                    ;;
        --mrglist)  # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_MRGLIST" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    MERGE_LIST="${OPT_ARRAY[$i]}"
                    OPT_ISSET_MRGLIST="true"
                    ;;
        --propval)  # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_PROPVAL" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    MERGE_OBJ=${OPT_ARRAY[$i]}
                    OPT_ISSET_PROPVAL="true"
                    ;;
        --source)   # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_SOURCE" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    SOURCE_URL="${OPT_ARRAY[$i]}"
                    OPT_ISSET_SOURCE="true"
                    ;;
        --target)   # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_TARGET" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    TARGET_URL="${OPT_ARRAY[$i]}"
                    OPT_ISSET_TARGET="true"
                    ;;
        --mrgpath)  # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_MRGPATH" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    WA_PATH="${OPT_ARRAY[$i]}"
                    OPT_ISSET_MRGPATH="true"
                    ;;
        --logfile)  # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_LOGFILE" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    # Log- Messages, which were written to default- LOG_FILE have to be moved to new location
                    if [ -f "$LOG_FILE" ]
                    then
                        cat "$LOG_FILE" >> ${OPT_ARRAY[$i]}
                        rm "$LOG_FILE"
                    fi
                    LOG_FILE=${OPT_ARRAY[$i]}
                    OPT_ISSET_LOGFILE="true"
                    ;;
        --msgfile)  # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_MSGFILE" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    # Messages, which were written to default- MSG_FILE have to be moved to new location
                    if [ -f $MSG_FILE ]
                    then
                        cat $MSG_FILE >> ${OPT_ARRAY[$i]}
                        rm $MSG_FILE
                    fi
                    MSG_FILE=${OPT_ARRAY[$i]}
                    OPT_ISSET_MSGFILE="true"
                    ;;
        --revision) # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_REVISION" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    REV_NO=${OPT_ARRAY[$i]}
                    OPT_ISSET_REVISION="true"
                    ;;
        --tmppath)  # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_TMPPATH" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    BUILD_TMP_PATH="${OPT_ARRAY[$i]}"
                    OPT_ISSET_TMPPATH="true"
                    ;;
        --propname) # Check, if Option is specified multiple times
                    if [ "$OPT_ISSET_PROPNAME" ]
                    then
                        echo "Option ${OPT_ARRAY[$i]} is specified multiple times!"
                        check_error 1
                    fi
                    ((i=i+1))
                    PROPNAME=${OPT_ARRAY[$i]}
                    OPT_ISSET_PROPNAME="true"
                    ;;
        *) echo "Unknown Option: ${OPT_ARRAY[$i]}"
           check_error 1;;
    esac
    ((i=i+1))
done

# check, if all necessary Arguments are set
if [ ! "$OPT_ISSET_SCOPE" ]
then
    echo "Option --scope not specified!"
    check_error 1
fi
if [ ! "$OPT_ISSET_SOURCE" ]
then
    echo "Option --source not specified!"
    check_error 1
fi
if [ ! "$OPT_ISSET_TARGET" ]
then
    echo "Option --target not specified!"
    check_error 1
fi
if [ ! "$OPT_ISSET_MRGPATH" ]
then
    echo "Option --mrgpath not specified!"
    check_error 1
fi

# check dependancies
if [ "$SCOPE" = "single" ]
then
    if [ ! "$OPT_ISSET_MRGOBJ" ]
    then
        echo "Option --scope $SCOPE set, but Option --mrgobj not specified!"
        check_error 1
    fi
fi
if [ "$SCOPE" = "itemlist" ]
then
    if [ ! "$OPT_ISSET_MRGLIST" ]
    then
        echo "Option --scope $SCOPE set, but Option --mrglist not specified!"
        check_error 1
    fi
fi
if [ "$SCOPE" = "property" ]
then
    if [ ! "$OPT_ISSET_PROPVAL" ]
    then
        echo "Option --scope $SCOPE set, but Option --propval not specified!"
        check_error 1
    fi
fi
if [ "$OPT_ISSET_PROPNAME" ]
then
    if [ "$SCOPE" != "property" ]
    then
        echo "Warning: Option --propname invalid for Option --scope $SCOPE! $PROPNAME ignored!"
        PROPNAME="$DEFAULT_PROPNAME"
    fi
fi

if [ "$OPT_ISSET_REVISION" ]
then
   REVISION="@$REV_NO"
else
   REVISION=""
fi

echo "Parameter SCOPE         : $SCOPE"
echo "Parameter MERGE_OBJ     : $MERGE_OBJ"
echo "Parameter MERGE_LIST    : $MERGE_LIST"
echo "Parameter SOURCE_URL    : $SOURCE_URL"
echo "Parameter TARGET_URL    : $TARGET_URL"
echo "Parameter WA_PATH       : $WA_PATH"
echo "Parameter LOG_FILE      : $LOG_FILE"
echo "Parameter MSG_FILE      : $MSG_FILE"
echo "Parameter REV_NO        : $REV_NO"
echo "Parameter REVISION      : $REVISION"
echo "Parameter BUILD_TMP_PATH: $BUILD_TMP_PATH"
echo "Parameter PROPNAME      : $PROPNAME"


#---------------------------------------------------------------------------
# 3.) Check if all necessary files exist
#---------------------------------------------------------------------------

# check if itemlist file provied with 2nd parameters exists and is not empty
if [ "$SCOPE" = "itemlist" ] ; then
    if [ ! -e "$MERGE_LIST" ] ; then
        echo "Itemlist $MERGE_LIST does not exist!"
        check_error 1
    fi
fi


#---------------------------------------------------------------------------
# 4.) If scope is limited to single object, perform merge of that object
#---------------------------------------------------------------------------

if [ "$SCOPE" = "single" ] ; then
#   only one merge is required
    echo "scope is single !!!!!"
    merge_single "$MERGE_OBJ"

#   prepare commit message file
    echo "Merged module $MERGE_OBJ from $SOURCE_URL to $TARGET_URL" > $MSG_FILE
    cat $MSG_FILE
    echo "... done."

#   commit changes
    $SVN commit "$WA_PATH" --file "$MSG_FILE" 2>&1
    check_error $?
fi


#---------------------------------------------------------------------------
# 5.) If scope is set to itemlist, then we only have to set a variable
#     and can then perform a single merge for each item in there
#---------------------------------------------------------------------------

if [ "$SCOPE" = "itemlist" ] ; then
#   setting
    echo "scope is item list !!!!!"
#    MERGE_LIST="$MERGE_OBJ"

#   potentially a lot of single file merges are required
    echo ""
    echo "Contents of merge list $MERGE_LIST"
    cat $MERGE_LIST
    echo ""
    cat $MERGE_LIST | while read MERGE_OBJ_FROM_LIST
    do
        echo "Merge object from list = $MERGE_OBJ_FROM_LIST"
        merge_single "$MERGE_OBJ_FROM_LIST"
    done

#   commit changes
    echo "Merged files/dirs as defined in $MERGE_LIST to $TARGET_URL" > $MSG_FILE
    cat $MSG_FILE

    $SVN commit "$WA_PATH" --file $MSG_FILE 2>&1
    check_error $?

fi


#---------------------------------------------------------------------------
# 6.) If scope is set to property, then find out which changes are relevant
#     and then merge those changes
#---------------------------------------------------------------------------

if [ "$SCOPE" = "property" ] ; then

    # define temp files that will be deleted afterwards
    START_DATE=`date +%Y%m%d_%H%M%S`

    MASTER_SOURCE_FILE_MODULE="$BUILD_TMP_PATH/`basename $0`.$START_DATE.master_source_module.txt"
    MASTER_TARGET_FILE_MODULE="$BUILD_TMP_PATH/`basename $0`.$START_DATE.master_target_module.txt"
    
    # get sed string used later to cut out leading URLs
    SOURCE_URL_SED=$( echo "$SOURCE_URL" | sed -e 's/\//\\\//'g )
    TARGET_URL_SED=$( echo "$TARGET_URL" | sed -e 's/\//\\\//'g )
    WA_PATH_SED=$( echo "$WA_PATH" | sed -e 's/\//\\\//'g )
    # replace the colon (:) separator by a pipe (|) separator and append '$' at the end of the string
    # the result is used as input to grep following the 'propget -R' command
    MERGE_OBJ_SED=`echo "$MERGE_OBJ" | sed -e 's/:/\$\|/g' | sed -e 's/$/\$/g'`
    
    
    # write variable values for debugging
    echo "Master source file list : $MASTER_SOURCE_FILE_MODULE"
    echo "Master target file list : $MASTER_TARGET_FILE_MODULE"
#    echo "SED search string (SOURCE_URL_SED)    : $SOURCE_URL_SED"
#    echo "SED search string (TARGET_URL_SED)    : $TARGET_URL_SED"
#    echo "SED search string (WA_PATH_SED)       : $WA_PATH_SED"
    echo ""
    
    # get files from source & target containing required module properties
    # clean leading URLs and write to temp file
    echo "getting files having SVN property $PROPNAME set to $MERGE_OBJ from"
    echo "    ${SOURCE_URL}${REVISION} ..."
    $SVN propget -R "${PROPNAME}" "${SOURCE_URL}${REVISION}" | grep -E `echo "$MERGE_OBJ_SED"` > $MASTER_SOURCE_FILE_MODULE
    echo "... done."
    echo ""
    echo "getting files having SVN property $PROPNAME set to $MERGE_OBJ from"
    echo "    $TARGET_URL ..."
    $SVN propget -R "${PROPNAME}" "$TARGET_URL" | grep -E `echo "$MERGE_OBJ_SED"` > $MASTER_TARGET_FILE_MODULE
    echo "... done."
    echo ""

	# this strange construct with the newline is necessary,
	# 	since sed on AIX does not understand the newline "\n" character
    echo "$MERGE_OBJ" | sed -e 's/:/\
/g' | while read READ_MERGE_OBJ
    do

        echo ""
        echo "###############################################################################"
        echo "# Processing property $READ_MERGE_OBJ:"
        echo "###############################################################################"
        echo ""

        SOURCE_FILE_MODULE="$BUILD_TMP_PATH/`basename $0`.$START_DATE.$READ_MERGE_OBJ.source_module.txt"
        TARGET_FILE_MODULE="$BUILD_TMP_PATH/`basename $0`.$START_DATE.$READ_MERGE_OBJ.target_module.txt"
        SORTED_FILE_MODULE="$BUILD_TMP_PATH/`basename $0`.$START_DATE.$READ_MERGE_OBJ.sorted_module.txt"
        STATUS_CHANGED_FILE="$BUILD_TMP_PATH/`basename $0`.$START_DATE.$READ_MERGE_OBJ.status_changed.txt"
        CHANGED_FILE="$BUILD_TMP_PATH/`basename $0`.$START_DATE.$READ_MERGE_OBJ.changed.txt"
        DELETE_FILE="$BUILD_TMP_PATH/`basename $0`.$START_DATE.$READ_MERGE_OBJ.delete.txt"
        MODLIST_FILE="$BUILD_TMP_PATH/`basename $0`.$START_DATE.$READ_MERGE_OBJ.modlist.txt"
        MERGE_FILE="$BUILD_TMP_PATH/`basename $0`.$START_DATE.$READ_MERGE_OBJ.merge.txt"
    
        # write variable values for debugging
        echo ""
        echo "Temp file (SOURCE_FILE_MODULE)        : $SOURCE_FILE_MODULE"
        echo "Temp file (TARGET_FILE_MODULE)        : $TARGET_FILE_MODULE"
        echo "Temp file (SORTED_FILE_MODULE)        : $SORTED_FILE_MODULE"
        echo "Temp file (STATUS_CHANGED_FILE)       : $STATUS_CHANGED_FILE"
        echo "Temp file (CHANGED_FILE)              : $CHANGED_FILE"
        echo "Temp file (DELETE_FILE)               : $DELETE_FILE"
        echo "Temp file (MODLIST_FILE)              : $MODLIST_FILE"
        echo "Temp file (MERGE_FILE)                : $MERGE_FILE"
        echo ""

        cat "$MASTER_SOURCE_FILE_MODULE" | grep "$READ_MERGE_OBJ$" | sed -e "s/$SOURCE_URL_SED\/\(.*\) - .*/\1/" | sort > $SOURCE_FILE_MODULE
        cat "$MASTER_TARGET_FILE_MODULE" | grep "$READ_MERGE_OBJ$" | sed -e "s/$TARGET_URL_SED\/\(.*\) - .*/\1/" | sort > $TARGET_FILE_MODULE
    
        
        # take filelist from source and target URL and consolidate to one filelist (UNION)
        # this makes sure that not only new and changed files, but also deleted files and
        # files with changed td:module properties will be in that list
        cat "$SOURCE_FILE_MODULE" "$TARGET_FILE_MODULE" | sort -u > $SORTED_FILE_MODULE
        $SVN diff --summarize --ignore-properties "$TARGET_URL" "${SOURCE_URL}${REVISION}" | sort > $STATUS_CHANGED_FILE
        
        # get all files that have been deleted
        echo "determine files that have been deleted ..."
        # the first 9 characters of svn diff result contain information about changes.
        # Careful: with SVN 1.6, there are 9 chars. With previous versions, there are 8 !!!!!!!!!!!!!!!
        cat "$STATUS_CHANGED_FILE" | grep ^D | cut -c 9- | sed -e "s/$TARGET_URL_SED\///" | while read DEL_OBJ
        do
            # "/" in grep parameter must be escaped
            DEL_OBJ_SED="$( echo "$DEL_OBJ" | sed -e 's/\//\\\//'g )"
            # check if object is in list of objects for specific module
            # if it is, it must come from the target URL, since the object was deleted
            CO_RELEVANCE=`cat "$SORTED_FILE_MODULE" | grep "^$DEL_OBJ_SED" | wc -l`
            if  [ "$CO_RELEVANCE" -ne 0 ] ; then
                # found, so delete from target
                echo "$WA_PATH/$DEL_OBJ" >> $DELETE_FILE
                echo "deleting $WA_PATH/$DEL_OBJ."
                $SVN delete "$WA_PATH/$DEL_OBJ"
            fi
        done
        echo "... done."
        echo ""
    
        # get all objects that have been changed or added
        echo "merging added/changed objects belonging to property $READ_MERGE_OBJ ..."
        # objects that have been discovered as deleted, cannot be merged
        # diffing the files is easier than using a loop
        # "^<" indicates that object is only in SORTED_FILE_MODULE, thus NOT deleted
        # result file is list of not-deleted objects with td:module property
        # if object in that module list was actually changed, will be determined afterwards
        touch "$DELETE_FILE"
        diff "$SORTED_FILE_MODULE" "$DELETE_FILE" | grep "^<" | cut -c 3- | sort > $MODLIST_FILE
        
        # search for changed (NOT )deleted objects, cut out that part and clean leading SVN URL
        cat "$STATUS_CHANGED_FILE" | grep -v ^D | cut -c 9- | sed -e "s/^$TARGET_URL_SED\///" > $CHANGED_FILE
        cat "$MODLIST_FILE" | while read UPD_OBJ
        do
            # "/" in grep parameter must be escaped
            UPD_OBJ_SED="$( echo "$UPD_OBJ" | sed -e 's/\//\\\//'g )"
            # check, if object from module list is in list of changed objects at all
            # only then it needs to be merged - performance!
            CO_RELEVANCE=`cat "$CHANGED_FILE" | grep "^$UPD_OBJ_SED" | wc -l`
            if  [ "$CO_RELEVANCE" -ne 0 ] ; then
                echo "$UPD_OBJ" >> $MERGE_FILE
                merge_single "$UPD_OBJ"
            fi
        done
        echo "... done."
        echo ""
    
        # commit changes
        echo "Merged objects ${REVISION} of property $READ_MERGE_OBJ to $TARGET_URL" > $MSG_FILE
        cat $MSG_FILE
    
        $SVN commit "$WA_PATH" --file $MSG_FILE 2>&1
        check_error $?
        echo ""

        # don't delete if files are used for debugging
        rm -f "$SOURCE_FILE_MODULE" "$TARGET_FILE_MODULE" "$SORTED_FILE_MODULE" "$STATUS_CHANGED_FILE"
        rm -f "$CHANGED_FILE" "$DELETE_FILE" "$MODLIST_FILE" "$MERGE_FILE"

    done

fi

#---------------------------------------------------------------------------
# 7.) Cleanup
#---------------------------------------------------------------------------

# don't delete if files are used for debugging
rm -f "$MASTER_SOURCE_FILE_MODULE" "$MASTER_TARGET_FILE_MODULE"

exit 0
