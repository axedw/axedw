#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: check_props.sh 4639 2012-08-09 00:32:28Z k9108499 $
# Last Changed By  : $Author: k9108499 $
# Last Change Date : $Date: 2012-08-09 02:32:28 +0200 (tor, 09 aug 2012) $
# Last Revision    : $Revision: 4639 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/check_props.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : Check consistency of all properties within the implementation area
# Project     : Axfood
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date          Author          Description
# 2010-08-05    S.Sutter        Initial version
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# Check consistency of SVN properties within the implementation area
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -m        Property path       yes         no          Path of code to check within the SVN repository
# -u        SVN URL             yes         no          Subversion main URL containing the code to check
# -c        Property name       yes         no          Name of SVN property to check
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps:
# --------------------------------------------------------------------------
# 1.) Initialization
# 2.) Check-out / update / switch work area to relevant directory
# 3.) get files that have no SVN property set 
# 4.) get files that have wrong SVN property set 
# 5.) get directories WITHOUT children that have NO SVN property set
#                     or WITH children that have AN SVN property set 
#                     or with an unknown SVN property value
# 6.) display  directory inconsistencies 
# 7.) Cleanup & finish
#

#---------------------------------------------------------------------------
# 1.) Initialization
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-u <url>] [-p <proppath>] [-n <propname>] [-h]"
    echo "Check consistency of td:module property within the implementation area"
    echo "       [-u <url>]      : Subversion base URL containing the code to check."
    echo "                         If omitted, use default value SVN_BASE_URL from the .scm_config file:"
    echo "                         ($SVN_BASE_URL)"
    echo "       [-p <proppath>] : Path of code to check within the SVN repository"
    echo "                         If omitted, use default value $PROP_PATH"
    echo "       [-n <propname>] : Name of SVN property to check."
    echo "                         If omitted, use default value $PROP_NAME."
    echo "       [-h]            : Print script syntax help"
    echo ""
    }



# set Defaults
SCRIPT_START_DATE=`date +%Y%m%d_%H%M%S`
LOG_FILE=$BUILD_LOG_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log
touch $LOG_FILE

# default values that can be overwritten by parameters
SVN_URL=$SVN_BASE_URL
PROP_PATH="trunk/code"
PROP_NAME="td:module"

# get parameters
USAGE="u:p:n:h"
while getopts "$USAGE" opt; do
    case $opt in
        u  ) SVN_URL="$OPTARG"
             ;;
        p  ) PROP_PATH="$OPTARG"
             ;;
        n  ) PROP_NAME="$OPTARG"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars



# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1


TMP_ALL_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.all.txt
TMP_PROP_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.prop.txt
TMP_REF_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.ref.txt
TMP_DIST_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.dist.txt
TMP_MISS_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.miss.txt
TMP_DIR_MISS_PROP_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.dir_miss_prop.txt
TMP_DIR_OBSOLETE_PROP_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.dir_obsolete_prop.txt

touch $TMP_MISS_FILE
touch $TMP_DIR_MISS_PROP_FILE
touch $TMP_DIR_OBSOLETE_PROP_FILE


#---------------------------------------------------------------------------
# 2.) Check-out / update / switch work area to relevant directory
#---------------------------------------------------------------------------

# echo ""
# echo "###############################################################################"
# echo "# Update work area for better performance"
# echo "###############################################################################"
# echo ""

# echo "Checking if there are changes in temp code area ..." 
# $SVN status $BUILD_CODE_PATH
if [ $($SVN status $BUILD_CODE_PATH | grep -v "not a working copy" | wc -l) -ne 0 ] ; then
    echo ""
    echo "Uncommitted changes in merge work area $BUILD_CODE_PATH!!!"
    echo "Please clean up before continuing. Exiting ..."
    exit 1
fi
# echo "... done"

# echo ""
td_updwa.sh \
        -w $BUILD_CODE_PATH \
        -u $SVN_URL/$PROP_PATH \
        -l $LOG_FILE \
        -r -rHEAD \
        -q
echo ""
check_error $?


#---------------------------------------------------------------------------
# 3.) get files that have no SVN property set 
#---------------------------------------------------------------------------

# get all files from SVN path (no matter which SVN property is set), and write result to temporary file
$SVN list --depth infinity $BUILD_CODE_PATH | grep -v /$ | sort > $TMP_ALL_FILE

# Build sed search string
SRC_PATH_SED=$( echo $BUILD_CODE_PATH | sed -e 's/\//\\\//'g )

# get all files with specified SVN property from SVN path, write result to temporary file
$SVN propget -R $PROP_NAME $BUILD_CODE_PATH | sed -e "s/$SRC_PATH_SED\/\(.*\) - .*/\1/" | sort > $TMP_PROP_FILE

# compare temporary files - get entries contained in all.txt, but not in prop.txt
# those are the entries with the specified SVN property NOT set
diff $TMP_ALL_FILE $TMP_PROP_FILE | grep "^<" | cut -c 3- > $TMP_MISS_FILE

if [ $( cat $TMP_MISS_FILE | wc -l ) -ne 0 ] ; then
    echo ""
    echo "###############################################################################"
    echo "# The following files don't have the $PROP_NAME property set:"
    echo "###############################################################################"
    echo ""
    cat $TMP_MISS_FILE
    echo ""
fi


#---------------------------------------------------------------------------
# 4.) get files that have wrong SVN property set 
#---------------------------------------------------------------------------

# get reference list
cat $MODULENAME_FILE | sort -u  > $TMP_REF_FILE

# get a distinct list of SVN property values
# Subversion 1.5 and earlier
# svn proplist --verbose --depth infinity $BUILD_CODE_PATH | grep "$PROP_NAME" | sort -u
# Subversion 1.6 and later
# get a list of all properties and print only those lines that follow
$SVN proplist --verbose --depth infinity $BUILD_CODE_PATH | sed -n "/^  $PROP_NAME/{n;p;}" | sed -e "s/^    //" | sort -u  > $TMP_DIST_FILE

# compare temporary files - get entries contained in dist.txt, but not in ref.txt
# those are the invalid property values that do not exist in the reference list
diff $TMP_REF_FILE $TMP_DIST_FILE | grep "^>" | cut -c 3-  | while read READ_MODULE_NAME
do
    echo ""
    echo "###############################################################################"
    echo "# The following files/directories have the SVN property $PROP_NAME set"
    echo "#     to the non-recognized value $READ_MODULE_NAME:"
    echo "###############################################################################"
    echo ""
    $SVN propget -R $PROP_NAME $BUILD_CODE_PATH \
        | grep "${READ_MODULE_NAME}$" \
        | sed -e "s/$SRC_PATH_SED\///" \
        | sed -e "s/ - $READ_MODULE_NAME$//" \
        | sort
    echo ""
done


#---------------------------------------------------------------------------
# 5.) get directories WITHOUT children that have NO SVN property set
#                     or WITH children that have AN SVN property set 
#                     or with an unknown SVN property value
#---------------------------------------------------------------------------

# get all directories from SVN path (no matter which SVN property is set), and write result to temporary file
$SVN list --depth infinity $BUILD_CODE_PATH | grep /$ | sort > $TMP_ALL_FILE

cat $TMP_ALL_FILE | while read DIR_NAME
do
    # reset variables
    DIR_HAS_PROP="false"
    DIR_HAS_CHILD="false"
    
    DIR_PROP=`$SVN propget $PROP_NAME $BUILD_CODE_PATH/$DIR_NAME`
    if [ $( $SVN propget $PROP_NAME $BUILD_CODE_PATH/$DIR_NAME | wc -l ) -eq 1 ] ; then
        DIR_HAS_PROP="true"
    fi
    if [ $($SVN list $BUILD_CODE_PATH/$DIR_NAME | wc -l ) -ne 0 ] ; then
        DIR_HAS_CHILD="true"
    fi
    
    # if directory has the property set, it should have no child
    if [ "$DIR_HAS_PROP" = "true" ] ; then
        if [ "$DIR_HAS_CHILD" = "true" ] ; then
            echo $DIR_NAME >> $TMP_DIR_OBSOLETE_PROP_FILE
        fi
    # if directory has no property set, it should have children, otherwise it's an inconsistency
    else
        if [ ! "$DIR_HAS_CHILD" = "true" ] ; then
            echo $DIR_NAME >> $TMP_DIR_MISS_PROP_FILE
        fi
    fi
done


#---------------------------------------------------------------------------
# 6.) display  directory inconsistencies 
#---------------------------------------------------------------------------

if [ $( cat $TMP_DIR_OBSOLETE_PROP_FILE | wc -l ) -ne 0 ] ; then
    echo ""
    echo "###############################################################################"
    echo "# The following directories have the SVN property $PROP_NAME set"
    echo "#     but have children - please remove that property!"
    echo "###############################################################################"
    echo ""
    cat $TMP_DIR_OBSOLETE_PROP_FILE
    echo ""
fi

if [ $( cat $TMP_DIR_MISS_PROP_FILE | wc -l ) -ne 0 ] ; then
    echo ""
    echo "###############################################################################"
    echo "# The following directories have no SVN property $PROP_NAME set"
    echo "#     but also have NO children - please add that property, if appropriate!"
    echo "###############################################################################"
    echo ""
    cat $TMP_DIR_MISS_PROP_FILE
    echo ""
fi


#---------------------------------------------------------------------------
# 7.) Cleanup & finish
#---------------------------------------------------------------------------

# remove temporary files
rm -f $TMP_ALL_FILE $TMP_PROP_FILE
rm -f $TMP_REF_FILE $TMP_DIST_FILE
rm -f $TMP_DIR_OBSOLETE_PROP_FILE $TMP_DIR_MISS_PROP_FILE

exit 0
