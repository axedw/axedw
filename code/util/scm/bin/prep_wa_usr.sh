#!/usr/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: prep_wa_usr.sh 4475 2012-07-24 12:47:33Z k9108499 $
# Last Changed By  : $Author: k9108499 $
# Last Change Date : $Date: 2012-07-24 14:47:33 +0200 (tis, 24 jul 2012) $
# Last Revision    : $Revision: 4475 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/prep_wa_usr.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : Create complete Informatica and shell work area from SVN for development user
# Project     : 
# Subproject  : 
# --------------------------------------------------------------------------
# Change History
# Date       Author         Description
# 2010-07-28 S.Sutter       Initial version
# 2010-03-08 S.Sutter       Adapt to Axfood specifics
# --------------------------------------------------------------------------
# Description
#   Create complete Informatica and shell work area from SVN for development user
#
# Dependencies
#   none
#
# Parameters
#
# Variables
#
# --------------------------------------------------------------------------
# Processing Steps
# 1.) Initialization
# 2.) Display parameters and variables
# 3.) Check, if target directory already exists. If yes then abort
# 4.) Checkout complete SAMBA folder structure
#
# --------------------------------------------------------------------------
# Open points
# 1.)
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialization 
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. $HOME/.infascm_profile
. basefunc.sh



#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: `basename $0`"
    echo "Create SAMBA code structure from SVN repository"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z $FINAL_LOG_FILE ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$SCRIPT_START_DATE.error
        fi

        mv $LOG_FILE $FINAL_LOG_FILE
        echo "Log file can be found under $FINAL_LOG_FILE"
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END `basename $0` at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

FINAL_LOG_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$SCRIPT_START_DATE.log
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log_msg.txt

SVNSOURCEURL=$SVN_BASE_URL/trunk/code

touch $LOG_MSG_FILE

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1


#---------------------------------------------------------------------------
# 2.) Display parameters and variables
#---------------------------------------------------------------------------

echo "###############################################################################"
echo "START `basename $0` at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Variables used:"
echo "###############################################################################"
echo ""
echo "SVN repository base path (SVN_BASE_URL)      : $SVN_BASE_URL"
echo "SVN directory with SAMBA code (SVNSOURCEURL) : $SVNSOURCEURL"
echo "Local logging path (BUILD_LOG_PATH)          : $BUILD_LOG_PATH"
echo "Final log file (FINAL_LOG_FILE)              : $FINAL_LOG_FILE"
echo ""


#---------------------------------------------------------------------------
# 4.) Create complete ddl & infa work structure
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get code from Subversion and update work area to latest revision"
echo "###############################################################################"
echo ""

echo ""
# get complete Informatica environment
td_updwa.sh -w $HOME/infa_shared -u $SVNSOURCEURL/infa_shared -l $LOG_FILE
check_error $?
echo ""

# get ddl code
td_updwa.sh -w $HOME/dbadmin -u $SVNSOURCEURL/dbadmin -l $LOG_FILE
check_error $?
echo ""


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0

