#!/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: 03_create_sys_build.sh 29483 2019-11-15 11:31:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-11-15 12:31:55 +0100 (fre, 15 nov 2019) $
# Last Revision    : $Revision: 29483 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/03_create_sys_build.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : generate new system test build
# Project     : EDW
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2009-08-13 S.Sutter       Initial version 
# 2011-03-07 S.Sutter       Adapt to Axfood specifics
# 2012-06-30 S.Sutter       Switch to getopts parameter handling; add security checks;
#                           add creation of build results & packages
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# generate system test build tag
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -e        Environment         yes         no          Set environment to build from (default uv2)
# -f        fast mode           no          no          omit security checks (no consistency check of runorder text files)
# -c        check DDL files     no          no          check if DDL are in runorder text files - only relevant if fast mode is NOT active
# -b        create build result no          no          create build result files, default is no
# -p        create packages     no          no          create packages - default is no; if provided, build result files are also created
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps 
# --------------------------------------------------------------------------
# 1.) initialize variables from ini file & function definitions
# 2.) Initialize other variables
# 3.) Check for correct syntax
# 4.) Get previous and new build number
# 5.) Use new build number to determine temp & log file names
# 6.) Check if there are any changes at all. If not then exit.
# 7.) Check runorder file consistency
# 8.) Create new system branch
# 9.) Generate diff between old and new build. Write result to temp file
# 10.) Create build result files: change lists, deploy scripts, packages
# 
# --------------------------------------------------------------------------
# Open points
# --------------------------------------------------------------------------
# 1.) 
# 2.) 
# --------------------------------------------------------------------------


#---------------------------------------------------------------------------
# 1.) initialize variables from ini file & function definitions
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. basefunc.sh

#---------------------------------------------------------------------------
# Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh [-e] [-f] [-c] [-b] [-p] [-h]"
    echo "Creates new system test build tag from system test staging branch."
    echo "       [-e <envname>]: Environment to build from. Default is uv2"
    echo "       [-f]          : Fast mode - omit security checks:"
    echo "                       - no consistency check of runorder text files."
    echo "       [-c]          : Check if DDL are in runorder text files"
    echo "                       - only relevant if fast mode is not active."
    echo "       [-b]          : Create build result files - similar to IT build."
    echo "                       If omitted, no files are created."
    echo "       [-p]          : Create build packages - similar to IT build."
    echo "                       If omitted, no files are created."
    echo "                       If provided, build result files are also created."
    echo "       [-h]          : Print script syntax help."
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured. Left here as template
        if [ -z "$FINAL_LOG_FILE" ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.int.$NEW_BUILD_NO.$SCRIPT_START_DATE.error
        fi
        # move temp log file to final position
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        rm -rf $RUNORDER_TMP_PATH
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#---------------------------------------------------------------------------
# 2.) Initialize other variables
#---------------------------------------------------------------------------

typeset -l ENVNAME

# get parameters
FASTMODE_FLAG="false"
DDLRUNORDER_FLAG="false"
RESULTFILE_FLAG="false"
PACKAGE_FLAG="false"
ENVNAME="uv2"

USAGE="e:fcbph"
while getopts "$USAGE" opt; do
    case $opt in
        e  ) ENVNAME="$OPTARG"
             ;;
        f  ) FASTMODE_FLAG="true"
             ;;
        c  ) DDLRUNORDER_FLAG="true"
             ;;
        b  ) RESULTFILE_FLAG="true"
             ;;
        p  ) PACKAGE_FLAG="true"
             RESULTFILE_FLAG="true"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))

#
# setup environment specific variables
#
. $SCM_BASE/scm/config/.scm_profile.$ENVNAME

# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1

echo ""
echo "###############################################################################"
echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""
echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter -e (Environment): $ENVNAME"
echo "Parameter -f (Fast Mode)  : $FASTMODE_FLAG"
echo "Parameter -c (DDL Check)  : $DDLRUNORDER_FLAG"
echo "Parameter -b (Build Files): $RESULTFILE_FLAG"
echo "Parameter -p (Packages)   : $PACKAGE_FLAG"
echo ""
echo "SVN repository URL        : $SVN_BASE_URL"
echo "Local temp path           : $BUILD_TMP_PATH"
echo "Local logging path        : $BUILD_LOG_PATH"
echo ""


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

# this function is currently not relevant, since there are no parameters
ERROR_CODE=0
if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


#---------------------------------------------------------------------------
# 4.) Get previous and new build number
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get previous and new build number"
echo "###############################################################################"
echo ""

PREV_CNT=`$SVN list $SVN_BASE_URL/tags/sys | grep "^sys-build" | sort | tail -1 | sed -e "s/\/$//g" | wc -l`
# if this is the first build no previous value can exist
if  [ $PREV_CNT -eq 0 ] ; then
    echo "No previous build tag exists. Defaulting to 0."
    PREV_BUILD_NO=0
else
    PREV_BUILD_TAG=`$SVN list $SVN_BASE_URL/tags/sys | grep "^sys-build" | sort | tail -1 | sed -e "s/\/$//g"`
    PREV_BUILD_NO=`echo $PREV_BUILD_TAG | cut -c 11-16`
    echo "Previous build number     : $PREV_BUILD_NO"
    echo "Previous build tag        : $PREV_BUILD_TAG"
fi

NEW_BUILD_NO=`expr $PREV_BUILD_NO + 1 | awk '{printf "%06d", $1}'`
NEW_SYS_TAG=tags/sys/sys-build.$NEW_BUILD_NO.$SCRIPT_START_DATE

echo "New build number          : $NEW_BUILD_NO"
echo "New build tag             : $NEW_SYS_TAG"


#---------------------------------------------------------------------------
# 5.) Use new build number to determine temp & log file names
#---------------------------------------------------------------------------

FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.sys.$NEW_BUILD_NO.$SCRIPT_START_DATE.log
CHANGE_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.sys.$NEW_BUILD_NO.$SCRIPT_START_DATE.changes.txt
touch $CHANGE_FILE
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$NEW_BUILD_NO.$SCRIPT_START_DATE.log_msg.txt

echo "Final log file            : $FINAL_LOG_FILE"
echo "List of changed files     : $CHANGE_FILE"
echo "Temp log message file     : $LOG_MSG_FILE"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 6.) Check if there are any changes at all. If not then exit.
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Checking if there are any changes at all requiring a new build"
echo "###############################################################################"
echo ""

PREV_BUILD_URL=$SVN_BASE_URL/tags/sys/$PREV_BUILD_TAG
COPY_SOURCE_URL=$SVN_BASE_URL/branches/sys/stage
echo "PREV_BUILD_URL            : $PREV_BUILD_URL"
echo "STAGE_URL                 : $COPY_SOURCE_URL"

# since two files "changes.txt" and "code/dbadmin/changes.txt" always exist in previous build
# those files need to be filtered out from diff file
# In order to find only those two files they preceeded with complete URL of previous build
PREV_BUILD_URL_SED=$( echo $PREV_BUILD_URL | sed -e 's/\//\\\//'g )

echo "performing diff ..."
CHANGED_FILE_CNT=`$SVN diff --summarize $PREV_BUILD_URL $COPY_SOURCE_URL \
                    | grep -v "^D\(.*\)$PREV_BUILD_URL_SED\/changes.txt$" \
                    | grep -v "^D\(.*\)$PREV_BUILD_URL_SED\/code\/dbadmin\/changes.txt$" \
                    | wc -l`
if [ $CHANGED_FILE_CNT -eq 0 ] ; then
    echo ""
    echo "No changes in system test staging area since previous build $PREV_BUILD_NO ($PREV_BUILD_TAG) was created!"
    echo "... exiting."
    exit 0
else
    echo "... OK. Will create a new build."
fi
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 7.) Check runorder file consistency
#---------------------------------------------------------------------------

if [ "$FASTMODE_FLAG" == "false" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# Checking if all entries applying DB changes exist as listed in runorder files"
    echo "###############################################################################"
    echo ""

    RUNORDER_INCONSISTENT_FLAG=0
    RUNORDER_TMP_PATH=$BUILD_TMP_PATH/$THIS_SCRIPT.$NEW_BUILD_NO.$SCRIPT_START_DATE.runorder_check
    echo "RUNORDER_TMP_PATH: $RUNORDER_TMP_PATH"
    mkdir ${RUNORDER_TMP_PATH}
    mkdir ${RUNORDER_TMP_PATH}/code
    mkdir ${RUNORDER_TMP_PATH}/code/dbadmin
    echo "Checking out ${COPY_SOURCE_URL}/code/dbadmin ..."
    $SVN checkout --quiet "${COPY_SOURCE_URL}/code/dbadmin" "${RUNORDER_TMP_PATH}/code/dbadmin"
    RUNORDER_CHECKOUT_SUCCESS=$?
    echo "...  done"
    echo "Checking consistency ..."
    if [ $RUNORDER_CHECKOUT_SUCCESS -eq 0 ] ; then
        f_check_runorder_completeness "${RUNORDER_TMP_PATH}/code/dbadmin" "$RUNORDER_TMP_PATH"
        if [ $? -ne 0 ] ; then
            echo "Problems in SVN path ${COPY_SOURCE_URL}/code/dbadmin"
            echo "Inconsistencies in at least one _runorder.txt file!"
            echo "... exiting."
            check_error 2
        fi

        if [ "$DDLRUNORDER_FLAG" == "true" ] ; then
            f_check_runorder_ddlentry "${RUNORDER_TMP_PATH}/code/dbadmin" "$RUNORDER_TMP_PATH"
            if [ $? -ne 0 ] ; then
                echo "Problems in SVN path ${COPY_SOURCE_URL}/code/dbadmin"
                echo "At least one DDL script does not exist in any _runorder.txt file!"
                echo "... exiting."
                check_error 2
            fi

        fi

    fi


    echo "...  done"
    echo ""
    echo "Consistency of _runorder.txt files OK."
    echo ""
    rm -rf $RUNORDER_TMP_PATH
    f_write_date
    echo ""
fi


#---------------------------------------------------------------------------
# 8.) Create new system test tag
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Create new system test tag"
echo "###############################################################################"
echo ""

# set SVN log message and do some logging
echo "Creating system tag $NEW_SYS_TAG for build $NEW_BUILD_NO from system test staging branch" > $LOG_MSG_FILE
cat $LOG_MSG_FILE

# set copy parameters and display them for debugging purpose
COPY_TARGET_URL=$SVN_BASE_URL/$NEW_SYS_TAG
echo "COPY_SOURCE_URL           : $COPY_SOURCE_URL"
echo "COPY_TARGET_URL           : $COPY_TARGET_URL"

# perform the copy
$SVN copy $COPY_SOURCE_URL $COPY_TARGET_URL --file $LOG_MSG_FILE 2>&1
check_error $?

echo "... done"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 9.) Generate diff between old and new build. Write result to changelist file
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Generate diff between old and new build. Write result to changelist file."
echo "###############################################################################"
echo ""

# get difference between previous and current build
# diff result contains complete URL path which is useless for packaging
# thus get rid of it before writing to file

# URL of prev sys build
# change all "\" from path to "/" (relevant for windows systems)
MOD_URL=$(echo $PREV_BUILD_URL | sed -e 's/\//\\\//'g )
# echo "modified PREV_BUILD_URL : $MOD_URL"

# perform diff, filter URL path, write to file
echo "performing diff between $PREV_BUILD_URL"
echo "                    and $COPY_TARGET_URL ..."
# echo $CHANGE_FILE

$SVN diff --summarize $PREV_BUILD_URL $COPY_TARGET_URL | sort | sed -e "s/$MOD_URL//g" > $CHANGE_FILE
check_error $?

echo "... done"
echo "Changes between current build $NEW_BUILD_NO and previous build $PREV_BUILD_NO"
echo "   can be found in $CHANGE_FILE"

# copy change log into build folder
echo "performing copy of build log into $NEW_BUILD_URL"

$SVN import -m 'add change log' $CHANGE_FILE $COPY_TARGET_URL/changes.txt
check_error $?
grep 'code/dbadmin/' $CHANGE_FILE >$CHANGE_FILE.dbadmin
$SVN import -m 'add change log' $CHANGE_FILE.dbadmin $COPY_TARGET_URL/code/dbadmin/changes.txt

echo "... done"
echo "Changes between current build $NEW_BUILD_NO and previous build $PREV_BUILD_NO"
echo "   can also be found in $COPY_TARGET_URL/changes.txt"
echo "   and database specific changes in $COPY_TARGET_URL/dbadmin/changes.txt"
echo ""
f_write_date
echo ""


#---------------------------------------------------------------------------
# 10.) Create build result files: change lists, deploy scripts, packages
#---------------------------------------------------------------------------

if [ "$RESULTFILE_FLAG" = "true" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# Calling td_build_result.sh to create build results"
    echo "###############################################################################"
    echo ""

    td_build_result.sh \
            -m "$THIS_SCRIPT" \
            -e "st" \
            -s "$SCRIPT_START_DATE" \
            -n "$NEW_BUILD_NO" \
            -o "$PREV_BUILD_NO" \
            -t "$COPY_TARGET_URL" \
            -p "$PREV_BUILD_URL" \
            -c "$PREV_CNT" \
            -l "$LOG_FILE"
    check_error $?

    echo ""
    f_write_date
    echo ""

fi

if [ "$PACKAGE_FLAG" = "true" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# Calling td_package.sh to create packages"
    echo "###############################################################################"
    echo ""

    td_package.sh \
            -e "st" \
            -b "$NEW_BUILD_NO" \
            -c "delta"
    check_error $?

    echo ""
    f_write_date
    echo ""

fi

#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0

