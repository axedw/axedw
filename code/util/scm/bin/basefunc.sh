#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: basefunc.sh 10344 2013-09-17 08:16:20Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2013-09-17 10:16:20 +0200 (tis, 17 sep 2013) $
# Last Revision    : $Revision: 10344 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/basefunc.sh $
#---------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : Base functions to be included by CM scripts
# Project     : Axfood
# Subproject  : all
# --------------------------------------------------------------------------
# Change History
# Date       Author         Description
# 2008-08-17 S.Sutter       Initial version 
# 2008-04-02 T.Bachmann     function init_vars (Initialize Default-Vars) and
#                           function parse_paras (parses Parameters) added
#
# --------------------------------------------------------------------------
# Description
#   Base functions that are to be included by all Config. Mgmt. scripts
#
# Dependencies
#   parent  : Called by all Config. Mgmt. scripts
#
# Parameters: none
#
# Variables (please add any other variables that you may use):
#   Variables that are used here are defined by the calling script
#   
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) function wlog
# 2.) function check_error
# 3.) function f_copy_with_path
# 4.) function init_vars
# 6.) function generate_new_release_number
# 8.) function f_write_date
# 9.) Functions to write and read content from restart file
# 
# --------------------------------------------------------------------------
# Open points
# --------------------------------------------------------------------------

###########################################################################
# 1.) function wlog
#     Write to stdout & log file
###########################################################################

wlog ()
    {
        echo "$@" | tee -a $LOG_FILE
    }


###########################################################################
# 2.) function check_error
#     Check if last command performed with error, if yes then exit
###########################################################################

check_error ()
    {
    if [ $1 -ne 0 ] ; then
        FINAL_LOG_FILE=`echo $FINAL_LOG_FILE | sed 's/log$/error/'`
        echo "An error occured! Error code is $1. Exiting $0 ..."
        exit $1
    fi
    }


###########################################################################
# 3.) f_copy_with_path
#     Copy file from $1 to $2. Create parents if necessary.
###########################################################################

f_copy_with_path ()
    {
    if [ -z "$2" -o -z "$1" ] ; then
        echo "Wrong number of parameters!"
        echo "f_copy_with_path <source-file> <target-file>"
        return 2
    fi
    if [ -e $1 ] ; then
        TARGET_DIR=`dirname $2`
        if [ ! -d "$TARGET_DIR" ] ; then
            mkdir -p "$TARGET_DIR" 2>&1
        fi
        cp -p $1 $2 2>&1
        return $?
    else
        echo "Copy source $1 does not exist!"
        return 1
    fi
    }


###########################################################################
# 4.) function init_vars
#     Initialise Default- Values
###########################################################################

init_vars()
{
    # get global script start date
    if [ ! "$SCRIPT_START_DATE" ] ; then
        SCRIPT_START_DATE=`date +%Y%m%d_%H%M%S`
    fi

    # determine SVN repository base name
    SVN_REP_NAME=$( basename $SVN_BASE_URL )
    # echo $SVN_REP_NAME

    # determine temporary log file name
    if [ ! "$LOG_FILE" ] ; then
        LOG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log
    fi

    # write beginning of log
    if [ ! -f "$LOG_FILE" ] ; then
        touch "$LOG_FILE"
        echo ""
        echo "###############################################################################" >> $LOG_FILE
        echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"                      >> $LOG_FILE
        echo "###############################################################################" >> $LOG_FILE
    fi


    # make sure all required folders exist
    test ! -d $BUILD_CODE_PATH && mkdir $BUILD_CODE_PATH
    test ! -d $BUILD_WA_PATH && mkdir $BUILD_WA_PATH
    test ! -d $BUILD_MRG_PATH && mkdir $BUILD_MRG_PATH
    test ! -d $BUILD_EXP_PATH && mkdir $BUILD_EXP_PATH
    test ! -d $BUILD_TMP_PATH && mkdir $BUILD_TMP_PATH
    test ! -d $BUILD_LOG_PATH && mkdir $BUILD_LOG_PATH
   
}


###########################################################################
# 6.) generate_new_release_number
#     
###########################################################################

generate_new_release_number()
{
    typeset new_reltype=$1
    typeset cur_relno=$2
    typeset major_relname
    typeset major_relyear
    typeset -Z2 major_relno
    typeset -Z3 minor_relno
    typeset -Z3 patch_relno
    typeset -Z3 hotfix_relno
    
    major_relname=`echo $cur_relno | cut -f1 -d.`
    minor_relno=`echo $cur_relno | cut -f2 -d.`
    patch_relno=`echo $cur_relno | cut -f3 -d.`
    hotfix_relno=`echo $cur_relno | cut -f4 -d.`
    case ${new_reltype} in
        major)
            major_relyear=`echo $major_relname | cut -c 1-4`
            major_relno=`echo $major_relname | cut -c 6-7`
            if [ "$major_relyear" != `date +%Y` ] ; then
                major_relyear=`date +%Y`
                major_relno=1
            else
                ((major_relno=major_relno+1))
            fi
            major_relname=${major_relyear}R${major_relno}
            minor_relno=0
            patch_relno=0
            hotfix_relno=0
            ;;
        minor)
            ((minor_relno=minor_relno+1))
            patch_relno=0
            hotfix_relno=0
            ;;
        newpatch)
            ((patch_relno=patch_relno+1))
            hotfix_relno=0
            ;;
        hotfix)
            ((hotfix_relno=hotfix_relno+1))
            ;;
        initrel)
            major_relname=`date +%Y`R01
            minor_relno=0
            patch_relno=0
            hotfix_relno=0
            ;;
        *)
            ;;
    esac
    
    NEW_RELEASE_NUMBER="${major_relname}.${minor_relno}.${patch_relno}.${hotfix_relno}"
    echo "New release number: $NEW_RELEASE_NUMBER"

    return
}


###########################################################################
# 7.) extract_release_number_from_tag
#     
###########################################################################

f_extract_release_number_from_tag()
{

    return
}


###########################################################################
# 8.) f_check_release_number_format
#     
###########################################################################

f_check_release_number_format()
{

    typeset release_no=$1
    typeset release_format="2[0-9][0-9][0-9]R[0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]"
    typeset -i return_value
    return_value=`echo "$release_no" | grep "$release_format" | wc -l`

    return $return_value

}


###########################################################################
# 9.) f_write_date
#     
###########################################################################

f_write_date()
{

    date "+%Y-%m-%d %T"
}


###########################################################################
# 10.) Functions to write and read content from restart file
#     
###########################################################################

f_get_par_from_restart_file()
{

    typeset restart_file=$1
    typeset parameter_name=$2

    GLOBAL_RETURN_VALUE=`grep "^PARAMETER#${parameter_name}#" $restart_file | sed -e "s/^PARAMETER#${parameter_name}#//"`

}

f_write_par_to_restart_file()
{

    typeset restart_file=$1
    typeset parameter_name=$2
    typeset parameter_value=$3

    echo "PARAMETER#${parameter_name}#${parameter_value}"       >> $RESTART_FILE

}

f_get_variable_from_restart_file()
{

    typeset restart_file=$1
    typeset variable_name=$2

    GLOBAL_RETURN_VALUE=`grep "^VARIABLE#${variable_name}#" $restart_file | sed -e "s/^VARIABLE#${variable_name}#//"`

}

f_write_variable_to_restart_file()
{

    typeset restart_file=$1
    typeset variable_name=$2
    typeset variable_value=$3

    echo "VARIABLE#${variable_name}#${variable_value}"       >> $RESTART_FILE

}

f_check_restart_point()
{

    typeset restart_file=$1
    typeset restart_point=$2

    RESTARTPOINT_FINISHED="false"
    if [ "$RESTART_FLAG" == "true" -a `grep "^RESTARTPOINT#${restart_point}" $restart_file | wc -l` -gt 0 ] ; then
        RESTARTPOINT_FINISHED="true"
    fi

}

f_write_restart_point()
{

    typeset restart_file=$1
    typeset restart_point=$2

    echo "RESTARTPOINT#${restart_point}"                     >> $RESTART_FILE

}

f_check_module_finished()
{

    typeset restart_file=$1
    typeset module_name=$2

    MODULE_FINISHED_FLAG="false"
    if [ "$RESTART_FLAG" == "true" -a `grep "^FINISHEDMODULE#${module_name}" $restart_file | wc -l` -gt 0 ] ; then
        MODULE_FINISHED_FLAG="true"
    fi

}

f_write_module_finished()
{

    typeset restart_file=$1
    typeset module_name=$2

    echo "FINISHEDMODULE#${module_name}"                     >> $RESTART_FILE

}


###########################################################################
# 11.) f_cleanup_work_area
#     
###########################################################################

f_cleanup_work_area()
{

    typeset work_area_path=$1
    typeset filename

    echo "Revert all changes existing in $work_area_path ..."
    $SVN revert -R -q $work_area_path
    echo "... done"

    echo "Check for any files not under SVN control and remove them ..."
    $SVN status $BUILD_MRG_PATH 2>/dev/null | cut -c 9- | while read filename
    do
        echo "removing ${filename}"
        rm -rf "${filename}"
    done
    echo "... done"

}


###########################################################################
# 12.) f_check_runorder_completeness
#     
###########################################################################

f_check_runorder_completeness()
{

    typeset runorder_root_path=$1
    typeset code_root_path=$2
    typeset code_root_path_sed
    typeset runordertxt
    typeset runordertxt_stripped
    typeset ro_command
    typeset ro_file
    typeset -i file_not_found=0
    typeset -i ro_file_ok=0

    
    echo ""
    code_root_path_sed=$( echo $code_root_path | sed -e 's/\//\\\//'g )
    find "$runorder_root_path" -name "_runorder.txt" | while read runordertxt
    do
        # echo  "Parsing $runordertxt ..."
		# ignore lines that start with --, treated as comments
        grep -v '^--' $runordertxt | while read ro_command ro_file
        do
            # echo "file: $code_root_path/$ro_file"
            db_runfile=$code_root_path/$ro_file
            if [ ! -e $db_runfile ] ; then
                runordertxt_stripped=$( echo $runordertxt | sed -e "s/$code_root_path_sed\///" )
                echo "$runordertxt_stripped: file $ro_file does not exist"
                file_not_found=1
            fi
        done
    done
    echo ""
    return $file_not_found

}


###########################################################################
# 13.) f_check_runorder_ddlentry
#     
###########################################################################

f_check_runorder_ddlentry()
{

    typeset runorder_root_path=$1
    typeset code_root_path=$2
    typeset code_root_path_sed
    typeset runordertxt
    typeset runordertxt_consolidated="$code_root_path/runorder.consolidated.txt"
    typeset ro_file
    typeset -i entry_not_found=0

    
    echo ""
    # consolidate all _runorder.txt files into one large file
    find "$runorder_root_path" -name "_runorder.txt" | while read runordertxt
    do
        cat "$runordertxt" | grep -v '^--' >> "$runordertxt_consolidated"
    done
	(
		cd "$runorder_root_path"
		find "." -name "*.btq" | while read filename
		do
			# echo  "Looking for entry $runordertxt ..."
			ro_file="code/dbadmin/$filename"

			if [ $( grep -c "$ro_file" "$runordertxt_consolidated" ) -eq 0 ] ; then
				echo "$filename has no entry in any _runorder.txt file"
				entry_not_found=1
			fi

		done
	)
    echo ""
    rm -rf "$runordertxt_consolidated"
    return $entry_not_found

}


###########################################################################
# 14.) f_extract_build_number
#     
###########################################################################

f_extract_build_number()
{

    typeset urlpath=$1
    typeset sed_str
    typeset maindir

    # echo "urlpath : $urlpath"
    # cleanse the url path as much as possible
    # 1. remove the http... base url
    sed_str=$( echo $SVN_BASE_URL | sed -e 's/\//\\\//'g )
    # echo "sed_str : $sed_str"
    urlpath=$( echo $urlpath | sed -e "s/$sed_str\///" )
    # echo "urlpath : $urlpath"

    # 2. remove trailing "code" directory
    urlpath=$( echo $urlpath | sed -e 's/\/code$//' )
    # echo "urlpath : $urlpath"

    # 3. remove trailing "code" directory plus revision number
    urlpath=$( echo $urlpath | sed -e 's/\/code@[0-9]*$//' )
    # echo "urlpath : $urlpath"

    # get main directory name 
    maindir=`basename $urlpath`
    # echo "maindir : $maindir"

    # if main directory is a staging directory then return the complete path
    if [ "$maindir" == "stage" ] ; then
        R_BUILD_NO="$urlpath"
        return 0
    fi

    # check if it is an integration build
    if [ $( echo "$maindir" | grep "int-build" | wc -l ) -ne 0 ] ; then
        R_BUILD_NO=$( echo "$maindir" | cut -d "." -f 2 )
        R_ENV_MAIN="int"
        return 0
    fi

    # check if it is a system build
    if [ $( echo "$maindir" | grep "sys-build" | wc -l ) -ne 0 ] ; then
        R_BUILD_NO=$( echo "$maindir" | cut -d "." -f 2 )
        R_ENV_MAIN="sys"
        return 0
    fi

    # check if it is a release
    if [ $( echo "$maindir" | grep "rel-" | wc -l ) -ne 0 ] ; then
        R_BUILD_NO=$( echo "$maindir" | cut -d "-" -f 2 | cut -d "." -f -4 )
        R_ENV_MAIN="rel"
        # it could als be a hotfix branch, in that case provide prefix to distinguish from regular release
        if [ $( echo "$urlpath" | grep "hfx" | wc -l ) -ne 0 ] ; then
            R_BUILD_NO="hfx-${R_BUILD_NO}"
            R_ENV_MAIN="hfx"
        fi
        return 0
    fi

    # in any other case, just return the cleansed url path
    R_BUILD_NO=$urlpath

}


###########################################################################
# 15.) f_find_value_in_param_list
#     
###########################################################################

f_find_value_in_param_list()
{
    typeset param_list=$1
    typeset search_val=$2
    typeset separator=$3

    # convert parameter list into separate lines and scan for search value
    echo "$param_list" | tr "[$separator]" '[\n]' | while read item; do
        if [ "$item" = "$search_val" ] ; then
            # search value exists in parameter list
            return 0
        fi
    done
    # search value not found - return error code 1
    return 1

}


