#!/usr/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# --------------------------------------------------------------------------
# ID               : $Id: td_updwa.sh 29383 2019-10-29 14:23:22Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-10-29 15:23:22 +0100 (tis, 29 okt 2019) $
# Last Revision    : $Revision: 29383 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/td_updwa.sh $
# --------------------------------------------------------------------------
# SVN Info END
#---------------------------------------------------------------------------

## Purpose     : update work copy from specified URL
# Project     : Axfood
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History:
# --------------------------------------------------------------------------
# Date       Author         Description
# 2008-08-06 S.Sutter       Initial version
# 2009-04-07 T.Bachmann     Parameter assignment changed
# 2011-03-07 S.Sutter       Adapt to Axfood specifics
#
# --------------------------------------------------------------------------
# Description:
# --------------------------------------------------------------------------
# check if work area exists; if yes, then switch, else check-out
#
# --------------------------------------------------------------------------
# Dependencies:
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -w        Work area path      yes         yes         Work area path that needs to be updated
# -u        SVN URL             yes         yes         SVN URL path
# -r        SVN revision        yes         no          
# -l        Log file            yes         no          name (and path) of logfile (if empty use default)
# -q        Quiet               no          no          Suppress SVN command details
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables (please add any other variables that you may use):
# --------------------------------------------------------------------------
#   
#
# --------------------------------------------------------------------------

. $HOME/.scm_profile
. basefunc.sh

# set Defaults
SCRIPT_START_DATE=`date +%Y%m%d_%H%M%S`
LOG_FILE=$BUILD_TMP_PATH/`basename $0 .sh`.$SCRIPT_START_DATE.log


USAGE="w:u:r:l:qh"
while getopts "$USAGE" opt; do
    case $opt in
        w  ) WA_PATH="$OPTARG"
             OPT_ISSET_WAPATH="true"
             ;;
        u  ) SVN_URL="$OPTARG"
             OPT_ISSET_URL="true"
             ;;
        r  ) REV_NO="$OPTARG"
             OPT_ISSET_REVISION="true"
             ;;
        l  ) LOG_FILE="$OPTARG"
             OPT_ISSET_LOGFILE="true"
             ;;
        q  ) OPT_ISSET_QUIET="true"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))


# check, if all necessary arguments are set
if [ ! "$OPT_ISSET_WAPATH" ] ; then
    echo "Option -w (work area path) not specified!"
    check_error 1
fi
if [ ! "$OPT_ISSET_URL" ] ; then
    echo "Option -u (URL) not specified!"
    check_error 1
fi
if [ "$OPT_ISSET_QUIET" ] ; then
    OPTION_QUIET="--quiet"
else
    OPTION_QUIET=""
fi

# check if work area exists - if not then check-out, otherwise update or switch
echo "Checking, if work area ${WA_PATH} exists."
$SVN info $WA_PATH > /dev/null 2>&1

if [ "$?" -ne "0" ] ; then
    echo "Work area does not exist yet."
    echo "Checking out ${SVN_URL} ${REV_NO} ..."
    $SVN checkout $REV_NO $OPTION_QUIET "$SVN_URL" "$WA_PATH"
    check_error $?
else
    CURSVNURL=`$SVN info $WA_PATH | grep "^URL: " | cut -c 6-`
    if [ "$CURSVNURL" == "$SVN_URL" ] ; then
        echo "Build work area exists and points to ${SVN_URL}."
        echo "Updating ..."
        $SVN update $OPTION_QUIET "$WA_PATH" $REV_NO
        check_error $?
    else
        echo "Build work area exists."
        echo "Switching to ${SVN_URL}..."
        $SVN switch --ignore-ancestry $OPTION_QUIET "$SVN_URL" "$WA_PATH" $REV_NO
        check_error $?
    fi
fi

echo "... done."

