#!/usr/bin/ksh
#
# --------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT LINES!)
# --------------------------------------------------------------------------
# ID                   : $Id: prep_wa_sys.sh 29489 2019-11-15 15:03:01Z  $
# Last ChangedBy       : $Author: $
# Last ChangedDate     : $Date: 2019-11-15 16:03:01 +0100 (fre, 15 nov 2019) $
# Last ChangedRevision : $Revision: 29489 $
# Subversion URL       : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/scm/bin/prep_wa_sys.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Purpose     : Deploy build into test environment from SVN tag
# Project     :
# Subproject  : all
#
# --------------------------------------------------------------------------
# Change History
# --------------------------------------------------------------------------
# Date       Author         Description
# 2011-03-16 S.Sutter       Initial version
# 2012-05-08 S.Sutter       adapt to new impxml script
#
# --------------------------------------------------------------------------
# Description
# --------------------------------------------------------------------------
# Deploy build into system test environment from SVN tag
#
# --------------------------------------------------------------------------
# Dependencies
# --------------------------------------------------------------------------
# none
#
# --------------------------------------------------------------------------
# Parameters:
# --------------------------------------------------------------------------
# Parameter Description         Requires    Is          Comment
#                               additional  mandatory
#                               parameter
# --------------------------------------------------------------------------
# -e        Environment         yes         yes
# -n        Build number        yes         yes
# -i        Perform Infa import no          no          Defaults to "no import", if not provided
# -d        Apply DB changes    no          no          Defaults to "don't apply changes", if not provided
# -s        Restart             no          no
# -h        Print script help   no          no
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Variables
# --------------------------------------------------------------------------
#
# --------------------------------------------------------------------------
# Processing Steps
# --------------------------------------------------------------------------
# 1.) Initialization
# 2.) Initialize other variables
# 3.) Check for correct syntax
# 4.) Display parameters and variables
# 5.) Get tag fitting to build number - exit if unsuccessful
# 6.) Get tag fitting to build currently deyployed - exit if unsuccessful
# 7.) Get difference between previous and current deployment
# 8.) Get code from Subversion build tag and update work area
# 9.) get list of changed DDLs and display them for information purpose
#     execute the changes in the database
# 10.)get a list of changed Informatica objects and import them 
# 11.)make shure the Informatica parameter file contains an entry for the database environment 
#
# --------------------------------------------------------------------------
# Open points
#---------------------------------------------------------------------------
# 1.)
# --------------------------------------------------------------------------

#---------------------------------------------------------------------------
# 1.) Initialization
#---------------------------------------------------------------------------

THIS_SCRIPT=`basename $0 .sh`
. $HOME/.scm_profile
. $HOME/.infascm_profile
. basefunc.sh
. infafunc.sh

typeset -Z6 CUR_BUILD_NO
typeset -Z6 PRV_BUILD_NO
typeset -l ENVNAME

#---------------------------------------------------------------------------
#  Print script syntax and help
#---------------------------------------------------------------------------

print_help ()
    {
    echo "Usage: ${THIS_SCRIPT}.sh -e <environment> -n <buildno> [-i] [-d] [-s] [-h]"
    echo "Deyploy code from any SVN path"
    echo "      -e <environment> : environment name (st1, st2, ...)"
    echo "      -n <buildno>     : valid build number from an existing build tag"
    echo "      [-i]             : import Informatica objects - defaults to \"no import\", if not provided"
    echo "      [-d]             : apply DB changes - defaults to \"don't apply changes\", if not provided"
    echo "      [-s]             : Restart the script and skip steps already performed"
    echo "      [-t]             : Do not execute install_stage.sh"
    echo "      [-h]             : Print script syntax help"
    echo ""
    }

#---------------------------------------------------------------------------
#  Standard procedure executed at every exit, with or w/o error
#---------------------------------------------------------------------------

at_exit ()
    {
#       Cleanup procedures at exit
#       If var for final log file was not defined yet,
#       then an error early in the script has occured.
        if [ -z $FINAL_LOG_FILE ] ; then
            FINAL_LOG_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.$ENVNAME.$CUR_BUILD_NO.$SCRIPT_START_DATE.error
        fi
        # move temp log file to final position
        if [ -e "$LOG_FILE" ] ; then
            mv $LOG_FILE $FINAL_LOG_FILE
            echo ""
            echo "###############################################################################"
            echo ""
            echo "Log file can be found under $FINAL_LOG_FILE"
        fi
        # remove other temp files
        rm -f $LOG_MSG_FILE
        echo ""
        echo "###############################################################################"
        echo "END $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
        echo "###############################################################################"
        echo ""
    }
# trap makes sure that at_exit is always called at script exit,
# with or without error, even with CTRL-c
trap at_exit EXIT


#---------------------------------------------------------------------------
# 2.) Initialize other variables
#---------------------------------------------------------------------------

RUN_INFA_IMPORT="false"
RUN_DB_APPLY="false"
RESTART_FLAG="false"
RESTART_FILE=$BUILD_LOG_PATH/`basename $SVN_BASE_URL`.$THIS_SCRIPT.restart.txt
RUN_INSTALL_STAGE="true"

# get parameters
USAGE="e:n:idsht"
while getopts "$USAGE" opt; do
    case $opt in
        e  ) ENVNAME="$OPTARG"
             OPT_ISSET_ENV="true"
             ;;
        n  ) CUR_BUILD_NO="$OPTARG"
             OPT_ISSET_BUILDNO="true"
             ;;
        i  ) RUN_INFA_IMPORT="true"
             ;;
        d  ) RUN_DB_APPLY="true"
             ;;
        s  ) if [ -e "$RESTART_FILE" ] ; then
                 RESTART_FLAG="true"
             fi
             ;;
        t  ) RUN_INSTALL_STAGE="false"
             ;;
        h  ) print_help
             exit 0
             ;;
        \? ) print_help
             exit 1
             ;;
    esac
done
shift $(($OPTIND - 1))


# Use init_vars for setting up $SCRIPT_START_DATE and $LOG_FILE
init_vars

# some standard variables required to run this script
FINAL_LOG_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$CUR_BUILD_NO.$SCRIPT_START_DATE.log
STATUS_CHANGED_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$CUR_BUILD_NO.$SCRIPT_START_DATE.changed_items.txt
INFA_CHANGED_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$CUR_BUILD_NO.$SCRIPT_START_DATE.infa_changed.txt
INFA_DELETED_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$CUR_BUILD_NO.$SCRIPT_START_DATE.infa_deleted.txt
DB_CHANGED_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$CUR_BUILD_NO.$SCRIPT_START_DATE.db_changed.txt
DB_DELETED_FILE=$BUILD_LOG_PATH/$SVN_REP_NAME.$THIS_SCRIPT.$ENVNAME.$CUR_BUILD_NO.$SCRIPT_START_DATE.db_deleted.txt
LOG_MSG_FILE=$BUILD_TMP_PATH/$THIS_SCRIPT.$SCRIPT_START_DATE.log_msg.txt
touch $LOG_MSG_FILE


# re-route all output to screen and logfile simultaneously
tee $LOG_FILE >/dev/tty |&
exec 1>&p
exec 2>&1


#---------------------------------------------------------------------------
# 3.) Check for correct syntax
#---------------------------------------------------------------------------

ERROR_CODE=0

if [ ! "$OPT_ISSET_ENV" ] ; then
        echo "Error: Parameter -e (environment) not provided!"
        ERROR_CODE=2
fi
if [ ! "$OPT_ISSET_BUILDNO" ] ; then
        echo "Error: Parameter -n (build number) not provided!"
        ERROR_CODE=2
fi

if [ $ERROR_CODE -ne 0 ] ; then
    echo ""
    print_help
    exit 1
fi


#---------------------------------------------------------------------------
# 4.) Display parameters and variables
#---------------------------------------------------------------------------

echo "###############################################################################"
echo "START $THIS_SCRIPT.sh at `date +%Y-%m-%d` `date +%H:%M:%S`"
echo "###############################################################################"
echo ""

if [ "$RESTART_FLAG" == "true" ] ; then
    echo ""
    echo "###############################################################################"
    echo "# Restart parameter provided - will use restart file $RESTART_FILE:"
    echo "###############################################################################"
    cat $RESTART_FILE
    echo "###############################################################################"
    echo ""
    f_get_par_from_restart_file $RESTART_FILE ENVNAME
    ENVNAME=$GLOBAL_RETURN_VALUE
    f_get_par_from_restart_file $RESTART_FILE CUR_BUILD_NO
    CUR_BUILD_NO=$GLOBAL_RETURN_VALUE
    f_get_par_from_restart_file $RESTART_FILE RUN_INFA_IMPORT
    RUN_INFA_IMPORT=$GLOBAL_RETURN_VALUE
    f_get_par_from_restart_file $RESTART_FILE RUN_DB_APPLY
    RUN_DB_APPLY=$GLOBAL_RETURN_VALUE

    f_get_variable_from_restart_file "$RESTART_FILE" SVNSOURCEURL
    SVNSOURCEURL=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" CUR_BUILD_NO
    CUR_BUILD_NO=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" CUR_BUILD_ENV
    CUR_BUILD_ENV=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" PREV_SVNSOURCEURL
    PREV_SVNSOURCEURL=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" PRV_BUILD_NO
    PRV_BUILD_NO=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" PRV_BUILD_ENV
    PRV_BUILD_ENV=$GLOBAL_RETURN_VALUE
    f_get_variable_from_restart_file "$RESTART_FILE" PREV_BUILD_CNT
    PREV_BUILD_CNT=$GLOBAL_RETURN_VALUE

else
    rm -f $RESTART_FILE
fi

echo ""
echo "###############################################################################"
echo "# Parameters and variables used:"
echo "###############################################################################"
echo ""
echo "Parameter -e (Environment)      : $ENVNAME"
echo "Parameter -r (Build number)     : $CUR_BUILD_NO"
echo "Parameter -i (Run Infa import)  : $RUN_INFA_IMPORT"
echo "Parameter -d (Apply DB changes) : $RUN_DB_APPLY"
echo "Parameter -s (Restart)          : $RESTART_FLAG"
echo "Parameter -t (install_stage)    : $RUN_INSTALL_STAGE"
echo ""
echo ""
echo "SVN repository base path        : $SVN_BASE_URL"
echo "Local logging path              : $BUILD_LOG_PATH"
echo "Final log file                  : $FINAL_LOG_FILE"
echo ""


# The initial checks are not required if the script is called with the restart parameter
# in that case the required variable values will be taken from the restart file
f_check_restart_point "$RESTART_FILE" INITIALCHECK
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

#---------------------------------------------------------------------------
# 5.) Get tag fitting to build number - exit if unsuccessful
#---------------------------------------------------------------------------

    echo ""
    echo "###############################################################################"
    echo "# Get tag fitting to build number"
    echo "###############################################################################"
    echo ""

    echo "Searching for sys build $CUR_BUILD_NO"
    BUILD_CNT=`$SVN list ${SVN_BASE_URL}/tags/sys | grep sys-build.${CUR_BUILD_NO} | wc -l`
    # echo $BUILD_CNT
    if [ $BUILD_CNT -eq 0 ] ; then
        echo "No tag for build $CUR_BUILD_NO found. Exiting ..."
        exit 1
    elif [ $BUILD_CNT -eq 1 ] ; then
        SVN_BUILD_TAG=${SVN_BASE_URL}/tags/sys/`$SVN list ${SVN_BASE_URL}/tags/sys | grep sys-build.${CUR_BUILD_NO} | sed -e "s/\/$//g"`
        SVN_BUILD_REV=`$SVN info $SVN_BUILD_TAG/code/infa_xml | grep Revision:  | cut -d" " -f2`
        echo "Found one tag for build $CUR_BUILD_NO:"
        echo $SVN_BUILD_TAG@$SVN_BUILD_REV
    else
        echo "More than one tag for build $CUR_BUILD_NO found."
        echo "Something is really wrong. Exiting ..."
        exit 1
    fi
    echo ""


#---------------------------------------------------------------------------
# 6.) Get tag fitting to build currently deyployed - exit if unsuccessful
#---------------------------------------------------------------------------

    echo ""
    echo "###############################################################################"
    echo "# Get tag fitting to build currently deyployed"
    echo "###############################################################################"
    echo ""

    PREV_BUILD_PATH=`$SVN info $INFA_SHARED_DIR | grep URL:  | cut -d" " -f2`
    PRV_BUILD_NO=`echo $PREV_BUILD_PATH | cut -d/ -f8 | cut -d. -f2`
    PREV_BUILD_REV=`$SVN info $INFA_SHARED_DIR | grep Revision:  | cut -d" " -f2`
    if [ -z "$PREV_BUILD_PATH" ] ; then
        PREV_BUILD_CNT=0
        PRV_BUILD_NO=0
        echo "No currently installed build was found. Considering all items as new."
        echo ""
    else
        PREV_SVN_BUILD_TAG=`dirname $PREV_BUILD_PATH`
        echo "Searching for build $PRV_BUILD_NO in Subversion tags"
        
        if [ $( $SVN info ${PREV_SVN_BUILD_TAG}@${PREV_BUILD_REV} | grep -i "Not a valid URL" | wc -l ) -ne 0 ] ; then
            echo "Previous Build URL ${PREV_SVN_BUILD_TAG}@${PREV_BUILD_REV} not found!"
            echo "Exiting ..."
            exit 1
        else
            echo "Found one tag for previous build ${PRV_BUILD_NO}:"
            echo "${PREV_SVN_BUILD_TAG}@${PREV_BUILD_REV}"
            echo ""
            PREV_BUILD_CNT=1
        fi
    fi



    echo ""
    SVNSOURCEURL=$SVN_BUILD_TAG/code
    PREV_SVNSOURCEURL=${PREV_SVN_BUILD_TAG}@${PREV_BUILD_REV}
    echo "SVN build URL             : ${SVNSOURCEURL}@${SVN_BUILD_REV}"
    echo "Previous SVN build URL    : $PREV_SVNSOURCEURL"
    echo ""

    # write parameters in case of a restart
    f_write_par_to_restart_file "$RESTART_FILE" "ENVNAME" "$ENVNAME"
    f_write_par_to_restart_file "$RESTART_FILE" "CUR_BUILD_NO" "$CUR_BUILD_NO"
    f_write_par_to_restart_file "$RESTART_FILE" "RUN_INFA_IMPORT" "$RUN_INFA_IMPORT"
    f_write_par_to_restart_file "$RESTART_FILE" "RUN_DB_APPLY" "$RUN_DB_APPLY"

    f_write_variable_to_restart_file "$RESTART_FILE" "SVNSOURCEURL" "$SVNSOURCEURL"
    f_write_variable_to_restart_file "$RESTART_FILE" "PREV_SVNSOURCEURL" "$PREV_SVNSOURCEURL"
    f_write_variable_to_restart_file "$RESTART_FILE" "PRV_BUILD_NO" "$PRV_BUILD_NO"
    f_write_variable_to_restart_file "$RESTART_FILE" "PREV_BUILD_CNT" "$PREV_BUILD_CNT"

    f_write_restart_point $RESTART_FILE INITIALCHECK

    f_write_date
    echo ""

else
    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping initial checks"
    echo "###############################################################################"
    echo ""
fi


#---------------------------------------------------------------------------
# 7.) Get difference between previous and current deployment
#---------------------------------------------------------------------------

f_check_restart_point "$RESTART_FILE" GETDIFFPREVCURR
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# Get difference between previous and current deployment"
    echo "###############################################################################"
    echo ""

    # if previous and current build are identical then do nothing and exit
    if [ "${PREV_SVNSOURCEURL}" = "${SVNSOURCEURL}@${SVN_BUILD_REV}" ] ; then
        echo "Installed build URL $PREV_SVNSOURCEURL and"
        echo " new build URL ${SVNSOURCEURL}@${SVN_BUILD_REV} are identical!"
        echo "No further action required by this script."
        echo "If really necessary, use standard SVN methods to update/revert/cleanup your work area."
        echo "Exiting ..."
        echo ""
        exit 0
    fi

    echo "Generating filelist $STATUS_CHANGED_FILE"
    echo "    containing new/changed items ..."

    # if a previous build exists, list only differences
    if [ $PREV_BUILD_CNT -eq 1 ] ; then
        MOD_URL=$(echo $PREV_SVNSOURCEURL | sed -e 's/\//\\\//'g | cut -f 1 -d @ )
        # echo "original PREV_SVNSOURCEURL : $PREV_SVNSOURCEURL"
        # echo "modified PREV_SVNSOURCEURL : $MOD_URL"
        # perform diff, filter URL path, write to file
        echo "performing diff between"
        echo "        $PREV_SVNSOURCEURL"
        echo "    and $SVNSOURCEURL ..."
        $SVN diff --summarize --ignore-properties $PREV_SVNSOURCEURL $SVNSOURCEURL \
            | grep -E "^A|^M|^D" \
            | sort \
            | sed -e "s/$MOD_URL\///g" \
              > $STATUS_CHANGED_FILE 
        check_error $?
    echo "CHANGED_FILES:"
    cat $STATUS_CHANGED_FILE
    # no previous build exists, so list everything
    # and add a leading 'A' to emulate the svn diff result
    else
        $SVN list --depth infinity $SVNSOURCEURL \
            | sed -e "s/\(^.*\)/A       \1/" \
              > $STATUS_CHANGED_FILE 
    fi
    echo "" 
    echo "... done"
    echo ""

    # write restart information
    f_write_variable_to_restart_file "$RESTART_FILE" STATUS_CHANGED_FILE $STATUS_CHANGED_FILE
    f_write_restart_point $RESTART_FILE GETDIFFPREVCURR

    f_write_date
    echo ""

else

    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping getting difference between previous and current deployment"
    echo "###############################################################################"
    echo ""
    # although this step can be skipped, the result of this step from the previous run is still required
    # get name of change list from previous script run (writes into variable GLOBAL_RETURN_VALUE)
    # write contents of old change list into current change list
    f_get_variable_from_restart_file "$RESTART_FILE" STATUS_CHANGED_FILE
    cat $GLOBAL_RETURN_VALUE > $STATUS_CHANGED_FILE
    check_error $?

fi


#---------------------------------------------------------------------------
# 8.) Get code from Subversion build tag and update work area and run install_stage
#---------------------------------------------------------------------------

echo ""
echo "###############################################################################"
echo "# Get code from Subversion build $CUR_BUILD_NO"
echo "###############################################################################"
echo ""

f_check_restart_point "$RESTART_FILE" UPDATEINFASHARED
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo "Updating Informatica run area:"
    echo ""
    # update Informatica run environment
    td_updwa.sh \
            -w "$INFA_SHARED_DIR" \
            -u "$SVNSOURCEURL/infa_shared" \
            -l "$LOG_FILE" \
            -q
    check_error $?
    echo ""

    f_write_restart_point $RESTART_FILE UPDATEINFASHARED

    f_write_date
    echo ""

else
    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping update of Informatica run area"
    echo "###############################################################################"
    echo ""
fi

f_check_restart_point "$RESTART_FILE" UPDATEDBADMIN
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo "Updating database DDL area:"
    echo ""
    # update database ddl area - required to apply the database changes
    td_updwa.sh \
            -w "$INFA_SHARED_DIR/../dbadmin" \
            -u "$SVNSOURCEURL/dbadmin" \
            -l "$LOG_FILE" \
            -q
    check_error $?
    echo ""

    f_write_restart_point $RESTART_FILE UPDATEDBADMIN

    f_write_date
    echo ""

else
    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping update of database DDL area"
    echo "###############################################################################"
    echo ""
fi

f_check_restart_point "$RESTART_FILE" UPDATEINFAXML
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo "Updating Informatica XML files:"
    echo ""
    # update Informatica objects - required for import into Informatica repository
    td_updwa.sh \
            -w "$INFA_SHARED_DIR/../infa_xml" \
            -u "$SVNSOURCEURL/infa_xml" \
            -l "$LOG_FILE" \
            -q
    check_error $?
    echo ""

    f_write_restart_point $RESTART_FILE UPDATEINFAXML

    f_write_date
    echo ""

else
    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping update of Informatica XML file area"
    echo "###############################################################################"
    echo ""
fi

if [ "$RUN_INSTALL_STAGE" == "true" ] ; then
	echo ""
	echo "###############################################################################"
	echo "# Run install_stage.sh for environment $ENVNAME"
	echo "###############################################################################"
	echo ""
	/opt/devutil/infa/install/install_stage.sh "$ENVNAME"
fi

#---------------------------------------------------------------------------
# 9.) get list of changed DDLs and display them for information purpose
#     execute the changes in the database
#---------------------------------------------------------------------------


if [ "$RUN_DB_APPLY" == "true" ] ; then

    f_check_restart_point "$RESTART_FILE" APPLYDDL
    if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

        DDL_PATTERN="........dbadmin"

        # checking for deleted database objects
        NUM_OF_CHANGED_DDL=`cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN" | grep "^D" | wc -l`
        if  [ $NUM_OF_CHANGED_DDL -ne 0 ] ; then
            echo ""
            echo "###############################################################################"
            echo "# Please be aware that the following database object definition files have been deleted."
            echo "# Please check if they can be dropped from the database."
            echo "###############################################################################"
            echo ""

            cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN" | grep "^D" > $DB_DELETED_FILE
            cat $DB_DELETED_FILE
            echo ""
            echo "This info can also be found in $DB_DELETED_FILE"
            echo ""
        fi

        NUM_OF_CHANGED_DDL=`cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN" | grep -v "^D" | wc -l`
        if  [ $NUM_OF_CHANGED_DDL -ne 0 ] ; then
            echo ""
            echo "###############################################################################"
            echo "# Please be aware that the following DDL scripts have also changed."
            echo "# They will now be applied accordingly to the environment's databases."
            echo "###############################################################################"
            echo ""

            cd $INFA_SHARED_DIR/..
            cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$DDL_PATTERN" | grep -v "^D" > $DB_CHANGED_FILE
            cat $DB_CHANGED_FILE
            echo ""
            apply_db_chg.sh $INFA_SHARED_DIR/../dbadmin/changes.txt $INFA_SHARED_DIR/../dbadmin $CUR_BUILD_NO $ENVNAME
            echo ""
            cd -
        fi

        f_write_restart_point $RESTART_FILE APPLYDDL

        echo ""
        f_write_date
        echo ""

    else
        echo ""
        echo "###############################################################################"
        echo "# Found restart point - skipping apply of database changes"
        echo "###############################################################################"
        echo ""
    fi
fi


#---------------------------------------------------------------------------
# 10.) get a list of changed Informatica objects and import them 
#---------------------------------------------------------------------------

if [ "$RUN_INFA_IMPORT" == "true" ] ; then

    f_check_restart_point "$RESTART_FILE" IMPORTINFAXML
    if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

        INFA_XML_PATTERN="........infa_xml"

        # checking for deleted Informatica objects
        NUM_OF_CHANGED_XML=`cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$INFA_XML_PATTERN" | grep "^D" | wc -l`
        if  [ $NUM_OF_CHANGED_XML -ne 0 ] ; then
            echo ""
            echo "###############################################################################"
            echo "# Please be aware that the following Informatica objects have been deleted."
            echo "# Please check if they can be deleted from the Informatica target repository."
            echo "###############################################################################"
            echo ""

            cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$INFA_XML_PATTERN" | grep "^D" > $INFA_DELETED_FILE
            cat $INFA_DELETED_FILE
            echo ""
            echo "This info can also be found in $INFA_DELETED_FILE"
            echo ""
        fi

        # checking for new or changed Informatica objects
        NUM_OF_CHANGED_XML=`cat $STATUS_CHANGED_FILE | grep -v "\/$" | grep "$INFA_XML_PATTERN" | grep -v "^D" | wc -l`
        if  [ $NUM_OF_CHANGED_XML -ne 0 ] ; then
            echo ""
            echo "###############################################################################"
            echo "# Please be aware that the following Informatica objects have changed."
            echo "# They will now be imported into the Informatica target repository."
            echo "###############################################################################"
            echo ""

            cat $STATUS_CHANGED_FILE | grep -v "\/$" \
                                     | grep "$INFA_XML_PATTERN" \
                                     | grep -v "^D" \
                                     | sed -e "s/^$INFA_XML_PATTERN\///" \
                                       > $INFA_CHANGED_FILE
            cat $INFA_CHANGED_FILE
            echo ""


            # run the import of XML files into the Informatica repository

            impxml.sh \
                    -l "$INFA_CHANGED_FILE" \
                    -p "$INFA_SHARED_DIR/../infa_xml" \
                    -e "$ENVNAME" \
                    -b "$CUR_BUILD_NO" \
                    -o "$PRV_BUILD_NO"
        fi

        f_write_restart_point $RESTART_FILE IMPORTINFAXML

        echo ""
        f_write_date
        echo ""

    else
        echo ""
        echo "###############################################################################"
        echo "# Found restart point - skipping import of Informatica objects"
        echo "###############################################################################"
        echo ""
    fi
fi


#---------------------------------------------------------------------------
# 11.) make shure the Informatica parameter file contains an entry for the database environment 
#---------------------------------------------------------------------------

f_check_restart_point "$RESTART_FILE" CHANGEINFAPARFILES
if [ "$RESTARTPOINT_FINISHED" == "false" ] ; then

    echo ""
    echo "###############################################################################"
    echo "# make shure the Informatica parameter file contains an entry for the database environment"
    echo "###############################################################################"
    echo ""
    #
    # make shure the Informatica parameter file contains an entry for the database environment
    #
    for file in par/axedwparameters.prm bin/PWD_BteqLogon
    do
        sed -e 's/\${DB_ENV}'/$ENVNAME/g \
        -e 's,\${INFA_SHARED_DIR}',$INFA_SHARED_DIR,g \
        <$INFA_SHARED_DIR/$file >$INFA_SHARED_DIR/$file.new
        mv $INFA_SHARED_DIR/$file $INFA_SHARED_DIR/$file.orig && \
        mv $INFA_SHARED_DIR/$file.new $INFA_SHARED_DIR/$file
    done

    sed -e 's/\${DB_ENV}'/$ENVNAME/ <$INFA_SHARED_DIR/par/axedwparameters.prm >$INFA_SHARED_DIR/par/axedwparameters.prm.new
    mv $INFA_SHARED_DIR/par/axedwparameters.prm $INFA_SHARED_DIR/par/axedwparameters.prm.orig && \
    mv $INFA_SHARED_DIR/par/axedwparameters.prm.new $INFA_SHARED_DIR/par/axedwparameters.prm

    #
    # make shure the logon information for batch scripts are correct
    #
    export DB_ENV=$ENVNAME

    for file in $INFA_SHARED_DIR/bin/*.var
    do
        sed -e "s/UserName.*=.*'/UserName = '${DB_ENV}_Common_Trf_00001'/" -e "s/UserPassword.*=.*'/UserPassword = '${DB_ENV}_Common_Trf'/" <$file >$file.new
        mv $file $file.orig && mv $file.new $file
    done

    f_write_restart_point $RESTART_FILE CHANGEINFAPARFILES

    echo ""
    f_write_date
    echo ""

else
    echo ""
    echo "###############################################################################"
    echo "# Found restart point - skipping change of Informatica parameter files"
    echo "###############################################################################"
    echo ""
fi


#---------------------------------------------------------------------------
# Finish script
#---------------------------------------------------------------------------

exit 0

