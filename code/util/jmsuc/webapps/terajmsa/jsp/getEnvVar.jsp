<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory' %>
<%@ page import='com.teradata.tjmsl.util.Constants' %>
<%@ page import="com.teradata.shared.environment.EnvironmentVariables" %>
<%@ page import="com.teradata.shared.environment.Variable" %>

<% 
	String varName = (String) request.getAttribute(Constants.EV_UI_VAR_NAME);
	String fn	   = (String) request.getAttribute(Constants.EV_UI_VAR_FUNC);
	String title   = "";
	String descReadOnly  = "";
		
	if( fn.equals("View") )
	{
		title =	ResourceBundleFactory.getString("UI_B_VIEW",request);
		descReadOnly  = "READONLY";
	}

	if( fn.equals("Edit") )
	{
		title =	ResourceBundleFactory.getString("UI_B_EDIT",request);
	}

	EnvironmentVariables env = (EnvironmentVariables) request.getAttribute(Constants.ENV_VARS);
	Variable var = env.getVariable(varName);

%>	

<html>
<head>

<script type="text/javascript">
function modify() 
{	
	inputs = document.myForm;
	vName = inputs.varName.value; 
	vValue = inputs.varValue.value;
	vType  = inputs.varType.value;
	vDesc  = inputs.varDesc.value;

	if(vDesc.length > 1024)
	{
		var diff = vDesc.length - 1024;
		var aStr = "<%= ResourceBundleFactory.getString("UI_A_MAX_LEN_DESC",request) %>";
		aStr += "\n " + "<%= ResourceBundleFactory.getString("UI_A_EXCEEDED_1024_MAX",request) %>" + diff;	
		alert(aStr);
		return false;
	}

	if(vType == 'java.lang.Integer')
	{
		if (vName == '<%= Constants.MAX_BATCH_SIZE %>')
		{
			if ((isNaN(vValue)) || vValue < 1 || vValue > 2000)
			{			
				alert("<%= ResourceBundleFactory.getString("UI_A_MAX_BATCH_SIZE",request) %>");
				return false;			
			}
		}
		else if (vName == '<%= Constants.RECEIVE_TIMEOUT %>')
		{
			if ((isNaN(vValue)) || vValue < 1 || vValue > 600)
			{			
				alert("<%= ResourceBundleFactory.getString("UI_A_RECEIVE_TIMEOUT",request) %>");
				return false;			
			}
		}
		else if (vName == '<%= Constants.LOADER_PULLING_INTERVAL %>')
		{
			if ((isNaN(vValue)) || vValue < 1 || vValue > 600)
			{			
				alert("<%= ResourceBundleFactory.getString("UI_A_LOADER_PULL_INTERVAL",request) %>");
				return false;			
			}
		}
		else if (vName == '<%= Constants.QUERY_TIMEOUT %>')
		{
			if ((isNaN(vValue)) || vValue < 1 || vValue > 600)
			{			
				alert("<%= ResourceBundleFactory.getString("UI_A_QUERY_TIMEOUT",request) %>");
				return false;			
			}
		}
		else if (vName == '<%= Constants.EXTRACTOR_PULLING_INTERVAL %>')
		{
			if ((isNaN(vValue)) || vValue < 1 || vValue > 600)
			{			
				alert("<%= ResourceBundleFactory.getString("UI_A_EXT_PULL_INTERVAL",request) %>");
				return false;			
			}
		}
		else if (vName == '<%= Constants.MAX_RETRIES %>')
		{
			if ((isNaN(vValue)) || vValue < 1 || vValue > 1000)
			{			
				alert("<%= ResourceBundleFactory.getString("UI_A_MAX_RETRIES",request) %>");
				return false;			
			}
		}
		else if (vName == '<%= Constants.EMAIL_LEVEL %>')
		{
			var field = document.myForm.emaillevelselect;
			var errorVal = "0";
			var stopVal = "0";
			var startVal = "0";
			for (i = 0; i< field.options.length; i++) {
				if (field.options[i].selected == true)
				{
					if (field.options[i].value == "ERROR") {
						errorVal = "1";
					} else if (field.options[i].value == "STOP") {
						stopVal = "1";
					} else {
						startVal = "1";
					}
				}
				else
				{	
					if (field.options[i].value == "ERROR") {
						errorVal = "0";
					} else if (field.options[i].value == "STOP") {
						stopVal = "0";
					} else {
						startVal = "0";
					}
				}
			}
			vValue = errorVal.concat(stopVal.concat(startVal));
			if ((isNaN(vValue)) || vValue < 0 || vValue > 111)
			{			UI_A_EMAIL_LEVEL
				alert("<%= ResourceBundleFactory.getString("UI_A_EMAIL_LEVEL",request) %>");
				return false;			
			}
			document.myForm.varValue.value = vValue;
		}
		else
		{
			if (vName == '<%= Constants.LOG_FILE_SIZE %>')
			{
				if (isNaN(vValue) || vValue < 0 || vValue > 2097151)
				{			
					alert("<%= ResourceBundleFactory.getString("UI_A_INTEGER_LOGFILESIZE_VALUES",request) %>");
					return false;
				}
			}
			else
			{
				if (isNaN(vValue) || vValue < -2147483648 || vValue > 2147483647)
				{			
					alert("<%= ResourceBundleFactory.getString("UI_A_INTEGER_VALUES",request) %>");
					return false;
				}
			}
		}
	}
		
	document.myForm.action = "Dispatcher?request=ModifyEnvVar&fn=Modify";
	document.myForm.submit();
}

function restore()
{
	var cStr = "<%= ResourceBundleFactory.getString("UI_D_RESTORE_DEFAULT",request) %>" + "<%= varName %>";
	var answer = confirm (cStr);

	if (answer)
	{
		document.myForm.action = "Dispatcher?request=ModifyEnvVar&fn=Restore";
		document.myForm.submit();
	}
		
	else
	{
		return false;
	}	
}
</script>

</head>
<body>

<form name="myForm" method="post">

	<INPUT TYPE="hidden" NAME="<%= Constants.EV_UI_VAR_NAME %>" VALUE="<%= var.getName() %>">
	<INPUT TYPE="hidden" NAME="varType" VALUE="<%= var.getType() %>">

	<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

		<jsp:include page="topbar.jsp"/>
		<tr>
			<td class="titleBox">&nbsp;<%= title %>:&nbsp;<font class="header"><%= varName %></font></td>
		</tr>
		<jsp:include page="bottombar.jsp"/>
	</table>

	<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">

		<tr><td class="whiteSpace"></td></tr>
		<tr>
			<td ALIGN="right">
				<font class="header"><%= ResourceBundleFactory.getString("ALL_D_NAME", request) %></font>:
			</td>

			<td ALIGN="left"><%= var.getName() %></td>
		</tr>

		<tr><td class="whiteSpace"></td></tr>
		<tr>
			<td ALIGN="right">
				<font class="header"><%= ResourceBundleFactory.getString("ALL_D_TYPE", request) %></font>:
			</td>

			<td ALIGN="left"><%= var.getType() %></td>
		</tr>
		
		<tr><td class="whiteSpace"></td></tr>
		<tr>
			<td ALIGN="right">
				<font class="header"><%= ResourceBundleFactory.getString("ALL_D_VALUE", request) %></font>:
			</td>

			<td ALIGN="left">

			<%
			if(fn.equals("Edit") )
			{
				if(varName.equals(Constants.LOGGER_LEVEL))
				{
					String logLevel = var.getDisplayValue();			
					%>
						<select NAME="<%= Constants.EV_UI_VAR_VALUE %>" size="4">

						<%if(logLevel.equalsIgnoreCase("DEBUG"))
						{%>
							<option value="DEBUG" selected>DEBUG</option>
						<%}
						else
						{%>
							<option value="DEBUG">DEBUG</option>
						<%}

						if(logLevel.equalsIgnoreCase("INFO"))
						{%>
							<option value="INFO" selected>INFO</option>
						<%}
						else
						{%>
							<option value="INFO">INFO</option>
						<%}

						if(logLevel.equalsIgnoreCase("WARN"))
						{%>
							<option value="WARN" selected>WARN</option>
						<%}
						else
						{%>
							<option value="WARN">WARN</option>
						<%}

						if(logLevel.equalsIgnoreCase("ERROR"))
						{%>
							<option value="ERROR" selected>ERROR</option>
						<%}
						else
						{%>
							<option value="ERROR">ERROR</option>
						<%}%>
						
					</select>

				<%}

				else if(varName.equals(Constants.EMAIL_LEVEL))
				{
					String emailLevel = "000";
					java.lang.Integer intObj = (java.lang.Integer) var.getValue();
					switch (intObj.intValue()) {
						case 0: emailLevel = "000"; break;
						case 1: emailLevel = "001"; break;
						case 10: emailLevel = "010"; break;
						case 11: emailLevel = "011"; break;
						case 100: emailLevel = "100"; break;
						case 101: emailLevel = "101"; break;
						case 110: emailLevel = "110"; break;
						case 111: emailLevel = "111"; break;
						default: emailLevel = "000"; break;
					}
					
					%>
						<select NAME="emaillevelselect" size="3" multiple>

						<%if(emailLevel.substring(0, 1).equalsIgnoreCase("1"))
						{%>
							<option value="ERROR" selected>ERROR</option>
						<%}
						else
						{%>
							<option value="ERROR">ERROR</option>
						<%}

						if(emailLevel.substring(1, 2).equalsIgnoreCase("1"))
						{%>
							<option value="STOP" selected>STOP</option>
						<%}
						else
						{%>
							<option value="STOP">STOP</option>
						<%}

						if(emailLevel.substring(2, 3).equalsIgnoreCase("1"))
						{%>
							<option value="START" selected>START</option>
						<%}
						else
						{%>
							<option value="START">START</option>
						<%}%>
						
					</select>
					<INPUT TYPE="hidden" NAME="<%= Constants.EV_UI_VAR_VALUE %>" SIZE="50" MAXLENGTH="512" VALUE="<%= var.getDisplayValue() %>">

				<%}
				else if(var.getType().equals("java.lang.Boolean"))
				{
					String boolVal = var.getDisplayValue();			
					%>
						<select NAME="<%= Constants.EV_UI_VAR_VALUE %>" size="2">

						<%if(boolVal.equalsIgnoreCase("true"))
						{%>
							<option value="true" selected>true</option>
						<%}
						else
						{%>
							<option value="true">true</option>
						<%}

						if(boolVal.equalsIgnoreCase("false"))
						{%>
							<option value="false" selected>false</option>
						<%}
						else
						{%>
							<option value="false">false</option>
						<%}%>

						</select>
				<%}

				else if(var.isPassProtected())
				  {%>
                     <INPUT TYPE="password" NAME="<%= Constants.EV_UI_VAR_VALUE %>" SIZE="50" MAXLENGTH="512" VALUE="">
				    
					<%}		
				else
				{%>			
					<% if(!varName.equals(Constants.LOG_FILE_SIZE)) {%>
						<INPUT TYPE="text" NAME="<%= Constants.EV_UI_VAR_VALUE %>" SIZE="50" MAXLENGTH="512" VALUE="<%= var.getDisplayValue() %>">
					<%} else {%>
						<INPUT TYPE="text" NAME="<%= Constants.EV_UI_VAR_VALUE %>" SIZE="50" MAXLENGTH="512" VALUE="<%= var.getDisplayValue() %>"> Kilobytes
					<%} %>
				<%}
			}

			else if(var.isPassProtected())
			{%>	
					
				<INPUT DISABLED TYPE="password" NAME="<%= Constants.EV_UI_VAR_VALUE %>" SIZE="50" MAXLENGTH="512" VALUE="">
			<%}
			else
			{%>
				<% if(varName.equals(Constants.EMAIL_LEVEL)) {
					String emailLevelValue = "";
					java.lang.Integer intObj = (java.lang.Integer) var.getValue();
					switch (intObj.intValue()) {
						case 0: emailLevelValue = ""; break;
						case 1: emailLevelValue = "START"; break;
						case 10: emailLevelValue = "STOP"; break;
						case 11: emailLevelValue = "STOP,START"; break;
						case 100: emailLevelValue = "ERROR"; break;
						case 101: emailLevelValue = "ERROR,START"; break;
						case 110: emailLevelValue = "ERROR,STOP"; break;
						case 111: emailLevelValue = "ERROR,STOP,START"; break;
						default: emailLevelValue = ""; break;
					}
					
				%>
					<INPUT TYPE="text" readonly NAME="<%= Constants.EV_UI_VAR_VALUE %>" SIZE="50" MAXLENGTH="512" VALUE="<%= emailLevelValue %>">
				<%} else if(varName.equals(Constants.LOG_FILE_SIZE)) {%>
					<INPUT TYPE="text" readonly NAME="<%= Constants.EV_UI_VAR_VALUE %>" SIZE="50" MAXLENGTH="512" VALUE="<%= var.getDisplayValue() %>"> Kilobytes
				<%} else {%>
					<INPUT TYPE="text" readonly NAME="<%= Constants.EV_UI_VAR_VALUE %>" SIZE="50" MAXLENGTH="512" VALUE="<%= var.getDisplayValue() %>">
				<%} %>
			<%}%>

			</td>
		</tr>

		<tr><td class="whiteSpace"></td></tr>
		<tr>
			<td ALIGN="right">
				<font class="header"><%= ResourceBundleFactory.getString("ALL_D_DESCRIPTION", request) %></font>:
			</td>

			<td ALIGN="left">
				(<%= ResourceBundleFactory.getString("UI_D_ENV_DESC_MAX",request) %>)
			</td>
		</tr>
		<tr>
			<td ALIGN="right"></td>

			<td ALIGN="left">
				<textarea <%= descReadOnly %> NAME="<%= Constants.EV_UI_VAR_DESC %>" ROWS="12" COLS="120"><%= var.getDescription() %></textarea>
			</td>
		</tr>

	</table>

	<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">

		<tr><td class="whiteSpace"></td></tr>
		<tr>
			<td>
				<input type="button" value=" <%= ResourceBundleFactory.getString("UI_B_BACK",request) %> " onClick="window.location='Dispatcher?request=GetEnvVars'">

			<%
			if( fn.equals("Edit") )
			{%>
				<input type="button" value=" <%= ResourceBundleFactory.getString("UI_B_MODIFY",request) %> " onClick='modify();'>
				<input type="button" value=" <%= ResourceBundleFactory.getString("UI_B_RESTORE_DEFAULT",request) %> " onClick='restore();'>

			<%}%>

			</td>
		</tr>
	</table>

</form>
</body>
</html>