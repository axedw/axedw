<%@ page import='com.teradata.tjmsl.util.Constants'%>
<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>
<%@ page import='java.util.HashMap'%>
<%@ page import='java.util.Set'%>
<%@ page import='java.util.Iterator'%>
<%@ page import='java.util.List'%>

<html>
<body>

	<% 
	Boolean boolShowAll = (Boolean) request.getAttribute("boolShowAll");
	boolean bShowAll = boolShowAll.booleanValue();

	String title = (String) request.getAttribute("title");
	List listMessages = (List) request.getAttribute("listMessages");
	%>

<table WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER">
	<tr>
		<jsp:include page="topbar.jsp"/>
		<td class="titleBox"><%= title %></td>
		<jsp:include page="bottombar.jsp"/>
	</tr>

	<%
	for(int i=0; i < listMessages.size(); i++)
	{
		boolean bShowMessage = true;

		if(!bShowAll)
		{
			if(i != (listMessages.size() - 1))
			{
				bShowMessage = false;
			}
		}
			
		HashMap hmJMSHeaders = (HashMap) listMessages.get(i);

		Set set = hmJMSHeaders.keySet();
		Iterator iter = set.iterator();

		if(bShowMessage)
		{
		%>
			<tr><td class="whiteSpace"></td></tr>

			<tr>
				<td>
					<table WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="0" ALIGN="LEFT">
						<tr>
							<td style="width:15%; padding-left:3px" ALIGN="LEFT" class="titleBox">
							<font class="header"><%= ResourceBundleFactory.getString("UI_D_HEADER_NAME",request) %></font>
							</td>
						
							<td style="width:85%; padding-left:3px" ALIGN="LEFT" class="titleBox">
							<font class="header"><%= ResourceBundleFactory.getString("UI_D_VALUE",request) %></font>
							</td>
						</tr>

						<%
						for(int j=0; iter.hasNext(); j++)
						{
							String headerName = (String) iter.next();
							String headerValue = (String) hmJMSHeaders.get(headerName);
							String rowClass = (j % 2 == 0) ? "evenRowL" : "oddRowL";
						%>

						<tr>
							<td style="width:15%; padding-left:3px" class="<%= rowClass %>"><%= headerName %></td>
							<td style="width:85%; padding-left:3px" class="<%= rowClass %>"><%= headerValue %></td>
						</tr>

						<%
						}
						%>

					</table>
				</td>
			</tr>
		<%
		}	
	}
	%>

</table>
</body>
</html>