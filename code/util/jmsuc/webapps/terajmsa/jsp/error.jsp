<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory' %>
<% 
	String sJMSException	= (String) request.getAttribute("JMSException");
	String sLinkedException	= (String) request.getAttribute("LinkedException");
	String sThrowable		= (String) request.getAttribute("Throwable");
	String sMessage	= (String) request.getAttribute("Message");
%>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<%
	if(sJMSException != null)
	{%>
		<tr>
			<td align="right"><%= ResourceBundleFactory.getString("UI_D_JMS_EXCEPTION",request) %>:</td>
			<td align="left"><font color="red"><%= sJMSException %></font></td>
		</tr>
	<%}%>

	<%
	if(sLinkedException != null)
	{%>
		<tr>
			<td align="right"><%= ResourceBundleFactory.getString("UI_D_LINKED_EXCEPTION",request) %>:</td>
			<td align="left"><font color="red"><%= sLinkedException %></font></td>
		</tr>
	<%}%>

	<%
	if(sThrowable != null)
	{%>
		<tr>
			<td align="right"><%= ResourceBundleFactory.getString("UI_D_THROWABLE",request) %>:</td>
			<td align="left"><font color="red"><%= sThrowable %></font></td>
		</tr>
	<%}%>

	<%
	if(sMessage != null)
	{%>
		<tr>
			<td><font class="error"><%= sMessage %></font></td>
		</tr>
	<%}%>
</table>
