<%@ page contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>
<%@ page import='com.teradata.tjmsl.util.Constants' %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title><%= ResourceBundleFactory.getString("UI_D_MAIN_APP_TITLE_WITH_PRODUCT_VERSION",request) %></title>
<link rel="stylesheet" type="text/css" href="css/tjmsl.css" />
</head>

<body>

<% String jspFileName = (String) request.getAttribute(Constants.JSP_FL_NAME); %>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">

	<tr>
	<td height="38">
		<jsp:include page="topnav.jsp"/>
	</td>
	</tr>

	<tr height="100%">
	<td height="100%">

		<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
		
		<tr height="100%">

		<td height="100%" id="Divider" width="4"><img src="images/spacer.gif" width="1" height="1"></td>

		<td height="100%" width="193" class="left_nav">
			<jsp:include page="mainNav.jsp" />
		</td>

		<td id=Divider height="100%" width=1 class="divider"><img src="images/spacer.gif" width="1" height="1"></td>

		<td>
			<table cellspacing="0" cellpadding="0" width="100%" height="100%" border="0">
			<jsp:include page="titlebarapp.jsp" />

			
			<% if (jspFileName == null || jspFileName.length() == 0) 
			{ %>
				<tr>
					<td style="padding: 0px; vertical-align: middle; background-image: url(images/splash.bmp);">
					</td>
				</tr>
			<% 
			} 
			else
			{ 
			%>
				<tr valign="top"><td style="padding-left: 4px; padding-top: 6px;">
				<jsp:include page="<%= jspFileName %>" />
				</td></tr>
			<%
			} 
			%>

			</table>
		</td>

		<td height="100%" id=Divider width="4"><img src="images/spacer.gif" width="1" height="1"></td>
		</tr>
		</table>
	</td>
	</tr>
</table>
</body>
</html>
