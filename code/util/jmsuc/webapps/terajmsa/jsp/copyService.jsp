<%@ page import='com.teradata.tjmsAdapter.Service'%>
<%@ page import='com.teradata.tjmsAdapter.service.ServiceConstants'%>
<%@ page import='com.teradata.tjmsAdapter.util.ResourceBundleFactory'%>
<%@ page import='com.teradata.tjmsl.util.Constants'%>
<%@ page import='java.util.List'%>

<% 
	List listServices = (List) request.getAttribute(Constants.UI_LIST_SERVICES); 
	String sAppType = (String) request.getAttribute(ServiceConstants.APP_TYPE);	
	String sAppName = "";
	if(sAppType.equals(ServiceConstants.APP_TYPE_LOADER))
	{
		sAppName = ResourceBundleFactory.getString("UI_D_LOADER",request);
	}
	else
	{
		sAppName = ResourceBundleFactory.getString("UI_D_ROUTER",request);
	}
%>

<script type="text/javascript">

function validate() 
{	
	inputs = document.myForm;
	vCopyFromServ = inputs.<%= ServiceConstants.SERVICE_ID_KEY %>.value;
	vServiceID = inputs.<%= ServiceConstants.SERVICE_ID %>.value; 
	vServiceIDTrimmed = vServiceID.replace(/^\s+|\s+$/g, '');
	inputs.<%= ServiceConstants.SERVICE_ID %>.value = vServiceIDTrimmed;
	vServiceID = vServiceIDTrimmed;

	if (vCopyFromServ == "-Select-")
	{
		alert("<%= ResourceBundleFactory.getString("UI_A_SEL_COPY_FROM_SERV",request) %>");
		return false;
	}

	if(vServiceID.length == 0)
	{
		alert("<%= ResourceBundleFactory.getString("UI_A_ENT_COPY_TO_SERV",request) %>");
		document.myForm.<%= ServiceConstants.SERVICE_ID %>.focus();
		return false;
	}

	for(var i = 0; i < vServiceID.length; i++)
	{
		var vASCIIValue = vServiceID.charCodeAt(i);
		
		//Numbers 0x30 ~ 0x39
		//Upper Letters 0x41 ~ 0x5a
		//_ 0x5f
		//Lower Letters 0x61 ~ 0x7a

		if(vASCIIValue < 0x30)
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if((vASCIIValue > 0x39) && (vASCIIValue < 0x41))
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if((vASCIIValue > 0x5a) && (vASCIIValue < 0x5f))
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if((vASCIIValue > 0x5f) && (vASCIIValue < 0x61))
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}

		if(vASCIIValue > 0x7a)
		{
			alert("The input parameter " + "<%= ResourceBundleFactory.getString("UI_D_SERVICE_ID", request) %>" + " should be a combination of upper and lower case letters, numbers and _");
			return false;
		}
	}

}

</script>

<form name="myForm" action="Dispatcher?apptype=<%= sAppType %>&request=performsrvact&<%= ServiceConstants.ACTION %>=<%= ServiceConstants.COPY %>" method="post" onSubmit="return validate();">

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">

	<tr>
		<jsp:include page="topbar.jsp"/>
		<td class="titleBox">&nbsp;<%= ResourceBundleFactory.getString("UI_D_COPY_SERV",request) %> - <%= sAppName %></td>
		<jsp:include page="bottombar.jsp"/>
	</tr>

	<tr><td class="whiteSpace"></td></tr>

	<%
		if (listServices.size() == 0)
		{
	%>
			<tr>
				<td><%= ResourceBundleFactory.getString("UI_D_SERVICE_EMPTY",request) %></td>
			</tr>
	<%
		}
		else
		{
	%>
			<tr>
				<td><b><%= ResourceBundleFactory.getString("UI_D_COPY_FROM_SERV",request) %></b></td>
			</tr>

			<tr>
				<td>		
					<select name="<%= ServiceConstants.SERVICE_ID_KEY %>" size="15">

						<option value="-Select-" selected>-<%= ResourceBundleFactory.getString("UI_D_SELECT",request) %>-</option>

						<%
							for(int i=0; i < listServices.size(); i++)
							{
								Service service = (Service) listServices.get(i);
								String sServiceID = service.getServiceID();
								String sServiceIDKey = service.getServiceIDKey();
						%>
								<option value="<%= sServiceIDKey %>"><%= sServiceID %></option>
						<%
							}
						%>

					</select>
				</td>
			</tr>

			<tr><td class="whiteSpace"></td></tr>

			<tr>
				<td><b><%= ResourceBundleFactory.getString("UI_D_COPY_TO_SERV",request) %></b></td>
			</tr>

			<tr>
				<td>
					<input type="text" name="<%= ServiceConstants.SERVICE_ID %>" size="50" maxlength="64">
				</td>
			</tr>

			<tr><td class="whiteSpace"></td></tr>

			<tr>
				<td>
					<input type="Submit" name="Button" value="<%= ResourceBundleFactory.getString("UI_B_COPY",request) %>">
				</td>
			</tr>
	<%
		}
	%>

</table>
</form>