#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: setenv.sh 31166 2020-04-02 07:42:58Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2020-04-02 09:42:58 +0200 (tor, 02 apr 2020) $
# Last Revision    : $Revision: 31166 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/util/jmsuc/bin/setenv.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# set CATALINA_PID so that shutdown.sh waits for the process to stop
#
export CATALINA_PID=$CATALINA_HOME/run.pid
export JRE_HOME=/opt/etl/java8_64/jre
