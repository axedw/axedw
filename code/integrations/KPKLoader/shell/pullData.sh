#!/bin/sh
# ----------------------------------------------------------------------------
# SCM Infostamp				(DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID			   : $Id: pullData.sh 32479 2020-06-16 14:05:47Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 16:05:47 +0200 (tis, 16 jun 2020) $
# Last Revision	   : $Revision: 32479 $
# SCM URL		   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/pullData.sh $
# --------------------------------------------------------------------------
# SCM Info END
#
binFolder="../bin"				 # where all programs are
logFile="../log/pullData.log"	 # general log file for errors
dayFile="../log/pullData.day"	 # day when the last alter partition retension was executed
stopFile="../log/stopPullData"	 # stop processing and exit if this file exists
inboxDir="../inbox"				 # where name files for files to load are stored
ArchiveDir="../Archive"			 # where name files are archived
varFile=../par/loadCompetitorPrice.var	 # where parameters for the database is
lastTimestamp="../log/lastTimestamp"	 # timestamp when last file was located
file="$1"								# entry point for this run if dir do recursive run if file process the file
level="$2"								# folder level when processing folders recursively

updPartitions()
{
		parameters="$1"

# Uncomment the following code to enable repartitioning and truncation of the stage table.
#		node="$(grep -i TargetTdpId ${parameters} | while IFS=' =' read name node; do echo $node; done|tr -d \')"
#		uid="$(grep -i TargetUserName ${parameters} | while IFS=' =' read name uid; do echo $uid; done|tr -d \')"
#		passwd="$(grep -i TargetUserPassword ${parameters} | while IFS=' =' read name passwd; do echo $passwd; done|tr -d \')"
#
#		(
#				echo ".LOGON $node/$uid,$passwd"
#
#				cat <<-'end'
#				--ALTER TABLE $#DB_ENV#$#STGKPK_DB#T.Stg_KPK_Transactions TO CURRENT WITH DELETE;
#				.LOGOFF
#				.EXIT
#end
#		) | bteq
}

test "$level" = "" && level=0

#test $level -eq 0 &&
#echo "($$) $(date) Processing: $file" >>"$logFile"

test ! -f "$dayFile" && touch "$dayFile"		# make sure the day file for previous repartitioning exists

while true
do
		if [ -f "$stopFile" ]
		then
				if [ $level -eq 0 ]
				then
						echo "($$) $(date) Stop file detected, shutdown of main process" >>"$logFile"
				else
						echo "($$) $(date) Stop file detected, shutdown of child process" >>"$logFile"
				fi
				exit 0
		fi

		currentDay="$(date +%D)"
		prevPartDay="$(cat $dayFile)"
		if [ "$currentDay" != "$prevPartDay" ]
		then
				updPartitions "$varFile"
				echo "$currentDay" >"$dayFile"
		fi
		if [ -f "$file" ]
		then
				nameFile="$(basename $file).name"
				if [ ! -f "$ArchiveDir/$nameFile" -a ! -f "$inboxDir/$nameFile)" ]
				then
						#
						# new data arrived, process the file
						#
						${binFolder}/getNextData.sh "$inboxDir" "$file"
				fi
				#
				# check if archive folder is getting full, then remove some files
				# if exceeding 10000 remove all but 5000 latest
				#
				${binFolder}/cleanArchive.sh "$ArchiveDir" 10000 5000
				exit 0
		elif [ -d "$file" ]
		then
				#
				# find all files created after last saved timestamp (in file $lastTimestamp) and not newer than now
				#
				
				#
				# create lastTimestamp file if not exists
				#
				currTime="$(date)"
				if [ ! -f "$lastTimestamp" ]
				then
					touch -d "$currTime" "$lastTimestamp"
				fi
				find "${file}" -maxdepth 1 \
						! -path "${file}" '(' -type f -o -type d ')' -a \
						! -name '.*' -a \
						! -name tmp -a \
						-newer "$lastTimestamp" -a \
						! -newermt "$currTime" -a \
						-print | \
				while read locFile
				do
						#
						# file or folder found, call myself to process the object
						#
						${binFolder}/pullData.sh "$locFile" $(($level + 1))
				done
				if [ $level -ne 0 ]
				then
						exit 0
				fi
				touch -d "$currTime" "$lastTimestamp"
		else
				echo "($$) $(date) Detected file of unprocessable type, stopping: $file" >>"$logFile"
				exit 1
		fi

		if [ $level -eq 0 ]
		then
				#
				# back to top level, respawn after 60 seconds
				#
				sleep 60
				exec ${binFolder}/pullData.sh "$file" 0
		fi
done
