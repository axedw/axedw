#!/bin/bash
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: startup.sh 32338 2020-06-11 14:13:48Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-11 16:13:48 +0200 (tor, 11 jun 2020) $
# Last Revision    : $Revision: 32338 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/KPKLoader/shell/startup.sh $
# --------------------------------------------------------------------------
# SCM Info END
#
jobName=$#DB_ENV#loadCompetitorPrice		# name of the TPT job

#
# check if loader is running, if not start it
#

pidFile=../log/loader.pid
binFolder="../bin"              # where all programs are
stat=0

$binFolder/mounts3.sh
stat=$?
if [ $stat -ne 0 ]
then
        echo "mount of s3 bucket failed($stat)"
        exit $stat
fi

$binFolder/startpuller.sh
stat=$?
if [ $stat -ne 0 ]
then
        echo "start of puller jobb failed($stat)"
        exit $stat
fi

#
# check if loader is running
#
echo "Checking if loader is running..."
loaderProcess="$(ps -fu`id -un`|grep "loadCompetitorPrice.sh $#DB_ENV#"|grep -v grep)"
if [ "${loaderProcess}" != "" ]
then
        echo "loadCompetitorPrice.sh is already running: ${loaderProcess}" >&2
else
        #
        # no it is not running so start the loader and save the PID
        #
        echo "No it is not running so I will start the loader and save the PID in ${pidFile}"
        nohup ./loadCompetitorPrice.sh $#DB_ENV# >/dev/null 2>&1&
        loaderPID="$!"

        echo "$loaderPID" >"${pidFile}"
fi

#
# verify the status of the loader
#
echo "Verifying the status of the loader..."
sleep 10
statInfo="$(twbstat)"
jobIDs="$(echo "$statInfo" | grep ${jobName})"
if [ "$jobIDs" = "" ]
then
        echo "no loader jobs found" >&2
        echo "$statInfo" >&2
        exit 2
fi

echo "loader is running with the following jobIds:"
echo "${jobIDs}"

exit 0
