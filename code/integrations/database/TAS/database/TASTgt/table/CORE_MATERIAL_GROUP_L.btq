/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CORE_MATERIAL_GROUP_L.btq 24838 2018-05-03 06:35:52Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $
# Last Revision    : $Revision: 24838 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/database/TAS/database/TASTgt/table/CORE_MATERIAL_GROUP_L.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE PRTASTgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASTgtT','CORE_MATERIAL_GROUP_L','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE PRTASTgtT.CORE_MATERIAL_GROUP_L
  NO FALLBACK,
  NO BEFORE JOURNAL,
  NO AFTER JOURNAL,
  CHECKSUM = DEFAULT,
  DEFAULT MERGEBLOCKRATIO
  (
    TAS_SOURCE_ID VARCHAR(10) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    MATERIAL_GROUP_ID VARCHAR(9) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    LANGUAGE_ID CHAR(2) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    MATERIAL_GROUP_DESCRIPTION VARCHAR(20) CHARACTER SET UNICODE CASESPECIFIC,
    MATERIAL_GROUP_LONG_TXT VARCHAR(60) CHARACTER SET UNICODE CASESPECIFIC,
    TAS_CREATE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_EXTRACTION_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_DELETION_FLAG CHAR(1) CHARACTER SET UNICODE CASESPECIFIC,
    PRIMARY KEY (TAS_SOURCE_ID,MATERIAL_GROUP_ID,LANGUAGE_ID)
  )
PRIMARY INDEX (MATERIAL_GROUP_ID);

COMMENT ON TABLE PRTASTgtT.CORE_MATERIAL_GROUP_L IS 'Language Table containing descriptions of material group.';
COMMENT ON COLUMN PRTASTgtT.CORE_MATERIAL_GROUP_L.TAS_SOURCE_ID IS 'SAP Source Identification';
COMMENT ON COLUMN PRTASTgtT.CORE_MATERIAL_GROUP_L.MATERIAL_GROUP_ID IS 'T023.MATKL - Material Group';
COMMENT ON COLUMN PRTASTgtT.CORE_MATERIAL_GROUP_L.LANGUAGE_ID IS 'T002.LAISO - Language according to ISO 639:T023T.SPRAS - Language key';
COMMENT ON COLUMN PRTASTgtT.CORE_MATERIAL_GROUP_L.MATERIAL_GROUP_DESCRIPTION IS 'T023T.WGBEZ - Material Group Description';
COMMENT ON COLUMN PRTASTgtT.CORE_MATERIAL_GROUP_L.MATERIAL_GROUP_LONG_TXT IS 'T023T.WGBEZ60 - Long text describing the material group';
COMMENT ON COLUMN PRTASTgtT.CORE_MATERIAL_GROUP_L.TAS_CREATE_DATE IS 'Date on which the Record Was Created';
COMMENT ON COLUMN PRTASTgtT.CORE_MATERIAL_GROUP_L.TAS_CHANGE_DATE IS 'Date on which the Record Was Changed';
COMMENT ON COLUMN PRTASTgtT.CORE_MATERIAL_GROUP_L.TAS_EXTRACTION_DATE IS 'Date on which the Record Was Extracted (SAP sysdate)';
COMMENT ON COLUMN PRTASTgtT.CORE_MATERIAL_GROUP_L.DWH_CHANGE_DATE IS 'Datawarehouse Change date - Extraction run on date';
COMMENT ON COLUMN PRTASTgtT.CORE_MATERIAL_GROUP_L.DWH_DELETION_FLAG IS 'Datawarehouse Deletion flag';

--DROP TABLE PRTASTgtT.CORE_MATERIAL_STATUS;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASTgtT','CORE_MATERIAL_GROUP_L','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('PRTASTgtT','CORE_MATERIAL_GROUP_L','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON PRTASTgtT.CORE_MATERIAL_GROUP_L IS '$Revision: 24838 $ - $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


