/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CORE_BUSINESS_AREA_L.btq 24838 2018-05-03 06:35:52Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $
# Last Revision    : $Revision: 24838 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/integrations/database/TAS/database/TASTgt/table/CORE_BUSINESS_AREA_L.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE PRTASTgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASTgtT','CORE_BUSINESS_AREA_L','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE PRTASTgtT.CORE_BUSINESS_AREA_L
  NO FALLBACK,
  NO BEFORE JOURNAL,
  NO AFTER JOURNAL,
  CHECKSUM = DEFAULT,
  DEFAULT MERGEBLOCKRATIO
  (
    TAS_SOURCE_ID VARCHAR(10) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    BUSINESS_AREA_ID CHAR(4) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    LANGUAGE_ID CHAR(2) CHARACTER SET UNICODE CASESPECIFIC NOT NULL,
    BUSINESS_AREA_NAME VARCHAR(30) CHARACTER SET UNICODE CASESPECIFIC,
    TAS_CREATE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    TAS_EXTRACTION_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_CHANGE_DATE TIMESTAMP(0) FORMAT 'yyyy-mm-ddBhh:mi:ss',
    DWH_DELETION_FLAG CHAR(1) CHARACTER SET UNICODE CASESPECIFIC,
    PRIMARY KEY (TAS_SOURCE_ID,BUSINESS_AREA_ID,LANGUAGE_ID)
  )
PRIMARY INDEX (BUSINESS_AREA_ID);

COMMENT ON TABLE PRTASTgtT.CORE_BUSINESS_AREA_L IS 'Language Table containing descriptions of business area.';
COMMENT ON COLUMN PRTASTgtT.CORE_BUSINESS_AREA_L.TAS_SOURCE_ID IS 'SAP Source Identification';
COMMENT ON COLUMN PRTASTgtT.CORE_BUSINESS_AREA_L.BUSINESS_AREA_ID IS 'TGSB.GSBER - Business Area';
COMMENT ON COLUMN PRTASTgtT.CORE_BUSINESS_AREA_L.LANGUAGE_ID IS 'T002.LAISO - Language according to ISO 639:TGSBT.SPRAS - Language Key';
COMMENT ON COLUMN PRTASTgtT.CORE_BUSINESS_AREA_L.BUSINESS_AREA_NAME IS 'TGSBT.GTEXT - Business area description';
COMMENT ON COLUMN PRTASTgtT.CORE_BUSINESS_AREA_L.TAS_CREATE_DATE IS 'Date on which the Record Was Created';
COMMENT ON COLUMN PRTASTgtT.CORE_BUSINESS_AREA_L.TAS_CHANGE_DATE IS 'Date on which the Record Was Changed';
COMMENT ON COLUMN PRTASTgtT.CORE_BUSINESS_AREA_L.TAS_EXTRACTION_DATE IS 'Date on which the Record Was Extracted (SAP sysdate)';
COMMENT ON COLUMN PRTASTgtT.CORE_BUSINESS_AREA_L.DWH_CHANGE_DATE IS 'Datawarehouse Change date - Extraction run on date';
COMMENT ON COLUMN PRTASTgtT.CORE_BUSINESS_AREA_L.DWH_DELETION_FLAG IS 'Datawarehouse Deletion flag';

--DROP TABLE PRTASTgtT.CORE_CAPACITY;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('PRTASTgtT','CORE_BUSINESS_AREA_L','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('PRTASTgtT','CORE_BUSINESS_AREA_L','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON PRTASTgtT.CORE_BUSINESS_AREA_L IS '$Revision: 24838 $ - $Date: 2018-05-03 08:35:52 +0200 (tor, 03 maj 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


