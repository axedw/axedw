#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: archive_axbopos.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/archive_axbopos.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

DB_ENVAchv="$DB_ENV"Achv
DB_ENVStgAxBOT="$DB_ENV"StgAxBOT
DB_ENVMetaDataVIN="$DB_ENV"MetaDataVIN


JobRunID=$2
echo "JobRunID=$JobRunID"

CURRENT_TS=`date "+%Y%m%d%H%M%S"`
echo "CURRENT_TS=$CURRENT_TS"

exec >$PMRootDir/log/`basename $0`_"Arch_"$CURRENT_TS"_script".log 2>&1 

echo "writing to log"
echo "PMRootDir = $PMRootDir"
echo "DB_ENV = $DB_ENV"
echo "CURRENT_TS = $CURRENT_TS"
echo "JobRunID = $JobRunID"

echo "DB_ENVAchv = ${DB_ENV}Achv"
echo "DB_ENVStgAxBOT = ${DB_ENV}StgAxBOT"
echo "DB_ENVMetaDataVIN = ${DB_ENV}MetaDataVIN"


###################################################################################################
#  Print script syntax and help
###################################################################################################

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir> <JobRunID>"
    echo ""
    echo "   Base directory is   : PMRootDir"
    echo "   <JobRunID>         : JobRunID to archive"
    echo ""
    }


############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 2 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


############################################################################
# Script and log file directories                                          #
############################################################################

# Log Directory
LOG_DIR=$1/log
echo "LOG_DIR = " $LOG_DIR


# Bteq Log File
LOG_FILE=$LOG_DIR/`basename $0 .sh`_$JobRunID"_"$CURRENT_TS"_bteq".log
echo "LOG_FILE = " $LOG_FILE
#chmod 777 $LOG_FILE


# Bteq Directory
SCRIPT_DIR=$1/bin
echo "SCRIPT_DIR = " $SCRIPT_DIR


# Bteq File
#BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/bin/`basename $0 .sh`_$CURRENT_TS.bteq
BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/tmp/`basename $0 .sh`_$CURRENT_TS.bteq
echo "BTEQ_ARCHIVE_SOURCE_SCRIPT = " $BTEQ_ARCHIVE_SOURCE_SCRIPT 
>$BTEQ_ARCHIVE_SOURCE_SCRIPT
#chmod 777 $BTEQ_ARCHIVE_SOURCE_SCRIPT


############################################################################
# TO GET LOGON INFORMATION FOR BTEQ                                        #
############################################################################

PWD_FILE=$SCRIPT_DIR/PWD_BteqLogon
echo "PWD_FILE = " $PWD_FILE

LOGON=`grep $DB_ENV"_"LOGON $PWD_FILE `

test "$LOGON" = "" && {
	echo "Error: Could not get LOGON info from $PWD_FILE file"
	return -1 
}

#LOGON_FILE=$SCRIPT_DIR/$DB_ENV"_"$JobRunID"_LOGON"
LOGON_FILE=$1/tmp/$DB_ENV"_"$JobRunID"_LOGON"
echo "LOGON_FILE = " $LOGON_FILE
#chmod 777 $LOGON_FILE

LOGON=`echo $LOGON | cut -d "=" -f2`

#Test condition to check if the file already exists

if [ -f $LOGON_FILE ]		
then
	#echo "Logon File Exists"
	EXISTING_LOGON=`head -1 $LOGON_FILE`
	
	if [[ $LOGON = $EXISTING_LOGON ]]
	then
	echo "Same credentials reusing existing file"
	else
	echo $LOGON > $LOGON_FILE
	fi
else 
	#file does not exist, use latest credentials
	echo $LOGON > $LOGON_FILE
fi
chmod 777 $LOGON_FILE

echo "
.SET SESSION TRANSACTION ANSI;

.RUN FILE = $LOGON_FILE" > $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=CreateArchiveTables;ClientUser=$LOGNAME;' FOR SESSION;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "commit;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

############################################################################
# BTEQ                                                                                     
############################################################################

echo "
-- ---------------------------------------------------------------------------------------------------------------------
-- The output may exceed the standard 80 characters, it is expanded to 5000
-- ---------------------------------------------------------------------------------------------------------------------
.width 5000
.set format on
.foldline off" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

start_time=`date`
echo "start_time = " $start_time > $LOG_FILE


echo "
------------------------------------------------------------------------------------------
-- 1) AxBO_POS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxBO_POS
(
         SequenceId
        ,SAPCustomerID
        ,StoreId
        ,TransactionID
		,LoyaltyMemberID 
        ,ContentType   
        ,SubContentType
        ,TransactionTS 
        ,ExtractionTS  
        ,IEReceivedTS  
        ,IEDeliveryTS  
        ,SourceVersion 
        ,AOFlag        
        ,TS            
        ,ArchiveKey    
        ,XmlData       
        ,JobRunId      
		,ARC_TS
)
SELECT
         Pos.SequenceId
        ,Pos.SAPCustomerID
        ,Pos.StoreId
        ,Pos.TransactionID
		,Pos.LoyaltyMemberID 
        ,Pos.ContentType    
        ,Pos.SubContentType 
        ,Pos.TransactionTS  
        ,Pos.ExtractionTS   
        ,Pos.IEReceivedTS   
        ,Pos.IEDeliveryTS   
        ,Pos.SourceVersion  
        ,Pos.AOFlag         
        ,Pos.TS             
        ,Pos.ArchiveKey     
        ,Pos.XmlData        
		,$JobRunID AS JobRunID
		,current_timestamp	
			FROM
			        ${DB_ENV}StgAxBOVOUT.Stg_AxBO_POS Pos
			LEFT OUTER JOIN
			        ${DB_ENV}StgAxBOT.Stg_AxBO_POSQ PosQ
			        ON
			        Pos.SequenceId      = PosQ.SequenceId
			    AND Pos.SAPCustomerID   = PosQ.SAPCustomerID
			    AND Pos.TransactionTS   = PosQ.TransactionTS
				AND Pos.IEDeliveryTS   = PosQ.IEDeliveryTS
				
			LEFT OUTER JOIN
			        ${DB_ENV}UtilT.AxBO_POS_Load_Cache POSCache
			        ON
			        Pos.SequenceId      = POSCache.SequenceId
			    AND Pos.SAPCustomerID   = POSCache.SAPCustomerID
			    AND Pos.TransactionTS   = POSCache.TransactionTS
				AND Pos.IEDeliveryTS   = POSCache.IEDeliveryTS				
			
			LEFT OUTER JOIN
			        ${DB_ENV}StgAxBOT.S1_AxBO_POS_Receipt Rpt
			        ON
			        Pos.ArchiveKey = Rpt.ArchiveKey		
			        
   LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Pos.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'AxBO_POS'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Pos.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'AxBO_POS'
        AND XPV.ResolveStatus = 'E'
        		
			
			WHERE
			        Pos.SubContentType IN ('Retail','Control','TenderControl','TransactionExportList')  AND  -- Incuding "TransactionExportList" for the moment
			        Rpt.ArchiveKey   IS NULL         AND
			        PosQ.SequenceId  IS NULL         AND
			        POSCache.SequenceId  IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
;


.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgAxBOVIN.Stg_AxBO_POS WHERE ArchiveKey IN
(
        SELECT 
                ArchiveKey
        FROM
        (
                SELECT
						Pos.ArchiveKey
										FROM
			        ${DB_ENV}StgAxBOVOUT.Stg_AxBO_POS Pos
			LEFT OUTER JOIN
			        ${DB_ENV}StgAxBOT.Stg_AxBO_POSQ PosQ
			        ON
			        Pos.SequenceId      = PosQ.SequenceId
			    AND Pos.SAPCustomerID   = PosQ.SAPCustomerID
			    AND Pos.TransactionTS   = PosQ.TransactionTS
				AND Pos.IEDeliveryTS   = PosQ.IEDeliveryTS
				
			LEFT OUTER JOIN
			        ${DB_ENV}UtilT.AxBO_POS_Load_Cache POSCache
			        ON
			        Pos.SequenceId      = POSCache.SequenceId
			    AND Pos.SAPCustomerID   = POSCache.SAPCustomerID
			    AND Pos.TransactionTS   = POSCache.TransactionTS
				AND Pos.IEDeliveryTS   = POSCache.IEDeliveryTS				
			
			LEFT OUTER JOIN
			        ${DB_ENV}StgAxBOT.S1_AxBO_POS_Receipt Rpt
			        ON
			        Pos.ArchiveKey = Rpt.ArchiveKey		
			        
   LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_SourceError SE
        ON
        Pos.ArchiveKey = SE.ArchiveKey
        AND SE.StreamName = 'AxBO_POS'
        AND SE.ResolveStatus = 'E'
     
LEFT OUTER JOIN
        ${DB_ENV}CntlCleanseT.R4_XMLParsingValidation XPV
        ON
        Pos.ArchiveKey = XPV.ArchiveKey
        AND XPV.StreamName = 'AxBO_POS'
        AND XPV.ResolveStatus = 'E'
        		
			
			WHERE
			        Pos.SubContentType IN ('Retail','Control','TenderControl','TransactionExportList')  AND  -- Incuding "TransactionExportList" for the moment
			        Rpt.ArchiveKey   IS NULL         AND
			        PosQ.SequenceId  IS NULL         AND
			        POSCache.SequenceId  IS NULL AND
		SE.ArchiveKey   IS NULL         AND
		XPV.ArchiveKey   IS NULL
        )tmp
        GROUP BY 1
)AND SubContentType IN ('Retail','Control','TenderControl','TransactionExportList') -- Incuding "TransactionExportList" for the moment
-- Archiving all content types
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 2) JobRun Finish
------------------------------------------------------------------------------------------

UPDATE ${DB_ENV}MetaDataVIN.JobRun
SET JobEnd = CURRENT_TIMESTAMP
WHERE
JobRunID = $JobRunID
;


.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE
COMMIT WORK;
.IF ERRORCODE <> 0 THEN  .QUIT ERRORCODE

.QUIT 0


------------------------------------------------------------------------------------------
-- 4) Error Handling
------------------------------------------------------------------------------------------

.LABEL RETURNERROR

.QUIT ERRORCODE" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT START ----------------" >> $LOG_FILE
echo >> $LOG_FILE

cat $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT END ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS START AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

bteq < $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE 2>&1

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS END AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo "start time $start_time" >> $LOG_FILE
echo "end time `date`" >> $LOG_FILE


BTEQ_RETURN_CODE=""
BTEQ_RETURN_CODE=`grep 'RC (return code)' $LOG_FILE `

test "$BTEQ_RETURN_CODE" = "" && {
        echo "Error: Unable to execute $BTEQ_ARCHIVE_SOURCE_SCRIPT"
        echo "Error: Could not find BTEQ_RETURN_CODE from $LOG_FILE"
        echo "Info: Try executing the command task again, check logon user and password"
        return -1
}

BTEQ_RETURN_CODE=`echo $BTEQ_RETURN_CODE | cut -d "=" -f2 | awk '{print $1}'`
echo "BTEQ_RETURN_CODE ="$BTEQ_RETURN_CODE

if [ "$BTEQ_RETURN_CODE" = "0" ]; then
        rm  -f $BTEQ_ARCHIVE_SOURCE_SCRIPT
        rm  -f $LOG_FILE
        rm -f $LOGON_FILE
        #rm -f $PMRootDir/log/`basename $0`_$JobRunID"_"$CURRENT_TS"_script".log
        return $BTEQ_RETURN_CODE
        

else
        return -1
fi