#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: execwf.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/execwf.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Usage: execwf folder workflow
#
if [ $# -ne 2 ]
then
	echo "Usage:  `basename $0` folder workflow"
	exit 1
fi
domain="$DOMAIN_NAME"
service="is_axedw"
user="$REPOSITORY_USER_NAME"
password="REPOSITORY_USER_PASSWORD"
folder="$1"
workflow="$2"

`dirname $0`/StartInfaWF.sh "$domain" "$service" "$user" "$password" "$folder" "$workflow"
status=$?

exit $status
