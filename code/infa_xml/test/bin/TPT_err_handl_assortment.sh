#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: TPT_err_handl_assortment.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/TPT_err_handl_assortment.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"


echo "PMRootDir=$1"
echo "PARAM_DIR=$PMRootDir/par"



if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"



logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq

SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=ErrorHandling;ClientUser=$LOGNAME;' FOR SESSION;

--*********************************************
--Handle TPT Worktables
--*********************************************

call ${DB_ENV}CntlCleanseT.CaptureTPTErrors('\$\$DB_ENVStgSAPT', 'S1_SAP_AssortmentMaster', 'XPK_MT_EDW_Assortment', 'Stg_Assortment', 'ET1_S1_SAP_AssortmentMaster', 'ET2_S1_SAP_AssortmentMaster', 'WT_S1_SAP_AssortmentMaster', 'LT_S1_SAP_AssortmentMaster', 'SAP_Assortment');

call ${DB_ENV}CntlCleanseT.CaptureTPTErrors('\$\$DB_ENVStgSAPT', 'S1_SAP_Assortment_User', 'XPK_User', 'Stg_Assortment', 'ET1_S1_SAP_Assortment_User', 'ET2_S1_SAP_Assortment_User', 'WT_S1_SAP_Assortment_User', 'LT_S1_SAP_Assortment_User', 'SAP_Assortment');

call ${DB_ENV}CntlCleanseT.CaptureTPTErrors('\$\$DB_ENVStgSAPT', 'S1_SAP_Assortment_Article', 'XPK_Article', 'Stg_Assortment', 'ET1_S1_SAP_Assortment_Article', 'ET2_S1_SAP_Assortment_Article', 'WT_S1_SAP_Assortment_Article', 'LT_S1_SAP_Assortment_Article', 'SAP_Assortment');

call ${DB_ENV}CntlCleanseT.CaptureTPTErrors('\$\$DB_ENVStgSAPT', 'S1_SAP_Assortment_UOM', 'XPK_UOM', 'Stg_Assortment', 'ET1_S1_SAP_Assortment_UOM', 'ET2_S1_SAP_Assortment_UOM', 'WT_S1_SAP_Assortment_UOM', 'LT_S1_SAP_Assortment_UOM', 'SAP_Assortment');


SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile
