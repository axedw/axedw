#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: RemoveTriggerExclusiveAccessToS1.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/RemoveTriggerExclusiveAccessToS1.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export TriggerFile=$2

echo "PMRootDir=$1"
TRIG_FILE=$PMRootDir/trg/$TriggerFile
echo $TRIG_FILE
sleep $((${RANDOM}%4))

status="running"
while [ "$status" != "done" ]
do
	while [ ! -f $TRIG_FILE ]
	do
		sleep 1
	done
	# if we can remove the file all is ok and we can exit
	# else another process most likely has removed it so we have to wait again
	rm $TRIG_FILE && status="done"
done
exit 0
