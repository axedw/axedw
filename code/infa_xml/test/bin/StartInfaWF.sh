#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: StartInfaWF.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/StartInfaWF.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Usage: StartInfaWF domain service user password folder workflow
#
. $HOME/.infascm_profile

if [ $# -lt 6 ]
then
	echo "Usage:  `basename $0` domain service user password folder workflow [parameter=value]..."
	exit 1
fi
domain="$1"
service="$2"
user="$3"
password="$4"
folder="$5"
workflow="$6"
if [ $# -gt 6 ]
then
	paramfile=/tmp/$$.prm
	parameterfile="-lpf $paramfile"
	shift; shift; shift; shift; shift; shift
	echo "[Service:$service]" >$paramfile
	for par in "$@"
	do
		echo "\$\$$par"
	done >>$paramfile
fi

pmcmd startworkflow -domain "$domain" -service "$service" -user "$user" -pv "$password" -folder "$folder" $parameterfile -wait "$workflow"
status=$?

test "$paramfile" != "" && rm -f $paramfile

exit $status
