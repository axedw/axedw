#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: CollectStatisticsSem_1.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/CollectStatisticsSem_1.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=COLLECT_STAT;' FOR SESSION;

--*********************************************
--SEMCMNT tabeller
--*********************************************

COLLECT STATISTICS ON ${DB_ENV}SemCMNT.ACTIVITY_CODE;
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.CAMPAIGN_SALES_TYPE;
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.DUMMY;
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.SALES_LOCATION_CODE;

--*********************************************
--SEMCMNT tabeller och aggjoinindex
--*********************************************
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.SALES_TRANSACTION_DAY_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.LOYALTY_MEMBER_DAY_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.LOYALTY_TRANSACTION_DAY_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.ORDER_CONTROL_DAY_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.SALES_TRAN_TOTALS_DAY_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.STOCK_B_R_CNTRL_DAY_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.J1SCAN_CODE_D;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_LOYALTY_CONTACT_MONTH_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_LOYALTY_CONTACT_WEEK_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_LOYALTY_MEMBER_MONTH_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_LOYALTY_MEMBER_WEEK_F;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_ORDER_CONTROL_WEEK;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_ORDER_CONTROL_MONTH;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_PERPETUAL_INVENTORY_WEEK;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_PERPETUAL_INVENTORY_MONTH;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_SALES_TRANSACTION_WEEK;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_SALES_TRANSACTION_MONTH;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_SALES_TRAN_D_L_ITEM_WEEK;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_SALES_TRAN_D_L_ITEM_MONTH;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI2_SALES_TRAN_L_MONTH_LVL2;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI2_SALES_TRAN_L_WEEK_LVL2;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_SALES_TRAN_TOTALS_WEEK;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_SALES_TRAN_TOTALS_MONTH;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_STOCK_B_R_CNTRL_WEEK;
.os date 
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI1_STOCK_B_R_CNTRL_MONTH;
.os date
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI3_SALES_TRAN_LINE_WEEK;
.os date
COLLECT STATISTICS ON ${DB_ENV}SemCMNT.AJI3_SALES_TRAN_LINE_MONTH;
.os date

SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile
