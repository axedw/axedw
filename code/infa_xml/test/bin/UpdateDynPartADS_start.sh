#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: UpdateDynPartADS_start.sh 21815 2017-03-06 08:43:12Z K9113030 $
# Last Changed By  : $Author: K9113030 $
# Last Change Date : $Date: 2017-03-06 09:43:12 +0100 (mån, 06 mar 2017) $
# Last Revision    : $Revision: 21815 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_xml/test/bin/UpdateDynPartADS_start.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
#/bin/ksh
edw_env=''

        if [[ $(hostname -s) = dwpret02 ]]
        then
                        edw_env=pr

        elif [[ $(hostname -s) = dwitet02 ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi

export PMRootDir="/opt/axedw/${edw_env}/is_axedw1/infa_shared"
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo PMRootDir="${PMRootDir}"


  if [ "${edw_env}" = "pr" ]
  then

      print running in production
      sudo -u etpradm1 /opt/axedw/pr/is_axedw1/infa_shared/bin/UpdateDynPartADS.sh /opt/axedw/pr/is_axedw1/infa_shared
      if [ "$?" = "1" ] ; then
         exit 1
      fi

  elif [ "${edw_env}" = "it" ]
  then
      print running in it
      sudo -u etitadm1 /opt/axedw/it/is_axedw1/infa_shared/bin/UpdateDynPartADS.sh /opt/axedw/it/is_axedw1/infa_shared
      if [ "$?" = "1" ] ; then
         exit 1
      fi


      print running in uv1
      ssh dwutet02 "sudo -u etuvadm1 /opt/axedw/uv/is_axedw1/infa_shared/bin/UpdateDynPartADS.sh /opt/axedw/uv/is_axedw1/infa_shared"
      if [ "$?" = "1" ] ; then
         exit 1
      fi

      print running in uv2
      ssh dwutet02 "sudo -u etuvadm2 /opt/axedw/uv/is_axedw2/infa_shared/bin/UpdateDynPartADS.sh /opt/axedw/uv/is_axedw2/infa_shared"
      if [ "$?" = "1" ] ; then
         exit 1
      fi


      print running in st1
      ssh dwutet02 "sudo -u etstadm1 /opt/axedw/st/is_axedw1/infa_shared/bin/UpdateDynPartADS.sh /opt/axedw/st/is_axedw1/infa_shared"
      if [ "$?" = "1" ] ; then
         exit 1
      fi


      print running in st2
      ssh dwutet02 "sudo -u etstadm2 /opt/axedw/st/is_axedw2/infa_shared/bin/UpdateDynPartADS.sh /opt/axedw/st/is_axedw2/infa_shared"
      if [ "$?" = "1" ] ; then
         exit 1
      fi

  fi
