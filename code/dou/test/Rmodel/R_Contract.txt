library(tdr)
Streamno <- 0
slno <- list(datatype="INTEGER_DT", bytesize="SIZEOF_INTEGER")
real <- list(datatype="REAL_DT", bytesize="SIZEOF_REAL")
ts <- list(datatype="VARCHAR_DT", charset="LATIN_CT", size.length=26)
coldef <- list(CustID=slno, predBuyProduct=real, score_timestamp=ts)
tdr.SetOutputColDef(Streamno, coldef)
