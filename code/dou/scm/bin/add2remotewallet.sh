#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: add2remotewallet.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/add2remotewallet.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-01-10	Teradata	Initial version
#
# --------------------------------------------------------------------------
# Description
#   Add entry to tdwallet on remote machine for another user
#
# Parameters:
#	1 = path to scm workspace
#	2 = remote user
#	3 = remote server
#	4 = username to add to tdwallet
#	5 = password for the username
#	eg. /workspace/dev deployuser deploynode.local.net dbowner dbownerPWD
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) copy needed files to remote system
# 2.) execute add2wallet.sh on remote machine to setup the user and password in tdwallet
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
if [ $# != 5 ]
then
	echo "$0: Usage: `basename $0` workspace remote_user remote_server name value" >&2
	exit 1
fi

export WORKSPACE="$1"
export remote_user="$2"
export remote_server="$3"
export username="$4"
export password="$5"
tmp_dir="/tmp"

cd "${WORKSPACE}/util/scm/bin" || exit 1

pwdtools="${tmp_dir}/${remote_user}/pwdtools"

echo "Create landing area on $remote_server for pwdtools: $pwdtools"
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"rm -rf ${pwdtools}\"" </dev/null
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"mkdir -p ${pwdtools}/bin\"" </dev/null
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"chmod -R a+rwx ${tmp_dir}/${remote_user}\"" </dev/null

echo "Distributing add2wallet.sh"
tar -czf - add2wallet.sh | ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"tar -xzvf - -C ${pwdtools}/bin\""
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"chmod a+rwx ${pwdtools}/bin/add2wallet.sh\"" </dev/null

echo "Add ${username} to tdwallet for user ${remote_user} on server ${remote_server}"
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" ${pwdtools}/bin/add2wallet.sh "${username}" "${password}" </dev/null

result="`ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" tdwallet list </dev/null`"
listeduser="`echo "${result}" | grep ${username}`"
if [ "${listeduser}" != "${username}" ]
then
    echo "$0: Failed to add ${username} to tdwallet: $result"
	#exit 1
	# commented bad exit since there is currently a limit of number of entires possible to list
	# ERROR: Could not get list of item names because:  End of file encountered during read.
else
    echo "$0: Successfully added ${username} to tdwallet for user ${remote_user} on server ${remote_server}"
fi
exit 0

