#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: add2remotekeystore.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/add2remotekeystore.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-01-10	Teradata	Initial version
#
# --------------------------------------------------------------------------
# Description
#   Add entry to keystore in remote home folder for remote user
#
# Parameters:
#	1 = path to scm workspace
#	2 = remote user
#	3 = remote server
#	4 = system name where the database user is
#	5 = username to add to tdwallet
#	6 = password for the username
#	eg. /workspace/dev deployuser deploynode.local.net deploydb dbowner dbownerPWD
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) copy needed files to remote system
# 2.) execute add2keystore.sh on remote machine to setup the user and password in keystore
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
if [ $# != 6 ]
then
	echo "$0: Usage: `basename $0` workspace remote_user remote_server databasesystem name value" >&2
	exit 1
fi

export WORKSPACE="$1"
export remote_user="$2"
export remote_server="$3"
export dbsystem="$4"
export username="$5"
export password="$6"
tmp_dir="/tmp"
result_file="`mktemp ${tmp_dir}/${remoteuser}XXXXX`"
cd "${WORKSPACE}/util/scm/bin" || exit 1ll 

pwdtools="${tmp_dir}/${remote_user}/pwdtools"

echo "Create landing area on $remote_server for pwdtools: $pwdtools"
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"rm -rf ${pwdtools}\"" </dev/null
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"mkdir -p ${pwdtools}/bin\"" </dev/null
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"mkdir -p ${pwdtools}/lib\"" </dev/null
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"chmod -R a+rwx ${tmp_dir}/${remote_user}\"" </dev/null

echo "Distribute add2keystore.sh"
tar -czf - add2keystore.sh | ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"tar -xzvf - -C ${pwdtools}/bin\""
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"chmod a+rwx ${pwdtools}/bin/add2keystore.sh\"" </dev/null

echo "Distribute TJEncryptPassword.class"
tar -czf - TJEncryptPassword.class | ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"tar -xzvf - -C ${pwdtools}/bin\""
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" "sh -c \"chmod a+rwx ${pwdtools}/bin/TJEncryptPassword.class\"" </dev/null

echo "Add ${username} to keystore for user ${remote_user} on server ${remote_server}"
ssh -oStrictHostKeyChecking=no "${remote_server}" sudo -iu"${remote_user}" ${pwdtools}/bin/add2keystore.sh "${dbsystem}" "${username}" "${password}" </dev/null | tee "$result_file"

grep "Decrypted password: ${password}" "$result_file" >/dev/null
if [ $? -ne 0 ]
then
    echo "$0: Failed to add ${username} to tdwallet: `cat ${result_file}`"
    exit 1
else
    echo "$0: Successfully added ${username} to tdwallet for user ${remote_user} on server ${remote_server}"
fi
exit 0
