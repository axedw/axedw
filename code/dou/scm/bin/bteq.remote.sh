#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: bteq.remote.sh 32502 2020-06-16 15:52:55Z  ${BTEQ_REMOTE_USER} $
# Last Changed By  : $Author: ${BTEQ_REMOTE_USER} $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/bteq.remote.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2018-03-26	Teradata	Initial version
#
# --------------------------------------------------------------------------
# Description
#   Run BTEQ script on remote node
#
# Parameters:
#	None, can only process scripts from standard input
#	eg. <script_to_execute.btq
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) copy needed files to remote system
# 2.) execute bteq on remote machine to execute the standard input
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
#
# Environment
# BTEQ_REMOTE_NODE - the name of the node to execute on
# BTEQ_REMOTE_USER - the name of the server login to use, this assiumes that the public key in .ssh has been setup
#    the file ~.ssh/$[uname -n) must exist on the remote node and be the public key for this node
# --------------------------------------------------------------------------
if [ $# != 0 ]
then
	echo "$0: Usage: `basename $0` <bteq_script_to_execute" >&2
	exit 1
fi

PrivateKeyFile="~/.ssh/$(uname -n)"
tmp_dir="/tmp"

#
# make sure run_ddl.sh is available
#
scp -i "${PrivateKeyFile}" "$(type run_ddl.sh | while read f1 f2 f3 rest; do echo $f3; done)" "${BTEQ_REMOTE_USER}@${BTEQ_REMOTE_NODE}:${tmp_dir}/run_ddl.sh"

#
# copy input files to remote node and execute bteq script on remote node with adjusted input
#
bteq_build_file="$(mktemp ${tmp_dir}/bteq.build.XXXXX.btq)"
bteq_result_file="$(mktemp ${tmp_dir}/bteq.result.XXXXX.log)"
cat | while read w1 w2 w3 rest
do
	if [ "${w1}" = ".RUN" ]
	then
		cat "${w3}"
	else
		case "${w1}" in
		".COMPILE")
			echo "--Unable to compile on remote node, please use .OS run_ddl.sh: $w1 $w2 $w3 $rest"
			;;
		".OS")
			runddl="$(echo "${rest}" | grep -i "run_ddl.sh")"
			if [ "${runddl}" != "" ]
			then
				#
				# process on remote node, run_ddl.sh should be available
				#
				echo "-- Original command: ${w1} ${w2} ${w3} ${rest}"
				set -- ${w1} ${w2} ${w3} ${rest}
				file_to_copy="$7"
				echo "-- file_to_copy: $file_to_copy"
				nfile="$(basename $file_to_copy)"
				echo "-- nfile: $nfile"
				new_filename="$(mktemp ${tmp_dir}/$nfile.XXXXX)"
				echo "-- new_filename: $new_filename"
				grep '^.COMPILE' "${file_to_copy}" | tr '=' ' ' | while read c f file rest
				do
					new_file_to_compile="$(mktemp ${tmp_dir}/$(basename $file).XXXXX)"
					scp -i "${PrivateKeyFile}" "${file}" "${BTEQ_REMOTE_USER}@${BTEQ_REMOTE_NODE}:${new_file_to_compile}"
					echo -e ".COMPILE FILE ${new_file_to_compile}" >"${new_filename}"
				done

				scp -i "${PrivateKeyFile}" "${new_filename}" "${BTEQ_REMOTE_USER}@${BTEQ_REMOTE_NODE}:$new_filename"
				echo -e "$1 $2 $3 $4 $5 ${tmp_dir}/run_ddl.sh ${new_filename} /tmp /tmp\c"
				shift
				echo " $9"
			else
				#
				# process locally at end
				#
				echo "--.DO_OS ${w2} ${w3} ${rest}"
			fi
			;;
		".IMPORT")
			echo "-- Original command: ${w1} ${w2} ${w3} ${rest}"
			file_to_copy="$(echo "${rest}"|tr '=' ' '|while read f1 file rest; do echo $file; done)"
			new_filename="$(mktemp ${tmp_dir}/$(basename $file_to_copy).XXXXX)"
			deferred="$(echo "${w3}" | grep -i "defercols=")"
			if [ "${deferred}" != "" ]
			then
				cat "${file_to_copy}" | tr '|' ' ' | while read file1 file2
				do
					new_file1_to_copy="$(mktemp ${tmp_dir}/$(basename $file1).XXXXX)"
					echo -e "${new_file1_to_copy}\c" >>"${new_filename}"
					scp -i "${PrivateKeyFile}" "${file1}" "${BTEQ_REMOTE_USER}@${BTEQ_REMOTE_NODE}:${new_file1_to_copy}"
					if [ "${file2}" != "" ]
					then
						new_file2_to_copy="$(mktemp ${tmp_dir}/$(basename $file2).XXXXX)"
						echo -e "|${new_file2_to_copy}\c" >>"${new_filename}"
						scp -i "${PrivateKeyFile}" "${file2}" "${BTEQ_REMOTE_USER}@${BTEQ_REMOTE_NODE}:${new_file2_to_copy}"
					fi
				done
				echo "" >>"${new_filename}"
			fi
			scp -i "${PrivateKeyFile}" "${new_filename}" "${BTEQ_REMOTE_USER}@${BTEQ_REMOTE_NODE}:$new_filename"
			echo ".IMPORT ${w2} ${w3} FILE=${new_filename}"
			;;
		*)
			echo "${w1} ${w2} ${w3} ${rest}"
			;;
		esac
	fi
done | tee "${bteq_build_file}" | ssh -i "${PrivateKeyFile}" -l ${BTEQ_REMOTE_USER} ${BTEQ_REMOTE_NODE} bash -c "bteq 2>&1" | tee "${bteq_result_file}" | while read w1 w2 w3 w4 w5 rest
do
	case "${w1}" in
	"--.DO_OS")
		bash -c "${w2} ${w3} ${w4} ${w5} ${rest}"
		;;
	esac
done

cat "${bteq_result_file}"
egrep "^ \*\*\* Failure|\*\*\*Error:" "${bteq_result_file}" >/dev/null
if [ $? = 0 ]
then
	stat=1
else
	stat=0
fi
exit $stat
