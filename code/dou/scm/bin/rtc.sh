#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: rtc.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/rtc.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2013-06-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
# Get list of changed files from RTC
if [ $# != 4 ]
then
	echo "Usage: `basename $0` snapshot_file change_list version components" >&2
	exit 1
fi

snapshot_file="$1"
change_list="$2"
version="$3"
components="$4"
if [ "$components" = "" ]
then
	components="ALL"
fi

echo "Searching for files to process..."
for component in $components
do
	orig_snapshot_file="${snapshot_file}"
	orig_change_list="${change_list}"
	
	if [ "${component}" != "ALL" ]
	then
		#
		# if the component is suffixed with ampersand & (for background build) remove it
		#
		bck="$(echo "$component" | sed 's/.*\(.\)$/\1/')"
		if [ "$bck" = '&' ]
		then
				component="$(echo "$component" | sed 's/\(.*\).$/\1/')"
		fi

		#
		# component specified, must use component specific snapshot file
		# if component specific snapshot does not exists try use the generic one
		#
		#
		# replace any slashes with a dot in component before using it as a suffix
		#
		file_suffix="$(echo $component | sed -e 's,/,.,g')"

		new_snapshot_file="${snapshot_file}.${file_suffix}"
		if [ ! -f "${new_snapshot_file}" -a -f "${snapshot_file}" ]
		then
				cp "${snapshot_file}" "${new_snapshot_file}"
		fi
		snapshot_file="${new_snapshot_file}"
		change_list="${change_list}.${file_suffix}"
	fi
	
	echo "Using snapshot file: $snapshot_file"
	echo "Using changelist file: $change_list"
	
	# only run compare if there is a previous snapshot available
	if [ -e "${snapshot_file}" ]
	then
		#
		# compare previous snapshot with current and generate change list file
		# first backup possible existing change list file
		#
		test -f "${change_list}" && cp --backup=numbered "${change_list}" "${change_list}.${version}"

		$SCM_HOME/scm login -n ${TD_ENVNAME} -r https://scm.local.net/ccm --username rtcuser --password "`cat $HOME/.scmpasswd`" || exit 1

		$SCM_HOME/scm compare -r ${TD_ENVNAME} -j -D 'YYYYMMDDHHmmsss' snapshot `perl -e 'print $ENV{"team.scm.snapshotUUID"}'` snapshot `cat "${snapshot_file}"`  > "${change_list}"
		if [ $? != 0 ]
		then
			$SCM_HOME/scm logout -r ${TD_ENVNAME} || exit 33
			exit 2
		fi

		$SCM_HOME/scm logout -r ${TD_ENVNAME} || exit 3
	else
		#
		# no snapshot file found, must be first time running so create it
		#
		perl -e 'print $ENV{"team.scm.snapshotUUID"}' > "${snapshot_file}" || exit 4
	fi
	snapshot_file="${orig_snapshot_file}"
	change_list="${orig_change_list}"
done

echo "Component change list generated: ${change_list}"
perl -e 'print "Current snapshot UUID: " . $ENV{"team.scm.snapshotUUID"} . "\n"'

exit 0
