#!/usr/bin/perl
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: getrtcinfo.pl 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/getrtcinfo.pl $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2013-06-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
# Find changed files and output information on stdout

#use strict;
use warnings;
use JSON qw( decode_json );

use Data::Dumper;
$input_file=$ARGV[0];
local $/; # enable slurp
open(my $difflist, "<", $input_file) or die "Can't open " . $input_file . ": $!";

my $json = <$difflist>;
my $text = decode_json($json);

#print  Dumper($text), "\n";

foreach ( @{ $text->{'direction'} } ) {
	foreach ( @{ $_->{'components'} } ) {
		$ComponentName = $_->{"name"};
		foreach ( @{ $_->{'changesets'} } ) {
			$ChangesetCreationDate = $_->{"creationDate"};
			$ChangesetUserId = $_->{"author"}->{"userId"};
			$ChangesetUserName = $_->{"author"}->{"userName"};
			foreach ( @{ $_->{'versionables'} } ) {
				$VersionablesPath = $_->{"path"};
				$VersionablesPath =~ tr/\\//;
				$VersionablesURL = $_->{"url"};
				$VersionablesURL =~ tr/\\//;
				print  "M;" . $VersionablesPath . ";" . $VersionablesURL . ";" . $ChangesetCreationDate . ";" . $ChangesetUserId . ";" . $ChangesetUserName . ";" . $ComponentName . "\n"
			}
		}
	}
}

close $difflist or die "$difflist: $!";
