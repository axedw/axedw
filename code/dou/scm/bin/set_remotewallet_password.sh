#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: set_remotewallet_password.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/set_remotewallet_password.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-10-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
#   set the users wallet password on the remote server
#
# Parameters:
#	1 = path to scm workspace where the source files are
#	2 = remote user
#	3 = remote server
#	4 = password to set in tdwallet
#	5 = old password already set in tdwallet
#	eg. /workspace/dev deployuser deploynode.local.net dbowner dbownerPWD
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) copy needed files to remote system
# 2.) execute set_wallet_password.sh on remote machine to set the tdwallet password for the user
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
# --------------------------------------------------------------------------
if [ $# != 5 ]
then
	echo "$0: Usage: `basename $0` workspace remote_user remote_server tdwalletPassword tdwalletOldPassword" >&2
	exit 1
fi

export WORKSPACE="$1"
export remote_user="$2"
export remote_server="$3"
export password="$4"
export oldPassword="$5"
sudo_cmd="sudo -iu"${remote_user}""
iam="`id -n -u`"
if [ "${iam}" = "${remote_user}" ]
then
	#
	# do not do sudo if remote is same as me
	#
	sudo_cmd=""
fi

cd "${WORKSPACE}/util/scm/bin" || exit 1

tmp_dir="/tmp"
pwdtools="${tmp_dir}/${remote_user}/pwdtools"

echo "Create landing area on $remote_server for pwdtools: $pwdtools"
ssh -oStrictHostKeyChecking=no "${remote_server}" "${sudo_cmd}" "sh -c \"rm -rf ${pwdtools}\"" </dev/null
ssh -oStrictHostKeyChecking=no "${remote_server}" "${sudo_cmd}" "sh -c \"mkdir -p ${pwdtools}/bin\"" </dev/null
ssh -oStrictHostKeyChecking=no "${remote_server}" "${sudo_cmd}" "sh -c \"chmod -R a+rwx ${tmp_dir}/${remote_user}\"" </dev/null

echo "Distributing set_wallet_password.sh"
tar -czf - set_wallet_password.sh | ssh -oStrictHostKeyChecking=no "${remote_server}" "${sudo_cmd}" "sh -c \"tar -xzvf - -C ${pwdtools}/bin\""
ssh -oStrictHostKeyChecking=no "${remote_server}" "${sudo_cmd}" "sh -c \"chmod a+rwx ${pwdtools}/bin/set_wallet_password.sh\"" </dev/null

echo "Set tdwallet password for user ${remote_user} on server ${remote_server}"
ssh -oStrictHostKeyChecking=no "${remote_server}" "${sudo_cmd}" ${pwdtools}/bin/set_wallet_password.sh "${password}" "$oldPassword" </dev/null

exit 0

