#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: DeleteInOrder.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/DeleteInOrder.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-06-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
# List delete in referenced order
#
# E.g: DeleteInOrder 'local1510/applAdmin,$tdwallet(applAdmin)' applDB
execSQL=no
if [ "$1" = "-e" ]
then
	echo "*** Execution enabled, will execute the generated SQL code ***"
	execSQL=yes
	shift
fi
if [ $# != 2 ]
then
	echo "Usage: `basename $0` login TargetTopDB" >&2
	exit 1
fi

LOGON="$1"
TargetTopDB="$2"
tmp_dir="${WORKSPACE}/tmp"

SQLFile="$(mktemp ${tmp_dir}/DeleteSQLXXXXX)"
bteq <<-end
.logon $LOGON
CREATE VOLATILE TABLE DbDep AS 
(
       WITH RECURSIVE descendants(DatabaseName, DBPath, LEVEL) AS
       (
              SELECT DatabaseName, CAST(databasename AS VARCHAR(1024)) AS dbpath, 0
              FROM DBC.databasesv
              WHERE DatabaseName = '$TargetTopDB'
              UNION  ALL
              SELECT c.DatabaseName, p.DBPath || '>' || c.DatabaseName, p.LEVEL + 1
              FROM DBC.databasesv c, descendants p
              WHERE p.DatabaseName = c.OwnerName AND c.DatabaseName <> '$TargetTopDB'
       )
       SELECT DatabaseName DB_NAME, DBPath
       FROM descendants
) WITH DATA
NO PRIMARY INDEX
ON COMMIT PRESERVE ROWS;

.remark 'Exporting to file ${SQLFile}'

.errorout stdout
.width 2048
.set QUIET ON
.set TITLEDASHES OFF
.set UNDERLINE OFF
.set HEADING ''
.export report file=${SQLFile}

CREATE VOLATILE TABLE SQLResultTable
AS
(
        WITH RECURSIVE heirarchy_Constraints (ParentDB, ParentTable, ChildDB, ChildTable, LevelC)AS
        (
       		/*
       		 * All tables that have no children = leaf nodes
       		 */
               SELECT DISTINCT T.DatabaseName, T.TableName, T.DatabaseName, T.TableName, 0 LevelC
                FROM DBC.TABLESV as T
                LEFT OUTER JOIN DBC.All_RI_ChildrenV Root
                ON Root.ParentDB = T.DatabaseName
                AND Root.ParentTable = T.TableName
                WHERE T.DatabaseName IN (SELECT DB_NAME FROM DbDep)
                AND T.TableKind IN ('T','O')
                AND Root.ParentTable is NULL

                UNION ALL

                /*
                 * get the parent for the previous child
                 */
                SELECT RIParent.ParentDB, RIParent.ParentTable, RIParent.ParentDB, RIParent.ParentTable, heirarchy_Constraints.LevelC + 1
                FROM DBC.All_RI_ParentsV RIParent
                JOIN  heirarchy_Constraints
                ON    heirarchy_Constraints.ParentDB = RIParent.ChildDB
                AND  heirarchy_Constraints.ParentTable = RIParent.ChildTable
		AND	RIParent.ParentDB||'.'||RIParent.ParentTable <> RIParent.ChildDB||'.'||RIParent.ChildTable
                WHERE LevelC < 10
        )
	SELECT  'Delete from '||
	                TRIM(B.DB) || '.' || TRIM(B.PT) ||
	        ';'
	        AS DeleteStatement,
		LevelC
        FROM
        (
                SELECT *
                FROM
                (
                        SELECT  ParentDB DB, CAST(ParentTable AS VARCHAR(100)) PT ,LevelC
                        FROM heirarchy_Constraints
                        QUALIFY ROW_NUMBER() OVER(PARTITION BY ParentDB,ParentTable ORDER BY  LevelC DESC) = 1
                ) A
        ) B
)
WITH DATA
NO PRIMARY INDEX
ON COMMIT PRESERVE ROWS
;
SELECT DeleteStatement AS "--DeleteOrder"
FROM SQLResultTable
ORDER BY LevelC ASC
;
.export reset
.logoff
.exit
end

echo "********** Starting execution of ${SQLFile} **********"
if [ "${execSQL}" = "yes" ]
then
	bteq <<-end
	.logon $LOGON
	.run file=${SQLFile}
	end
else
	cat ${SQLFile}
fi

echo "Delete commands generated into file ${SQLFile}"
