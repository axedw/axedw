#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: chg2prepost.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/chg2prepost.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-10-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
# convert a TD2 sql script to a dbadmin btq script with PRE and POST calls
#
# chg2prepost [-o] [-r riFolder] [-d relDir] workspace file [file]...
#
# -o = only create a _runorder.txt file
# -r riFolder = where to find foreign key information, default = ../../ri_constraints/<currentFolderName>
# -d relDir = top folder where to find related files - must be relative to current folder, default = ..
#
# Example in applDB folder: chg2prepost.sh -r ../../ri_constraints/applDB -d .. $HOME/git/applDB/ *.sql
#
onlyRunorder="no"
if [ "$1" = "-o" ]
then
	shift
	onlyRunorder="yes"
fi
riFolder="../../ri_constraints/`basename \`pwd\``"
if [ "$1" = "-r" ]
then
	shift
	riFolder="$1"
	shift
fi
relDir=".."
if [ "$1" = "-d" ]
then
	shift
	relDir="$1"
	shift
fi
if [ $# -lt 2 ]
then
	echo "Usage: `basename $0` [-r riFolder] [-d relDir] workspace file [file]..." >&2
	echo "-r riFolder = where to find foreign key information, default = ../../ri_constraints/<currentFolderName>" >&2
	echo "-d relDir = top folder where to find related files - must be relative to current folder, default = .." >&2
	exit 1
fi
workspace="$1"
shift

doParent()
{
	local doParent_bf="$1"
	local doParent_f="$2"
	local level="$3"

	local indent="`echo | awk '{ for (i=0; i < level; i++) { printf( "\t" ); } }' level="${level}"`"
	local chkFile=""

	while read cmd doParent_file
	do
		chkFile="`basename ${doParent_file}`"
		egrep "[[:blank:]/]${chkFile}$|/${chkFile}" "${runorderNewFile}" >/dev/null
		if [ $? -eq 0 ]
		then
			#
			# already processed this file
			#
			echo "${indent}${doParent_file} is already in _runorder.txt"
			continue
		fi
		if [ -r "${doParent_file}".parent ]
		then
			doParent "${doParent_file}" "${doParent_file}".parent $(($level + 1))
		else
			egrep "[[:blank:]/]${chkFile}$" "${runorderNewFile}" >/dev/null
			if [ $? -eq 1 ]
			then
				echo -e "BTQRUN_ANSI\t${doParent_file}" >>"${runorderNewFile}"
				echo "${indent}${doParent_file} added"
			fi
		fi
	done <"$doParent_f"
	chkFile="`basename ${doParent_bf}`"
	egrep "[[:blank:]/]${chkFile}$" "${runorderNewFile}" >/dev/null
	if [ $? -eq 1 ]
	then
		echo -e "BTQRUN_ANSI\t${doParent_bf}" >>"${runorderNewFile}"
		echo "${indent}${doParent_bf} added"
	else
		echo "${indent}${doParent_bf} is already in _runorder.txt"
	fi
}

#
# init pattern variables
#
sqlProlog=".SET MAXERROR 0;

DATABASE %s\$#TD2_ENVNAME#;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMIT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL \$#DBADM_ENV#.DBA_NewTabDef_ANSI('%s\$#TD2_ENVNAME#','%s','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMIT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

";

scmHead="/*
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : \$Id:  \$
# Last Changed By  : \$Author:  \$
# Last Change Date : \$Date:  \$
# Last Revision    : \$Revision:  \$
# SCM URL          : \$HeadURL:  \$
# --------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
*/
";

sqlPostlog=".IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
COMMIT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL \$#DBADM_ENV#.DBA_NewTabDef_ANSI('%s\$#TD2_ENVNAME#','%s','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMIT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
";

#
# process input files
# expect a .sql file and output to a .btq file
#
runorderFile="_runorder.txt"
runorderNewFile="_runorder.txt.new"
if [ -r "${runorderFile}" -a "${onlyRunorder}" = "no" ]
then
	mv -f --backup=numbered "${runorderFile}" "${runorderFile}.old" || exit 1
fi
if [ -r "${runorderNewFile}" ]
then
	mv -f --backup=numbered "${runorderNewFile}" "${runorderNewFile}.old" || exit 1
fi

baseDir="`pwd | sed -e "s,$workspace,,"`"
riDir="${riFolder}"
dbSuffix='$#TD2_ENVNAME#'

if [ "$onlyRunorder" = "no" ]
then
	for inputFile in $@
	do
		echo "Processing file $inputFile"
		if [ ! -r "${inputFile}" ]
		then
			echo "`basename $0`: Unable to read ${inputFile}" >&2
			exit 1
		elif [ "`basename "${inputFile}" .sql`" = "${inputFile}" ]
		then
			echo "`basename $0`: Expecting the input file ($inputFile) with .sql as suffix" >&2
			exit 1
		fi

		outputFile="`basename "${inputFile}" .sql`.btq"
		if [ -r "${outputFile}" ]
		then
			mv -f --backup=numbered "${outputFile}" "${outputFile}.old" || exit 1
		fi

		awk '
		BEGIN	{
			IGNORECASE = 1;
			constraintsGenerated = 0;
		}
		/create.*table/,/;/ {
			if ( $1 == "create" ) {
				nrFields = split( $0, words );
				for ( f in words ) {
					if ( words[f] == "table" ) {
						n = split(  words[ f + 1 ], db_tab, "." );
						db = db_tab[1];
						tab = db_tab[2];
					}
				}
				printf( "%s", h );
				printf( p, db, db, tab, db, tab );
				i = 0;
				while ( i < nrFields ) {
					printf( "%s ", words[ i ] );
					i++;
				}
				printf( "%s$#TD2_ENVNAME#.%s\n", db, tab );
			}
			else {
				if ( NF == 1 && $1 == ")" && !constraintsGenerated ) {
					#
					# add constraints from constraint file in riDir folder if the file exists
					#	
					riFile=sprintf( "%s/%s", riDir, FILENAME );
					cmd=sprintf( "test -f %s &&", riFile );
					cmd=sprintf( "%s %s %s %s |", cmd, "egrep -i", "constraint", riFile );
					cmd=sprintf( "%s sed", cmd );
					cmd=sprintf( "%s -e \"s/.*add constraint/,constraint/\" ", cmd );
					cmd=sprintf( "%s -e \"s/;//\"", cmd );
					cmd=sprintf( "%s -e \"s/\\(@.*@\\)/\\1\\%s/\"", cmd, dbSuffix );
					while ( (cmd | getline constraints) > 0) {
						printf( "%s\n", constraints );
					}
					close( cmd );

					constraintsGenerated = 1;
					print $0;
				}
				else {
					print $0;
				}
			}
			next;
		}
		/commit/	{
			printf( o, db, tab );
			next;
		}
		{
			print $0;
		}
		END	{
			printf( "BTQRUN_ANSI\t%s/%s\n", b, FILENAME ) >>r;
		}
		' \
			p="$sqlProlog" \
			h="$scmHead" \
			o="$sqlPostlog" \
			r="$runorderFile" \
			b="${baseDir}" \
			riDir="${riDir}" \
			dbSuffix="${dbSuffix}" \
			"${inputFile}" >"${outputFile}"

	done
fi

#
# sort _runorder.txt file in dependency order
#
(
	(
		echo "--"
		echo "-- files without references"
		echo "--"
		for file in `egrep -L -i 'foreign.*key' *.btq`
		do
			echo -e "BTQRUN_ANSI\t${file}"
		done

		echo "--"
		echo "-- files with references"
		echo "--"
	)  >>"${runorderNewFile}"

	fkFile="${runorderFile}".fk

	#
	# generate list of files containing references
	#
	for file in `egrep -l -i 'foreign.*key' *.btq`
	do
		echo $file
	done >"${fkFile}"

	for file in `cat "${fkFile}"`
	do
		#
		# get information about parents for each file
		#
		awk '
		/foreign key/	{
			nrFields = split( $0, words );
			for ( f in words ) {
				pos=0;
				if ( words[f] == "references" ) {
					if ( words[f] == "references" && words[f+1] == "with" && words[f+2] == "check" && words[f+3] == "option" ) {
						pos = f + 4;
					}
					else if ( words[f] == "references" && words[f+1] == "with" && words[f+2] == "no" && words[f+3] == "check" ) {
						pos = f + 5;
					}
					else if ( words[f] == "references" ) {
						pos = f + 1;
					}
				}
				if ( pos != 0 ) {
					#
					# we have a reference, find the create table file for this reference
					#
					n = split(  words[ pos ], db_tab, "." );
					db = db_tab[1];
					gsub( "\\$", ".", db ); # $ has special meaning in grep, replace with a dot
					tab = db_tab[2];
					gsub( "\\$", ".", db ); # $ has special meaning in grep, replace with a dot
					cmd = sprintf( "find %s -type f -a -name \"*.btq\" -a -print |", relDir );
					cmd = sprintf( "%s tr \\$ . |", cmd );
					cmd = sprintf( "%s xargs egrep -l -i \"create table %s\\.%s[[:blank:]]|", cmd, db, tab );
					cmd = sprintf( "%screate table %s\\.%s\\$\" |", cmd, db, tab );
					cmd = sprintf( "%s sort | uniq", cmd );
					while ( (cmd | getline parent) > 0) {
						printf( "PARENT: %s\n", parent );
					}
					close( cmd );
					break;
				}
			}
		}' relDir="$relDir" "${file}" | sort | uniq >"${file}".parent
	done >>"${runorderNewFile}"

	echo "Resolving references"
	for file in *.parent
	do
		echo "${file}"
		doParent "`basename $file .parent`" "$file" 1
	done
)
sed -i -e "s,\t,\t${baseDir}/," "${runorderNewFile}"

mv -f --backup=numbered "${runorderFile}" "${runorderFile}.old" || exit 1
mv -f --backup=numbered "${runorderNewFile}" "${runorderFile}" || exit 1

exit 0
