#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LoadInOrder.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/LoadInOrder.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-06-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
# List select-insert in referenced order
#
# E.g: LoadInOrder 'local1510/applAdmin,$tdwallet(applAdmin)' applDB _STG 11
# or: LoadInOrder 'local1510/applAdmin,$tdwallet(applAdmin)' applDB +3 H
# or: LoadInOrder -r "s/from applDB\./from applDB_STG./g s/from applDB2\./from applDB2_STG./g" 'local1510/applAdmin,$tdwallet(applAdmin)' applDB X X
ExecSQL=no
if [ "$1" = "-e" ]
then
	echo "*** Execution enabled, will execute the generated SQL code ***"
	ExecSQL=yes
	shift
fi
ExecRepl=no
if [ "$1" = "-r" ]
then
	echo "*** replace enabled, will apply replace pattern on generated SQL code ***"
	ExecRepl=yes
	shift
	SubstPattern="$1"
	shift
fi
if [ $# != 4 ]
then
	echo "Usage: `basename $0` login TargetTopDB SrcDBPattern TargetDBPattern" >&2
	exit 1
fi

LOGON="$1"
TargetTopDB="$2"
SrcDBPattern="$3"
TargetDBPattern="$4"
tmp_dir="${WORKSPACE}/tmp"

if [ "${SrcDBPattern:0:1}" = "+" ]
then
	PartOneLength="$((${SrcDBPattern:1}-1))"
	ReplPattern="TRIM(SUBSTRING(B.DB FROM 1 FOR ${PartOneLength} ) || '$TargetDBPattern')"
else
	ReplPattern="TRIM(OREPLACE(B.DB, '$TargetDBPattern', '$SrcDBPattern'))"
fi
SQLFile="$(mktemp ${tmp_dir}/LoadSQLXXXXX)"
bteq <<-end
.logon $LOGON
CREATE VOLATILE TABLE DbDep AS 
(
       WITH RECURSIVE descendants(DatabaseName, DBPath, LEVEL) AS
       (
              SELECT DatabaseName, CAST(databasename AS VARCHAR(1024)) AS dbpath, 0
              FROM DBC.databasesv
              WHERE DatabaseName = '$TargetTopDB'
              UNION  ALL
              SELECT c.DatabaseName, p.DBPath || '>' || c.DatabaseName, p.LEVEL + 1
              FROM DBC.databasesv c, descendants p
              WHERE p.DatabaseName = c.OwnerName AND c.DatabaseName <> '$TargetTopDB'
       )
       SELECT DatabaseName DB_NAME, DBPath
       FROM descendants
) WITH DATA
NO PRIMARY INDEX
ON COMMIT PRESERVE ROWS;

.remark 'Exporting to file ${SQLFile}'

.errorout stdout
.width 2048
.set QUIET ON
.set TITLEDASHES OFF
.set UNDERLINE OFF
.set HEADING ''
.export report file=${SQLFile}

CREATE VOLATILE TABLE SQLResultTable
AS
(
        WITH RECURSIVE heirarchy_Constraints (ParentDB, ParentTable, ChildDB, ChildTable, LevelC)AS
        (
        	/*
        	 * All tables that have no parents = top nodes
        	 */
                SELECT DISTINCT T.DatabaseName, T.TableName, T.DatabaseName, T.TableName, 0 LevelC
                FROM DBC.TABLESV as T
                LEFT OUTER JOIN DBC.All_RI_ParentsV Root
                ON Root.ChildDB = T.DatabaseName
                AND Root.ChildTable = T.TableName
                WHERE T.DatabaseName IN (SELECT DB_NAME FROM DbDep)
                AND T.TableKind IN ('T','O')
                AND Root.ParentTable is NULL
                
                UNION ALL

                /*
                 * get the child for the previous parent
                 */
                SELECT RIChild.ChildDB, RIChild.ChildTable, RIChild.ChildDB, RIChild.ChildTable, heirarchy_Constraints.LevelC + 1
                FROM DBC.All_RI_ChildrenV RIChild
                JOIN  heirarchy_Constraints
                ON    heirarchy_Constraints.ParentDB = RIChild.ParentDB
                AND  heirarchy_Constraints.ParentTable = RIChild.ParentTable
		AND	RIChild.ParentDB||'.'||RIChild.ParentTable <> RIChild.ChildDB||'.'||RIChild.ChildTable
                WHERE LevelC < 10
        )
        SELECT  (Case when SourceDB.DatabaseName is null then '--' else '' end) || 'Insert into '||
                        TRIM(B.DB) || '.' || TRIM(B.PT) ||
                ' Select * from ' ||
		${ReplPattern} ||
                '.' || TRIM(B.PT)|| ' ' ||
                ';'
                AS InsertStatement,
                LevelC
        FROM
        (
                SELECT *
                FROM
                (
                        SELECT  ParentDB DB, CAST(ParentTable AS VARCHAR(100)) PT ,LevelC
                        FROM heirarchy_Constraints
                        QUALIFY ROW_NUMBER() OVER(PARTITION BY ParentDB,ParentTable ORDER BY  LevelC DESC) = 1
                ) A
        ) B
        LEFT OUTER JOIN dbc.TablesV as SourceDB
        On B.PT = SourceDB.TableName
	and ${ReplPattern} = TRIM(SourceDB.DatabaseName) 
)
WITH DATA
NO PRIMARY INDEX
ON COMMIT PRESERVE ROWS
;
SELECT InsertStatement AS "--InsertOrder", LevelC
FROM SQLResultTable
ORDER BY LevelC ASC
;
.export reset
.logoff
.exit
end

if [ "${ExecRepl}" = "yes" ]
then
	echo "********** Starting substitution of ${SQLFile} using pattern: **********"
	echo "sed -i ${SubstPattern} ${SQLFile}"
	echo "sed -i ${SubstPattern} ${SQLFile}" | sh
	echo "********** End of substitution of ${SQLFile} **********"
fi

echo "********** Starting execution of ${SQLFile} **********"
if [ "${ExecSQL}" = "yes" ]
then
	bteq <<-end
	.logon $LOGON
	.run file=${SQLFile}
	end
else
	cat ${SQLFile}
fi

echo "Load commands generated into file ${SQLFile}"
