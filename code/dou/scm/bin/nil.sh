#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: nil.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/nil.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2017-10-10	Teradata	Initial version
# --------------------------------------------------------------------------
# Description
# Get list of changed files from Plain File System
if [ $# != 6 ]
then
	echo "Usage: `basename $0` component snapshot_file change_list version build_all checkpoint" >&2
	exit 1
fi

component="$1"
snapshot_file="$2"
change_list="$3"
version="$4"
build_all="$5"
checkpoint="$6"

snapshot_exist="true"

#
# if the component is suffixed with ampersand & (for background build) remove it
#
bck="$(echo "$component" | sed 's/.*\(.\)$/\1/')"
if [ "$bck" = '&' ]
then
	component="$(echo "$component" | sed 's/\(.*\).$/\1/')"
fi

#
# component specified, must use component specific snapshot file
# if component specific snapshot does not exists try use the generic one
#
# replace any slashes with a dot in component before using it as a suffix
#
file_suffix="$(echo $component | sed -e 's,/,.,g')"
component_snapshot_file="${snapshot_file}.${file_suffix}"	
component_change_list="${change_list}.${file_suffix}"

echo "Using snapshot file: $component_snapshot_file"
echo "Using changelist file: $component_change_list"

cd ${ROOT}/${component}	
	
if [ ! -e "${component_snapshot_file}" ]
then
	#
	# no snapshot file found, must be first time running so create it
	#		
	echo "$component has no snapshot file so we need to create"
	lastChangedFile="$(find . -printf '%T@ %p\n' | sort -n -r | head -1 | cut -d' ' -f2)"
	lastChangedFileTimestamp="$(stat --format='%y' "${lastChangedFile}")"
	echo "$(lastChangedFileTimestamp)"  > "${component_snapshot_file}"
	touch -d "$(lastChangedFileTimestamp)"  "${component_snapshot_file}"
	snapshot_exist="false"
fi
prevRevision="$(stat --format='%y' "${component_snapshot_file}")"
lastChangedFile="$(find . -printf '%T@ %p\n' | sort -n -r | head -1 | cut -d' ' -f2)"
currRevision="$(stat --format='%y' "${lastChangedFile}")"

#
# compare previous snapshot with current and generate change list file
# first backup possible existing change list file
#
echo "Searching for files to process..."
test -f "${component_change_list}" && cp --backup=numbered "${component_change_list}" "${component_change_list}.${version}"						
if [ ${build_all:0:1} = "N" ] 
then
	if [ "${checkpoint}" != "" ]
	then
		prevRevision="${checkpoint}"
	fi
	if [ "${prevRevision}" != "${currRevision}" ]
	then
		find . -newer "${component_snapshot_file}" | \
			while read file
			do
				echo "${currRevision}|$(stat --format=%U $file)|$(stat --format=%y $file | sed -e 's/\..* //')" | \
				awk -F '|' -v c="${component}" -v f="${file}" '{ printf( ">%s;%s;%s;%s;%s\nM\t%s\n", $1, substr($3,1,25), $2, $2, c, f ) }'
			done | sed -e 's,./,,' \
			> "${component_change_list}"
		echo "Building changes from revision $prevRevision to $currRevision"
	else
		cat /dev/null > "${component_change_list}"
		echo "Nothing to build since previous revision $prevRevision is same as current revision $currRevision"
	fi

else
	(
		echo "${ROOT}/${component}"
		find "${ROOT}/${component}" ! -path "${ROOT}/${component}" -prune -type d -print | egrep -v "^${ROOT}$"
	) | \
	while read entry
	do
	(
		cd "$entry" || continue

		#
		# find all _runorder.txt files up and down from current folder
		# and check if the files mentioned in the runorder files have changed using the SCM log
		#
		(find . -name '_runorder.txt' -print; upfind -name '_runorder.txt' -print) | xargs egrep -v '^--|^CD|^$' | \
		while IFS='' read -r op file 
		do						
			case "$op" in
				"EXPAND")							
					(cd $file && find . -type f -print | sed -e 's,^\./,,' | while read file; do echo "INSTALL $entry/$file"; done)
					;;
				*)							
					echo "$op $file" 
					;;
			esac
		done | egrep "[[:space:]]${component}/" | awk '{printf("%s/%s\n", r, $2)}' r="${ROOT}" | \
			while read file
			do
				echo "${currRevision}|$(stat --format=%U $file)|$(stat --format=%y $file | sed -e 's/\..* //')" | \
				awk -F '|' -v c="${component}" -v f="${file}" '{ printf( ">%s;%s;%s;%s;%s\nM\t%s\n", substr($1,2), substr($3,1,25), $2, $2, c, f ) }'
			done | sed -e "s,${ROOT}/${component}/,,"
	)
	done > ${component_change_list}
fi	

if [ "${snapshot_exist}" = "true" -o "${build_all:0:1}" != "N" ]
then
	echo "Component change list generated: ${component_change_list}"
	exit 0
fi

exit 1
