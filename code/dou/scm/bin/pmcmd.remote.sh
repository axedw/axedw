#!/bin/bash
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: pmcmd.remote.sh 32502 2020-06-16 15:52:55Z  ${BTEQ_REMOTE_USER} $
# Last Changed By  : $Author: ${BTEQ_REMOTE_USER} $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/pmcmd.remote.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2018-03-26	Teradata	Initial version
#
# --------------------------------------------------------------------------
# Description
#   Run pmcmd script on remote node
#
# Parameters:
#	None, can only process scripts from standard input
#	eg. <script_to_execute.btq
#
# --------------------------------------------------------------------------
# Processing Steps 
# 1.) copy needed files to remote system
# 2.) execute pmcmd on remote machine to execute the standard input
# --------------------------------------------------------------------------
# Open points
# 1.) 
# 2.) 
#
# Environment
# PMREP_REMOTE_NODE - the name of the node to execute on
# PMREP_REMOTE_USER - the name of the server login to use, this assiumes that the public key in .ssh has been setup
# INFA_TRUSTSTORE - the name of folder where the file infa_truststore.pem is
# INFA_REPCNX_INFO - the name of the connection file to use
# the file ~.ssh/$[uname -n) must exist on the remote node and be the public key for this node
# --------------------------------------------------------------------------
. buildFunctions.sh

PrivateKeyFile="~/.ssh/$(uname -n)"
tmp_dir="/tmp"

#
# copy input files to remote node and execute pmcmd script on remote node with adjusted input
#
pmcmd_result_file="$(mktemp ${tmp_dir}/pmcmd.result.XXXXX.log)"

printTrace 0 "pmcmd: got $1 as command"

case "$1" in
"run")
	shift
	TEMP=`getopt -o f: -- "$@"`

	if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

	# Note the quotes around `$TEMP': they are essential!
	eval set -- "$TEMP"

	while true ; do
		case "$1" in
			-f)
				input_file="$2"
				shift 2
				;;
			--) shift ; break ;;
			*) echo "Internal error!" >&2 ; exit 1 ;;
		esac
	done

	rest="`for arg do echo $arg ; done`"
	new_input_file="$(mktemp ${tmp_dir}/$(basename $input_file).XXXXX)"
	
	printTrace 0 "scp -i \"${PrivateKeyFile}\" \"${input_file}\" \"${PMREP_REMOTE_USER}@${PMREP_REMOTE_NODE}:${new_input_file}\""
	scp -i "${PrivateKeyFile}" "${input_file}" "${PMREP_REMOTE_USER}@${PMREP_REMOTE_NODE}:${new_input_file}"
	
	printTrace 0 "ssh -n -i \"${PrivateKeyFile}\" -l ${PMREP_REMOTE_USER} ${PMREP_REMOTE_NODE} \". ./.profile; INFA_TRUSTSTORE=. INFA_REPCNX_INFO=\\\"${INFA_REPCNX_INFO}\\\" pmcmd -f ${new_input_file} ${rest} 2>&1\""
	ssh -n -i "${PrivateKeyFile}" -l ${PMREP_REMOTE_USER} ${PMREP_REMOTE_NODE} ". ./.profile; INFA_TRUSTSTORE=. INFA_REPCNX_INFO=\"${INFA_REPCNX_INFO}\" pmcmd -f ${new_input_file} ${rest} 2>&1" 
	;;

*)
	echo "`basename $0`: error: Do not know how to do $1 in pmcmd" 2>&1
	;;
esac >"${pmcmd_result_file}" 2>&1

cat "${pmrep_result_file}"
egrep -i "[1-9][0-9]* error" "${pmcmd_result_file}" >/dev/null
if [ $? = 0 ]
then
	stat=1
else
	stat=0
fi
exit $stat
