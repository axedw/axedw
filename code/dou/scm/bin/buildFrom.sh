#!/bin/bash 
if [ "$BUILD_DEBUG" = "2" ]
then
	set -xv
fi
#
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: buildFrom.sh 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/scm/bin/buildFrom.sh $
#---------------------------------------------------------------------------
# SCM Info END
# --------------------------------------------------------------------------
# Change History
# Date       	Author         	Description
# 2011-01-10	Teradata	Initial version
# 2013-06-10	Teradata	Adjusted for RTC requirements
# 2017-06-10	Teradata	Adjusted for GIT requirements
# 2017-10-10	Teradata	Merged SCM specific scripts into one
#
# --------------------------------------------------------------------------
# Description
#   Apply all changes from SCM
#
# Parameters:
#	Usage buildFrom.sh 
#
# --------------------------------------------------------------------------
#
#
export build_start_time="`date +%Y%m%d%H%M%S`"
. buildFunctions.sh
setProperties "${BUILD_PROPERTIES:-/dev/null}"

cd ${ROOT}

if [ "${BUILD_SCM}" = "RTC" ]
then
	#
	# on exit check if the RTC jazz lock file is there. If so remove it so we can do another build in the same build area later (rebuilds)
	# sometimes the lock file is left around after the build has finished for some undocumented reasons
	#
	trap 'test -f $ROOT/.jazz5/.jazzlock && echo "Found Jazz lockfile at exit, removing it"; rm -f $ROOT/.jazz5/.jazzlock;' EXIT

	#
	# if the lock file exists at start we should remove it, it is only this script that should use the workspace
	#
	jazzlockfile="$ROOT/.jazz5/.jazzlock"
	if [ -r "${jazzlockfile}" ]
	then
		echo "RTC lockfile ${jazzlockfile} exists, removing..."
		rm -f "${jazzlockfile}"
	fi
fi

#
# check if we are debugging or not and set parameters accordingly
#
export BUILD_DEBUG="${BUILD_DEBUG:-0}" # set no debug if variable is not defined

#
# set default values if variables not set
#
export BUILD_OS="${BUILD_OS:-`uname -s`}" # get type of operating system if not set
export BUILD_ALL_OBJECTS="${BUILD_ALL_OBJECTS:-N}" # set to build from SCM changes if not set
export BUILD_OBJECT_LIST="${BUILD_OBJECT_LIST:-SCM}" # set to get list from changes in SCM if not set
export BUILD_BLACKLIST="${BUILD_BLACKLIST:-}" # set to empty list if not set, include all entries in include file
export BUILD_COMPONENTS="${BUILD_COMPONENTS:-`(find . ! -path . -prune -type d ! -name '.*' -a ! -name tmp -print | while read dir; do basename "$dir"; done)`}" # set to all components found (except tmp) if not set

#
# split BUILD_ALL_OBJECTS into Y/N flag and target system to build if supplied in the value
# if BUILD_CHECKPOINT value contains a slash character then that part and all following parts are treated as objects to build else use as checkpoint to start from
#
export BUILD_TARGET_SYSTEM="${BUILD_TARGET_SYSTEM:-`echo "$BUILD_ALL_OBJECTS" | while read p1 p2; do test echo $p2 | grep / || echo $p2; done`}"
export BUILD_ALL_OBJECTS="`echo "$BUILD_ALL_OBJECTS" | while read p1 p2; do echo $p1; done`"
export BUILD_SPECIFIC_OBJECTS="`echo "$BUILD_CHECKPOINT" | while read p1; do echo \$p1 | grep /; done`"
if [ "${BUILD_SPECIFIC_OBJECTS}" != "" ]
then
	BUILD_CHECKPOINT=""
fi

echo "PATH=${PATH}"
echo "ROOT=${ROOT}"
env | sort | grep BUILD_
echo "TD_ENVNAME=${TD_ENVNAME}"
echo "TD_SYSTEM=${TD_SYSTEM}"
echo "TD_LOGMECH=${TD_LOGMECH}"
echo "TD_USER=${TD_USER}"
echo "TD_PASSWORD=***"
echo "TD2_ENVNAME=${TD2_ENVNAME}"
echo "TD2_LOGMECH=${TD2_LOGMECH}"
echo "TD2_SYSTEM=${TD2_SYSTEM}"
echo "TD2_USER=${TD2_USER}"
echo "TD2_PASSWORD=***"
echo "INFA_DOMAIN=${INFA_DOMAIN}"
echo "INFA_IS=${INFA_IS}"
echo "INFA_USER=${INFA_USER}"
echo "INFA_PASSWORD=***"
echo "INFA_PASSWORD_VARIABLE=${INFA_PASSWORD_VARIABLE}"
echo "INFA_REPOSITORY=${INFA_REPOSITORY}"
echo "INFA_SECDOMAIN=${INFA_SECDOMAIN}"
echo "INFA_SHARED=${INFA_SHARED}"
echo "INFA_SHARED_NODE=${INFA_SHARED_NODE}"
echo "ORA_ENVNAME=${ORA_ENVNAME}"
echo "ORA_USER=${ORA_USER}"
echo "ORA_PASSWORD=***"
echo "ORA_SCHEMA=${ORA_SCHEMA}"

export execute_build_command="yes"
export execute_install_command="yes"
case "$BUILD_DEBUG" in
"1")
	#
	# do minimal build, just execute main and no build scripts
	#
	export execute_build_command="no"
	;;
"2")
	#
	# do medium build, execute main and build scripts but no install
	#
	export execute_build_command="yes"
	export execute_install_command="no"
	;;
esac

change_list="${ROOT}/compare.log"
snapshot_file="${ROOT}/latest_snapshot"

tmp_path="${ROOT}/tmp"
if [ ! -d "${tmp_path}" ]
then
	mkdir -p "${tmp_path}" || exit 1
fi

case "${BUILD_SCM}" in
"RTC")
	version="${buildDefinitionId}-${buildLabel:-$build_start_time}";
	;;
"GIT")
	version="${BUILD_TAG:-$build_start_time}";
	;;
*)
	version="$build_start_time";
	;;
esac

version_file="${PWD}/version.txt"
TD_ENVNAME="$TD_ENVNAME"
ORA_ENVNAME="$ORA_ENVNAME"

INCLUDE_FILE="${ROOT}/include.${version}.txt"

echo "$version" > "${version_file}"

#
# status, timings and checkpoints are kept per component and the component suffix will be added when building the component
#
statuscodefile="${tmp_path}/statuscode.${version}"
timefile="${tmp_path}/timeinfo.${version}"
checkpointfile="${tmp_path}/checkpoint.${version}"
checkpoint=""

#
# get list of files that has been changed since last checkpoint
# if requester has specified components then generate one change list per component
# do not generate change_list if the build requester has supplied one
#
snapshot_exists="true"

if [ "${BUILD_OBJECT_LIST}" = "SCM" ]
then
	printTrace 0 "Build using ${BUILD_SCM} as source code managment system"
	
	case "${BUILD_SCM}" in
	"GIT")
		for c in $BUILD_COMPONENTS
		do		
			bck="$(echo "$c" | sed 's/.*\(.\)$/\1/')"
			if [ "$bck" = '&' ]
			then
				c="$(echo "$c" | sed 's/\(.*\).$/\1/')"
			fi
			checkpoint="${BUILD_CHECKPOINT}"
			git.sh "${c}" "${snapshot_file}" "${change_list}" "${version}" "${BUILD_ALL_OBJECTS}" "${checkpoint}"
			if [ $? -ne 0 ]
			then			
				snapshot_exists="false"
			fi
		done
		if [ "${snapshot_exists}" = "false" ]
		then
			echo "Not all components had snapshot files so we have to abort, please try again but be aware that incremental builds may not work as expected as snapshot files have just been created"
			exit 1
		fi
		;;
	"RTC")
		rtc.sh "${snapshot_file}" "${change_list}" "${version}" "${BUILD_COMPONENTS}"
		test $? != 0 && exit 1
		;;
	"SVN")
		for c in $BUILD_COMPONENTS
		do		
			bck="$(echo "$c" | sed 's/.*\(.\)$/\1/')"
			if [ "$bck" = '&' ]
			then
				c="$(echo "$c" | sed 's/\(.*\).$/\1/')"
			fi
			checkpoint="${BUILD_CHECKPOINT}"
			svn.sh "${c}" "${snapshot_file}" "${change_list}" "${version}" "${BUILD_ALL_OBJECTS}" "${checkpoint}"
			if [ $? -ne 0 ]
			then			
				snapshot_exists="false"
			fi
		done
		if [ "${snapshot_exists}" = "false" ]
		then
			echo "Not all components had snapshot files so we have to abort, please try again but be aware that incremental builds may not work as expected as snapshot files have just been created"
			exit 1
		fi
		;;
	"NIL")
		for c in $BUILD_COMPONENTS
		do		
			bck="$(echo "$c" | sed 's/.*\(.\)$/\1/')"
			if [ "$bck" = '&' ]
			then
				c="$(echo "$c" | sed 's/\(.*\).$/\1/')"
			fi
			checkpoint="${BUILD_CHECKPOINT}"
			nil.sh "${c}" "${snapshot_file}" "${change_list}" "${version}" "${BUILD_ALL_OBJECTS}" "${checkpoint}"
			if [ $? -ne 0 ]
			then			
				snapshot_exists="false"
			fi
		done
		if [ "${snapshot_exists}" = "false" ]
		then
			echo "Not all components had snapshot files so we have to abort, please try again but be aware that incremental builds may not work as expected as snapshot files have just been created"
			exit 1
		fi
		;;
	*)
		echo "No SCM specified, I need information how to build the change list"
		exit 1
		;;
	esac
fi

status=0

export components_to_build="${BUILD_COMPONENTS}"

#
# generate a list of entries to be generated into the include file, i.e. the files to process
# The format of this list is:
# op;file_path;COMPONENT
# where op is a letter M for Modified 
# file_path is the path in the workspace to the object to deploy
#
# If the parameter BUILD_OBJECT_LIST is set to other value than SCM the name will be used as file name for a change list instead of generated from SCM
# If the parameter BUILD_ALL_OBJECTS is set to Y the list will contain all objects else generated from SCM
# If the parameter BUILD_BLACKLIST is set to a semicolon separated list of regular expressions it is used to exclude entries from processing
#
# generate file containing regular expressions specified in BUILD_BLACKLIST
#
if [ "${BUILD_BLACKLIST}" != "" ]
then
	printTrace 0 "Build using ${BUILD_BLACKLIST} as blacklist"
	regexpfile="${INCLUDE_FILE}.regexp"
	rm -f "${regexpfile}"
	echo "${BUILD_BLACKLIST}" | tr ';' '\n' >"${regexpfile}"
else
	printTrace 0 "No blacklist specified"
	regexpfile="`mktemp ${tmp_path}/emptyFileXXXXX`"
fi

rm -f "${INCLUDE_FILE}"
echo 0 >"${statuscodefile}"

if [ "${BUILD_OBJECT_LIST}" != "SCM" ]
then
	printTrace 0 "Build using ${BUILD_OBJECT_LIST} as objectlist"
	#
	# an include file is specified, if the file exists use that as object list to process else display error
	#
	if [ -f "${BUILD_OBJECT_LIST}" ]
	then
		cat "${BUILD_OBJECT_LIST}"
	else
		echo "Specified BUILD_OBJECT_LIST does not exist: $BUILD_OBJECT_LIST" >&2
		echo 1 >"${statuscodefile}"
	fi
else
	if [ "${BUILD_SPECIFIC_OBJECTS}" != "" ]
	then
		for file in ${BUILD_SPECIFIC_OBJECTS}
		do
			echo "M;$file"
		done
	else
		printTrace 0 "Build using log from ${BUILD_SCM} as objectlist"

		#
		# extract all filenames from the change list into include file
		# add version and information from SCM to enable variable expansions
		# make sure we only get one line per object
		#		
		for c in ${components_to_build}
		do		
			bck="$(echo "$c" | sed 's/.*\(.\)$/\1/')"
			if [ "$bck" = '&' ]
			then
				c="$(echo "$c" | sed 's/\(.*\).$/\1/')"
			fi
			#
			# replace any slashes with a dot in component before using it as a suffix
			#
			fileSuffix="$(echo $c | sed -e 's,/,.,g')"

			case "${BUILD_SCM}" in
			"GIT")
				getgitinfo.pl "${change_list}.${fileSuffix}" "$c" || echo 1 >"${statuscodefile}"
				;;
			"RTC")
				getrtcinfo.pl "${change_list}.${fileSuffix}" "$c" || echo 1 >"${statuscodefile}"
				;;
			"SVN")
				getsvninfo.pl "${change_list}.${fileSuffix}" "$c" || echo 1 >"${statuscodefile}"
				;;
			"NIL")
				getnilinfo.pl "${change_list}.${fileSuffix}" "$c" || echo 1 >"${statuscodefile}"
				;;
			esac

		done	
	fi
fi | grep -v -f "${regexpfile}" | sort -t';' -k2,2 -u > "${INCLUDE_FILE}"
status="$(cat "${statuscodefile}")"
test $status != 0 && exit 1

for bpart in ${components_to_build}
do
	printTrace 0 "\nStart to build component: $bpart"

	background="$(echo "$bpart" | sed 's/.*\(.\)$/\1/')"
	if [ "$background" = '&' ]
	then
		bpart="$(echo "$bpart" | sed 's/\(.*\).$/\1/')"
		background='Y'
	else
		background='N'
	fi

	entry="${ROOT}/${bpart}"
	run_folder="${ROOT}/${bpart}"
	bprog=${bpart}
	
	#
	# replace any slashes with a dot in component before using it as a suffix
	#
	file_suffix="$(echo $bpart | sed -e 's,/,.,g')"

	
	build_include_file="${INCLUDE_FILE}.${file_suffix}"
	rm -f "${build_include_file}"
	build_statuscodefile="${statuscodefile}.${file_suffix}"
	rm -f "${build_statuscodefile}"
	build_timefile="${timefile}.${file_suffix}"
	rm -f "${build_timefile}"
	build_checkpointfile="${checkpointfile}.${file_suffix}"
	# do not remove checkpoint file in case of a restart
	if [ "${BUILD_RESTART_FROM_POINT_FAILURE}" != "Y" ]
	then
		rm -f "${build_checkpointfile}"
	fi
	
	if [ "${BUILD_PROGRAM}" != "" ]
	then
		build_program="${BUILD_PROGRAM}"
	else
		build_program="apply_${file_suffix}_chg.sh"
	fi
	build_program="$(locateProgram $build_program)"
	if [ "${build_program}" != "" ]
	then
		echo "Using build program: ${build_program}"
	else
		echo "Sorry, I do not know how to build ${bpart}"
		echo "Need a program called apply_${file_suffix}_chg.sh"
		exit 1
	fi
	
	(
		start_time="`date +%Y%m%d%H%M%S`"
		grep ";${bpart}$" "${INCLUDE_FILE}" >"${build_include_file}"
		if [ -s "${build_include_file}" ]
		then
			#
			# build the objects
			#
			(
				cd "${entry}" || exit 1
				echo ${build_program} "${build_include_file}" "${entry}" "${version}" "${TD_ENVNAME}" "${tmp_path}" "${run_folder}" ""
				if [ "${execute_build_command}" = "yes" ]
				then
					echo "build_program:${build_program}"
					echo "build_include_file:${build_include_file}"
					echo "entry:${entry}"
					echo "version:${version}"
					echo "TD_ENVNAME:${TD_ENVNAME}"
					echo "tmp_path:${tmp_path}"
					echo "run_folder:${run_folder}"
					echo "build_checkpointfile:${build_checkpointfile}"						
					${build_program} "${build_include_file}" "${entry}" "${version}" "${TD_ENVNAME}" "${tmp_path}" "${run_folder}" "" "${build_checkpointfile}" "${bpart}"
					status=$?
					if [ ${status} = 0 -a "${BUILD_OBJECT_LIST}" = "SCM" -a "$BUILD_COMPONENTS" != "" -a "$BUILD_DEBUG" = "0" ]
					then
						#
						# requester specified components so save component specific snapshot information
						#
						case "${BUILD_SCM}" in
						"GIT")
							echo $(git rev-parse HEAD 2>&1) > "${snapshot_file}.${file_suffix}"
							;;
						"RTC")
							perl -e 'print $ENV{"team.scm.snapshotUUID"}' > "${snapshot_file}.${file_suffix}"
							;;
						"SVN")
							echo "$(${SVN:-svn} info | grep '^Last Changed Rev:' | while read n1 n2 n3 r; do echo $r; done)"  > "${snapshot_file}.${file_suffix}"
							;;
						"NIL")
							#
							# no scm system, save last modified file and its modification timestamp
							#
							lastChangedFile="$(find . ! -regex '.*/\.svn.*' -printf "%T@ %p\n" | sort -n -r | head -1 | cut -d' ' -f2)"
							lastChangedFileTimestamp="$(stat --format='%y' "${lastChangedFile}")"
							echo "${lastChangedFileTimestamp}"  > "${snapshot_file}.${file_suffix}"
							touch -d "${lastChangedFileTimestamp}"  "${snapshot_file}.${file_suffix}"
							;;
						esac
						build_snapshot_file="${snapshot_file}.${file_suffix}.${version}"
						cp "${snapshot_file}.${file_suffix}" "${build_snapshot_file}"

						if [ "${BUILD_UPDATE_DATABASE:-N}" = "Y" ]
						then
							#
							# the build was ok so update the database with the checkpoint information and the snapshot information
							#
							cpssID="$(cat "${build_snapshot_file}")"
							
							echo "Adding checkpoint information (${cpssID}) into the database from file: ${build_checkpointfile}"
							putCheckpoint "${bpart}" "${build_checkpointfile}" "${cpssID}"
							
							echo "Adding snapshot information (${cpssID}) into the database from file: ${build_snapshot_file}"
							putSnapshot "${bpart}" "${build_snapshot_file}" "${cpssID}"
						fi
					fi
				fi
				echo "${bpart} build returned: $status"
				echo "status=${status}; nr_recs=`cat ${build_include_file} | wc -l`; nr_apprecs=`cat ${build_checkpointfile} | wc -l` app_files=\"${build_checkpointfile}\";" >"${build_statuscodefile}"
				if [ "${status}" = "0" -a "${nr_apprecs}" != "0" -a "${BUILD_CREATE_TAG}" = "Y" -a "$BUILD_DEBUG" = "0" ]
				then
						#
						# we have a successful build and objects applied and user erquested us to build a tag
						# create the tag and add build information such as build script, snapshot and checkpoint files
						#
						tagName="$(basename "${build_include_file}")"
						tagName="${tagName:8}"
						tagName="${TD_ENVNAME}-${tagName}"

						echo "creating a tag of the successful build: ${tagName}"
						tagMessage="Tag ${tagName} created for build with status: $(cat ${build_statuscodefile})"

						case "${BUILD_SCM}" in
						"GIT")
							git tag "${tagName}"
							;;
						"RTC")
							# TODO - insert code to: create a tag of the successful build
							echo "RTC tag not implemented"
							;;
						"SVN")
							svnInfo="$(${SVN:-svn} info --xml)"
							URL="$(echo "${svnInfo}" | grep '<url>' | sed -e 's/<[/]*url>//g')"
							repositoryRoot="$(echo "${svnInfo}" | grep '<root>' | sed -e 's/<[/]*root>//g')"
							echo "URL: ${URL}"
							echo "repositoryRoot: ${repositoryRoot}"
							#
							# create the tag
							#
							${SVN:-svn} copy -r "$(cat ${build_snapshot_file})" "${URL}" "${repositoryRoot}/tags/${TD_ENVNAME}/${tagName}" --parents --message "${tagMessage}"
							#
							# create folders inside the tag
							#
							echo "...create build folder in tag"
							${SVN:-svn} mkdir "${repositoryRoot}/tags/${TD_ENVNAME}/${tagName}/build" --parents --message "Created build folder"
							echo "...create build/results/filelists folder in tag"
							${SVN:-svn} mkdir "${repositoryRoot}/tags/${TD_ENVNAME}/${tagName}/build/results/filelists" --parents --message "Created build/results/filelists folder"
							echo "...create build/packages folder in tag"
							${SVN:-svn} mkdir "${repositoryRoot}/tags/${TD_ENVNAME}/${tagName}/build/packages" --parents --message "Created build/packages folder"

							#
							# import filelists into the tag
							#
							echo "...import checkpoint into build folder in tag"
							file_to_import="$(basename "${build_checkpointfile}")"
							${SVN:-svn} import "${build_checkpointfile}" "${repositoryRoot}/tags/${TD_ENVNAME}/${tagName}/build/results/filelists/${file_to_import}" --message "Added checkpoint"
							echo "...import snapshot into build folder in tag"
							file_to_import="$(basename "${build_snapshot_file}")"
							${SVN:-svn} import "${build_snapshot_file}" "${repositoryRoot}/tags/${TD_ENVNAME}/${tagName}/build/results/filelists/${file_to_import}" --message "Added snapshot"
							echo "...import changelist into build folder in tag"
							file_to_import="$(basename "${build_include_file}")"
							${SVN:-svn} import "${build_include_file}" "${repositoryRoot}/tags/${TD_ENVNAME}/${tagName}/build/results/filelists/${file_to_import}" --message "Added changelist"
							
							#
							# import the package into the tag
							#
							echo "...import package into build folder in tag"
							tar_file="${PWD}/$(cat "${version_file}").tar"
							fileList="${PWD}/fileList"
							(
								cd "${tmp_path}"; ls *$(cat "${version_file}")* >"${fileList}"
								tar -cvE -f "${tar_file}" -L "${fileList}"
							)
							gzip "${tar_file}"
							file_to_import="$(basename "${tar_file}")"
							${SVN:-svn} import "${tar_file}".gz "${repositoryRoot}/tags/${TD_ENVNAME}/${tagName}/build/packages/${file_to_import}.gz" --message "Added package"
							rm -f "${tar_file}".gz "${fileList}"
							;;
						"NIL")
							#
							# no scm system, save last build info as a tag in a file
							#
							echo "${tagMessage}" >"$(dirname "${snapshot_file}.${file_suffix}")/${tagName}"
							;;
						esac
						echo "tag ${tagName} created"
				fi
			)
		else
			echo "Nothing to build"
			echo "status=0; nr_recs=0 nr_apprecs=0" >"${build_statuscodefile}"
		fi

		end_time="`date +%Y%m%d%H%M%S`"
		echo "start_time=$start_time; end_time=$end_time" >"${build_timefile}"
	) 2>&1 | while read line; do echo "$bpart:$SECONDS: $line"; done &
	pid=$!
	echo "Started $bpart as $pid"
	if [ "$background" = "Y" ]
	then
			echo "$bpart is running in background"
	else
			echo "waiting for $bpart to end"
			wait $pid
			echo "ok $bpart ended"
	fi
done

#
# wait for all sub processes to end
#
echo "`date`: waiting for all background processes to end"
wait
echo "`date`: ok all processes ended"

awk "{ printf( \"%40.40s %-6.6s %-14.14s %-14.14s %14.14s %14.14s\n\", \$1, \$2, \$3, \$4, \$5, \$6 ); }" <<-end
Component Status StartTime EndTime #ObjConsidered #ObjApplied
---------------------------------------- ------ -------------- -------------- -------------- --------------
end

#
# print status from all subprocesses (version based)
#
build_status=0
rm -f "${statuscodefile}"
for component in ${components_to_build}
do
	#
	# replace any slashes with a dot in component before using it as a suffix
	#
	file_suffix="$(echo $component | sed -e 's,/,.,g')"

	file="${statuscodefile}.${file_suffix}"

	statuses="status=unknown; nr_recs=0; nr_apprecs=0; app_files=;"; eval $statuses # in case the file is empty
	test -f "${file}" && statuses="`cat "${file}"`"; eval $statuses
	
	timings="start_time=unknown; end_time=unknown;"; eval $timings # in case the file is empty
	test -f "${timefile}.${file_suffix}" && timings="`cat "${timefile}.${file_suffix}"`"; eval $timings
	
	echo -e "$component $status $start_time $end_time $nr_recs $nr_apprecs\c"
	if [ -s "${app_files}" ]
	then
		if [ "${NODE_NAME}" = "master" ]
		then
			url_prefix="{JOB_URL}/ws/tmp"
		else
			url_prefix="http://${NODE_NAME}${WORKSPACE}/tmp"
		fi
		echo " ${url_prefix}/$(basename $app_files)"
	fi
	test ! -s "${app_files}" && echo
	
	if [ $build_status = 0 -a $status != 0 ]
	then
		build_status=1
	fi
	echo ${build_status} >${statuscodefile}
done | \
	awk "{ printf( \"%40.40s %-6.6s %-14.14s %-14.14s %14.14s %14.14s %s\n\", substr(\$1,length(\$1)-20+1), \$2, \$3, \$4, \$5, \$6, \$7 ); }"

build_end_time="`date +%Y%m%d%H%M%S`"
test -f "${statuscodefile}" && build_status="`cat "${statuscodefile}"`"

awk "{ printf( \"%40.40s %-6.6s %-14.14s %-14.14s %14.14s %14.14s\n\", \$1, \$2, \$3, \$4, \$5, \$6 ); }" <<-end
---------------------------------------- ------ -------------- -------------- -------------- --------------
Totalbuild $build_status $build_start_time $build_end_time
end

if [ ${build_status} != 0 ]
then
	exit ${build_status}
fi
case "${BUILD_SCM}" in
"RTC")
	#
	# save the previous and latest snapshot id unless requester has provided own include file (change_list) and has not specified modules
	# (if requester gave components the snapshots are already created in the deployment loop)
	#
	if [ "${BUILD_OBJECT_LIST}" = "SCM" -a "$BUILD_COMPONENTS" = "" -a "$BUILD_DEBUG" = "0" ]
	then
		if [ "${execute_build_command}" = "yes" ]
		then
			#
			# we have executed the build commands so save the current snapshot
			#
			cp "${snapshot_file}" "${snapshot_file}.${version}"
			perl -e 'print $ENV{"team.scm.snapshotUUID"}' > "${snapshot_file}"
		fi
	fi
	;;
esac

exit 0
