/*
# ----------------------------------------------------------------------------
# SCM Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: template.btq 32502 2020-06-16 15:52:55Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-16 17:52:55 +0200 (tis, 16 jun 2020) $
# Last Revision    : $Revision: 32502 $
# SCM URL          : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dou/template/database/template.btq $
# --------------------------------------------------------------------------
# SCM Info END
*/
-- ----------------------------------------------------------------------------
-- Database: $#DB_ENV#<databasename>
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '$#DB_ENV#<databasename>';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE $#DB_ENV#<databasename>
FROM $#DB_ENV#dbadmin AS -- always create from dbadmin, then move
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

-- Move-database to place in hierarchy
SELECT * FROM DBC.Databases WHERE DatabaseName = '$#DB_ENV#<databasename>' AND OwnerName <> '$#DB_ENV#<parent-db>';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE $#DB_ENV#<databasename> TO $#DB_ENV#<parent-db>
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPMV

COMMENT ON DATABASE $#DB_ENV#<databasename> AS
'Parent Database for <databasename>'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON $#DB_ENV#<databasename> TO $#DB_ENV#dbadmin,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- Database: $#DB_ENV#<databasename>T
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '$#DB_ENV#<databasename>T';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE $#DB_ENV#<databasename>T
FROM $#DB_ENV#dbadmin AS -- always create from dbadmin, then move
   PERM = 50000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

-- Move-database to place in hierarchy
SELECT * FROM DBC.Databases WHERE DatabaseName = '$#DB_ENV#<databasename>T' AND OwnerName <> '$#DB_ENV#<databasename>';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE $#DB_ENV#<databasename>T TO $#DB_ENV#<databasename>
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPMV

COMMENT ON DATABASE $#DB_ENV#<databasename>T AS
'Database holding tables for <purpose>'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON $#DB_ENV#<databasename>T TO $#DB_ENV#dbadmin,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- Database: $#DB_ENV#<databasename>VIN
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '$#DB_ENV#<databasename>VIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE $#DB_ENV#<databasename>VIN
FROM $#DB_ENV#dbadmin AS -- always create from dbadmin, then move
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

-- Move-database to place in hierarchy
SELECT * FROM DBC.Databases WHERE DatabaseName = '$#DB_ENV#<databasename>VIN' AND OwnerName <> '$#DB_ENV#<databasename>';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE $#DB_ENV#<databasename>VIN TO $#DB_ENV#<databasename>
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPMV

COMMENT ON DATABASE $#DB_ENV#<databasename>VIN AS
'Database holding views for loading into <databasename>T'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON $#DB_ENV#<databasename>VIN TO $#DB_ENV#dbadmin,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT SELECT,DELETE,INSERT,UPDATE,STATISTICS ON $#DB_ENV#<databasename>T TO $#DB_ENV#<databasename>VIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- ----------------------------------------------------------------------------
-- Database: $#DB_ENV#<databasename>VOUT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '$#DB_ENV#<databasename>VOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE $#DB_ENV#<databasename>VOUT
FROM $#DB_ENV#dbadmin AS -- always create from dbadmin, then move
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

-- Move-database to place in hierarchy
SELECT * FROM DBC.Databases WHERE DatabaseName = '$#DB_ENV#<databasename>VOUT' AND OwnerName <> '$#DB_ENV#<databasename>';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE $#DB_ENV#<databasename>VOUT TO $#DB_ENV#<databasename>
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPMV

COMMENT ON DATABASE $#DB_ENV#<databasename>VOUT AS
'Database holding views for reading from <databasename>T'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON $#DB_ENV#<databasename>VOUT TO $#DB_ENV#dbadmin,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON $#DB_ENV#<databasename>VOUT TO $#DB_ENV#<databasename> WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT SELECT ON $#DB_ENV#<databasename>T TO $#DB_ENV#<databasename>VOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: $#DB_ENV#<databasename>VOUT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '$#DB_ENV#<databasename>L';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE $#DB_ENV#<databasename>L
FROM $#DB_ENV#dbadmin AS -- always create from dbadmin, then move
   PERM = 10000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

-- Move-database to place in hierarchy
SELECT * FROM DBC.Databases WHERE DatabaseName = '$#DB_ENV#<databasename>L' AND OwnerName <> '$#DB_ENV#<databasename>';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPMV
GIVE $#DB_ENV#<databasename>L TO $#DB_ENV#<databasename>
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPMV

COMMENT ON DATABASE $#DB_ENV#<databasename>L AS
'Database holding logics objects (views/proc/macro) for <purpose>'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON $#DB_ENV#<databasename>L TO $#DB_ENV#dbadmin,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON $#DB_ENV#<databasename>L TO $#DB_ENV#<databasename> WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT SELECT ON SYS_CALENDAR TO $#DB_ENV#<databasename>L WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT EXECUTE FUNCTION ON SYSLIB TO $#DB_ENV#<databasename>L WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT EXECUTE FUNCTION ON TD_SYSFNLIB TO $#DB_ENV#<databasename>L WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT SELECT,INSERT,UPDATE,DELETE,STATISTICS ON $#DB_ENV#<databasename>VIN TO $#DB_ENV#<databasename>L WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON $#DB_ENV#<databasename>VOUT TO $#DB_ENV#<databasename>L WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
