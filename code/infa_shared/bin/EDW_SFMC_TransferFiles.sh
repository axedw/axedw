#!/usr/bin/ksh
##############################################################################
#                                                                            #
# SFTP transfer of files to Salesforce                                       #
#                                                                            #
# 2019-04-01 Erik Kaar                                                       #
##############################################################################
PMRootDir=$1
FileDir=$1/TgtFiles/SFMC
IntegrationID=$2
Chain=$3
ChainID=$4

if [ -f ${FileDir}/${IntegrationID}_${ChainID}_*.csv.gz ]
then
 for FILE in ${FileDir}/${IntegrationID}_${ChainID}_*.csv.gz
  do
   ${PMRootDir}/bin/EDW_SFMC_sftp.sh ${FileDir} ${IntegrationID} ${Chain} ${ChainID} ${FILE}
   sleep 60
 done

 if [ $? == 0 ]
 then
    mv ${FileDir}/${IntegrationID}_${ChainID}_*.csv.gz ${FileDir}/Archive
    echo "Transfer successful"
 else
    echo "ERROR when transferring file"
 fi
fi
