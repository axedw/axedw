#!/usr/bin/ksh
###########################################################################
#                                                                         #
# pictureboxzip - Packa och komprimera alla filer for skick till PB       #
#                                                                         #
# 2013-08-01  Peter Holmlund                               	              #
###########################################################################

# Satt variabler med biblioteks och filnamn mm.
PBBaseDir=/opt/axedw/UV1/is_axedw2/infa_shared
PBBaseDir=$1
PBUtDir=${PBBaseDir}/TgtFiles/PictureBox
PBArkDir=${PBUtDir}/Archive
Timefile=time.txt
OutfilePrefix=tmp_Axfood_
OutfilePrefixEnd=Axfood_
Timestamp=`date +%Y%m%d_%H%M%S`
echo "start"
if [ ! -s ${PBUtDir}/${Timefile} ]
then
  echo "Hittar inte filen ${Timefile}!"
  exit
fi

# Ta bort eventuella gamla flaggfiler.
echo Ta bort gammal flaggfil
rm -f ${PBUtDir}/*.flg

# Hamta aktuellt datum ur tidsfilen.
CurWeek=`cat ${PBUtDir}/${Timefile} | awk '{print $6}'`

# Skapa tar-fil och zippa den.
OutTar=${OutfilePrefix}${CurWeek}.tar
cd ${PBUtDir}
tar cvf ${OutTar} *.txt ; gzip ${PBUtDir}/$OutTar

# Kontrollera att zipfilen existerar och har innehall.
# Skapa i sa fall flaggfilen, lagg en kopia i arkivet,
# ta bort txt-filerna och returnera ok (0).
if [ -s ${PBUtDir}/${OutTar}.gz ]
then
  #cp ${PBUtDir}/${OutTar}.gz ${PBArkDir}/${OutTar}.gz.${Timestamp}
  rm -f ${PBUtDir}/*.txt
  touch ${PBUtDir}/${CurWeek}.flg
  mv ${PBUtDir}/${OutTar}.gz ${PBUtDir}/${OutfilePrefixEnd}${CurWeek}.tar.gz
  exit 0
fi

# Returnera fel (1).
exit 1
