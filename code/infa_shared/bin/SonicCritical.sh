#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SonicCritical.sh 12210 2014-01-22 14:10:26Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2014-01-22 15:10:26 +0100 (ons, 22 jan 2014) $
# Last Revision    : $Revision: 12210 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/SonicCritical.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Decription:
# Insert an Alert Request for change of TASM state to/from SonicCritical
#
# Usage: SonicCritical.sh
#
if [ $# -ne 2 ]
then
	echo "Usage:  `basename $0` PMRootDir {set,reset}"
	exit 1
fi

export PMRootDir="$1"
export Action="$2"

typeset -l Action

case "$Action" in
"set")
	AlertRequest=SetSonicCritical
	;;
"reset")
	AlertRequest=ResetSonicCritical
	;;
esac

export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi

case "$DB_ENV" in
"PR")
	Destination="EDWP ${AlertRequest}"
	;;
*)
	Destination="EDWT ${AlertRequest}"
	;;
esac

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
rcfile="`basename $logfile .log`.rc"
(
echo "--------------------------------"
echo "`date`"
echo "PMRootDir=$PMRootDir"
echo "Action=$Action"
echo "AlertRequest=$AlertRequest"
echo "DB_ENV=$DB_ENV"
echo "Destination=$Destination"
echo "logfile=$logfile"
echo "rcfile=$logfile"
echo "--------------------------------"

bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=$AlertRequest;ClientUser=$LOGNAME;' FOR SESSION;

Insert Into dbcmngr.AlertRequest
    (
		ReqDate,
		ReqTime,
		JobName,
		Description,
		EventValue,
		ActionCode,
		RepeatPeriod,
		Destination,
		Message
    )
    Values
    (
		Date,
		Time,
		'$AlertRequest',
		'Request to $AlertRequest',
		Null,
		'+',
		Null,
		'$Destination',
		'Sonic queues are `if [ "$Action" = "set" ]; then echo "filling up"; else echo "back to normal"; fi`'
    );
SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end
echo $? >$rcfile
)  2>&1 | tee $logfile
exit `cat $rcfile`
