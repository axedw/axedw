#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: execwf.sh 1106 2011-11-29 14:52:51Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2011-11-29 15:52:51 +0100 (tis, 29 nov 2011) $
# Last Revision    : $Revision: 1106 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/execwf.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
# Usage: execwf folder workflow
#
if [ $# -ne 2 ]
then
	echo "Usage:  `basename $0` folder workflow"
	exit 1
fi
domain="$DOMAIN_NAME"
service="is_axedw"
user="$REPOSITORY_USER_NAME"
password="REPOSITORY_USER_PASSWORD"
folder="$1"
workflow="$2"

`dirname $0`/StartInfaWF.sh "$domain" "$service" "$user" "$password" "$folder" "$workflow"
status=$?

exit $status
