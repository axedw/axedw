#!/usr/bin/ksh
#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: archive_axcrm.sh 23307 2017-09-28 08:37:39Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2017-09-28 10:37:39 +0200 (tor, 28 sep 2017) $
# Last Revision    : $Revision: 23307 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/archive_axcrm.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"


echo "PMRootDir=$1"

if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

DB_ENVAchv="$DB_ENV"Achv
DB_ENVAxCRMBI="$DB_ENV"AxCRMBI
DB_ENVMetaDataVIN="$DB_ENV"MetaDataVIN

JobRunID=$2
echo "JobRunID=$JobRunID"

CURRENT_TS=`date "+%Y%m%d%H%M%S"`
echo "CURRENT_TS=$CURRENT_TS"

exec >$PMRootDir/log/`basename $0`_"Arch_"$CURRENT_TS"_script".log 2>&1 

echo "writing to log"
echo "PMRootDir = $PMRootDir"
echo "DB_ENV = $DB_ENV"
echo "CURRENT_TS = $CURRENT_TS"
echo "JobRunID = $JobRunID"

echo "DB_ENVAchv = ${DB_ENV}Achv"
echo "DB_ENVAxCRMBI = ${DB_ENV}AxCRMBI"
echo "DB_ENVMetaDataVIN = ${DB_ENV}MetaDataVIN"


###################################################################################################
#  Print script syntax and help
###################################################################################################

print_help ()
    {
    echo "Usage: `basename $0` <PMRootDir> <JobRunID>"
    echo ""
    echo "   Base directory is   : PMRootDir"
    echo "   <JobRunID>         : JobRunID to archive"
    echo ""
    }


############################################################################
# Check for correct syntax                                                 #
############################################################################

ERROR_CODE=0

if [ $# -ne 2 ] ; then
    echo "Invalid number of parameters!!!"
    echo ""
    ERROR_CODE=1
fi

if [ $ERROR_CODE -ne 0 ] ; then
    print_help
    exit 1
fi


############################################################################
# Script and log file directories                                          #
############################################################################

# Log Directory
LOG_DIR=$1/log
echo "LOG_DIR = " $LOG_DIR


# Bteq Log File
LOG_FILE=$LOG_DIR/`basename $0 .sh`_$JobRunID"_"$CURRENT_TS"_bteq".log
echo "LOG_FILE = " $LOG_FILE
#chmod 777 $LOG_FILE


# Bteq Directory
SCRIPT_DIR=$1/bin
echo "SCRIPT_DIR = " $SCRIPT_DIR


# Bteq File
#BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/bin/`basename $0 .sh`_$CURRENT_TS.bteq
BTEQ_ARCHIVE_SOURCE_SCRIPT=$1/tmp/`basename $0 .sh`_$CURRENT_TS.bteq
echo "BTEQ_ARCHIVE_SOURCE_SCRIPT = " $BTEQ_ARCHIVE_SOURCE_SCRIPT 
>$BTEQ_ARCHIVE_SOURCE_SCRIPT
#chmod 777 $BTEQ_ARCHIVE_SOURCE_SCRIPT


############################################################################
# TO GET LOGON INFORMATION FOR BTEQ                                        #
############################################################################

PWD_FILE=$SCRIPT_DIR/PWD_BteqLogon
echo "PWD_FILE = " $PWD_FILE

LOGON=`grep $DB_ENV"_"LOGON $PWD_FILE `

test "$LOGON" = "" && {
	echo "Error: Could not get LOGON info from $PWD_FILE file"
	return -1 
}

#LOGON_FILE=$SCRIPT_DIR/$DB_ENV"_"$JobRunID"_LOGON"
LOGON_FILE=$1/tmp/$DB_ENV"_"$JobRunID"_LOGON"
echo "LOGON_FILE = " $LOGON_FILE
#chmod 777 $LOGON_FILE

LOGON=`echo $LOGON | cut -d "=" -f2`

#Test condition to check if the file already exists

if [ -f $LOGON_FILE ]		
then
	#echo "Logon File Exists"
	EXISTING_LOGON=`head -1 $LOGON_FILE`
	
	if [[ $LOGON = $EXISTING_LOGON ]]
	then
	echo "Same credentials reusing existing file"
	else
	echo $LOGON > $LOGON_FILE
	fi
else 
	#file does not exist, use latest credentials
	echo $LOGON > $LOGON_FILE
fi
chmod 777 $LOGON_FILE

echo "
.SET SESSION TRANSACTION ANSI;

.RUN FILE = $LOGON_FILE" > $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=CreateArchiveTables;ClientUser=$LOGNAME;' FOR SESSION;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo "commit;" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

############################################################################
# BTEQ                                                                                     
############################################################################

echo "
-- ---------------------------------------------------------------------------------------------------------------------
-- The output may exceed the standard 80 characters, it is expanded to 5000
-- ---------------------------------------------------------------------------------------------------------------------
.width 5000
.set format on
.foldline off" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

start_time=`date`
echo "start_time = " $start_time > $LOG_FILE


echo "
------------------------------------------------------------------------------------------
-- 1) AxCRM W_LOY_ACCRUAL_ITEM_FS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_W_LOY_ACC_ITEM_FS
(
	PROCESS_DT, 
	PROGRAM_ID, 
 	PARTNER_ID, 
	MEMBER_ID, 
	PROMO_ID,
	POINTTYPE_ID, 
	TRANSACTION_ID, 
	POINT_BLK_ID, 
	CONTACT_GEO_ID, 
	ACCRUAL_ID_NUM,
	ACCRUALED_VALUE, 
	COST_PER_PT, 
	EXPIRATION_DT, 
	PRTNR_PT_DEBT_QTY,
	DELETE_FLG, 
	REJECT_FLG, 
	DOC_CURR_CODE, 
	LOC_CURR_CODE, 
	LOC_EXCH_RATE_TYPE,
	LOC_EXCHANGE_RATE, 
	EXCHANGE_DT, 
	CREATED_BY_ID, 
	CHANGED_BY_ID,
	CREATED_ON_DT, 
	CHANGED_ON_DT, 
	AUX1_CHANGED_ON_DT, 
	AUX2_CHANGED_ON_DT,
	AUX3_CHANGED_ON_DT, 
	AUX4_CHANGED_ON_DT, 
	DATASOURCE_NUM_ID, 
	INTEGRATION_ID,
	TENANT_ID, 
	X_CUSTOM, 
	XN_ACTION_ID, 
	XN_ITEM_NUM, 
	XN_PT_SUB_TYPE_ID,
	XN_STATUS_CD, 
	XN_TXN_ID, 
	XN_TYPE_CD, 
	XN_USED_VALUE, 
	XN_ETL_DATE,
	JobRunId
)
SELECT
	PROCESS_DT, 
	PROGRAM_ID, 
 	PARTNER_ID, 
	MEMBER_ID, 
	PROMO_ID,
	POINTTYPE_ID, 
	TRANSACTION_ID, 
	POINT_BLK_ID, 
	CONTACT_GEO_ID, 
	ACCRUAL_ID_NUM,
	ACCRUALED_VALUE, 
	COST_PER_PT, 
	EXPIRATION_DT, 
	PRTNR_PT_DEBT_QTY,
	DELETE_FLG, 
	REJECT_FLG, 
	DOC_CURR_CODE, 
	LOC_CURR_CODE, 
	LOC_EXCH_RATE_TYPE,
	LOC_EXCHANGE_RATE, 
	EXCHANGE_DT, 
	CREATED_BY_ID, 
	CHANGED_BY_ID,
	CREATED_ON_DT, 
	CHANGED_ON_DT, 
	AUX1_CHANGED_ON_DT, 
	AUX2_CHANGED_ON_DT,
	AUX3_CHANGED_ON_DT, 
	AUX4_CHANGED_ON_DT, 
	DATASOURCE_NUM_ID, 
	INTEGRATION_ID,
	TENANT_ID, 
	X_CUSTOM, 
	XN_ACTION_ID, 
	XN_ITEM_NUM, 
	XN_PT_SUB_TYPE_ID,
	XN_STATUS_CD, 
	XN_TXN_ID, 
	XN_TYPE_CD, 
	XN_USED_VALUE, 
	XN_ETL_DATE,
	$JobRunID AS JobRunID
FROM
        ${DB_ENV}AxCRMBI.W_LOY_ACCRUAL_ITEM_FS    
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.W_LOY_ACCRUAL_ITEM_FS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 2) AxCRM WC_LOY_ATTRDEFN_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_LOY_ATTRD_DS
(
	INTEGRATION_ID, 
	DATASOURCE_NUM_ID, 
	ACTIVE_FLG, 
	ATTR_TYPE_CD,
	DEFAULT_VAL, 
	DISPLAY_NAME, 
	PROGRAM_ID, 
	PROMOTION_ID, 
	INTERNAL_NAME,
	CHANGED_BY_ID, 
	CHANGED_ON_DT, 
	CREATED_BY_ID, 
	CREATED_ON_DT, 
	ETL_DATE,
	JobRunId
)
SELECT
	INTEGRATION_ID, 
	DATASOURCE_NUM_ID, 
	ACTIVE_FLG, 
	ATTR_TYPE_CD,
	DEFAULT_VAL, 
	DISPLAY_NAME, 
	PROGRAM_ID, 
	PROMOTION_ID, 
	INTERNAL_NAME,
	CHANGED_BY_ID, 
	CHANGED_ON_DT, 
	CREATED_BY_ID, 
	CREATED_ON_DT, 
	ETL_DATE,	
	$JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.WC_LOY_ATTRDEFN_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_LOY_ATTRDEFN_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 3) AxCRM WC_LOY_CARD_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_LOY_CARD_DS
(
       INTEGRATION_ID,
       DATASOURCE_NUM_ID,
       CARD_NUM,
       CARD_NUM_MASKED,
       CARD_TYPE_CD,
       NAME_ON_CARD,
       CONTACT_ID,
       STATUS_CD,
       ACTIVE_FLG,
       PROD_REQUEST_DT,
       PROD_SENT_DT,
       CHANGED_BY_ID,
       CHANGED_ON_DT,
       CREATED_BY_ID,
       CREATED_ON_DT,
       ETL_DATE,
       CONTACT_BU_ID,
       JobRunId
)
SELECT
       INTEGRATION_ID,
       DATASOURCE_NUM_ID,
       CARD_NUM,
       CARD_NUM_MASKED,
       CARD_TYPE_CD,
       NAME_ON_CARD,
       CONTACT_ID,
       STATUS_CD,
       ACTIVE_FLG,
       PROD_REQUEST_DT,
       PROD_SENT_DT,
       CHANGED_BY_ID,
       CHANGED_ON_DT,
       CREATED_BY_ID,
       CREATED_ON_DT,
       ETL_DATE,
       CONTACT_BU_ID,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.WC_LOY_CARD_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_LOY_CARD_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 4) AxCRM WC_LOY_MEM_CON_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_LOY_MEM_CON_DS
(
	   INTEGRATION_ID,
       DATASOURCE_NUM_ID,
       IS_EXPORTED_FLG,
       MEMBER_ID,
       PER_ID,
       DEL_FLG,
       CHANGED_BY_ID,
       CHANGED_ON_DT,
       CREATED_BY_ID,
       CREATED_ON_DT,
       ETL_DATE,
       JobRunId
)
SELECT
	   INTEGRATION_ID,
       DATASOURCE_NUM_ID,
       IS_EXPORTED_FLG,
       MEMBER_ID,
       PER_ID,
       DEL_FLG,
       CHANGED_BY_ID,
       CHANGED_ON_DT,
       CREATED_BY_ID,
       CREATED_ON_DT,
       ETL_DATE,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.WC_LOY_MEM_CON_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_LOY_MEM_CON_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 5) AxCRM W_LOY_MEMBER_DS Load Archiving
------------------------------------------------------------------------------------------
INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_W_LOY_MEMBER_DS
(
	   ACCRUAL_TO_MEM_NAME,
       PROGRAM_ID,
       CHILD_NUM,
       EXPIRY_DT,
       LANG_NAME,
       LAST_ACCR_TXN_DT,
       LAST_RDM_TXN_DT,
       LAST_TXN_PROCED_DT,
       LOUNGE_CODE,
       LOUNGE_NAME,
       LOY_LST_STMT_DT,
       LST_TIER_CHANGE_DT,
       MAX_POINT_LOAN,
       MEM_CLASS_CODE,
       MEM_CLASS_NAME,
       MEM_GRP_CODE,
       MEM_GRP_NAME,
       MEM_PHASE_CODE,
       MEM_PHASE_NAME,
       MEM_TYPE_CODE,
       MEM_TYPE_NAME,
       MEMBER_NAME,
       PIN_NUM,
       PNR_NAME,
       PREF_COMM_METH_CODE,
       PREF_COMM_METH_NAME,
       REC_PROM_FLG,
       REC_PTNR_PROM_FLG,
       ROOT_MEM_NAME,
       START_DT,
       W_STATUS_CODE,
       W_STATUS_NAME,
       STATUS_CODE,
       STATUS_NAME,
       SUPPRESS_CALL_FLG,
       SUPPRESS_EMAIL_FLG,
       SUPPRESS_FAX_FLG,
       SUPPRESS_MAIL_FLG,
       SUPPRESS_SMS_FLG,
       VAL_LIFE_SCORE,
       VAL_SCORE,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       SRC_EFF_FROM_DT,
       SRC_EFF_TO_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       XN_REG_CHANNEL_CD,
       XN_MEM_NUM,
       XN_PR_CON_ID,
       XN_STORE_ID,
       XN_POINT_TYPE_A_VAL,
       XN_POINT_TYPE_B_VAL,
       XN_POINT_TYPE_C_VAL,
       XN_POINT_TYPE_D_VAL,
       XN_POINT_TYPE_E_VAL,
       XN_POINT_TYPE_F_VAL,
       XN_POINT_TYPE_G_VAL,
       XN_POINT_TYPE_H_VAL,
       XN_LFTM_PT_TYPE_A_VAL,
       XN_LFTM_PT_TYPE_B_VAL,
       XN_LFTM_PT_TYPE_C_VAL,
       XN_LFTM_PT_TYPE_D_VAL,
       XN_LFTM_PT_TYPE_E_VAL,
       XN_LFTM_PT_TYPE_F_VAL,
       XN_LFTM_PT_TYPE_G_VAL,
       XN_LFTM_PT_TYPE_H_VAL,
       XN_STORE_NUMBER,
       XN_BU_ID,
       XN_ETL_DATE,
	   XN_CHARITY,
       XN_DIGITAL_VCHR_FLG,
	   XN_ACCNT_ID,
	   XN_SAP_ID,
	   JobRunId
)
SELECT
	   ACCRUAL_TO_MEM_NAME,
       PROGRAM_ID,
       CHILD_NUM,
       EXPIRY_DT,
       LANG_NAME,
       LAST_ACCR_TXN_DT,
       LAST_RDM_TXN_DT,
       LAST_TXN_PROCED_DT,
       LOUNGE_CODE,
       LOUNGE_NAME,
       LOY_LST_STMT_DT,
       LST_TIER_CHANGE_DT,
       MAX_POINT_LOAN,
       MEM_CLASS_CODE,
       MEM_CLASS_NAME,
       MEM_GRP_CODE,
       MEM_GRP_NAME,
       MEM_PHASE_CODE,
       MEM_PHASE_NAME,
       MEM_TYPE_CODE,
       MEM_TYPE_NAME,
       MEMBER_NAME,
       PIN_NUM,
       PNR_NAME,
       PREF_COMM_METH_CODE,
       PREF_COMM_METH_NAME,
       REC_PROM_FLG,
       REC_PTNR_PROM_FLG,
       ROOT_MEM_NAME,
       START_DT,
       W_STATUS_CODE,
       W_STATUS_NAME,
       STATUS_CODE,
       STATUS_NAME,
       SUPPRESS_CALL_FLG,
       SUPPRESS_EMAIL_FLG,
       SUPPRESS_FAX_FLG,
       SUPPRESS_MAIL_FLG,
       SUPPRESS_SMS_FLG,
       VAL_LIFE_SCORE,
       VAL_SCORE,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       SRC_EFF_FROM_DT,
       SRC_EFF_TO_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       XN_REG_CHANNEL_CD,
       XN_MEM_NUM,
       XN_PR_CON_ID,
       XN_STORE_ID,
       XN_POINT_TYPE_A_VAL,
       XN_POINT_TYPE_B_VAL,
       XN_POINT_TYPE_C_VAL,
       XN_POINT_TYPE_D_VAL,
       XN_POINT_TYPE_E_VAL,
       XN_POINT_TYPE_F_VAL,
       XN_POINT_TYPE_G_VAL,
       XN_POINT_TYPE_H_VAL,
       XN_LFTM_PT_TYPE_A_VAL,
       XN_LFTM_PT_TYPE_B_VAL,
       XN_LFTM_PT_TYPE_C_VAL,
       XN_LFTM_PT_TYPE_D_VAL,
       XN_LFTM_PT_TYPE_E_VAL,
       XN_LFTM_PT_TYPE_F_VAL,
       XN_LFTM_PT_TYPE_G_VAL,
       XN_LFTM_PT_TYPE_H_VAL,
       XN_STORE_NUMBER,
       XN_BU_ID,
       XN_ETL_DATE,
	   XN_CHARITY,
       XN_DIGITAL_VCHR_FLG,
	   XN_ACCNT_ID,
	   XN_SAP_ID,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.W_LOY_MEMBER_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.W_LOY_MEMBER_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 6) AxCRM WC_PARTY_ORG_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_PARTY_ORG_DS
(
       W_CUSTOMER_CLASS,
       PARTY_TYPE,
       NAME,
       MGR_NAME,
       MAIN_PH_NUM,
       ST_ADDRESS,
       CITY,
       STATE,
       ZIPCODE,
       COUNTRY,
       ACCNT_FLG,
       ACCNT_LOC,
       ACCNT_REVN,
       ACCNT_REVN_CURCY,
       ACCNT_REVN_DT,
       ACCNT_STATUS,
       ACCNT_TYPE_CD,
       CUST_TYPE_CODE,
       CUST_TYPE_NAME,
       ACTIVE_FLG,
       BASE_CURCY_CD,
       BU_ID,
       BU_NAME,
       CHANNEL_FLG,
       CHNL_ANNL_SALES,
       CHNL_SALES_GROWTH,
       EXCH_DT,
       COMPETITOR_FLG,
       CREATED_DT,
       DIVN_FLG,
       DIVN_TYPE_CD,
       DOM_ULT_DUNS_NUM,
       DUNS_NUM,
       EMP_COUNT,
       EXPERTISE,
       FORMED_DT,
       FRGHT_TERMS_CD,
       GLBLULT_DUNS_NUM,
       HIST_SLS_VOL,
       HIST_SLS_CURCY_CD,
       HIST_SLS_EXCH_DT,
       LINE_OF_BUSINESS,
       NUM_EMPLOYEES,
       ORG_TERR_NAME,
       ORG_FLG,
       ORG_PRTNR_FLG,
       ORG_PRTNR_TIER,
       ORG_PRTNR_TYPE,
       PAR_DUNS_NUM,
       PAR_INTEGRATION_ID,
       PAR_ORG_NAME,
       PRI_LST_NAME,
       PROSPECT_FLG,
       PRTNRSHP_START_DT,
       PRTNR_FLG,
       PRTNR_NAME,
       PRTNR_SALES_RANK,
       PR_COMPETITOR,
       PR_INDUST_NAME,
       PR_ORG_TRGT_MKT,
       PR_PTSHP_MKTSEG,
       PR_POSTN_ID,
       PTNTL_SLS_VOL,
       PTNTL_SLS_CURCY_CD,
       PTNTL_SLS_EXCH_DT,
       PTSHP_END_DT,
       PTSHP_FEE_PAID_FLG,
       PTSHP_PRTNR_ACCNT,
       PTSHP_RENEWAL_DT,
       PTSHP_SAT_INDEX,
       PTSHP_STAGE,
       PUBLIC_LISTING_FLG,
       REGION,
       SALES_EMP_CNT,
       SERVICE_EMP_CNT,
       VIS_PR_BU_ID,
       VIS_PR_POS_ID,
       ACCNT_AHA_NUM,
       ACCNT_CLASS,
       ACCNT_HIN_NUM,
       ACCNT_REGION,
       ACCNT_VALUE,
       AGENCY_FLG,
       AGNC_CONTRACT_DT,
       ANNUAL_REVENUE,
       ANNUAL_REVN,
       BOOK_VALUE,
       REVN_GROWTH,
       BRANCH_FLG,
       CALL_FREQUENCY,
       CLIENT_FLG,
       CREDIT_SCORE,
       CRIME_TYPE_CD,
       CURR_ASSET,
       CURR_LIAB,
       CUST_END_DT,
       CUST_SINCE_DT,
       CUST_STATUS_CODE,
       DIVIDEND,
       EXCHANGE_LOC,
       FACILITY_FLG,
       FACILITY_TYPE,
       FIFTYTWO_HIGH,
       FIFTYTWO_LOW,
       FIN_METHOD,
       GROSS_PROFIT,
       GROWTH_HORIZ,
       GROWTH_OBJ,
       GROWTH_PERCNTG,
       IDENTIFIED_DT,
       INVESTOR_FLG,
       KEY_COMPETITOR,
       LEADER_NAME,
       LEGAL_FORM,
       LOYAL_SCORE1,
       LOYAL_SCORE2,
       LOYAL_SCORE3,
       LOYAL_SCORE4,
       LOYAL_SCORE5,
       LOYAL_SCORE6,
       LOYAL_SCORE7,
       MARGIN_VS_INDUST,
       MARKET_CLASS,
       MARKET_TYPE,
       MED_PROC,
       MEMBER_NUM,
       MKT_POTENTIAL,
       MRKT_CAP_PREF,
       NAME_EFF_DT,
       NET_INCOME,
       NON_CASH_EXP,
       NUMB_BEDS_EFF_DT,
       NUMB_OF_BEDS,
       NUM_PROD,
       NUM_PROD_EFF_DT,
       OBJECTIVE,
       OPER_INCOME,
       PAR_ORG_EFF_DT,
       PERSIST_RATIO,
       PRIM_MARKET,
       PROJ_EPS,
       PR_SPEC_NAME,
       PR_SYN_ID,
       QUICK_RATIO,
       SHARE_OUTST,
       SRV_PROVDR_FLG,
       STAT_REASON_CD,
       TICKER,
       TOTAL_DEBT,
       TOTAL_NET_WORTH,
       TOT_ASSET,
       TOT_LIABILITY,
       TRAIL_EPS,
       VER_DT,
       VOLUME_TR,
       X_NUM_PROD,
       CUST_CAT_CODE,
       CUST_CAT_NAME,
       SIC_CODE,
       SIC_NAME,
       GOVT_ID_TYPE,
       GOVT_ID_VALUE,
       DUNNS_SITE_NAME,
       DUNNS_GLOBAL_NAME,
       DUNNS_LEGAL_NAME,
       CUSTOMER_NUM,
       ALT_CUSTOMER_NUM,
       ALT_PHONE_NUM,
       INTERNET_HOME_PAGE,
       LEGAL_STRUCT_CODE,
       LEGAL_STRUCT_NAME,
       DIRECT_MKTG_FLG,
       SOLICITATION_FLG,
       CUSTOMER_HIER1_CODE,
       CUSTOMER_HIER1_NAME,
       CUSTOMER_HIER2_CODE,
       CUSTOMER_HIER2_NAME,
       CUSTOMER_HIER3_CODE,
       CUSTOMER_HIER3_NAME,
       CUSTOMER_HIER4_CODE,
       CUSTOMER_HIER4_NAME,
       CUSTOMER_HIER5_CODE,
       CUSTOMER_HIER5_NAME,
       CUSTOMER_HIER6_CODE,
       CUSTOMER_HIER6_NAME,
       OOB_IND,
       MINORITY_OWNED_IND,
       WOMAN_OWNED_IND,
       DISADV_8A_IND,
       SMALL_BUS_IND,
       YR_ESTABLISHED,
       TAXPAYER_ID,
       STOCK_SYMBOL,
       DB_RATING,
       SIC_CODE_TYPE,
       INTERNAL_FLG,
       SUPPLIER_NUM,
       CONTACT_NAME,
       EMAIL_ADDRESS,
       SUPPLIER_GRP_CODE,
       SUPPLIER_TYPE_CODE,
       MINORITY_GROUP_CODE,
       SEARCH_STR,
       PREF_ORDER_METHOD,
       EXT_NETWORK_ID,
       SUPPLIER_ONE_TIME_FLG,
       SUPPLIER_ACTIVE_FLG,
       SETID_VENDOR,
       VENDOR_ID,
       PARENT_VENDOR_ID,
       SETID_CUSTOMER,
       CUST_ID,
       ST_ADDRESS2,
       ST_ADDRESS3,
       ST_ADDRESS4,
       FLEX_ATTRIB_1_CHAR,
       FLEX_ATTRIB_2_CHAR,
       FLEX_ATTRIB_3_CHAR,
       FLEX_ATTRIB_4_CHAR,
       FLEX_ATTRIB_5_CHAR,
       FLEX_ATTRIB_6_CHAR,
       FLEX_ATTRIB_7_CHAR,
       FLEX_ATTRIB_8_CHAR,
       FLEX_ATTRIB_9_CHAR,
       FLEX_ATTRIB_10_CHAR,
       FLEX_ATTRIB_11_CHAR,
       FLEX_ATTRIB_12_CHAR,
       FLEX_ATTRIB_13_CHAR,
       FLEX_ATTRIB_14_CHAR,
       FLEX_ATTRIB_15_CHAR,
       FLEX_ATTRIB_16_CHAR,
       FLEX_ATTRIB_17_CHAR,
       FLEX_ATTRIB_18_CHAR,
       FLEX_ATTRIB_19_CHAR,
       FLEX_ATTRIB_20_CHAR,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       SRC_EFF_FROM_DT,
       SRC_EFF_TO_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
	   XN_SAP_ID,
       XN_ETL_DATE,
       JobRunId
)
SELECT
       W_CUSTOMER_CLASS,
       PARTY_TYPE,
       NAME,
       MGR_NAME,
       MAIN_PH_NUM,
       ST_ADDRESS,
       CITY,
       STATE,
       ZIPCODE,
       COUNTRY,
       ACCNT_FLG,
       ACCNT_LOC,
       ACCNT_REVN,
       ACCNT_REVN_CURCY,
       ACCNT_REVN_DT,
       ACCNT_STATUS,
       ACCNT_TYPE_CD,
       CUST_TYPE_CODE,
       CUST_TYPE_NAME,
       ACTIVE_FLG,
       BASE_CURCY_CD,
       BU_ID,
       BU_NAME,
       CHANNEL_FLG,
       CHNL_ANNL_SALES,
       CHNL_SALES_GROWTH,
       EXCH_DT,
       COMPETITOR_FLG,
       CREATED_DT,
       DIVN_FLG,
       DIVN_TYPE_CD,
       DOM_ULT_DUNS_NUM,
       DUNS_NUM,
       EMP_COUNT,
       EXPERTISE,
       FORMED_DT,
       FRGHT_TERMS_CD,
       GLBLULT_DUNS_NUM,
       HIST_SLS_VOL,
       HIST_SLS_CURCY_CD,
       HIST_SLS_EXCH_DT,
       LINE_OF_BUSINESS,
       NUM_EMPLOYEES,
       ORG_TERR_NAME,
       ORG_FLG,
       ORG_PRTNR_FLG,
       ORG_PRTNR_TIER,
       ORG_PRTNR_TYPE,
       PAR_DUNS_NUM,
       PAR_INTEGRATION_ID,
       PAR_ORG_NAME,
       PRI_LST_NAME,
       PROSPECT_FLG,
       PRTNRSHP_START_DT,
       PRTNR_FLG,
       PRTNR_NAME,
       PRTNR_SALES_RANK,
       PR_COMPETITOR,
       PR_INDUST_NAME,
       PR_ORG_TRGT_MKT,
       PR_PTSHP_MKTSEG,
       PR_POSTN_ID,
       PTNTL_SLS_VOL,
       PTNTL_SLS_CURCY_CD,
       PTNTL_SLS_EXCH_DT,
       PTSHP_END_DT,
       PTSHP_FEE_PAID_FLG,
       PTSHP_PRTNR_ACCNT,
       PTSHP_RENEWAL_DT,
       PTSHP_SAT_INDEX,
       PTSHP_STAGE,
       PUBLIC_LISTING_FLG,
       REGION,
       SALES_EMP_CNT,
       SERVICE_EMP_CNT,
       VIS_PR_BU_ID,
       VIS_PR_POS_ID,
       ACCNT_AHA_NUM,
       ACCNT_CLASS,
       ACCNT_HIN_NUM,
       ACCNT_REGION,
       ACCNT_VALUE,
       AGENCY_FLG,
       AGNC_CONTRACT_DT,
       ANNUAL_REVENUE,
       ANNUAL_REVN,
       BOOK_VALUE,
       REVN_GROWTH,
       BRANCH_FLG,
       CALL_FREQUENCY,
       CLIENT_FLG,
       CREDIT_SCORE,
       CRIME_TYPE_CD,
       CURR_ASSET,
       CURR_LIAB,
       CUST_END_DT,
       CUST_SINCE_DT,
       CUST_STATUS_CODE,
       DIVIDEND,
       EXCHANGE_LOC,
       FACILITY_FLG,
       FACILITY_TYPE,
       FIFTYTWO_HIGH,
       FIFTYTWO_LOW,
       FIN_METHOD,
       GROSS_PROFIT,
       GROWTH_HORIZ,
       GROWTH_OBJ,
       GROWTH_PERCNTG,
       IDENTIFIED_DT,
       INVESTOR_FLG,
       KEY_COMPETITOR,
       LEADER_NAME,
       LEGAL_FORM,
       LOYAL_SCORE1,
       LOYAL_SCORE2,
       LOYAL_SCORE3,
       LOYAL_SCORE4,
       LOYAL_SCORE5,
       LOYAL_SCORE6,
       LOYAL_SCORE7,
       MARGIN_VS_INDUST,
       MARKET_CLASS,
       MARKET_TYPE,
       MED_PROC,
       MEMBER_NUM,
       MKT_POTENTIAL,
       MRKT_CAP_PREF,
       NAME_EFF_DT,
       NET_INCOME,
       NON_CASH_EXP,
       NUMB_BEDS_EFF_DT,
       NUMB_OF_BEDS,
       NUM_PROD,
       NUM_PROD_EFF_DT,
       OBJECTIVE,
       OPER_INCOME,
       PAR_ORG_EFF_DT,
       PERSIST_RATIO,
       PRIM_MARKET,
       PROJ_EPS,
       PR_SPEC_NAME,
       PR_SYN_ID,
       QUICK_RATIO,
       SHARE_OUTST,
       SRV_PROVDR_FLG,
       STAT_REASON_CD,
       TICKER,
       TOTAL_DEBT,
       TOTAL_NET_WORTH,
       TOT_ASSET,
       TOT_LIABILITY,
       TRAIL_EPS,
       VER_DT,
       VOLUME_TR,
       X_NUM_PROD,
       CUST_CAT_CODE,
       CUST_CAT_NAME,
       SIC_CODE,
       SIC_NAME,
       GOVT_ID_TYPE,
       GOVT_ID_VALUE,
       DUNNS_SITE_NAME,
       DUNNS_GLOBAL_NAME,
       DUNNS_LEGAL_NAME,
       CUSTOMER_NUM,
       ALT_CUSTOMER_NUM,
       ALT_PHONE_NUM,
       INTERNET_HOME_PAGE,
       LEGAL_STRUCT_CODE,
       LEGAL_STRUCT_NAME,
       DIRECT_MKTG_FLG,
       SOLICITATION_FLG,
       CUSTOMER_HIER1_CODE,
       CUSTOMER_HIER1_NAME,
       CUSTOMER_HIER2_CODE,
       CUSTOMER_HIER2_NAME,
       CUSTOMER_HIER3_CODE,
       CUSTOMER_HIER3_NAME,
       CUSTOMER_HIER4_CODE,
       CUSTOMER_HIER4_NAME,
       CUSTOMER_HIER5_CODE,
       CUSTOMER_HIER5_NAME,
       CUSTOMER_HIER6_CODE,
       CUSTOMER_HIER6_NAME,
       OOB_IND,
       MINORITY_OWNED_IND,
       WOMAN_OWNED_IND,
       DISADV_8A_IND,
       SMALL_BUS_IND,
       YR_ESTABLISHED,
       TAXPAYER_ID,
       STOCK_SYMBOL,
       DB_RATING,
       SIC_CODE_TYPE,
       INTERNAL_FLG,
       SUPPLIER_NUM,
       CONTACT_NAME,
       EMAIL_ADDRESS,
       SUPPLIER_GRP_CODE,
       SUPPLIER_TYPE_CODE,
       MINORITY_GROUP_CODE,
       SEARCH_STR,
       PREF_ORDER_METHOD,
       EXT_NETWORK_ID,
       SUPPLIER_ONE_TIME_FLG,
       SUPPLIER_ACTIVE_FLG,
       SETID_VENDOR,
       VENDOR_ID,
       PARENT_VENDOR_ID,
       SETID_CUSTOMER,
       CUST_ID,
       ST_ADDRESS2,
       ST_ADDRESS3,
       ST_ADDRESS4,
       FLEX_ATTRIB_1_CHAR,
       FLEX_ATTRIB_2_CHAR,
       FLEX_ATTRIB_3_CHAR,
       FLEX_ATTRIB_4_CHAR,
       FLEX_ATTRIB_5_CHAR,
       FLEX_ATTRIB_6_CHAR,
       FLEX_ATTRIB_7_CHAR,
       FLEX_ATTRIB_8_CHAR,
       FLEX_ATTRIB_9_CHAR,
       FLEX_ATTRIB_10_CHAR,
       FLEX_ATTRIB_11_CHAR,
       FLEX_ATTRIB_12_CHAR,
       FLEX_ATTRIB_13_CHAR,
       FLEX_ATTRIB_14_CHAR,
       FLEX_ATTRIB_15_CHAR,
       FLEX_ATTRIB_16_CHAR,
       FLEX_ATTRIB_17_CHAR,
       FLEX_ATTRIB_18_CHAR,
       FLEX_ATTRIB_19_CHAR,
       FLEX_ATTRIB_20_CHAR,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       SRC_EFF_FROM_DT,
       SRC_EFF_TO_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
	   XN_SAP_ID,
       XN_ETL_DATE,       
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.WC_PARTY_ORG_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_PARTY_ORG_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 7) AxCRM WC_PARTY_PER_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_PARTY_PER_DS
(
	   PARTY_TYPE,
       FST_NAME,
       FULL_NAME,
       MID_NAME,
       LAST_NAME,
       DEPARTMENT_NAME,
       CONTACT_ID,
       PR_HOUSEHOLD,
       PROSPECT_ID,
       PRSP_CON_UID,
       CONTACT_TYPE,
       CON_BU_NAME,
       FAX_PH_NUM,
       PAGER_NUM,
       MOBILE_NUM,
       WORK_PHONE,
       SEX_MF,
       SEX_MF_CD,
       HAIR_COLOR,
       HEIGHT,
       WEIGHT,
       ETHNICITY_CD,
       COMPLEXION_CD,
       MARITAL_STAT_CD,
       SPOUSE_NAME,
       BIRTH_DT,
       BIRTH_PLACE,
       DEATH_DT,
       ST_ADDRESS,
       CITY,
       STATE,
       ZIPCODE,
       COUNTRY,
       CITIZENSHIP_CD,
       EDUCATION_BCKGND,
       EDUCATION_YEARS,
       GRAD_YR,
       EMAIL_ADDR,
       JOB_CATEGORY,
       JOB_TITLE,
       LOGIN,
       MEMBER_NUM,
       ADDR_EFF_DT,
       ANNL_INCOME,
       ANNL_REVENUE,
       CREDIT_SCORE,
       TAX_BRACKET,
       AGENT_FLG,
       CALL_FLG,
       COMP_OWNER_FLG,
       CONSUMER_FLG,
       CON_ACTIVE_FLG,
       CON_ACTIVE_FLG1,
       CON_ACTIVE_FLG2,
       CON_ACTIVE_FLG3,
       CON_FORMED_DT,
       CUSTOMER_END_DT,
       CUSTOMER_SINCE_DT,
       CUST_VAL_EFF_DT,
       DECEASE_FLG,
       EMAIL_SR_UPD_FLG,
       ENTREPRISE_FLG,
       FST_PROMO_DT,
       HARD_TO_REACH,
       INCOME_RNG_EFF_DT,
       LST_PROMO_DT,
       MEMBER_FLG,
       NAME_EFF_DT,
       NET_WORTH_EFF_DT,
       NUM_CMPGNS,
       NUM_OFRS_PRSNTD,
       NUM_PROD,
       NUM_PROD_EFF_DT,
       PRESCRIBER_FLG,
       PROVIDER_FLG,
       RESDNCE_VAL,
       SELF_EMPL_FLG,
       SEMINAR_INVIT_FLG,
       SPEAKER_FLG,
       SUPPRESS_CALL_FLG,
       SUPPRESS_EMAIL_FLG,
       SUPPRESS_FAX_FLG,
       SUPPRESS_MAIL_FLG,
       USER_ACTIVE_FLG,
       USER_FLG,
       USER_FORMED_DT,
       WPH_EFF_DT,
       YEARS_AT_ACCNT,
       YRS_AT_RESIDENCE,
       ACCESS_LVL,
       ACCNT_ID,
       AGE_RANGE,
       EMP_ACCNT_BU_NAME,
       EMP_ACCNT_LOC,
       EMP_ACCNT_NAME,
       TERR_NAME,
       EMP_ACTIVE_FLG,
       EMP_FLG,
       EMP_FORMED_DT,
       EMP_HIRE_DT,
       APPR_AUTH,
       APPR_CURCY_CD,
       CALL_LST_NAME,
       COM_PREFERENCE,
       CSN,
       CUST_VALUE,
       DEPT_TYPE,
       DISABILITY,
       EMPLMNT_STAT_CD,
       EXT_CON_STORE,
       EYE_COLOR_LEFT,
       EYE_COLOR_RIGHT,
       GOALS,
       HOBBY,
       INCOME_RANGE,
       INS_OCCUPATION,
       INVST_EXPERIENCE,
       INVST_HORIZON,
       INVST_KNOWLEDGE,
       INVST_OBJECTIVE,
       INVST_PROFILE,
       INVST_RISK,
       MKT_POTENTIAL,
       MRKT_CAP_PREF,
       NATIONALITY,
       NET_WORTH,
       OU_MAIL_STOP,
       PAR_HELD_POSTN,
       PR_POSTN,
       HELD_POSTN,
       PREF_COMM_MEDIA,
       PREF_LANG_ID,
       PRE_LANG,
       PROF_CALL_FREQ,
       PROF_TITLE,
       PR_MKT_SEGMENT,
       PR_SPECIALTY,
       PR_SPEC_NAME,
       RACE,
       REGION,
       RESDNCE_CATEGORY,
       RESDNCE_TYPE,
       RESIDENCY_INSTN,
       RSRCH_CHNL_PREF,
       SPECIALTY_BRICK,
       STAFF,
       STATUS_CD,
       STAT_REASON_CD,
       VIS_PR_BU_ID,
       VIS_PR_POS_ID,
       X_CUST_VALUE,
       X_INCOME_RANGE,
       X_NUM_PROD,
       ETHNICITY1_CD,
       ETHNICITY2_CD,
       ANNL_INCM_EXCH_DT,
       ANNL_INCM_CURCY_CD,
       ANNL_REV_CURCY_CD,
       VER_DT,
       VER_DT1,
       VER_DT2,
       VER_DT3,
       ST_ADDRESS2,
       ST_ADDRESS3,
       ST_ADDRESS4,
       VETERAN_FLG,
       STUDENT_FLG,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       SRC_EFF_FROM_DT,
       SRC_EFF_TO_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       SUPPLIER_ID,
       CUSTOMER_NUM,
       XN_SOC_SECURITY_NUM,
       XN_CELL_PH_NUM,
       XN_PR_PER_ADDR_ID,
       XN_SUPPRESS_SMS_FLG,
       XN_SEND_NEWS_FLG,
       XN_CUST_STAT_CD,
       XN_PAR_UPD_DT,
       XN_NO_WEEKLY_COMM,
       XN_MEM_CARD_NUMBER,
       XN_GOODBYE_SENT_FLG,
       XN_IS_PROTECTED_FLG,
       XN_REG_CHANNEL_CD,
       XN_SELFSCAN_UPD_DT,
       XN_SOC_SECURITY_NUM2,
       XN_SSN_READ_ONLY_FLG,
       XN_WELCOME_SENT_FLG,
       XN_EMAIL_STATUS_CD,
       XN_LST_EMLADR_UPD_TS,
       XN_NUM_HARD_BNCE,
       XN_NUM_SOFT_BNCE,
       XN_USER_ROW_ID,
       XN_USER_PAR_ROW_ID,
       XN_ETL_DATE,
	   XN_NO_ADD_COMM,
	   XN_REG_DT,
	   XN_DIGITAL_RECEIPT_PREF,
       JobRunId
)
SELECT
	   PARTY_TYPE,
       FST_NAME,
       FULL_NAME,
       MID_NAME,
       LAST_NAME,
       DEPARTMENT_NAME,
       CONTACT_ID,
       PR_HOUSEHOLD,
       PROSPECT_ID,
       PRSP_CON_UID,
       CONTACT_TYPE,
       CON_BU_NAME,
       FAX_PH_NUM,
       PAGER_NUM,
       MOBILE_NUM,
       WORK_PHONE,
       SEX_MF,
       SEX_MF_CD,
       HAIR_COLOR,
       HEIGHT,
       WEIGHT,
       ETHNICITY_CD,
       COMPLEXION_CD,
       MARITAL_STAT_CD,
       SPOUSE_NAME,
       BIRTH_DT,
       BIRTH_PLACE,
       DEATH_DT,
       ST_ADDRESS,
       CITY,
       STATE,
       ZIPCODE,
       COUNTRY,
       CITIZENSHIP_CD,
       EDUCATION_BCKGND,
       EDUCATION_YEARS,
       GRAD_YR,
       EMAIL_ADDR,
       JOB_CATEGORY,
       JOB_TITLE,
       LOGIN,
       MEMBER_NUM,
       ADDR_EFF_DT,
       ANNL_INCOME,
       ANNL_REVENUE,
       CREDIT_SCORE,
       TAX_BRACKET,
       AGENT_FLG,
       CALL_FLG,
       COMP_OWNER_FLG,
       CONSUMER_FLG,
       CON_ACTIVE_FLG,
       CON_ACTIVE_FLG1,
       CON_ACTIVE_FLG2,
       CON_ACTIVE_FLG3,
       CON_FORMED_DT,
       CUSTOMER_END_DT,
       CUSTOMER_SINCE_DT,
       CUST_VAL_EFF_DT,
       DECEASE_FLG,
       EMAIL_SR_UPD_FLG,
       ENTREPRISE_FLG,
       FST_PROMO_DT,
       HARD_TO_REACH,
       INCOME_RNG_EFF_DT,
       LST_PROMO_DT,
       MEMBER_FLG,
       NAME_EFF_DT,
       NET_WORTH_EFF_DT,
       NUM_CMPGNS,
       NUM_OFRS_PRSNTD,
       NUM_PROD,
       NUM_PROD_EFF_DT,
       PRESCRIBER_FLG,
       PROVIDER_FLG,
       RESDNCE_VAL,
       SELF_EMPL_FLG,
       SEMINAR_INVIT_FLG,
       SPEAKER_FLG,
       SUPPRESS_CALL_FLG,
       SUPPRESS_EMAIL_FLG,
       SUPPRESS_FAX_FLG,
       SUPPRESS_MAIL_FLG,
       USER_ACTIVE_FLG,
       USER_FLG,
       USER_FORMED_DT,
       WPH_EFF_DT,
       YEARS_AT_ACCNT,
       YRS_AT_RESIDENCE,
       ACCESS_LVL,
       ACCNT_ID,
       AGE_RANGE,
       EMP_ACCNT_BU_NAME,
       EMP_ACCNT_LOC,
       EMP_ACCNT_NAME,
       TERR_NAME,
       EMP_ACTIVE_FLG,
       EMP_FLG,
       EMP_FORMED_DT,
       EMP_HIRE_DT,
       APPR_AUTH,
       APPR_CURCY_CD,
       CALL_LST_NAME,
       COM_PREFERENCE,
       CSN,
       CUST_VALUE,
       DEPT_TYPE,
       DISABILITY,
       EMPLMNT_STAT_CD,
       EXT_CON_STORE,
       EYE_COLOR_LEFT,
       EYE_COLOR_RIGHT,
       GOALS,
       HOBBY,
       INCOME_RANGE,
       INS_OCCUPATION,
       INVST_EXPERIENCE,
       INVST_HORIZON,
       INVST_KNOWLEDGE,
       INVST_OBJECTIVE,
       INVST_PROFILE,
       INVST_RISK,
       MKT_POTENTIAL,
       MRKT_CAP_PREF,
       NATIONALITY,
       NET_WORTH,
       OU_MAIL_STOP,
       PAR_HELD_POSTN,
       PR_POSTN,
       HELD_POSTN,
       PREF_COMM_MEDIA,
       PREF_LANG_ID,
       PRE_LANG,
       PROF_CALL_FREQ,
       PROF_TITLE,
       PR_MKT_SEGMENT,
       PR_SPECIALTY,
       PR_SPEC_NAME,
       RACE,
       REGION,
       RESDNCE_CATEGORY,
       RESDNCE_TYPE,
       RESIDENCY_INSTN,
       RSRCH_CHNL_PREF,
       SPECIALTY_BRICK,
       STAFF,
       STATUS_CD,
       STAT_REASON_CD,
       VIS_PR_BU_ID,
       VIS_PR_POS_ID,
       X_CUST_VALUE,
       X_INCOME_RANGE,
       X_NUM_PROD,
       ETHNICITY1_CD,
       ETHNICITY2_CD,
       ANNL_INCM_EXCH_DT,
       ANNL_INCM_CURCY_CD,
       ANNL_REV_CURCY_CD,
       VER_DT,
       VER_DT1,
       VER_DT2,
       VER_DT3,
       ST_ADDRESS2,
       ST_ADDRESS3,
       ST_ADDRESS4,
       VETERAN_FLG,
       STUDENT_FLG,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       SRC_EFF_FROM_DT,
       SRC_EFF_TO_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       SUPPLIER_ID,
       CUSTOMER_NUM,
       XN_SOC_SECURITY_NUM,
       XN_CELL_PH_NUM,
       XN_PR_PER_ADDR_ID,
       XN_SUPPRESS_SMS_FLG,
       XN_SEND_NEWS_FLG,
       XN_CUST_STAT_CD,
       XN_PAR_UPD_DT,
       XN_NO_WEEKLY_COMM,
       XN_MEM_CARD_NUMBER,
       XN_GOODBYE_SENT_FLG,
       XN_IS_PROTECTED_FLG,
       XN_REG_CHANNEL_CD,
       XN_SELFSCAN_UPD_DT,
       XN_SOC_SECURITY_NUM2,
       XN_SSN_READ_ONLY_FLG,
       XN_WELCOME_SENT_FLG,
       XN_EMAIL_STATUS_CD,
       XN_LST_EMLADR_UPD_TS,
       XN_NUM_HARD_BNCE,
       XN_NUM_SOFT_BNCE,
       XN_USER_ROW_ID,
       XN_USER_PAR_ROW_ID,
       XN_ETL_DATE,
	   XN_NO_ADD_COMM,
	   XN_REG_DT,
	   XN_DIGITAL_RECEIPT_PREF,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.WC_PARTY_PER_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_PARTY_PER_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 8) AxCRM WC_ADDR_PER_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_ADDR_PER_DS
(
	   INTEGRATION_ID,
       DATASOURCE_NUM_ID,
       ACTIVE_FLG,
       CITY,
       COUNTRY,
       ZIPCODE,
       ADDR,
       ADDR_TYPE_CD,
       CHANGED_BY_ID,
       CHANGED_ON_DT,
       CREATED_BY_ID,
       CREATED_ON_DT,
       ETL_DATE,
       JobRunId
)
SELECT
	   INTEGRATION_ID,
       DATASOURCE_NUM_ID,
       ACTIVE_FLG,
       CITY,
       COUNTRY,
       ZIPCODE,
       ADDR,
       ADDR_TYPE_CD,
       CHANGED_BY_ID,
       CHANGED_ON_DT,
       CREATED_BY_ID,
       CREATED_ON_DT,
       ETL_DATE,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.WC_ADDR_PER_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_ADDR_PER_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 9) AxCRM WC_CON_ADDR_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_CON_ADDR_DS
(
       INTEGRATION_ID,
       DATASOURCE_NUM_ID,
       CONTACT_ID,
       ADDR_PER_ID,
       START_DT,
       END_DT,
       CHANGED_BY_ID,
       CHANGED_ON_DT,
       CREATED_BY_ID,
       CREATED_ON_DT,
       ETL_DATE,
       JobRunId
)
SELECT
       INTEGRATION_ID,
       DATASOURCE_NUM_ID,
       CONTACT_ID,
       ADDR_PER_ID,
       START_DT,
       END_DT,
       CHANGED_BY_ID,
       CHANGED_ON_DT,
       CREATED_BY_ID,
       CREATED_ON_DT,
       ETL_DATE,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.WC_CON_ADDR_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_CON_ADDR_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 10) AxCRM W_LOY_PROGRAM_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_W_LOY_PROGRAM_DS
(
       ACTIVE_FLG,
       END_DT,
       HOST_ORGANIZATION,
       PROGRAM,
       PARTNER_FLG,
       PNT_TO_PAY_FLG,
       POINT_EXPIRY_BASIS_CODE,
       POINT_EXPIRY_BASIS_NAME,
       PUR_STRT_MON_DAY,
       START_DT,
       APPLY_CODE,
       APPLY_NAME,
       TXFR_STRT_MON_DAY,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       XN_HOST_ORG_ID,
       XN_BU_ID,
       XN_ETL_DATE,
	   JobRunId  
)
SELECT
       ACTIVE_FLG,
       END_DT,
       HOST_ORGANIZATION,
       PROGRAM,
       PARTNER_FLG,
       PNT_TO_PAY_FLG,
       POINT_EXPIRY_BASIS_CODE,
       POINT_EXPIRY_BASIS_NAME,
       PUR_STRT_MON_DAY,
       START_DT,
       APPLY_CODE,
       APPLY_NAME,
       TXFR_STRT_MON_DAY,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       XN_HOST_ORG_ID,
       XN_BU_ID,
       XN_ETL_DATE,  
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.W_LOY_PROGRAM_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.W_LOY_PROGRAM_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 11) AxCRM W_LOY_PROMOTION_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_W_LOY_PROMOTION_DS
(
       PROMOTION,
       PROMO_START_DT,
       PROMO_END_DT,
       PKG_ACRL_FLG,
       BASE_PROMO_FLG,
       ACTIVE_FLG,
       ADMIN_FLG,
       ALWAYS_APPLY_FLG,
       PROMOTION_DESC,
       ENROLLMENT_START_DT,
       ENROLLMENT_END_DT,
       ENROLLMENT_REQUIRED_FLG,
       POINT_LIMIT_CODE,
       POINT_LIMIT_NAME,
       PROD_INCLUSION_CODE,
       PROD_INCLUSION_NAME,
       SUB_TYPE_CODE,
       SUB_TYPE_NAME,
       TYPE_CODE,
       TYPE_NAME,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       XN_LOY_PROG_ID,
       XN_PROMO_NUM,
       XN_TIER_ID,
       XN_ETL_DATE,
	   JobRunId
)
SELECT
       PROMOTION,
       PROMO_START_DT,
       PROMO_END_DT,
       PKG_ACRL_FLG,
       BASE_PROMO_FLG,
       ACTIVE_FLG,
       ADMIN_FLG,
       ALWAYS_APPLY_FLG,
       PROMOTION_DESC,
       ENROLLMENT_START_DT,
       ENROLLMENT_END_DT,
       ENROLLMENT_REQUIRED_FLG,
       POINT_LIMIT_CODE,
       POINT_LIMIT_NAME,
       PROD_INCLUSION_CODE,
       PROD_INCLUSION_NAME,
       SUB_TYPE_CODE,
       SUB_TYPE_NAME,
       TYPE_CODE,
       TYPE_NAME,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       XN_LOY_PROG_ID,
       XN_PROMO_NUM,
       XN_TIER_ID,
       XN_ETL_DATE,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.W_LOY_PROMOTION_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.W_LOY_PROMOTION_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 12) AxCRM W_LOY_REDEMPTION_ITEM_FS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_W_LOY_RED_ITEM_FS
(
       PROCESS_DT,
       PROGRAM_ID,
       PARTNER_ID,
       MEMBER_ID,
       PROMO_ID,
       POINTTYPE_ID,
       TRANSACTION_ID,
       CONTACT_GEO_ID,
       ACCRUAL_ID_NUM,
       REDEEMED_VALUE,
       DELETE_FLG,
       REJECT_FLG,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       EXPIRED_FLG,
       XN_ACTION_ID,
       XN_ITEM_NUM,
       XN_MEMBER_ID,
       XN_TXN_ID,
       XN_TYPE_CD,
       XN_ETL_DATE,
	   JobRunId  
)
SELECT
       PROCESS_DT,
       PROGRAM_ID,
       PARTNER_ID,
       MEMBER_ID,
       PROMO_ID,
       POINTTYPE_ID,
       TRANSACTION_ID,
       CONTACT_GEO_ID,
       ACCRUAL_ID_NUM,
       REDEEMED_VALUE,
       DELETE_FLG,
       REJECT_FLG,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       EXPIRED_FLG,
       XN_ACTION_ID,
       XN_ITEM_NUM,
       XN_MEMBER_ID,
       XN_TXN_ID,
       XN_TYPE_CD,
       XN_ETL_DATE,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.W_LOY_REDEMPTION_ITEM_FS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.W_LOY_REDEMPTION_ITEM_FS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 13) AxCRM W_LOY_TIER_TIERCLS_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_W_LOY_TIER_TC_DS
(
       TIER_CLASS_NAME,
       ACTIVE_FLG,
       EXPIRE_POINTS_FLG,
       TIER_NAME,
       CARD_DESC_TEXT,
       CARD_TYPE_CODE,
       CARD_TYPE_NAME,
       LOUNGE_CODE,
       LOUNGE_NAME,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       TIER_CLASS_ID,
       TIER_ID,
       PROGRAM_ID,
       XN_ETL_DATE,
	   JobRunId
)
SELECT
       TIER_CLASS_NAME,
       ACTIVE_FLG,
       EXPIRE_POINTS_FLG,
       TIER_NAME,
       CARD_DESC_TEXT,
       CARD_TYPE_CODE,
       CARD_TYPE_NAME,
       LOUNGE_CODE,
       LOUNGE_NAME,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       TIER_CLASS_ID,
       TIER_ID,
       PROGRAM_ID,
       XN_ETL_DATE,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.W_LOY_TIER_TIERCLS_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.W_LOY_TIER_TIERCLS_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 14) AxCRM W_LOY_TRANSACTION_ID_DS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_W_LOY_TRANS_ID_DS
(
       TXN_NUM_ID,
       TXN_STATUS_CODE,
       TXN_STATUS_NAME,
       TXN_TYPE_CODE,
       TXN_TYPE_NAME,
       TXN_SUB_TYPE_CODE,
       TXN_SUB_TYPE_NAME,
       VCHR_FLG,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       W_TXN_STATUS_CODE,
       W_TXN_STATUS_NAME,
       W_TXN_TYPE_CODE,
       W_TXN_TYPE_NAME,
       W_TXN_SUB_TYPE_CODE,
       W_TXN_SUB_TYPE_NAME,
       PROGRAM_ID,
       XN_MEMBER_ID,
       XN_TXN_DT,
       XN_TXN_CHANNEL_CD,
       XN_VOUCHER_ID,
       XN_AMT_VAL,
       XN_BOOKING_DT,
       XN_TICKET_NUM,
       XN_FARE_TYPE_CD,
       XN_TOTAL_DISCOUNT,
       XN_EXTRA_BONUS_AMT,
       XN_EMPL_DISCOUNT,
       XN_MEM_DISCOUNT,
       XN_GEN_DISCOUNT,
       XN_POST_REG_FLG,
       XN_OFFLINE_TNX_FLG,
       XN_PROCESS_DT,
       XN_PROD_ID,
       XN_RECEIPT_NUM,
       XN_STORE_ID,
       XN_MEM_CARD_NUM,
       XN_IDENTIF_NUM,
       XN_IDENTIF_TYPE_CD,
       XN_STORE_NUMBER,
       XN_ETL_DATE,
       XN_ORIG_AMOUNT,
       XN_PARTNER_ID ,
	   XN_DIGITAL_RECEIPT_AVAIL,
	   JobRunId	   
)
SELECT
       TXN_NUM_ID,
       TXN_STATUS_CODE,
       TXN_STATUS_NAME,
       TXN_TYPE_CODE,
       TXN_TYPE_NAME,
       TXN_SUB_TYPE_CODE,
       TXN_SUB_TYPE_NAME,
       VCHR_FLG,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DELETE_FLG,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       W_TXN_STATUS_CODE,
       W_TXN_STATUS_NAME,
       W_TXN_TYPE_CODE,
       W_TXN_TYPE_NAME,
       W_TXN_SUB_TYPE_CODE,
       W_TXN_SUB_TYPE_NAME,
       PROGRAM_ID,
       XN_MEMBER_ID,
       XN_TXN_DT,
       XN_TXN_CHANNEL_CD,
       XN_VOUCHER_ID,
       XN_AMT_VAL,
       XN_BOOKING_DT,
       XN_TICKET_NUM,
       XN_FARE_TYPE_CD,
       XN_TOTAL_DISCOUNT,
       XN_EXTRA_BONUS_AMT,
       XN_EMPL_DISCOUNT,
       XN_MEM_DISCOUNT,
       XN_GEN_DISCOUNT,
       XN_POST_REG_FLG,
       XN_OFFLINE_TNX_FLG,
       XN_PROCESS_DT,
       XN_PROD_ID,
       XN_RECEIPT_NUM,
       XN_STORE_ID,
       XN_MEM_CARD_NUM,
       XN_IDENTIF_NUM,
       XN_IDENTIF_TYPE_CD,
       XN_STORE_NUMBER,
       XN_ETL_DATE,
       XN_ORIG_AMOUNT,
       XN_PARTNER_ID,
	   XN_DIGITAL_RECEIPT_AVAIL,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.W_LOY_TRANSACTION_ID_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.W_LOY_TRANSACTION_ID_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 15) AxCRM W_LOY_MEMBER_TIER_HIST_FS Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_W_LOY_MEM_T_H_FS
(
       CHAIN_ID,
       PROGRAM_ID,
       MEMBER_ID,
       PARTY_ID,
       HOUSEHOLD_ID,
       CONTACT_GEO_ID,
       FROM_TIER_DT,
       FROM_TIER_ID,
       TO_TIER_DT,
       TO_TIER_ID,
       ACTIVE_FLG,
       APPR_STAT_CODE_ID,
       MOVING_FLG,
       DURATION,
       FROM_SEQ_NUM,
       TO_SEQ_NUM,
       TIER_CLASS_ID,
       DELETE_FLG,
       REJECT_FLG,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       XN_TIER_ID,
       XN_START_DT,
       XN_END_DT,
       XN_ETL_DATE,
	   JobRunId
  )
SELECT
       CHAIN_ID,
       PROGRAM_ID,
       MEMBER_ID,
       PARTY_ID,
       HOUSEHOLD_ID,
       CONTACT_GEO_ID,
       FROM_TIER_DT,
       FROM_TIER_ID,
       TO_TIER_DT,
       TO_TIER_ID,
       ACTIVE_FLG,
       APPR_STAT_CODE_ID,
       MOVING_FLG,
       DURATION,
       FROM_SEQ_NUM,
       TO_SEQ_NUM,
       TIER_CLASS_ID,
       DELETE_FLG,
       REJECT_FLG,
       CREATED_BY_ID,
       CHANGED_BY_ID,
       CREATED_ON_DT,
       CHANGED_ON_DT,
       AUX1_CHANGED_ON_DT,
       AUX2_CHANGED_ON_DT,
       AUX3_CHANGED_ON_DT,
       AUX4_CHANGED_ON_DT,
       DATASOURCE_NUM_ID,
       INTEGRATION_ID,
       TENANT_ID,
       X_CUSTOM,
       XN_TIER_ID,
       XN_START_DT,
       XN_END_DT,
       XN_ETL_DATE,
	   $JobRunID AS JobRunID
FROM
    ${DB_ENV}AxCRMBI.W_LOY_MEMBER_TIER_HIST_FS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.W_LOY_MEMBER_TIER_HIST_FS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR


------------------------------------------------------------------------------------------
-- 16) LDAP Stg_LDAP_MemberAccount Load Archiving
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_LDAP_MemberAccount
(
    MemberNo,
    ProgramID,
    TransactionTS,
    ContentType,
    SubContentType,
    ExtractionTS,
    IEReceivedTS,
    IEDeliveryTS,
    SourceVersion,
    TS,
    ArchiveKey,
	JobRunId
  )
SELECT
    MemberNo,
    ProgramID,
    TransactionTS,
    ContentType,
    SubContentType,
    ExtractionTS,
    IEReceivedTS,
    IEDeliveryTS,
    SourceVersion,
    TS,
    ArchiveKey,
	$JobRunID AS JobRunID
FROM
    ${DB_ENV}StgLDAPT.Stg_LDAP_MemberAccount
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}StgLDAPT.Stg_LDAP_MemberAccount
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 17) Arc_AxCRM_WC_PRODUCT_DS
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_PRODUCT_DS(
PROD_NAME, PRODUCT_TYPE_CODE, PRODUCT_TYPE_DESC, PR_PROD_LN,
PR_PROD_LN_DESC, CONFIG_CAT_CODE, CONFIG_CAT_DESC, PRICE_TYPE_CD,
BASIC_PRODUCT, PR_EQUIV_PROD_NAME, CONFIG_PROD_IND, CONTAINER_CODE,
CONTAINER_DESC, INDUSTRY_CODE, INDUSTRY_NAME, INDUSTRY_STD_DESC,
INTRODUCTION_DT, LOW_LEVEL_CODE, MAKE_BUY_IND, PROD_LIFE_CYCL_CD,
PROD_REPRCH_PERIOD, PROD_STRUCTURE_TYPE, PROD_GRP_CODE, PROD_GRP_DESC,
STORAGE_TYPE_CODE, STORAGE_TYPE_DESC, BATCH_IND, BRAND, COLOR,
CUSTOM_PROD_FLG, PACKAGED_PROD_FLG, RTRN_DEFECTIVE_FLG, SALES_PROD_FLG,
SALES_SRVC_FLG, SERIALIZED_FLG, NRC_FLG, FRU_FLG, ITEM_SIZE,
LEAD_TIME, MTBF, MTTR, PART_NUM, VENDOR_LOC, VENDOR_NAME, VENDR_PART_NUM,
DISCONTINUATION_DT, HAZARD_MTL_DESC, HAZARD_MTL_CODE, SALES_UOM_CODE,
SALES_UOM_DESC, SERIALIZED_COUNT, SHELF_LIFE, SHIP_MTHD_GRP_CODE,
SHIP_MTHD_GRP_DESC, SHIP_MTL_GRP_CODE, SHIP_MTL_GRP_DESC, SHIP_TYPE_CODE,
SHIP_TYPE_DESC, SOURCE_OF_SUPPLY, SPRT_WITHDRAWL_DT, UOM, BASE_UOM_CODE,
BASE_UOM_DESC, UNIT_GROSS_WEIGHT, UNIT_NET_WEIGHT, UNIT_VOLUME,
UNIV_PROD_CODE, UNIV_PROD_DESC, UOV_CODE, UOV_DESC, UOW_CODE,
UOW_DESC, APPLICATION_FLG, BODY_STYLE_CD, CASE_PACK, CTLG_CAT_ID,
DEALER_INV_PRICE, DEALER_INV_PRICE_EXCH_DT, DEALER_INV_PRICE_CURCY_CD,
DETAIL_TYPE, DOORS_TYPE_CD, DRIVE_TRAIN_CD, ENGINE_TYPE_CD, FUEL_TYPE_CD,
GROSS_MRGN, INVENTORY_FLG, MAKE_CD, MODEL_CD, MODEL_YR, MSRP,
MSRP_EXCH_DT, MSRP_CURCY_CD, ORDERABLE_FLG, PROD_NDC_ID, PROD_TYPE,
PROFIT_RANK, REFERRAL_FLG, RX_AVG_PRICE, RX_AVG_PRICE_EXCH_DT,
RX_AVG_PRICE_CURCY_CD, SERVICE_TYPE, STATUS, R_TYPE, SUB_TYPE,
SUB_TYPE_CD, TGT_CUST_TYPE, TRANSMISSION_CD, TRIM_CD, UNIT_CONV_FACTOR,
VER_DT, PAR_INTEGRATION_ID, PROD_HIER1_CODE, PROD_HIER1_NAME,
PROD_HIER2_CODE, PROD_HIER2_NAME, PROD_HIER3_CODE, PROD_HIER3_NAME,
PROD_HIER4_CODE, PROD_HIER4_NAME, PROD_HIER5_CODE, PROD_HIER5_NAME,
PROD_HIER6_CODE, PROD_HIER6_NAME, CREATED_BY_ID, CHANGED_BY_ID,
CREATED_ON_DT, CHANGED_ON_DT, AUX1_CHANGED_ON_DT, AUX2_CHANGED_ON_DT,
AUX3_CHANGED_ON_DT, AUX4_CHANGED_ON_DT, SRC_EFF_FROM_DT, SRC_EFF_TO_DT,
DELETE_FLG, DATASOURCE_NUM_ID, INTEGRATION_ID, SET_ID, TENANT_ID,
X_CUSTOM, PROD_CAT1, PROD_CAT2, PROD_CAT3, PROD_CAT4, PROD_CAT5,
PROD_CAT6, PROD_CAT7, PROD_CAT8, PROD_CAT9, PROD_CAT10, UNSPSC_CODE,
PRODUCT_CATEGORY_FLG, FLEX_ATTRIB_1_CHAR, FLEX_ATTRIB_2_CHAR,
FLEX_ATTRIB_3_CHAR, FLEX_ATTRIB_4_CHAR, FLEX_ATTRIB_5_CHAR, FLEX_ATTRIB_6_CHAR,
FLEX_ATTRIB_7_CHAR, FLEX_ATTRIB_8_CHAR, FLEX_ATTRIB_9_CHAR, FLEX_ATTRIB_10_CHAR,
FLEX_ATTRIB_11_CHAR, FLEX_ATTRIB_12_CHAR, FLEX_ATTRIB_13_CHAR,
FLEX_ATTRIB_14_CHAR, FLEX_ATTRIB_15_CHAR, FLEX_ATTRIB_16_CHAR,
FLEX_ATTRIB_17_CHAR, FLEX_ATTRIB_18_CHAR, FLEX_ATTRIB_19_CHAR,
FLEX_ATTRIB_20_CHAR, XN_SUB_TYPE, XN_ETL_DATE,JobRunId)
SELECT	
PROD_NAME, PRODUCT_TYPE_CODE, PRODUCT_TYPE_DESC, PR_PROD_LN,
PR_PROD_LN_DESC, CONFIG_CAT_CODE, CONFIG_CAT_DESC, PRICE_TYPE_CD,
BASIC_PRODUCT, PR_EQUIV_PROD_NAME, CONFIG_PROD_IND, CONTAINER_CODE,
CONTAINER_DESC, INDUSTRY_CODE, INDUSTRY_NAME, INDUSTRY_STD_DESC,
INTRODUCTION_DT, LOW_LEVEL_CODE, MAKE_BUY_IND, PROD_LIFE_CYCL_CD,
PROD_REPRCH_PERIOD, PROD_STRUCTURE_TYPE, PROD_GRP_CODE, PROD_GRP_DESC,
STORAGE_TYPE_CODE, STORAGE_TYPE_DESC, BATCH_IND, BRAND, COLOR,
CUSTOM_PROD_FLG, PACKAGED_PROD_FLG, RTRN_DEFECTIVE_FLG, SALES_PROD_FLG,
SALES_SRVC_FLG, SERIALIZED_FLG, NRC_FLG, FRU_FLG, ITEM_SIZE,
LEAD_TIME, MTBF, MTTR, PART_NUM, VENDOR_LOC, VENDOR_NAME, VENDR_PART_NUM,
DISCONTINUATION_DT, HAZARD_MTL_DESC, HAZARD_MTL_CODE, SALES_UOM_CODE,
SALES_UOM_DESC, SERIALIZED_COUNT, SHELF_LIFE, SHIP_MTHD_GRP_CODE,
SHIP_MTHD_GRP_DESC, SHIP_MTL_GRP_CODE, SHIP_MTL_GRP_DESC, SHIP_TYPE_CODE,
SHIP_TYPE_DESC, SOURCE_OF_SUPPLY, SPRT_WITHDRAWL_DT, UOM, BASE_UOM_CODE,
BASE_UOM_DESC, UNIT_GROSS_WEIGHT, UNIT_NET_WEIGHT, UNIT_VOLUME,
UNIV_PROD_CODE, UNIV_PROD_DESC, UOV_CODE, UOV_DESC, UOW_CODE,
UOW_DESC, APPLICATION_FLG, BODY_STYLE_CD, CASE_PACK, CTLG_CAT_ID,
DEALER_INV_PRICE, DEALER_INV_PRICE_EXCH_DT, DEALER_INV_PRICE_CURCY_CD,
DETAIL_TYPE, DOORS_TYPE_CD, DRIVE_TRAIN_CD, ENGINE_TYPE_CD, FUEL_TYPE_CD,
GROSS_MRGN, INVENTORY_FLG, MAKE_CD, MODEL_CD, MODEL_YR, MSRP,
MSRP_EXCH_DT, MSRP_CURCY_CD, ORDERABLE_FLG, PROD_NDC_ID, PROD_TYPE,
PROFIT_RANK, REFERRAL_FLG, RX_AVG_PRICE, RX_AVG_PRICE_EXCH_DT,
RX_AVG_PRICE_CURCY_CD, SERVICE_TYPE, STATUS, R_TYPE, SUB_TYPE,
SUB_TYPE_CD, TGT_CUST_TYPE, TRANSMISSION_CD, TRIM_CD, UNIT_CONV_FACTOR,
VER_DT, PAR_INTEGRATION_ID, PROD_HIER1_CODE, PROD_HIER1_NAME,
PROD_HIER2_CODE, PROD_HIER2_NAME, PROD_HIER3_CODE, PROD_HIER3_NAME,
PROD_HIER4_CODE, PROD_HIER4_NAME, PROD_HIER5_CODE, PROD_HIER5_NAME,
PROD_HIER6_CODE, PROD_HIER6_NAME, CREATED_BY_ID, CHANGED_BY_ID,
CREATED_ON_DT, CHANGED_ON_DT, AUX1_CHANGED_ON_DT, AUX2_CHANGED_ON_DT,
AUX3_CHANGED_ON_DT, AUX4_CHANGED_ON_DT, SRC_EFF_FROM_DT, SRC_EFF_TO_DT,
DELETE_FLG, DATASOURCE_NUM_ID, INTEGRATION_ID, SET_ID, TENANT_ID,
X_CUSTOM, PROD_CAT1, PROD_CAT2, PROD_CAT3, PROD_CAT4, PROD_CAT5,
PROD_CAT6, PROD_CAT7, PROD_CAT8, PROD_CAT9, PROD_CAT10, UNSPSC_CODE,
PRODUCT_CATEGORY_FLG, FLEX_ATTRIB_1_CHAR, FLEX_ATTRIB_2_CHAR,
FLEX_ATTRIB_3_CHAR, FLEX_ATTRIB_4_CHAR, FLEX_ATTRIB_5_CHAR, FLEX_ATTRIB_6_CHAR,
FLEX_ATTRIB_7_CHAR, FLEX_ATTRIB_8_CHAR, FLEX_ATTRIB_9_CHAR, FLEX_ATTRIB_10_CHAR,
FLEX_ATTRIB_11_CHAR, FLEX_ATTRIB_12_CHAR, FLEX_ATTRIB_13_CHAR,
FLEX_ATTRIB_14_CHAR, FLEX_ATTRIB_15_CHAR, FLEX_ATTRIB_16_CHAR,
FLEX_ATTRIB_17_CHAR, FLEX_ATTRIB_18_CHAR, FLEX_ATTRIB_19_CHAR,
FLEX_ATTRIB_20_CHAR, XN_SUB_TYPE, XN_ETL_DATE,$JobRunID AS JobRunID
FROM
${DB_ENV}AxCRMBI.WC_PRODUCT_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_PRODUCT_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 18) Arc_AxCRM_W_LOY_VOUCHER_DS
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_W_LOY_VOUCHER_DS(
VOUCHER_NUM_ID, VOUCHER_TYPE_CODE, VOUCHER_TYPE_NAME, VOUCHER_STATUS_CODE,
VOUCHER_STATUS_NAME, EXPIRE_DT, USED_DT, CREATED_BY_ID, CHANGED_BY_ID,
CREATED_ON_DT, CHANGED_ON_DT, AUX1_CHANGED_ON_DT, AUX2_CHANGED_ON_DT,
AUX3_CHANGED_ON_DT, AUX4_CHANGED_ON_DT, DELETE_FLG, DATASOURCE_NUM_ID,
INTEGRATION_ID, TENANT_ID, X_CUSTOM, XN_MEMBER_ID, XN_MERGE_MEM_ID,
XN_TXN_ID, XN_TXN_ITEM_ID, XN_BASIS_CD, XN_EAN_CODE, XN_UNIQUE_CODE,
XN_AMOUNT, XN_VCHR_BASIS_CD, XN_VCHR_EFF_START_DT, XN_DISCNT_PCT,
XN_SUB_TYPE,XN_ETL_DATE,JobRunId)
SELECT	
VOUCHER_NUM_ID, VOUCHER_TYPE_CODE, VOUCHER_TYPE_NAME, VOUCHER_STATUS_CODE,
VOUCHER_STATUS_NAME, EXPIRE_DT, USED_DT, CREATED_BY_ID, CHANGED_BY_ID,
CREATED_ON_DT, CHANGED_ON_DT, AUX1_CHANGED_ON_DT, AUX2_CHANGED_ON_DT,
AUX3_CHANGED_ON_DT, AUX4_CHANGED_ON_DT, DELETE_FLG, DATASOURCE_NUM_ID,
INTEGRATION_ID, TENANT_ID, X_CUSTOM, XN_MEMBER_ID, XN_MERGE_MEM_ID,
XN_TXN_ID, XN_TXN_ITEM_ID, XN_BASIS_CD, XN_EAN_CODE, XN_UNIQUE_CODE,
XN_AMOUNT, XN_VCHR_BASIS_CD, XN_VCHR_EFF_START_DT, XN_DISCNT_PCT,
XN_SUB_TYPE,XN_ETL_DATE,$JobRunID AS JobRunID
FROM	
${DB_ENV}AxCRMBI.W_LOY_VOUCHER_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.W_LOY_VOUCHER_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 19) Arc_AxCRM_WC_CAMP_HIST_FS
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_CAMP_HIST_FS(
    DATASOURCE_NUM_ID,
    INTEGRATION_ID,
    COMPLETED_DT,
    CONTACTED_DT,
    CONTACTED_FLG,
    CREATED_DT,
    MSG_OPEN_TS,
    NUM_ATTEMPTS,
    ACCNT_ID,
    BNCE_REASON,
    BNCE_TYPE,
    CALL_STATUS,
    CAMPAIGN_ID,
    CAMP_OWNER_ORG_ID,
    CAMP_PARTNER_ID,
    CONTACT_ID,
    CONTACT_TYPE,
    CON_OUTCOME,
    KEY01,
    KEY02,
    KEY03,
    KEY04,
    KEY05,
    KEY06,
    KEY07,
    LD_WAVE_ID,
    PR_EMP_ID,
    SEGMENT_ID,
    SOURCE_CODE,
    SRC_REGN_ID,
    VENDOR_ID,
    WAVE_ID,
    CAMP_START_DT,
    CAMP_END_DT,
    CAMP_LAUNCH_DT,
    LOADED_TS,
    LAUNCHED_TS,
    SCHEDULED_TS,
    X_CUSTOM,
    PR_CAMP_OWNER_ORG_BU_ID,
    OFFER_ID,
    PARTY_ID,
    XN_CONTACT_COMM_ADDR,
    XN_CONTACT_OFFICIAL_ADDR,
    XN_LOY_MEMBER_CARD_ID,
    XN_LOY_MEMBER_ID,
    XN_PR_CONTACT_ID,
    XN_PR_CON_COMM_ADDR,
    XN_PR_CON_OFFICIAL_ADDR,
    XN_OFFER_NUM_RANK1,
    XN_OFFER_NUM_RANK2,
    XN_OFFER_NUM_RANK3,
    XN_OFFER_NUM_RANK4,
	XN_OFFER_NUM,
    XN_ETL_DATE,
	XN_VOUCHER_ID,
    JobRunId)
SELECT DATASOURCE_NUM_ID,
    INTEGRATION_ID,
    COMPLETED_DT,
    CONTACTED_DT,
    CONTACTED_FLG,
    CREATED_DT,
    MSG_OPEN_TS,
    NUM_ATTEMPTS,
    ACCNT_ID,
    BNCE_REASON,
    BNCE_TYPE,
    CALL_STATUS,
    CAMPAIGN_ID,
    CAMP_OWNER_ORG_ID,
    CAMP_PARTNER_ID,
    CONTACT_ID,
    CONTACT_TYPE,
    CON_OUTCOME,
    KEY01,
    KEY02,
    KEY03,
    KEY04,
    KEY05,
    KEY06,
    KEY07,
    LD_WAVE_ID,
    PR_EMP_ID,
    SEGMENT_ID,
    SOURCE_CODE,
    SRC_REGN_ID,
    VENDOR_ID,
    WAVE_ID,
    CAMP_START_DT,
    CAMP_END_DT,
    CAMP_LAUNCH_DT,
    LOADED_TS,
    LAUNCHED_TS,
    SCHEDULED_TS,
    X_CUSTOM,
    PR_CAMP_OWNER_ORG_BU_ID,
    OFFER_ID,
    PARTY_ID,
    XN_CONTACT_COMM_ADDR,
    XN_CONTACT_OFFICIAL_ADDR,
    XN_LOY_MEMBER_CARD_ID,
    XN_LOY_MEMBER_ID,
    XN_PR_CONTACT_ID,
    XN_PR_CON_COMM_ADDR,
    XN_PR_CON_OFFICIAL_ADDR,
    XN_OFFER_NUM_RANK1,
    XN_OFFER_NUM_RANK2,
    XN_OFFER_NUM_RANK3,
    XN_OFFER_NUM_RANK4,
	XN_OFFER_NUM,
    XN_ETL_DATE,
	XN_VOUCHER_ID,
	$JobRunID AS JobRunID
FROM	
${DB_ENV}AxCRMBI.WC_CAMP_HIST_FS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_CAMP_HIST_FS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 20) Arc_AxCRM_WC_LD_WAVE_DS
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_LD_WAVE_DS(
    DATASOURCE_NUM_ID,
    INTEGRATION_ID,
    ALLOC_PCT,
    LAG,
    LAUNCHED_TS,
    LOADED_TS,
    LOAD_NUM,
    NUM_CONS_LOADED,
    SCHEDULED_TS,
    WAVE_NUM,
    WAVE_SETTING_NUM,
    CAMPAIGN,
    CAMP_ID,
    EXEC_STATUS,
    LAG_UNITS,
    PR_DCP,
    X_CUSTOM,
    XN_EXPORTED_TS,
    XN_ETL_DATE,
    JobRunId)
SELECT 
    DATASOURCE_NUM_ID,
    INTEGRATION_ID,
    ALLOC_PCT,
    LAG,
    LAUNCHED_TS,
    LOADED_TS,
    LOAD_NUM,
    NUM_CONS_LOADED,
    SCHEDULED_TS,
    WAVE_NUM,
    WAVE_SETTING_NUM,
    CAMPAIGN,
    CAMP_ID,
    EXEC_STATUS,
    LAG_UNITS,
    PR_DCP,
    X_CUSTOM,
    XN_EXPORTED_TS,
    XN_ETL_DATE,
	$JobRunID AS JobRunID
FROM	
${DB_ENV}AxCRMBI.WC_LD_WAVE_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_LD_WAVE_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 21) Arc_AxCRM_WC_OFFER_DS
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_OFFER_DS(
    DATASOURCE_NUM_ID,
    INTEGRATION_ID,
    END_DT,
    LGL_APPR_DT,
    LGL_APPR_FLG,
    MKTG_APPR_DT,
    MKTG_APPR_FLG,
    START_DT,
    COMM_METHOD,
    LGL_APPR_BY,
    MEDIA_TYPE,
    MKTG_APPR_BY,
    OFFER_NAME,
    OFFER_TYPE,
    OFR_LANGUAGE,
    OFR_LOCALE,
    OFR_PRI_LST,
    STATUS,
    X_CUSTOM,
    PAR_OFFER_ID,
    PAR_OFFER_NAME,
    PAR_START_DT,
    PAR_END_DT,
    PAR_OFFER_TYPE,
    XN_OFFER_NUM,
    XN_OFFER_DESC,
    XN_OFFER_HEADING,
    XN_COMMENTS,
    MSG_SUBJ_TEXT,
    ETL_DATE,
    JobRunId)
SELECT
    DATASOURCE_NUM_ID,
    INTEGRATION_ID,
    END_DT,
    LGL_APPR_DT,
    LGL_APPR_FLG,
    MKTG_APPR_DT,
    MKTG_APPR_FLG,
    START_DT,
    COMM_METHOD,
    LGL_APPR_BY,
    MEDIA_TYPE,
    MKTG_APPR_BY,
    OFFER_NAME,
    OFFER_TYPE,
    OFR_LANGUAGE,
    OFR_LOCALE,
    OFR_PRI_LST,
    STATUS,
    X_CUSTOM,
    PAR_OFFER_ID,
    PAR_OFFER_NAME,
    PAR_START_DT,
    PAR_END_DT,
    PAR_OFFER_TYPE,
    XN_OFFER_NUM,
    XN_OFFER_DESC,
    XN_OFFER_HEADING,
    XN_COMMENTS,
    MSG_SUBJ_TEXT,
    ETL_DATE,
	$JobRunID AS JobRunID
FROM	
${DB_ENV}AxCRMBI.WC_OFFER_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_OFFER_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 22) Arc_AxCRM_WC_RESPONSE_DS
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_RESPONSE_DS(
    CREATED,
    CREATED_BY,
    DATASOURCE_NUM_ID,
    INTEGRATION_ID,
    CODED_RESP_FLG,
    RESP_DATE,
    HYPERLINK_NAME,
    OUTCOME,
    REJECT_REASON,
    RESP_METHOD,
    RESP_TYPE,
    RPLY_DLVRY_MTHD,
    RPLY_PRIORITY,
    RPLY_TIME,
    STATUS,
    SUBSCR_DRCT_ML,
    SUBSCR_EMAIL,
    SUBSCR_FAX,
    SUBSCR_PHONE,
    X_CUSTOM,
    XN_CAMP_HIST_ID,
    ETL_DATE,
    JobRunId)
SELECT CREATED,
    CREATED_BY,
    DATASOURCE_NUM_ID,
    INTEGRATION_ID,
    CODED_RESP_FLG,
    RESP_DATE,
    HYPERLINK_NAME,
    OUTCOME,
    REJECT_REASON,
    RESP_METHOD,
    RESP_TYPE,
    RPLY_DLVRY_MTHD,
    RPLY_PRIORITY,
    RPLY_TIME,
    STATUS,
    SUBSCR_DRCT_ML,
    SUBSCR_EMAIL,
    SUBSCR_FAX,
    SUBSCR_PHONE,
    X_CUSTOM,
    XN_CAMP_HIST_ID,
    ETL_DATE,
	$JobRunID AS JobRunID
FROM	
${DB_ENV}AxCRMBI.WC_RESPONSE_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_RESPONSE_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 23) Arc_AxCRM_WC_SOURCE_DS
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_SOURCE_DS(
    DATASOURCE_NUM_ID,
    INTEGRATION_ID,
    BDGT_AMT,
    BUDGET_DT,
    BUDGET_REQ,
    CAMP_TGT_CALLS,
    END_DT,
    SA_CAMP_FLG,
    START_DT,
    BU,
    BUDGET_CURCY_CD,
    CAMP_CALL_SCR,
    CAMP_DNIS,
    CUST_TYPE,
    EVT_BUD_PERIOD,
    EVT_STATUS,
    EVT_TYPE,
    LANG,
    MKTPLN_ID,
    MKTPLN_STATUS,
    NAME,
    OU,
    PAR_ID,
    PERIOD,
    PLAN_TYPE,
    PRIORITY,
    PROMO_NUM,
    REGN,
    REGN_ID,
    SRC_NUM,
    SRC_TYPE,
    STATUS,
    SUB_TYPE,
    TMPL_ID,
    ULT_PAR_ID,
    VIS_PR_BU_ID,
    VIS_PR_POS_ID,
    X_CUSTOM,
    XN_SRC_SUBTYPE_CD,
    XN_TARGET_GROUP,
    XN_ETL_DATE,
    JobRunId)
SELECT DATASOURCE_NUM_ID,
    INTEGRATION_ID,
    BDGT_AMT,
    BUDGET_DT,
    BUDGET_REQ,
    CAMP_TGT_CALLS,
    END_DT,
    SA_CAMP_FLG,
    START_DT,
    BU,
    BUDGET_CURCY_CD,
    CAMP_CALL_SCR,
    CAMP_DNIS,
    CUST_TYPE,
    EVT_BUD_PERIOD,
    EVT_STATUS,
    EVT_TYPE,
    LANG,
    MKTPLN_ID,
    MKTPLN_STATUS,
    NAME,
    OU,
    PAR_ID,
    PERIOD,
    PLAN_TYPE,
    PRIORITY,
    PROMO_NUM,
    REGN,
    REGN_ID,
    SRC_NUM,
    SRC_TYPE,
    STATUS,
    SUB_TYPE,
    TMPL_ID,
    ULT_PAR_ID,
    VIS_PR_BU_ID,
    VIS_PR_POS_ID,
    X_CUSTOM,
    XN_SRC_SUBTYPE_CD,
    XN_TARGET_GROUP,
    XN_ETL_DATE,
	$JobRunID AS JobRunID
FROM	
${DB_ENV}AxCRMBI.WC_SOURCE_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_SOURCE_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 24) Arc_AxCRM_WC_SRC_OFFR_FS
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_SRC_OFFR_FS(
    INTEGRATION_ID,
    DATASOURCE_NUM_ID,
    CAMPAIGN_ID,
    OFFER_ID,
    MKTG_OFFR_STAT_CD,
    OFFR_VALID_IN_DAYS,
    CHANGED_BY_ID,
    CHANGED_ON_DT,
    CREATED_BY_ID,
    CREATED_ON_DT,
    ETL_DATE,
    JobRunId)
SELECT INTEGRATION_ID,
    DATASOURCE_NUM_ID,
    CAMPAIGN_ID,
    OFFER_ID,
    MKTG_OFFR_STAT_CD,
    OFFR_VALID_IN_DAYS,
    CHANGED_BY_ID,
    CHANGED_ON_DT,
    CREATED_BY_ID,
    CREATED_ON_DT,
    ETL_DATE,
	$JobRunID AS JobRunID
FROM	
${DB_ENV}AxCRMBI.WC_SRC_OFFR_FS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_SRC_OFFR_FS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
------------------------------------------------------------------------------------------
-- 25) Arc_AxCRM_WC_SEGMENT_DS
------------------------------------------------------------------------------------------

INSERT INTO ${DB_ENV}Achv.Arc_AxCRM_WC_SEGMENT_DS(
		DATASOURCE_NUM_ID, 
		INTEGRATION_ID, 
		NUM_CONTACTS, 
		BU_ID, 
		CUST_HIER,
		CUST_HIER_LVL, 
		DESC_TEXT, 
		PAR_CALL_LST_ID, 
		PR_POSTN_ID, 
		SGMT_NAME,
		SGMT_SRC_ID, 
		SUBTYPE_CD, 
		SEGMENT_TYPE, 
		X_CUSTOM, 
		XN_ETL_DATE, 
		JobRunId
		)
SELECT 	DATASOURCE_NUM_ID, 
		INTEGRATION_ID, 
		NUM_CONTACTS, 
		BU_ID,
		CUST_HIER, 
		CUST_HIER_LVL, 
		DESC_TEXT, 
		PAR_CALL_LST_ID, 
		PR_POSTN_ID,
		SGMT_NAME, 
		SGMT_SRC_ID, 
		SUBTYPE_CD, 
		SEGMENT_TYPE, 
		X_CUSTOM, 
		XN_ETL_DATE,
		$JobRunID AS JobRunID
FROM	
${DB_ENV}AxCRMBI.WC_SEGMENT_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

DELETE FROM ${DB_ENV}AxCRMBI.WC_SEGMENT_DS
;

.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR
COMMIT WORK;
.IF ERRORCODE <> 0 THEN .GOTO RETURNERROR

------------------------------------------------------------------------------------------
-- 26) JobRun Finish
------------------------------------------------------------------------------------------

UPDATE ${DB_ENV}MetaDataVIN.JobRun
SET JobEnd = CURRENT_TIMESTAMP
WHERE
JobRunID = $JobRunID
;


.IF ERRORCODE <> 0 THEN .QUIT ERRORCODE
COMMIT WORK;
.IF ERRORCODE <> 0 THEN  .QUIT ERRORCODE

.QUIT 0


------------------------------------------------------------------------------------------
-- 27) Error Handling
------------------------------------------------------------------------------------------

.LABEL RETURNERROR

.QUIT ERRORCODE" >> $BTEQ_ARCHIVE_SOURCE_SCRIPT

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT START ----------------" >> $LOG_FILE
echo >> $LOG_FILE

cat $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- FILE: BTEQ SCRIPT END ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS START AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

bteq < $BTEQ_ARCHIVE_SOURCE_SCRIPT >> $LOG_FILE 2>&1

echo >> $LOG_FILE
echo "---------------- EXECUTION: BTEQ PROCESS END AT `date` ----------------" >> $LOG_FILE
echo >> $LOG_FILE

echo "start time $start_time" >> $LOG_FILE
echo "end time `date`" >> $LOG_FILE


BTEQ_RETURN_CODE=""
BTEQ_RETURN_CODE=`grep 'RC (return code)' $LOG_FILE `

test "$BTEQ_RETURN_CODE" = "" && {
        echo "Error: Unable to execute $BTEQ_ARCHIVE_SOURCE_SCRIPT"
        echo "Error: Could not find BTEQ_RETURN_CODE from $LOG_FILE"
        echo "Info: Try executing the command task again, check logon user and password"
        return -1
}

BTEQ_RETURN_CODE=`echo $BTEQ_RETURN_CODE | cut -d "=" -f2 | awk '{print $1}'`
echo "BTEQ_RETURN_CODE ="$BTEQ_RETURN_CODE

if [ "$BTEQ_RETURN_CODE" = "0" ]; then
        rm  -f $BTEQ_ARCHIVE_SOURCE_SCRIPT
        rm  -f $LOG_FILE
        rm -f $LOGON_FILE
        #rm -f $PMRootDir/log/`basename $0`_$JobRunID"_"$CURRENT_TS"_script".log
        return $BTEQ_RETURN_CODE
        

else
        return -1
fi