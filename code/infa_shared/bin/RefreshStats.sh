#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: RefreshStats.sh 11755 2013-12-11 08:56:05Z k9108547 $
# Last Changed By  : $Author: k9108547 $
# Last Change Date : $Date: 2013-12-11 09:56:05 +0100 (ons, 11 dec 2013) $
# Last Revision    : $Revision: 11755 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/RefreshStats.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export PARAM_DIR="$PMRootDir/par"
export PARAM_FILE="axedwparameters.prm"

echo "PMRootDir=$1"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
	current_day_num="`date +%d`"
	oddeven_current_day_num="`expr $current_day_num % 2`"
	for FILE in $PMRootDir/bin/CollectStatistics_[0-9]*.sh
	do
			filenum="`echo $FILE | sed -e 's,.*_,,' -e 's,\..*,,'`" # extract filenumber from filename
			oddeven_filenum="`expr $filenum % 2`" # 1 = filenum is odd, 0 = filenum is even
			if [ "$oddeven_filenum" = "$oddeven_current_day_num" -a "$filenum" -gt "5" ]
			then
					echo "$FILE: filenum:$filenum oddeven_filenum:$oddeven_filenum oddeven_current_day_num:$oddeven_current_day_num"
					$FILE $PMRootDir &
			elif [ "$filenum" -le "5" ]
			then
					$FILE $PMRootDir &
			fi
	done

wait

)  2>&1 | tee $logfile



