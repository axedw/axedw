#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: UpdateDynPartDaily.sh 33136 2020-07-27 12:58:47Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-07-27 14:58:47 +0200 (mån, 27 jul 2020) $
# Last Revision    : $Revision: 33136 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/UpdateDynPartDaily.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
PMRootDir=$1
export PMRootDir
PARAM_DIR="$PMRootDir/par"
export PARAM_DIR
PARAM_FILE="axedwparameters.prm"
export PARAM_FILE

echo "PMRootDir=$1"

if [ "$DB_ENV" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=$DB_ENV"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
( 
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=UPDATE PARTITIONS;' FOR SESSION;

--***********************************************************************
--Cleaning up LOYALTY_INDIVIDUAL_OFFER table (create next months partitions)
--***********************************************************************
.os date 

.SET MAXERROR 0;

SELECT * FROM DBC.TABLESV WHERE DATABASENAME = '${DB_ENV}TgtT' AND TABLENAME = 'LOYALTY_INDIVIDUAL_OFFER_CLEANUP';

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.IF Activitycount > 0 THEN .GOTO TABEXIST

CT ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER_CLEANUP AS ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER WITH NO DATA AND STATS;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.label TABEXIST

DELETE FROM ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER_CLEANUP ALL;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER_CLEANUP
sel *  from ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER
WHERE (MODEL_ID = 4501 and Model_Apply_Dt = (SELECT MAX(Model_Apply_Dt) FROM ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER WHERE MODEL_ID = 4501))
OR  (MODEL_ID = 4511 and Model_Apply_Dt = (SELECT MAX(Model_Apply_Dt) FROM ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER WHERE MODEL_ID = 4511)) 
OR  (MODEL_ID = 4523 and Model_Apply_Dt = (SELECT MAX(Model_Apply_Dt) FROM ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER WHERE MODEL_ID = 4523))
OR  (MODEL_ID = 4524 and Model_Apply_Dt = (SELECT MAX(Model_Apply_Dt) FROM ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER WHERE MODEL_ID = 4524))
OR  (MODEL_ID = 4525 and Model_Apply_Dt = (SELECT MAX(Model_Apply_Dt) FROM ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER WHERE MODEL_ID = 4525))
OR  (MODEL_ID = 4526 and Model_Apply_Dt =(SELECT MAX(Model_Apply_Dt) FROM ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER WHERE MODEL_ID = 4526))
OR  (MODEL_ID  IN (4521 ,4522 ,4536 ,4537 ,4599 ,4529 ,4530 ,4531 ,4538 ,4539) and Model_Apply_Dt between current_date - 550  and current_date +30)
OR  (Model_Id in (4003 ,4004 , 4005 , 4006 , 4007 ) and Model_Apply_Dt between current_date - 90  and current_date +30 )
OR  ((Model_Id BETWEEN 4200 AND 4299) and (Model_Id, Model_Apply_Dt) = (SELECT Model_Id, MAX(Model_Apply_Dt) FROM ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER WHERE MODEL_ID between 4200 AND 4299 GROUP BY 1))
OR  ((Model_Id BETWEEN 4300 AND 4399) and Model_Apply_Dt between current_date - 550  and current_date +30)
OR  ((Model_Id BETWEEN 4400 AND 4499) and Model_Apply_Dt between current_date - 90  and current_date +30);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

DELETE FROM ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER ALL;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

ALTER TABLE ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER to current with delete;

.IF ERRORCODE = 9247 THEN .GOTO INSERTFROMCLEANUP

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.label INSERTFROMCLEANUP

INSERT INTO ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER
sel *  from ${DB_ENV}TgtT.LOYALTY_INDIVIDUAL_OFFER_CLEANUP;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.os date
SET QUERY_BAND=NONE FOR SESSION;
.EXIT 0
end

)  2>&1 | tee $logfile

BTEQ_RETURN_CODE=`grep 'RC (return code)' $logfile`
BTEQ_RETURN_CODE=`echo $BTEQ_RETURN_CODE | cut -d "=" -f2 | awk '{print $1}'`

if [ "$BTEQ_RETURN_CODE" = "0" ]
then
        exit $BTEQ_RETURN_CODE
else
        exit 1
fi