#!/bin/sh
######################################################################################
#                                                                                    #
# acncreatefilelist_SFF - Creates file with list of SFF files in Nielsen ftp         #
#                                                                                    #
# 2018-02-22 Erik Kaar                                                               #
######################################################################################
HOST='euse.nielsen.com'
USER='AX'
PASSWD='4Xsten'
PMRootDir=$1/SrcFiles/Nielsen

ftp -n $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
prompt off
passive
nlist SFF $PMRootDir/ACNFileList_SFF.txt
quit
END_SCRIPT
exit 0


