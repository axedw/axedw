#
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: RemoveTriggerExclusiveAccessToS1.sh 10403 2013-09-23 14:23:38Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2013-09-23 16:23:38 +0200 (mån, 23 sep 2013) $
# Last Revision    : $Revision: 10403 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/RemoveTriggerExclusiveAccessToS1.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

export PMRootDir=$1
export TriggerFile=$2

echo "PMRootDir=$1"
TRIG_FILE=$PMRootDir/trg/$TriggerFile
echo $TRIG_FILE
sleep $((${RANDOM}%4))

status="running"
while [ "$status" != "done" ]
do
	while [ ! -f $TRIG_FILE ]
	do
		sleep 1
	done
	# if we can remove the file all is ok and we can exit
	# else another process most likely has removed it so we have to wait again
	rm $TRIG_FILE && status="done"
done
exit 0
