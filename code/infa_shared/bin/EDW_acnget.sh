#!/bin/sh
##############################################################################
#                                                                            #
# acnget - Get specified FFS/HEA/CHA zip file from  Nielsen                  #
#                                                                            #
# 2016-02-03 Erik Kaar                                                       #
##############################################################################
HOST='ftp.acnielsen.se'
USER='AX'
PASSWD='4Xsten'
PMRootDir=$1/SrcFiles/Nielsen
FILE=$2.*

ftp -n $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
prompt off
lcd $PMRootDir
cd FFS
bin
passive
mget $FILE
quit
END_SCRIPT
exit 0

