#!/usr/bin/ksh
#  
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Backup_start.sh 30649 2020-03-06 09:15:54Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2020-03-06 10:15:54 +0100 (fre, 06 mar 2020) $
# Last Revision    : $Revision: 30649 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/Backup_start.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#
#
############################################################################
# -----------
# Description:
# -----------
# Usage: Backup_start is launched to start a backup
#        It calls a second script (called Backup.sh)
#        Otherwise the release_lock-backup-script
#        wouldn't be launched when backup was failing
#
############################################################################

. $HOME/.infascm_profile
taraserver=dwprba02.axfood.se

# The following flag will make all backups but Achv and DBC to take Arcmain-file-backups to disk. All big tables are excluded in this backup and this functionality is built solely for IT-refresh
backup_to_file=false

edw_srv=$(hostname -s)
edw_env=''

        if [[ ${edw_srv%??} = dwpret ]]
        then
                        edw_env=pr

        elif [[ ${edw_srv%??} = dwitet ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi

bar_bup="/opt/axedw/${edw_env}/is_axedw1/infa_shared/bin"

display_help()
{
        print "\n\tUSAGE:\n\t\t$(basename ${0}) -s <bar script> -d <database name>\n\n"
        exit 355
}

chk_args()
{

set -x

        if [[ -z ${bar_script} ]]
        then
                # Run function <display_help>

                display_help
        fi

        if [[ -z ${td_db} ]]
        then
                # Run function <display_help>

                display_help
        fi
}

	while getopts :s:d: args
	do
		case ${args} in
			s) bar_script=${OPTARG};;
			d) td_db=${OPTARG};;
            ?) display_help;;
		esac
	done

	# If test then run cloud-backup-script
	if [ "${edw_env}" = "it" ]
	then
		#
		# disabled backup on IT since it is running from IntelliCloud
		#
		sleep 10
		exit 0

		ssh ${taraserver} "/opt/backups/bin/start_backup_in_cloud.sh ${td_db}"
	elif [ "${backup_to_file}" = "true" ]
	then
		if [ "${td_db}" = " PRAchv" ]
		then
			# Ordinary BAR-backup - Achv is not needed in IT-refresh
			${bar_bup}/Backup.sh -s "${bar_script}" -d ${td_db}
			if [ "$?" = "1" ] ; then
				exit 1
			fi

		elif [ "${td_db}" = " DBC" ]
		then
			# Ordinary BAR-backup - DBC is not needed in IT-refresh
			${bar_bup}/Backup.sh -s "${bar_script}" -d ${td_db}
			if [ "$?" = "1" ] ; then
				exit 1
			fi

		else
			# Backup to file - these backups will exclude all the big tables
			ssh ${taraserver} "/opt/backups/bin/start_backup_on_dwtsba03.sh ${td_db}"
		fi

	else
		# Do ordinary backup
		${bar_bup}/Backup.sh -s "${bar_script}" -d ${td_db}
	fi


