#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: UpdateDynPart.sh 23921 2018-01-09 16:38:38Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2018-01-09 17:38:38 +0100 (tis, 09 jan 2018) $
# Last Revision    : $Revision: 23921 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/UpdateDynPart.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------

PMRootDir=$1
export PMRootDir
PARAM_DIR="$PMRootDir/par"
export PARAM_DIR
PARAM_FILE="axedwparameters.prm"
export PARAM_FILE

echo "PMRootDir=$1"

if [ "${DB_ENV}" = "" ]
then
        DB_ENV="`awk -F= '/^\\$\\$DB_ENV/ {print $2}' <$PARAM_DIR/$PARAM_FILE`"
fi
echo "DB_ENV=${DB_ENV}"

logfile="$PMRootDir/log/`basename $0`.`date +%Y%m%d_%H%M%S`.log"
(
bteq <<-end
.run file=$PARAM_DIR/dbadmin_logon.btq
SET QUERY_BAND='ApplicationName=BTEQ;Source=`basename $0`;Action=UPDATE PARTITIONS;' FOR SESSION;

--***********************************************************************
--Update tables with dynamic partitioning (create next months partitions)
--***********************************************************************
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','AUTO_REPLENISHMENT_CONTROL',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','INV_REQ_STATUS_LINE_HIST',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','INVENTORY_REQUEST_LINE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','KNOWN_STOCK_LOSS',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','NON_SALES_TENDER_TRAN_LINE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','POS_REGISTER_EVENT',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRAN_CIRCUMSTANCE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRAN_DISCOUNT_LINE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRAN_LINE_CIRCUMSTANCE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRAN_LOY_CONTACT',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRAN_LOY_ID_METHOD',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRAN_LOYALTY_ACCOUNT',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRANSACTION',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRANSACTION_AGREEMENT',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRANSACTION_CHARGE_LINE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRANSACTION_LINE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRANSACTION_LINE_ACTUAL',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRANSACTION_PARTY',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRANSACTION_TOTALS',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','STOCK_BALANCE_REQ_STATUS_B',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','STOCK_BALANCE_REQUEST',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','TENDER_TRANSACTION_LINE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}SemEXPT','LOY_LOG_CRM_EXP',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}SemEXPT','LOYALTY_SALES_EXP_HIST',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}SemCMNT','SALES_TRANSACTION_LINE_X',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}SemCMNT','SALES_TRANSACTION_LINE_X_1',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}SemCMNT','SALES_TRANSACTION_LINE_X_2',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','ASSOCIATE_ACTUAL_LEAVE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','ASSOCIATE_LABOR_ACTUAL',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','ASSOCIATE_LABOR_SCHEDULE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','ASSOCIATE_LABOR_TASK_ACTUAL',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','ASSOCIATE_LABOR_TASK_SCHEDULE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','ASSOCIATE_SCHEDULED_LEAVE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_ORDER',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_ORDER_LINE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_ORDER_CIRCUMSTANCE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_ORDER_DELIVERY_PIECES',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','INTERNAL_PICKING_ROUTE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','INTERNAL_PICKING_ROUTE_LINES',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','COMM_AD_CONTACT',1,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','COMM_AD_CONTACT_RESPONSE',1,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','DISCOUNT_LABEL',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','DISTRIBUTION_EVENT',1,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','DISTRIBUTION_EVENT_PIECE',1,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','EVENT',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','INVENTORY_TRANSACTION',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','INV_REQ_STATUS_LINE',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','JOURNAL_ENTRY_HEADER',1,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','JOURNAL_ENTRY_LINE',1,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','PIECE_ROUTING',1,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}TgtT','SALES_TRANSACTION_NAT_ID',0,1);
.os date 
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_SALES_TRAN_DISCOUNT_LINE',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_PIECE_ROUTING',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_DIGITAL_RECEIPT',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_ASSOCIATE_LABOR_TASK_ACTUAL',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_DISTRIBUTION_EVENT_PIECE',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_SALES_TRANSACTION_NAT_ID',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_INV_REQ_STATUS_LINE',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_JOURNAL_ENTRY_HEADER',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_COMM_AD_CONTACT',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_JOURNAL_ENTRY_LINE',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_INV_REQ_STATUS_LINE_HIST',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_DISTRIBUTION_EVENT',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_EVENT',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_COMM_AD_CONTACT_RESPONSE',0,1);
.os date
CALL ${DB_ENV}dbadmin.DBA_UpdPartitions('${DB_ENV}CntlLoadReadyT','LR_DISCOUNT_LABEL',0,1);
.os date


SET QUERY_BAND=NONE FOR SESSION;
.EXIT
end

)  2>&1 | tee $logfile

BTEQ_RETURN_CODE=`grep 'RC (return code)' $logfile`
BTEQ_RETURN_CODE=`echo $BTEQ_RETURN_CODE | cut -d "=" -f2 | awk '{print $1}'`

if [ "$BTEQ_RETURN_CODE" = "0" ]
then
        exit $BTEQ_RETURN_CODE
else
        exit 1
fi