#/bin/ksh
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Purge_SAPMAP.sh 22897 2017-08-28 10:14:02Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2017-08-28 12:14:02 +0200 (mån, 28 aug 2017) $
# Last Revision    : $Revision: 22897 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/infa_shared/bin/Purge_SAPMAP.sh $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
#!/usr/bin/ksh

edw_srv=$(hostname -s)
edw_env=''

        if [[ ${edw_srv%??} = dwpret ]]
        then
                        edw_env=pr

        elif [[ ${edw_srv%??} = dwitet ]]
        then
                        edw_env=it
        fi

        if [[ -z ${edw_env} ]]
        then
                print "\n\tThis is not a valid system to execute the program ${0##*/} on!!!!!\n"
                exit
        fi

export DATE=`date +"%Y%m%d%H%M%S"`

mv /opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/Export_SAPMAP/EDW_SAPMAP.txt /opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/Export_SAPMAP/EDW_SAPMAP_${DATE}

find /opt/axedw/${edw_env}/is_axedw1/infa_shared/TgtFiles/Export_SAPMAP/EDW_SAPMAP_2* -mtime +14 -exec rm {} \;
