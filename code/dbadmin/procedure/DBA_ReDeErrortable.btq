/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: DBA_ReDeErrortable.btq 5671 2012-09-13 18:24:09Z k9106728 $
# Last Changed By  : $Author: k9106728 $
# Last Change Date : $Date: 2012-09-13 20:24:09 +0200 (tor, 13 sep 2012) $
# Last Revision    : $Revision: 5671 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/procedure/DBA_ReDeErrortable.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose     : Restores/Removes Error-table on specified table(s)
# Project     : EDW1
# Subproject  :
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2011-11-28 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#   Restores/Removes Error-table on specified table(s)
#   If "p_TrgTab" is NULL then all tables in spec database are targeted
#   If "p_TrgTab" parameter ends with a "*" a simple pattern-match is performed
#
# Dependencies
#   ${DB_ENV}dbadmin.DBA_ReDeDefData (Table)
#   ${DB_ENV}dbadmin.DBA_DebugLogData (Table)
#	SYSLIB.REPLACES UDF
#
# Parameters
#	Required Parameters
#	 p_TrgDb	- Target database containing table(s) to de/re-Errortable
#	 p_Operation	- Operation type (DE,RE,DR or SV)
#	 		DE = De-Errortable(save Errortable-def & drop)
#	 		RE = Re-Errortable(re-create from saved Errortable-def)
#	 		DR = Drop Errortable (no save)
#	 		SV = Save Errortable definition (no drop)
#	Optional parameters:
#	 p_TrgTab	- Table to de/re-Errortable.
#	 p_Options	- Options
#	 		B = Create backup of Errortable if count(*)>0
#	 p_Debug	- If "1" then insert generated sql into the metadata Debug-Log
#		If "99" no exec, only insert generated sql into DBA_DebugLogData table
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}dbadmin.DBA_ReDeErrortable
(	p_TrgDb	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_TrgTab	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_Operation	CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_Options	VARCHAR(3) CHARACTER SET LATIN NOT CASESPECIFIC
	,p_Debug	BYTEINT
)
P0: BEGIN
/*****************************************************************************
Required Parameters:
 p_TrgDb	- Target database containing table(s) to de/re-Errortable
 p_Operation	- Operation type (DE,RE,DR or SV)
 		DE = De-Errortable(save Errortable-def & drop)
 		RE = Re-Errortable(re-create from saved Errortable-def)
 		DR = Drop Errortable (no save)
 		SV = Save Errortable definition (no drop)
Optional parameters:
 p_TrgTab	- Table to de/re-Errortable.
 p_Options	- Options
		B = Create backup of Errortable if count(*)>0
 p_Debug	- If "1" then insert generated sql into the metadata Debug-Log
		If "99" no exec, only insert generated sql into DBA_DebugLogData table

Description:	Restores/Removes Error-table on specified table(s)
	If "p_TrgTab" is NULL then all tables in spec database are targeted
	If "p_TrgTab" parameter ends with a "*" a simple pattern-match is performed

Requires:	Table "DBA_ReDeDefData" for saved Errortable-definitions
*****************************************************************************/
DECLARE v_SQLstate	CHAR(5);
DECLARE v_PrcName	VARCHAR(65) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_PrcDttm	TIMESTAMP(3);
DECLARE v_EOL		CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_Blank	CHAR(1) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_TrgObj	VARCHAR(65) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_OthrObj	VARCHAR(65) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_TrgTab	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_Match	VARCHAR(30) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_DelSQL	VARCHAR(250) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_BackupSQL	VARCHAR(250) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_DropSQL	VARCHAR(250) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_InsSQL	VARCHAR(32000) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_CntSQL	VARCHAR(1000) CHARACTER SET LATIN NOT CASESPECIFIC;
DECLARE v_Cnt	INTEGER;
DECLARE c_Cnt	CURSOR FOR s_Count;


SET v_EOL=TRANSLATE('0D'xc USING UNICODE_TO_LATIN WITH ERROR);
SET v_Blank=TRANSLATE('' USING UNICODE_TO_LATIN WITH ERROR);
SET v_PrcName = '${DB_ENV}dbadmin.DBA_ReDeErrortable';
SET v_PrcDttm = CURRENT_TIMESTAMP(3);

-- Verify parameters
IF COALESCE(p_TrgDb,'')=''
OR POSITION('${DB_ENV}' IN COALESCE(p_TrgDb,''))<>1
OR COALESCE(p_Operation,'') NOT IN ('DE','RE','DR','SV')
THEN
	LEAVE P0;
END IF
;
IF POSITION('B' IN COALESCE(p_Options,''))=0
AND TRIM(COALESCE(p_Options,''))<>''
THEN
	LEAVE P0;
END IF
;

SET v_TrgTab = TRANSLATE(TRIM(COALESCE(p_TrgTab,v_Blank)) USING UNICODE_TO_LATIN WITH ERROR)
;
IF POSITION('*' IN v_TrgTab) > 0
THEN
	SET v_Match = TRIM(SUBSTRING(v_TrgTab FROM 1 FOR POSITION('*' IN v_TrgTab)-1))
	;
ELSE
	SET v_Match = NULL
	;
END IF;

IF p_Operation='RE'
THEN	-- Get saved definitions and Re-Errortable
	F_RET: FOR cur_RET AS cur_ReErrortableTab CURSOR FOR
		LOCKING ROW FOR ACCESS
		SELECT rd0.TrgDbName,rd0.TrgTabName
		FROM ${DB_ENV}dbadmin.DBA_ReDeDefData AS rd0
		WHERE	rd0.TrgDbName	= :p_TrgDb
		AND	(CASE
			WHEN :p_TrgTab IS NULL
			THEN (CASE WHEN POSITION('#' IN rd0.TrgTabName)=1 THEN 0 ELSE 1 END)
			WHEN COALESCE(:v_Match,'***') = ''
			THEN 1
			WHEN :v_Match IS NOT NULL
			THEN (CASE WHEN POSITION(:v_Match IN rd0.TrgTabName)=1 THEN 1
				ELSE 0 END)
			WHEN TRIM(rd0.TrgTabName) = TRIM(:p_TrgTab)
			THEN 1
			ELSE 0
			END) = 1
		AND	rd0.DefClass = 'ET' --Errortable def
		GROUP BY 1,2
		ORDER BY 1,2
	DO
		F_RE: FOR cur_RE AS cur_ReErrortable CURSOR FOR
			LOCKING ROW FOR ACCESS
			SELECT	rd0.TrgDbName,rd0.TrgTabName,rd0.DefName,rd0.OthrDbName,rd0.OthrTabName
				,(	'CREATE ERROR TABLE '||TRIM(rd0.OthrDbName)||'.'||TRIM(rd0.OthrTabName)||:v_EOL
					||' FOR '||TRIM(rd0.TrgDbName)||'.'||TRIM(rd0.TrgTabName)||';'||:v_EOL
					)	(VARCHAR(250))	AS x_CreSQL
				,(	'DELETE FROM ${DB_ENV}dbadmin.DBA_ReDeDefData'||:v_EOL
					||' WHERE TrgDbName='''||TRIM(rd0.TrgDbName)||''''||:v_EOL
					||' AND TrgTabName='''||TRIM(rd0.TrgTabName)||''''||:v_EOL
					||' AND DefClass = ''ET'''||:v_EOL
					||';'
					)	(VARCHAR(250))	AS x_DelSQL
			FROM ${DB_ENV}dbadmin.DBA_ReDeDefData AS rd0
			WHERE	rd0.TrgDbName	= :cur_RET.TrgDbName
			AND	rd0.TrgTabName	= :cur_RET.TrgTabName
			AND	rd0.DefClass = 'ET' --index def
			ORDER BY rd0.TrgDbName,rd0.TrgTabName
		DO
			SET v_TrgObj='"'||TRIM(cur_RE.TrgDbName)
				||'"."'||TRIM(cur_RE.TrgTabName)||'"'
			;
			SET v_OthrObj=CASE WHEN POSITION('.' IN COALESCE(cur_RE.DefName,''))>0
				THEN '"'||TRIM(cur_RE.OthrDbName)
				||'"."'||TRIM(cur_RE.OthrTabName)||'"'
				ELSE '"'||TRIM(cur_RE.DefName)||'"'
				END
			;
			IF cur_RE.x_CreSQL IS NOT NULL
			THEN	-- Create (Restore) Errortable
				IF ZEROIFNULL(p_Debug)<>0
				THEN	-- Insert debug-text
					INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
					(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
					) VALUES (
						:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
						,:v_TrgObj,:v_OthrObj
						,USER,SESSION
						,:cur_RE.x_CreSQL
					);
				END IF
				;
				IF ZEROIFNULL(p_Debug)<>99
				THEN
					B1: BEGIN
						-- ignore errors
						DECLARE CONTINUE HANDLER
						FOR SQLEXCEPTION, SQLWARNING
						SET v_sqlstate=SQLSTATE
						;
						CALL DBC.SysExecSQL(:cur_RE.x_CreSQL);
					END B1;
				END IF
				;
			END IF
			;
			IF cur_RE.x_DelSQL IS NOT NULL
			THEN	-- Remove saved definition
				IF ZEROIFNULL(p_Debug)<>0
				THEN	-- Insert debug-text
					INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
					(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
					) VALUES (
						:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
						,:v_TrgObj,:v_OthrObj
						,USER,SESSION
						,:cur_RE.x_DelSQL
					);
				END IF
				;
				IF ZEROIFNULL(p_Debug)<>99
				THEN
					CALL DBC.SysExecSQL(:cur_RE.x_DelSQL);
				END IF
				;
			END IF;
		END FOR F_RE;
	END FOR F_RET;
END IF;

IF p_Operation='DE'
THEN	-- Create definitions and De-Errortable

	F_DET: FOR cur_DET AS cur_DeErrortableTab CURSOR FOR
		LOCKING ROW FOR ACCESS
		SELECT	t0.DatabaseName	AS TrgDbName
			,t0.TableName	AS TrgTabName
			,et0.ErrTblDbName	AS OthrDbName
			,et0.ErrTblName	AS OthrTabName
			,TRIM(et0.ErrTblDbName)||'.'||TRIM(et0.ErrTblName)	AS DefName
			,MAX(CASE WHEN et0.BaseTblDbName IS NOT NULL THEN 1 ELSE 0 END) AS x_ErrortableExist
			,MAX(CASE WHEN rd0.TrgDbName IS NOT NULL THEN 1 ELSE 0 END) AS x_DelOld
		FROM DBC.Tables AS t0
		LEFT OUTER JOIN DBC.ErrorTblsV AS et0
		ON et0.BaseTblDbName = t0.DatabaseName
		AND et0.BaseTblName = t0.TableName
		LEFT OUTER JOIN ${DB_ENV}dbadmin.DBA_ReDeDefData AS rd0
		ON rd0.TrgDbName = t0.DatabaseName
		AND rd0.TrgTabName = t0.TableName
		AND rd0.DefClass = 'ET' --index def
		WHERE	t0.DatabaseName = :p_TrgDb
		AND	(CASE
			WHEN :p_TrgTab IS NULL
			THEN (CASE WHEN POSITION('#' IN t0.TableName)=1 THEN 0 ELSE 1 END)
			WHEN COALESCE(:v_Match,'***') = ''
			THEN 1
			WHEN :v_Match IS NOT NULL
			THEN (CASE WHEN POSITION(:v_Match IN t0.TableName)=1 THEN 1
				ELSE 0 END)
			WHEN TRIM(t0.TableName) = TRIM(:p_TrgTab)
			THEN 1
			ELSE 0
			END) = 1
		AND	(	et0.BaseTblDbName IS NOT NULL
			OR	rd0.TrgDbName IS NOT NULL
			)
		GROUP BY 1,2,3,4,5
		ORDER BY 1,2,3,4,5
	DO
		SET v_TrgObj=TRANSLATE(('"'||TRIM(cur_DET.TrgDbName)
			||'"."'||TRIM(cur_DET.TrgTabName)||'"'
			) USING UNICODE_TO_LATIN WITH ERROR )
		;
		IF cur_DET.x_DelOld = 1
		THEN --remove all old saved Errortable definitions for table
			SET v_DelSQL=TRANSLATE((
				'DELETE FROM ${DB_ENV}dbadmin.DBA_ReDeDefData'
				||' WHERE TrgDbName='''||TRIM(cur_DET.TrgDbName)||''''||v_EOL
				||' AND TrgTabName='''||TRIM(cur_DET.TrgTabName)||''''||v_EOL
				||' AND DefClass = ''ET'''||v_EOL
				||';'
				) USING UNICODE_TO_LATIN WITH ERROR)
			;
			IF ZEROIFNULL(p_Debug)<>0
			THEN	-- Insert debug-text
				INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
				(	ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
				) VALUES (
					:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
					,:v_TrgObj,:v_TrgObj
					,USER,SESSION
					,:v_DelSQL
				);
			END IF
			;
			IF ZEROIFNULL(p_Debug)<>99
			THEN
				CALL DBC.SysExecSQL(:v_DelSQL);
			END IF;
		END IF
		;
		IF cur_DET.x_ErrortableExist = 1
		THEN
			IF COALESCE(p_Options,'')='B'
			THEN -- Create backup statement

				SET v_CntSQL = 'SELECT COUNT(*) FROM '
					||TRIM(cur_DET.OthrDbName)||'.'||TRIM(cur_DET.OthrTabName)
				;
				PREPARE s_Count FROM v_CntSQL
				;
				OPEN c_Cnt
				;
				FETCH c_Cnt INTO v_Cnt
				;
				CLOSE c_Cnt
				;
				IF NULLIFZERO(v_Cnt)>0
				THEN
					LOCKING ROW FOR ACCESS
					SELECT (	'CREATE TABLE '||TRIM(:cur_DET.OthrDbName)
							||'.#'||TRIM(SUBSTRING(:cur_DET.OthrTabName FROM 1 FOR 26))
							||'#'||CAST(t1.x_TmpVer AS FORMAT'99')
							||v_EOL
							||' AS '||TRIM(:cur_DET.OthrDbName)
							||'.'||TRIM(:cur_DET.OthrTabName)
							||v_EOL
							||'WITH DATA AND NO STATISTICS;'
							)
					INTO	:v_BackupSQL
					FROM (
						SELECT ZEROIFNULL(CAST(MAX(SUBSTRING(t0.TableName FROM CHARACTER_LENGTH(TRIM(t0.TableName))-1
								FOR 2)) AS BYTEINT))+1 AS x_TmpVer
						FROM DBC.Tables AS t0
						WHERE	t0.TableKind IN ('T','O')
						AND	t0.DatabaseName = :cur_DET.OthrDbName
						AND	POSITION('#'||TRIM(SUBSTRING(:cur_DET.OthrTabName FROM 1 FOR 26))||'#' IN t0.TableName) = 1
					) AS t1
					;
				END IF;
			END IF
			;
			SET v_DropSQL=TRANSLATE(('DROP ERROR TABLE FOR '
				||TRIM(cur_DET.TrgDbName)||'.'||TRIM(cur_DET.TrgTabName)
				||';'
				) USING UNICODE_TO_LATIN WITH ERROR)
			;
			SET v_InsSQL=(
				TRANSLATE(TRIM(
					'INSERT INTO ${DB_ENV}dbadmin.DBA_ReDeDefData'||v_EOL
					||'(TrgDbName,TrgTabName,OthrDbName,OthrTabName,DefClass,DefName,SaveDttm,SessionId)'||v_EOL
					||'VALUES ('''||TRIM(cur_DET.TrgDbName)||''','''||TRIM(cur_DET.TrgTabName)||''''||v_EOL
					||','''||TRIM(cur_DET.OthrDbName)||''','''||TRIM(cur_DET.OthrTabName)||''''||v_EOL
					||',''ET'','''||TRIM(cur_DET.DefName)||''',CURRENT_TIMESTAMP(3),SESSION);'
					) USING UNICODE_TO_LATIN WITH ERROR)
				)
			;
			IF p_Operation IN ('DE','SV')
			THEN	-- Save definition
				IF ZEROIFNULL(p_Debug)<>0
				THEN	-- Insert debug-text
					INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
					(ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
					) VALUES (
						:v_PrcName, :v_PrcDttm, CURRENT_TIMESTAMP(3)
						,:v_TrgObj,:cur_DET.DefName
						,USER,SESSION
						,:v_InsSQL
					);
				END IF
				;
				IF ZEROIFNULL(p_Debug)<>99
				THEN
					CALL DBC.SysExecSQL(:v_InsSQL);
				END IF;
			END IF
			;
			IF p_Operation IN ('DE','DR')
			AND COALESCE(p_Options,'')='B'
			AND NULLIFZERO(v_Cnt)>0
			AND v_BackupSQL IS NOT NULL
			THEN	-- Backup Errortable
				IF ZEROIFNULL(p_Debug)<>0
				THEN	-- Insert debug-text
					INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
					(ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
					) VALUES (
						:v_PrcName,:v_PrcDttm, CURRENT_TIMESTAMP(3)
						,:v_TrgObj,:cur_DET.DefName
						,USER,SESSION
						,:v_BackupSQL
					);
				END IF
				;
				IF ZEROIFNULL(p_Debug)<>99
				THEN
					B1: BEGIN
						-- ignore errors
						DECLARE CONTINUE HANDLER
						FOR SQLEXCEPTION, SQLWARNING
						SET v_sqlstate=SQLSTATE
						;
						CALL DBC.SysExecSQL(:v_BackupSQL)
						;
					END B1;
				END IF;
			END IF
			;
			IF p_Operation IN ('DE','DR')
			AND v_DropSQL IS NOT NULL
			THEN	-- Remove Errortable
				IF ZEROIFNULL(p_Debug)<>0
				THEN	-- Insert debug-text
					INSERT INTO ${DB_ENV}dbadmin.DBA_DebugLogData
					(ProcedureName,ProcedureDttm,StatementDttm,TargetName,OtherName,UserName,SessionId,StatementTxt
					) VALUES (
						:v_PrcName,:v_PrcDttm, CURRENT_TIMESTAMP(3)
						,:v_TrgObj,:cur_DET.DefName
						,USER,SESSION
						,:v_DropSQL
					);
				END IF
				;
				IF ZEROIFNULL(p_Debug)<>99
				THEN
					B1: BEGIN
						-- ignore errors
						DECLARE CONTINUE HANDLER
						FOR SQLEXCEPTION, SQLWARNING
						SET v_sqlstate=SQLSTATE
						;
						CALL DBC.SysExecSQL(:v_DropSQL)
						;
					END B1;
				END IF;
			END IF;
		END IF;
	END FOR F_DET;
END IF;
END P0
;

