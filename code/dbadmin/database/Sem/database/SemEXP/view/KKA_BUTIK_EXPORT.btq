/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: KKA_BUTIK_EXPORT.btq 4137 2012-06-27 11:51:58Z k9106728 $
# Last Changed By  : $Author: k9106728 $
# Last Change Date : $Date: 2012-06-27 13:51:58 +0200 (ons, 27 jun 2012) $
# Last Revision    : $Revision: 4137 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/KKA_BUTIK_EXPORT.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}SemEXPVOUT
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemEXPVOUT.KKA_BUTIK_EXPORT
(	BUTIK_ID
	,BUTIK_NAMN
	,KSTALLE_KOD
	,BUTIKPRISGRUPP_ID
	,BUTIKPRISGRUPP_BENAM
	,KEDJA_SORTGRP_ID
	,KEDJA_SORTGRP_BENAM
	,KEDJA_REGION_ID
	,KEDJA_REGION_BENAM
	,KEDJA_ID,KEDJA_BENAM
	,EDW_CREATE_DATE
) AS
SELECT	sa1.Store_Alias_Id	(INTEGER)	AS BUTIK_ID
	,TRIM(SUBSTRING(ln1.Location_Name
		FROM 1 FOR 50))	(VARCHAR(50))	AS BUTIK_NAMN
	,CASE WHEN o1.N_Org_Party_Id IN ('S004','S005')
		THEN s1.N_Store_ID
		ELSE NULL
		END	(INTEGER)	AS KSTALLE_KOD
	,-1	(INTEGER)	AS BUTIKPRISGRUPP_ID
	,'ODEFINIERAD'	(VARCHAR(30))	AS BUTIKPRISGRUPP_BENAM
	,-1	(INTEGER)	AS KEDJA_SORTGRP_ID
	,'ODEFINIERAD'	(VARCHAR(32))	AS KEDJA_SORTGRP_BENAM
	,TRIM(lo1.Retail_Region_Cd)	(VARCHAR(6))	AS KEDJA_REGION_ID
	,TRIM(SUBSTRING(rr1.Retail_Region_Name
		FROM 1 FOR 32))	(VARCHAR(32))	AS KEDJA_REGION_BENAM
	,CASE COALESCE(sd1.Concept_Cd,'-1')
		WHEN 'EUC' THEN 6
		WHEN 'HEA' THEN 4
		WHEN 'HEM' THEN 4
		WHEN 'NEX' THEN 18
		WHEN 'PRX' THEN 13
		WHEN 'SNG' THEN 12
		WHEN 'TEM' THEN 2
		WHEN 'WH2' THEN 99
		WHEN 'WHE' THEN 8
		WHEN 'WIL' THEN 6
		ELSE -1
		END	(SMALLINT)	AS KEDJA_ID
	,TRIM(SUBSTRING(COALESCE(c1.Concept_Name,'ODEFINIERAD')
		FROM 1 FOR 32))	(VARCHAR(32))	AS KEDJA_BENAM
	,CASE	WHEN sa1.Valid_From_Dttm >= lo1.Valid_From_Dttm
		AND sa1.Valid_From_Dttm >= COALESCE(ln1.Valid_From_Dttm, LowTSVal())
		AND sa1.Valid_From_Dttm >= COALESCE(sd1.Valid_From_Dttm, LowTSVal())
		THEN sa1.Valid_From_Dttm
		WHEN lo1.Valid_From_Dttm >= sa1.Valid_From_Dttm
		AND lo1.Valid_From_Dttm >= COALESCE(ln1.Valid_From_Dttm, LowTSVal())
		AND lo1.Valid_From_Dttm >= COALESCE(sd1.Valid_From_Dttm, LowTSVal())
		THEN lo1.Valid_From_Dttm
		WHEN COALESCE(ln1.Valid_From_Dttm, LowTSVal()) >= lo1.Valid_From_Dttm
		AND COALESCE(ln1.Valid_From_Dttm, LowTSVal()) >= sa1.Valid_From_Dttm
		AND COALESCE(ln1.Valid_From_Dttm, LowTSVal()) >= COALESCE(sd1.Valid_From_Dttm, LowTSVal())
		THEN COALESCE(ln1.Valid_From_Dttm, LowTSVal())
		ELSE COALESCE(sd1.Valid_From_Dttm, LowTSVal())
		END	AS EDW_CREATE_DATE
FROM ${DB_ENV}TgtVOUT.STORE AS s1
INNER JOIN ${DB_ENV}TgtVOUT.STORE_ALIAS_B AS sa1
ON sa1.Store_Seq_Num = s1.Store_Seq_Num
AND sa1.Valid_To_Dttm = HighTSVal()
INNER JOIN ${DB_ENV}TgtVOUT.LOCATION_ORGANIZATION AS lo1
ON lo1.Location_Seq_Num = s1.Store_Seq_Num
AND lo1.Valid_To_Dttm = HighTSVal()
INNER JOIN ${DB_ENV}TgtVOUT.RETAIL_REGION AS rr1
ON rr1.Retail_Region_Cd = lo1.Retail_Region_Cd
INNER JOIN ${DB_ENV}TgtVOUT.ORGANIZATION AS o1
ON o1.Org_Party_Seq_Num = lo1.Org_Party_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOCATION_NAME_B AS ln1
ON ln1.Location_Seq_Num = s1.Store_Seq_Num
AND ln1.Valid_To_Dttm = HighTSVal()
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.STORE_DETAILS_B AS sd1
ON sd1.Store_Seq_Num = s1.Store_Seq_Num
AND sd1.Valid_To_Dttm = HighTSVal()
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.CONCEPT AS c1
ON c1.Concept_Cd = sd1.Concept_Cd
WHERE sa1.Store_Alias_Type_Cd = 'TRA'
AND lo1.Location_Org_Role_Cd = 'HOS'
AND o1.Org_Type_Cd = 'INT'
AND o1.Internal_Org_Type_Cd = 'SORG'
AND o1.N_Org_Party_Id IN ('S004','S005','S901','S903')
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


