/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRAN_LINE_LOCAL_DL_F.btq 29892 2020-01-16 13:31:18Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-01-16 14:31:18 +0100 (tor, 16 jan 2020) $
# Last Revision    : $Revision: 29892 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/SALES_TRAN_LINE_LOCAL_DL_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemEXPVIN	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemEXPVIN.SALES_TRAN_LINE_LOCAL_DL_F
(
Sales_Tran_Seq_Num,
Sales_Tran_Line_Num,
Store_Seq_Num,
Store_Department_Seq_Num,
POS_Register_Seq_Num,
Sales_Associate_Id,
Reciept_Type,
Sold_To_Party_Id,
Loyalty_Account_Id,
Tran_Dt,
Tran_End_Dttm,
Scan_Code_Seq_Num,
Sales_Location_Cd,
Tran_Line_Type_Cd,
Sales_Tran_Id,
Scan_Cd,
UOM_Category_Cd,
Scan_Cd_Desc,
Lvl_4_Seq_Num,
Lvl_4_Alias_Id,
Lvl_4_Desc,
Item_Qty,
Unit_Selling_Price_Amt,
Unit_Cost_Amt,
Unit_Org_Cost_Amt,
Tax_Group_Cd,
Tax_Amt,
Taxable_Amt,
Return_Reason_Cd,
Additional_Tax_Amt,
OpenJobRunId,
Insert_Dttm
)
As
Select 
st.Sales_Tran_Seq_Num,
stl.Sales_Tran_Line_Num,
st.Store_Seq_Num,
st.Store_Department_Seq_Num,
st.POS_Register_Seq_Num,
st.Sales_Associate_Id,
stc.Circumstance_Value_Cd As Reciept_Type,
Coalesce(stp.Sold_To_Party_Id,'-1') As Sold_To_Party_Id,
Coalesce(stla.Loyalty_Account_Id,'-1') As Loyalty_Account_Id,
st.Tran_Dt_DD As Tran_Dt,
st.Tran_End_Dttm_DD As Tran_End_Dttm,
stl.Scan_Code_Seq_Num,
stl.Sales_Location_Cd_DD As Sales_Location_Cd,
stl.Tran_Line_Type_Cd,
st.N_Sales_Tran_Id As Sales_Tran_Id,
sc.N_Scan_Cd As Scan_Cd,
Coalesce(scUOM.UOM_Category_Cd, 'N/A') As UOM_Category_Cd,
'ODEFENIERAT' as Scan_Cd_Desc,
-1 As Lvl_4_Seq_Num,
-1 As Lvl_4_Alias_Id,
'ODEFENIERAT' As Lvl_4_Desc,
Case When sta.Actual_Item_Qty Is Null Then stl.Item_Qty
Else (sta.Actual_Item_Qty * stl.Item_Qty)
End As Item_Qty,
stl.Unit_Selling_Price_Amt,
stl.Unit_Cost_Amt_DD As Unit_Cost_Amt,
stl.Unit_Org_Cost_Amt_DD As Unit_Org_Cost_Amt,
stl.Tax_Group_Cd,
Case When stl.Tax_Amt Is Null Then (stl.Unit_Selling_Price_Amt * 0.12)
Else (stl.Tax_Amt)
End As Tax_Amt,
Case When stl.Taxable_Amt Is Null Then stl.Unit_Selling_Price_Amt
Else (stl.Taxable_Amt)
End As Taxable_Amt,
stl.Return_Reason_Cd,
Coalesce(stcl.Charge_Amt,0) As Additional_Tax_Amt,
stl.OpenJobRunId,
st.InsertDttm As Insert_Dttm

From ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_VALIDATED As st
Inner Join ${DB_ENV}SemEXPT.E1_DL_SALES_STORE_DATE as f1
on  st.Store_Seq_Num = f1.Store_Seq_Num 
 And st.Tran_Dt_DD = f1.Tran_Dt
Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl 
 On st.Sales_Tran_Seq_Num = stl.Sales_Tran_Seq_Num
 And st.Store_Seq_Num = stl.Store_Seq_Num 
 And st.Tran_Dt_DD = stl.Tran_Dt_DD
 
Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta
 On stl.Sales_Tran_Seq_Num = sta.Sales_Tran_Seq_Num
 And stl.Sales_Tran_Line_Num = sta.Sales_Tran_Line_Num
 And stl.Store_Seq_Num = sta.Store_Seq_Num 
 And stl.Tran_Dt_DD = sta.Tran_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_PARTY As stp
 On st.Sales_Tran_Seq_Num = stp.Sales_Tran_Seq_Num
 And st.Store_Seq_Num = stp.Store_Seq_Num 
 And st.Tran_Dt_DD = stp.Tran_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOYALTY_ACCOUNT As stla
 On st.Sales_Tran_Seq_Num = stla.Sales_Tran_Seq_Num
 And st.Store_Seq_Num = stla.Store_Seq_Num 
 And st.Tran_Dt_DD = stla.Tran_Dt_DD
Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_CIRCUMSTANCE As stc
 On st.Sales_Tran_Seq_Num = stc.Sales_Tran_Seq_Num
 And st.Store_Seq_Num = stc.Store_Seq_Num 
 And st.Tran_Dt_DD = stc.Tran_Dt_DD
 And stc.Circumstance_Cd = 'COTP'
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_CHARGE_LINE As stcl
 On stl.Sales_Tran_Seq_Num = stcl.Sales_Tran_Seq_Num
 And stl.Store_Seq_Num = stcl.Store_Seq_Num 
 And stl.Tran_Dt_DD = stcl.Tran_Dt_DD
 And stl.Sales_Tran_Line_Num = stcl.Sales_Tran_Line_Num
 And stcl.Charge_Type_Cd = 'LUX'

Left Outer Join ${DB_ENV}TgtVOUT.MU_SCAN_CODE_LOCATION AS scLoc
 ON stl.Scan_Code_Seq_Num = scLoc.Scan_Code_Seq_Num
 And stl.Store_Seq_Num = scLoc.Location_Seq_Num
Inner Join ${DB_ENV}TgtVOUT.MU_SCAN_CODE sc
 On stl.Scan_Code_Seq_Num = sc.Scan_Code_Seq_Num
Left Outer Join ${DB_ENV}TgtVOUT.MU_SCAN_CODE_LOC_UOM_B AS scUOM
 On scLoc.Scan_Code_Seq_Num = scUOM.Scan_Code_Seq_Num
 And scLoc.Location_Seq_Num = scUOM.Location_Seq_Num
 And st.Tran_End_Dttm_DD Between scUOM.Valid_From_Dttm And scUOM.Valid_To_Dttm

Where st.Tran_Dt_DD >= (current_date - 120)
And st.Tran_Status_Cd in ( 'CT', 'VO')
And st.Tran_Type_Cd = 'POS'
And stl.Tran_Line_Status_Cd = 'CT'
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE