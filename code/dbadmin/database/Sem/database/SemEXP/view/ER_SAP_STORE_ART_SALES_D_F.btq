/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ER_SAP_STORE_ART_SALES_D_F.btq 20006 2016-09-19 08:56:33Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2016-09-19 10:56:33 +0200 (mån, 19 sep 2016) $
# Last Revision    : $Revision: 20006 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/ER_SAP_STORE_ART_SALES_D_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

Database ${DB_ENV}SemEXPVOUT;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemEXPVOUT.ER_SAP_STORE_ART_SALES_D_F 
(
	Store_Seq_Num
	, Article_Seq_Num
	, Calendar_Dt
	, Calendar_Week_Id
	, Calendar_Month_Id
	, Fiscal_Period
	, Fiscal_Year_Variant
	, Campaign_Sales_Type_Cd
	, Inv_Tran_Quality_Cd
	, Item_Qty
	, GP_Unit_Selling_Price_Amt
	, Unit_Selling_Price_Amt
	, Unit_Cost_Amt
	, Store_Margin_Amt
	, Floating_Discount_Amt
)
AS 
Lock Row For Access
select 
	f1.Store_Seq_Num
	, sc1.Article_Seq_Num
	, f1.Sales_Tran_Dt as Calendar_Dt
	, f1.Calendar_Week_Id
	, f1.Calendar_Month_Id
	, cast(ca1.calendar_year_id as char(4)) || '0' || substr(cast(ca1.calendar_month_id as char(6)),5,2) (char(7)) as Fiscal_Period
	, 'K4' (char(2)) as Fiscal_Year_Variant
	, f1.Campaign_Sales_Type_Cd
	, Coalesce(itq.Inv_Tran_Quality_Cd,-1) as Inv_Tran_Quality_Cd
	, (f1.Item_Qty) as Item_Qty
	, (f1.GP_Unit_Selling_Price_Amt) as GP_Unit_Selling_Price_Amt
	, (f1.Unit_Selling_Price_Amt) as Unit_Selling_Price_Amt
	, (f1.Unit_Cost_Amt) as Unit_Cost_Amt
	, (f1.GP_Unit_Selling_Price_Amt - f1.Unit_Cost_Amt) as Store_Margin_Amt
    , ((Coalesce(pos1.Floating_Discount_Amt,0) * -1) * f1.Item_Qty) as Floating_Discount_Amt
FROM ${DB_ENV}SemCMNVOUT.SALES_TRAN_LINE_DAY_F f1

INNER JOIN ${DB_ENV}SemCMNVOUT.SCAN_CODE_D sc1
on sc1.Scan_Code_Seq_Num = f1.Scan_Code_Seq_Num

/*
INNER JOIN (select Article_Seq_Num,
SAP_Article_ID as Article_ID
FROM ${DB_ENV}SemCMNVOUT.ARTICLE_D) a1
on sc1.Article_Seq_Num = a1.Article_Seq_Num

INNER JOIN (select store_seq_num,cast(Store_Id as char(4)) as Store_Id FROM ${DB_ENV}SemCMNVOUT.STORE_D where store_id between 1000 and 9999) s1
on f1.Store_Seq_Num = s1.Store_Seq_Num
*/

INNER JOIN ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D as ca1
on ca1.calendar_dt = f1.Sales_Tran_Dt

LEFT OUTER JOIN 
(
SELECT 
	pob1.Promotion_Offer_Seq_Num,
	pob1.Store_Seq_Num,
	pob1.Article_Seq_Num,
Sum(Case 
   When pob1.Scanback_Type_Cd = 1 Then (pob1.ScanBack_Value / 100) * pob1.Final_Cost_Price_Amt
   When pob1.Scanback_Type_Cd = 2 Then pob1.ScanBack_Value 
   Else 0
  End) (DECIMAL (10,2)) as Floating_Discount_Amt
FROM ${DB_ENV}SemCMNVOUT.PROMOTION_OFFER_BRIDGE as pob1
INNER JOIN (
select * from ${DB_ENV}TgtVOUT.VENDOR_FUND_DETAILS_B
 where Deletion_Ind = 0 And Vendor_Fund_Category_Cd = '99'
 And Valid_To_Dttm = SYSLIB.HighTSVal() 
 ) as vfd1
 on pob1.Vendor_Fund_Seq_Num = vfd1.Vendor_Fund_Seq_Num
--INNER JOIN ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D as c1
--on c1.Calendar_Dt between pob1.Campaign_Promotion_Start_Dt and pob1.Campaign_Promotion_End_Dt
GROUP BY 
	pob1.Promotion_Offer_Seq_Num
	, pob1.Store_Seq_Num
	, pob1.Article_Seq_Num
) pos1
on pos1.Promotion_Offer_Seq_Num = f1.Pref_Promotion_Offer_Seq_Num
And pos1.Store_Seq_Num = f1.Store_Seq_Num
and pos1.Article_Seq_Num = sc1.Article_Seq_Num 

LEFT OUTER JOIN 
(Select Location_Seq_Num as Store_Seq_Num, Article_Seq_Num, Inv_Tran_Quality_Cd
FROM ${DB_ENV}TgtVOUT.INV_TRAN_QUALITY_LVL 
WHERE Valid_To_Dttm = Timestamp '9999-12-31 23:59:59.999999') as itq
on itq.Store_Seq_Num = f1.Store_Seq_Num
and itq.Article_Seq_Num = sc1.Article_Seq_Num

/*
group by f1.Store_Seq_Num
	, sc1.Article_Seq_Num
	, f1.Sales_Tran_Dt 
	, f1.Calendar_Week_Id
	, f1.Calendar_Month_Id
	, ca1.Fiscal_Period
	, ca1.Fiscal_Year_Variant
	, f1.Campaign_Sales_Type_Cd
	, itq.Inv_Tran_Quality_Cd
*/
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemEXPVOUT.ER_SAP_STORE_ART_SALES_D_F IS '$Revision: 20006 $ - $Date: 2016-09-19 10:56:33 +0200 (mån, 19 sep 2016) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
