/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: PICTUREBOX_ITEM.btq 29892 2020-01-16 13:31:18Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-01-16 14:31:18 +0100 (tor, 16 jan 2020) $
# Last Revision    : $Revision: 29892 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/PICTUREBOX_ITEM.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}SemEXPVIN
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemEXPVIN.PICTUREBOX_ITEM
( TIME_KEY
 ,GEOG_KEY
 ,PROD_KEY
 ,ANTAL_VARDE
 ,BELOPP_INKL_MOMS
 ,BELOPP_EXCL_MOMS
 ,Store_Seq_Num
 ,Scan_Code_Seq_Num
 ,Calendar_Week_Start_Dt
 ,Calendar_Week_End_Dt
 ,Calendar_Week_Start_Dttm
 ,Calendar_Week_End_Dttm
) AS
SELECT pbs0.TIME_KEY
 ,pbs0.STORE_NO AS GEOG_KEY
 ,pbp0.PROD_KEY
 ,SUM(COALESCE(stla0.Actual_Item_Qty,1.0000)
  * stl0.Item_Qty) (DECIMAL (10,2)) AS ANTAL_VARDE
 ,SUM(stl0.Unit_Selling_Price_Amt
  + stl0.Tax_Amt) (DECIMAL (10,2)) AS BELOPP_INKL_MOMS
 ,SUM(stl0.Unit_Selling_Price_Amt) (DECIMAL (10,2)) AS BELOPP_EXCL_MOMS
 ,MAX(stl0.Store_Seq_Num) AS Store_Seq_Num
 ,MAX(stl0.Scan_Code_Seq_Num) AS Scan_Code_Seq_Num
 ,MAX(pbs0.Calendar_Week_Start_Dt) AS Calendar_Week_Start_Dt
 ,MAX(pbs0.Calendar_Week_End_Dt) AS Calendar_Week_End_Dt
 ,MAX(pbs0.Calendar_Week_Start_Dttm) AS Calendar_Week_Start_Dttm
 ,MAX(pbs0.Calendar_Week_End_Dttm) AS Calendar_Week_End_Dttm
FROM ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_VALIDATED st0
INNER JOIN ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE AS stl0
ON st0.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
INNER JOIN ${DB_ENV}SemEXPT.ER_PICTUREBOX_TIME AS pbt0 -- has two years of calendar-dates
ON pbt0.TIME_KEY = stl0.Tran_Dt_DD
INNER JOIN ${DB_ENV}SemEXPT.ER_PICTUREBOX_STORES AS pbs0
ON pbs0.Store_Seq_Num = stl0.Store_Seq_Num
INNER JOIN ${DB_ENV}SemEXPT.ER_PICTUREBOX_PRODUCTS AS pbp0
ON pbp0.Scan_Code_Seq_Num = stl0.Scan_Code_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As stla0
ON stla0.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
AND stla0.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
AND stla0.Store_Seq_Num = stl0.Store_Seq_Num 
AND stla0.Tran_Dt_DD = stl0.Tran_Dt_DD
WHERE st0.Tran_Status_Cd in ( 'CT', 'VO')
AND st0.Tran_Type_Cd = 'POS'
AND stl0.Tran_Line_Status_Cd = 'CT'
AND pbt0.TIME_KEY BETWEEN pbt0.Calendar_Week_Start_Dt AND pbt0.Calendar_Week_End_Dt
AND pbp0.Scan_Code_Seq_Num IS NOT NULL
GROUP BY 1,2,3
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemEXPVIN.PICTUREBOX_ITEM IS '$Revision: 29892 $ - $Date: 2020-01-16 14:31:18 +0100 (tor, 16 jan 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

