/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ER_OCB_RECEIPT_HEADERS_R.btq 29892 2020-01-16 13:31:18Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-01-16 14:31:18 +0100 (tor, 16 jan 2020) $
# Last Revision    : $Revision: 29892 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemEXP/view/ER_OCB_RECEIPT_HEADERS_R.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

--- The default database is set.--
Database ${DB_ENV}SemEXPVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemExpVout.ER_OCB_RECEIPT_HEADERS_R
 AS LOCKING ROW FOR ACCESS 
Select                        p.N_Party_Id as CustomerId
                            , st.Sales_Tran_Seq_Num
                            , st.Store_Seq_Num 
                            , st.Tran_Dt_DD
                            , b.N_Store_Id as StoreId
                            , st.N_Sales_Tran_Id as TransactionId
                            , ni.N_Src_Tech_Seq_Num as SequenceNumber
                            , st.Tran_End_Dttm_DD as TransactionDateTime
                            , sd.N_Store_Department_Id as StoreDepartment
                            , st.Tran_Status_Cd as TranStatusCd
                            , t.Total_Amt as TotalAmount
FROM   ${DB_ENV}TgtVOUT.PARTY p
                        
inner join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_PARTY as stp
on p.Party_Seq_Num = stp.Party_Seq_Num

Inner Join ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_VALIDATED As st
  On stp.Sales_Tran_Seq_Num = st.Sales_Tran_Seq_Num
  And stp.Store_Seq_Num = st.Store_Seq_Num 
  And stp.Tran_Dt_DD = st.Tran_Dt_DD

Inner join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS t
on  st.Sales_Tran_Seq_Num = t.Sales_Tran_Seq_Num
and st.Store_Seq_Num = t.Store_Seq_Num
and st.Tran_Dt_DD = t.Tran_Dt_DD
and t.Tran_Type_Cd = 'SA'

Inner Join ${DB_ENV}TgtVOUT.store b
on st.Store_Seq_Num = b.Store_Seq_Num

inner join ${DB_ENV}TgtVOUT.STORE_DEPARTMENT sd
on st.Store_Department_Seq_Num = sd.Store_Department_Seq_Num

left outer join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_NAT_ID ni
on st.Sales_Tran_Seq_Num = ni.Sales_Tran_Seq_Num
and st.Store_Seq_Num = ni.Store_Seq_Num
and st.Tran_Dt_DD = ni.Tran_Dt_DD

WHERE st.Tran_Type_Cd = 'POS'
  And st.Tran_Status_Cd in ('CT')
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
