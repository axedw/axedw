/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Upd_LOYALTY_TRANSACTION_DAY_F.btq 12916 2014-05-13 12:27:24Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2014-05-13 14:27:24 +0200 (tis, 13 maj 2014) $
# Last Revision    : $Revision: 12916 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/procedure/Upd_LOYALTY_TRANSACTION_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
REPLACE PROCEDURE ${DB_ENV}SemCMNVIN.Upd_LOYALTY_TRANSACTION_DAY_F
(	 INOUT p_TranDt	DATE
	,INOUT p_Days	SMALLINT
)
P0: BEGIN
/*****************************************************************************
Optional parameters:
 p_TranDt	- Date to update, if NULL THEN use (current-date - 1)
 p_Days	- Number of additional days following p_TranDt
 Description:	Updates LOYALTY_TRANSACTION_DAY_F
*****************************************************************************/
DECLARE v_TranDt	DATE;
DECLARE v_Days	SMALLINT;
DECLARE v_StartDt	DATE;
DECLARE v_EndDt	DATE;
-- Global continue-handler for dynamic partitioning creation error 9247
DECLARE T9247 CONDITION FOR SQLSTATE VALUE 'T9247';
DECLARE CONTINUE HANDLER FOR T9247 BEGIN END;

SET v_TranDt = COALESCE(p_TranDt,CURRENT_DATE-1)
;
SET v_Days = CASE WHEN p_TranDt IS NULL OR p_Days IS NULL THEN 0 ELSE ABS(p_Days) END
;
SET v_StartDt = v_TranDt
;
SET v_EndDt = v_TranDt + v_Days
;

-- Check if dynamic partitioning ON target table needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(MONTH FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(MONTH FROM CURRENT_DATE) AND COALESCE(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions AND add new
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.LOYALTY_TRANSACTION_DAY_F TO CURRENT WITH DELETE;')
	;
END IF
;


MERGE INTO ${DB_ENV}SemCMNT.LOYALTY_TRANSACTION_DAY_F AS ltd1
USING (
	SELECT	lt0.Store_Seq_Num
		,lt0.Processing_Dt
		,lt0.Member_Account_Seq_Num
		,lt0.Partner_Seq_Num
		,c0.Calendar_Week_Id
		,c0.Calendar_Month_Id
		,SUM(lt0.Total_Loyalty_Amt) AS Total_Loyalty_Amt
		,SUM(lt0.Bonus_Applicable_Amt) AS Bonus_Applicable_Amt
		,SUM(lt0.Accrual_Amt) AS Accrual_Amt
		,SUM(lt0.Purchase_Amt) AS Purchase_Amt
		,SUM(lt0.Downgraded_Amt) AS Downgraded_Amt
		,SUM(lt0.Remittance_Amt) AS Remittance_Amt
	FROM ${DB_ENV}SemCMNVOUT.LOYALTY_TRANSACTION_F AS lt0

	INNER JOIN ${DB_ENV}TgtVOUT.CALENDAR_DATE AS c0
		ON c0.Calendar_Dt = lt0.Processing_Dt

	WHERE lt0.Processing_Dt in
	(
		/*
		** Get all Processing_Dt that needs to be recalculated
		*/
		Select ltr1.Processing_Dt

		FROM ${DB_ENV}TgtVOUT.LOYALTY_TRANSACTION AS ltr1

		LEFT OUTER JOIN ${DB_ENV}TgtVOUT.LOYALTY_TRAN_MEMBER_HIST AS ltmh1
		ON ltr1.Loyalty_Trans_Id = ltmh1.Loyalty_Trans_Id
		AND ltmh1.Valid_To_Dttm = SYSLIB.HighTSVal()

		/*
		** To capture changes due to late arrival of transactions do the following:
		** If daily run then use MEMBER_HIST arrive date (Insert_Dt) if it exists else use Processing_Dt of transaction
		** If history run then use Processing_Dt of transaction
		*/
		WHERE Coalesce(
			Case When :p_Days > 0 
			Then Cast(ltmh1.InsertDttm as DATE) 
			Else NULL end
			,ltr1.Processing_Dt
			) BETWEEN :v_StartDt AND :v_EndDt
              and ltr1.Processing_Dt
                       BETWEEN ADD_MONTHS((DATE - (CAST(((EXTRACT(DAY FROM (DATE )))- 1 ) AS INTERVAL DAY(2)))),(-17 )) 
                       AND (ADD_MONTHS((DATE - (CAST(((EXTRACT(DAY FROM (DATE )))- 1 ) AS INTERVAL DAY(2)))),(1 )))- INTERVAL '1' DAY		
	)
	GROUP BY 1,2,3,4,5,6
	) AS lt1
	ON lt1.Member_Account_Seq_Num = ltd1.Member_Account_Seq_Num
	AND lt1.Processing_Dt = ltd1.Processing_Dt
	AND lt1.Store_Seq_num = ltd1.Store_Seq_num
	AND lt1.Partner_Seq_Num = ltd1.Partner_Seq_Num

WHEN MATCHED THEN UPDATE
SET	Total_Loyalty_Amt = COALESCE(lt1.Total_Loyalty_Amt,0)
	,Bonus_Applicable_Amt = COALESCE(lt1.Bonus_Applicable_Amt,0)
	,Accrual_Amt = COALESCE(lt1.Accrual_Amt,0)
	,Purchase_Amt = COALESCE(lt1.Purchase_Amt,0)
	,Downgraded_Amt = COALESCE(lt1.Downgraded_Amt,0)
	,Remittance_Amt = COALESCE(lt1.Remittance_Amt,0)
	

WHEN NOT MATCHED THEN INSERT
( 	Store_Seq_Num
	,Processing_Dt
	,Member_Account_Seq_Num
	,Calendar_Week_Id
	,Calendar_Month_Id
	,Partner_Seq_Num
	,Total_Loyalty_Amt
	,Bonus_Applicable_Amt
	,Accrual_Amt
	,Purchase_Amt
	,Downgraded_Amt
	,Remittance_Amt
) VALUES (
	lt1.Store_Seq_Num
	,lt1.Processing_Dt
	,lt1.Member_Account_Seq_Num
	,lt1.Calendar_Week_Id
	,lt1.Calendar_Month_Id
	,lt1.Partner_Seq_Num
	,COALESCE(lt1.Total_Loyalty_Amt,0)
	,COALESCE(lt1.Bonus_Applicable_Amt,0)
	,COALESCE(lt1.Accrual_Amt,0)
	,COALESCE(lt1.Purchase_Amt,0)
	,COALESCE(lt1.Downgraded_Amt,0)
	,COALESCE(lt1.Remittance_Amt,0)
)
;

END P0
;
