/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Upd_SALES_TRAN_TOTALS_DAY_F.btq 21461 2017-02-13 13:08:19Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-02-13 14:08:19 +0100 (mån, 13 feb 2017) $
# Last Revision    : $Revision: 21461 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/procedure/Upd_SALES_TRAN_TOTALS_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}SemCMNVIN.Upd_SALES_TRAN_TOTALS_DAY_F
(	 INOUT p_TranDt	DATE
	,INOUT p_Days	SMALLINT
	,INOUT p_TargetPrevExtractTime TIMESTAMP(6)
	,INOUT p_TargetExtractTime	TIMESTAMP(6)
) SQL SECURITY INVOKER
P0: BEGIN
/*****************************************************************************
Normal loading and restart loading through Informatica:
Mandatory parameters
 p_TranDt	- Date to update 
 p_Days	- Number of additional days following p_TranDt 
 p_TargetPrevExtractTime  - Last loadingtime
 p_TargetExtractTime - Current loadingtime 

Manually (call procedure) historyloading: 
Mandatory parameters 
 p_TranDt	- Date to update
 p_Days	- Number of additional days, p_Days is negative
 p_TargetExtractTime 
Optional parameters  
 p_TargetPrevExtractTime  - Last loadingtime
  
 Description:	Updates SALES_TRAN_TOTALS_DAY_F
*****************************************************************************/
DECLARE v_TranDt	DATE;
DECLARE v_Days	SMALLINT;
DECLARE v_StartDt	DATE;
DECLARE v_EndDt		DATE;
DECLARE v_NegCount	INTEGER;
DECLARE v_TargetPrevExtractTime	TIMESTAMP(6);
DECLARE v_TargetExtractTime	TIMESTAMP(6);
DECLARE v_OpStart	TIMESTAMP(6);

-- Global continue-handler for dynamic partitioning creation error 9247
DECLARE T9247 CONDITION FOR SQLSTATE VALUE 'T9247';
DECLARE CONTINUE HANDLER FOR T9247 BEGIN END;

/*
** v_NegCount is set to 1 if the p_Days is negative. v_NegCount is then used for selecting the date to use as where condition:
**     v_NegCount <= 0 then use Trans_Dt to select records (load records from history)
**     v_NegCount > 0 then use InsertDttm (daily load)
*/
SET v_NegCount = 0
;
IF Coalesce(p_Days,-1) > 0
THEN
	SET v_NegCount = 0;
ELSE
	SET v_NegCount = 1;
END IF
;

SET v_TranDt = COALESCE(p_TranDt,CURRENT_DATE-1)
;
SET v_Days = CASE WHEN p_TranDt IS NULL OR p_Days IS NULL THEN 0 ELSE ABS(p_Days) END
;
SET v_StartDt = v_TranDt
;
SET v_EndDt = v_TranDt + v_Days
;
SET v_TargetPrevExtractTime = p_TargetPrevExtractTime
;
SET v_TargetExtractTime = p_TargetExtractTime
;

-- Check if dynamic partitioning on target table needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(MONTH FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(MONTH FROM CURRENT_DATE) AND Coalesce(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions and add new'
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.SALES_TRAN_TOTALS_DAY_F TO CURRENT WITH DELETE;')
	;
END IF
;
-- Check if dynamic partitioning on AJI needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(YEAR FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(YEAR FROM CURRENT_DATE) AND Coalesce(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions and add new
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_SALES_TRAN_TOTALS_WEEK TO CURRENT;')
	;
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_SALES_TRAN_TOTALS_MONTH TO CURRENT;')
	;
END IF
;

/*
** get stores and dates that will be calculated and recalculated (late arrived transactions and loyalty contact relation to transaction)
*/
CREATE VOLATILE TABLE Tran_Totals_Day#Volatile, NO LOG
as
(
	Select st_loc.Store_Seq_Num, st_loc.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRANSACTION st_loc
	Where
	      (
--	Historical load, just look at transaction date
           :v_NegCount = 1 And
           st_loc.Tran_Dt_DD Between :v_StartDt And :v_EndDt
           )
           OR
		   (
--	Online load, load transactions that arrived into target between last run and this run
           :v_NegCount = 0 And
		   st_loc.InsertDttm >= :v_TargetPrevExtractTime
		   and
           st_loc.InsertDttm < 	:v_TargetExtractTime
		   and
		   st_loc.Tran_Dt_DD < CURRENT_DATE
			)
	UNION
	Select stlc_loc.Store_Seq_Num, stlc_loc.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT stlc_loc
	Where
	       (
--	Historical load, just look at transaction date
           :v_NegCount = 1 And
           stlc_loc.Tran_Dt_DD Between :v_StartDt And :v_EndDt
           )
           OR
		   (
--	Online load, load transactions that arrived into target between last run and this run
           :v_NegCount = 0 And
           stlc_loc.InsertDttm >= :v_TargetPrevExtractTime
		   and
           stlc_loc.InsertDttm < :v_TargetExtractTime
   		   and
		   stlc_loc.Tran_Dt_DD < CURRENT_DATE
			)
)
with data
Primary index(Store_Seq_Num, Tran_Dt_DD)
on commit preserve rows
;

Collect statistics
index(Store_Seq_Num, Tran_Dt_DD)
On Tran_Totals_Day#Volatile
;

-- Delete operation
SET v_OpStart = CURRENT_TIMESTAMP(6)
;

/*
** Remove all old stores,dates that will be recalculated
*/
DELETE FROM ${DB_ENV}SemCMNT.SALES_TRAN_TOTALS_DAY_F
	WHERE (Store_Seq_Num, Tran_Dt ) in
	(
		Select Store_Seq_Num, Tran_Dt_DD
		From Tran_Totals_Day#Volatile
	)
;

CALL ${DB_ENV}MetadataVIN.UpdTargetLoad ('SemCMNT.SALES_TRAN_TOTALS_DAY_F'
	,'DELETE',PERIOD(:v_OpStart,CURRENT_TIMESTAMP(6)),:ACTIVITY_COUNT, NULL)
;

-- Insert operation
SET v_OpStart = CURRENT_TIMESTAMP(6)
;

/*
** Calculate new and recalculate old stores,dates selected earlier
*/
INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_TOTALS_DAY_F
(
	Contact_Account_Seq_Num,
	Store_Seq_Num,
	Tran_Dt,
	Calendar_Week_Id,
	Calendar_Month_Id,
	Junk_Seq_Num,
	Grand_Amt,
	Tot_Discount_Amt,
	Employee_Discount_Based_Amt,
	Tot_Loy_Discount_Amt
)   
    Select	
	st1.Contact_Account_Seq_Num as Contact_Account_Seq_Num,
	st1.Store_Seq_Num as Store_Seq_Num,
	st1.Tran_Dt as Tran_Dt,
	st1.Calendar_Week_Id as Calendar_Week_Id,
	st1.Calendar_Month_Id as Calendar_Month_Id,
	st1.Junk_seq_num,
	SUM(st1.Grand_Amt) as Grand_Amt,
	SUM(st1.Tot_Discount_Amt) as Tot_Discount_Amt,
	SUM(st1.Employee_Discount_Based_Amt) as Employee_Discount_Based_Amt,
	SUM(st1.Tot_Loy_Discount_Amt) as Tot_Loy_Discount_Amt
	
	From
	(
		Select 
			st.Sales_Tran_Seq_Num,
			st.N_Sales_Tran_Id Sales_Tran_Id,
			st.Store_Seq_Num,
			st.Tran_Dt_DD As Tran_Dt,
			st.Tran_End_Dttm_DD As Tran_End_Dttm,
			st.Store_Department_Seq_Num,
			Coalesce(stp.Sold_To_Party_Id,'-1') As Customer_Id,
			Coalesce(stp.Party_Seq_Num,'-1') As Customer_Seq_Num,
			Coalesce(stla.Loyalty_Account_Id,'-1') As Loyalty_Account_Id,
			Coalesce(stlc.Contact_Account_Seq_Num,-1) As Contact_Account_Seq_Num,
			Coalesce(stlim.Loyalty_Identifer_Seq_Num,-1) AS Loyalty_Identifier_Seq_Num, --FELSTAVAT Identifer
			stc.Circumstance_Value_Cd As Receipt_Type_Cd,
			st.POS_Register_Seq_Num,
			st.Sales_Associate_Id,
			Coalesce(j1.Junk_Seq_Num,-1) as Junk_Seq_Num,
			Coalesce(sttpd.Total_Amt , 0) As Employee_Discount_Based_Amt,
			Coalesce(sttga.Total_Amt , 0) As Grand_Amt,
			Coalesce(stttd.Total_Amt , 0) As Tot_Discount_Amt,
			Coalesce(sttld.Total_Amt , 0) As Tot_Loy_Discount_Amt,

			st.OpenJobRunID,
			st.InsertDttm,
			c.Calendar_Week_Id,
			c.Calendar_Month_Id 
		From ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st
		Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On st.Tran_Dt_DD = c.Calendar_Dt

		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttpd
		 On st.Sales_Tran_Seq_Num = sttpd.Sales_Tran_Seq_Num 
		 And st.Store_Seq_Num = sttpd.Store_Seq_Num 
		 And st.Tran_Dt_DD = sttpd.Tran_Dt_DD
		 And sttpd.Tran_Type_Cd = 'PD' 

		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttga
		 On st.Sales_Tran_Seq_Num = sttga.Sales_Tran_Seq_Num 
		 And st.Store_Seq_Num = sttga.Store_Seq_Num 
		 And st.Tran_Dt_DD = sttga.Tran_Dt_DD
		 And sttga.Tran_Type_Cd = 'GA' 

		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As stttd
		 On st.Sales_Tran_Seq_Num = stttd.Sales_Tran_Seq_Num 
		 And st.Store_Seq_Num = stttd.Store_Seq_Num 
		 And st.Tran_Dt_DD = stttd.Tran_Dt_DD
		 And stttd.Tran_Type_Cd = 'TD' 
		 
		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttld
		 On st.Sales_Tran_Seq_Num = sttld.Sales_Tran_Seq_Num 
		 And st.Store_Seq_Num = sttld.Store_Seq_Num 
		 And st.Tran_Dt_DD = sttld.Tran_Dt_DD
		 And sttld.Tran_Type_Cd = 'LD'  

		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT As stlc 
			On st.Sales_Tran_Seq_Num = stlc.Sales_Tran_Seq_Num
			And st.Store_Seq_Num = stlc.Store_Seq_Num 
			And st.Tran_Dt_DD = stlc.Tran_Dt_DD
			And stlc.InsertDttm < :v_TargetExtractTime

		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_ID_METHOD As stlim 
			On st.Sales_Tran_Seq_Num = stlim.Sales_Tran_Seq_Num
			And st.Store_Seq_Num = stlim.Store_Seq_Num 
			And st.Tran_Dt_DD = stlim.Tran_Dt_DD
		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_PARTY As stp
			On st.Sales_Tran_Seq_Num = stp.Sales_Tran_Seq_Num
			And st.Store_Seq_Num = stp.Store_Seq_Num 
			And st.Tran_Dt_DD = stp.Tran_Dt_DD
		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOYALTY_ACCOUNT As stla
			On st.Sales_Tran_Seq_Num = stla.Sales_Tran_Seq_Num
			And st.Store_Seq_Num = stla.Store_Seq_Num 
			And st.Tran_Dt_DD = stla.Tran_Dt_DD
		Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_CIRCUMSTANCE As stc
			On st.Sales_Tran_Seq_Num = stc.Sales_Tran_Seq_Num
			And st.Store_Seq_Num = stc.Store_Seq_Num 
			And st.Tran_Dt_DD = stc.Tran_Dt_DD
			And stc.Circumstance_Cd = 'COTP'	
		LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
			On st.Store_Seq_Num = std1.Store_Seq_Num and st.Tran_Dt_DD = std1.Calendar_Dt
		LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
			on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') 
			and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
			and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') 
			and j1.Loyalty_Ind = 0
			
		Where st.Tran_Status_Cd in ( 'CT', 'VO')
		And st.Tran_Type_Cd = 'POS'
	) As st1
	(
		Sales_Tran_Seq_Num,
		Sales_Tran_Id,
		Store_Seq_Num,
		Tran_Dt,
		Tran_End_Dttm,
		Store_Department_Seq_Num,
		Customer_Id,
		Customer_Seq_Num,
		Loyalty_Account_Id,
		Contact_Account_Seq_Num,
		Loyalty_Identifier_Seq_Num,
		Receipt_Type_Cd,
		POS_Register_Seq_Num,
		Sales_Associate_Id,
		Junk_Seq_Num,
		
		Employee_Discount_Based_Amt,
		Grand_Amt,
		Tot_Discount_Amt,
		Tot_Loy_Discount_Amt,

		OpenJobRunID,
		InsertDttm,
		Calendar_Week_Id,
		Calendar_Month_Id 
	)
	INNER JOIN Tran_Totals_Day#Volatile as aloc
	on st1.Store_Seq_Num = aloc.Store_Seq_Num 
	And st1.Tran_Dt = aloc.Tran_Dt_DD
	
	And st1.InsertDttm < :v_TargetExtractTime
	
	Group by
		st1.Contact_Account_Seq_Num,
		st1.Store_Seq_Num,
		st1.Tran_Dt,
		st1.Calendar_Week_Id,
		st1.Calendar_Month_Id,
		st1.Junk_Seq_Num
;

CALL ${DB_ENV}MetadataVIN.UpdTargetLoad ('SemCMNT.SALES_TRAN_TOTALS_DAY_F'
	,'INSERT',PERIOD(:v_OpStart,CURRENT_TIMESTAMP(6)),:ACTIVITY_COUNT, PERIOD(:v_TargetPrevExtractTime,:v_TargetExtractTime))
;

DROP TABLE Tran_Totals_Day#Volatile
;

END P0
;
