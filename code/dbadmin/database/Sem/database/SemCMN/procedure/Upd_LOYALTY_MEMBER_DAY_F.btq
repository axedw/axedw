/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Upd_LOYALTY_MEMBER_DAY_F.btq 29766 2020-01-01 17:38:50Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-01-01 18:38:50 +0100 (ons, 01 jan 2020) $
# Last Revision    : $Revision: 29766 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/procedure/Upd_LOYALTY_MEMBER_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
/*
** NOTE! This code is replicated from the view LOYALTY_MEMBER_F for performance reasons
** Changes in this file must be done also in the file for the view LOYALTY_MEMBER_F
*/

REPLACE PROCEDURE ${DB_ENV}SemCMNVIN.Upd_LOYALTY_MEMBER_DAY_F
(	 INOUT p_TranDt	DATE
	,INOUT p_Days	INTEGER
)
P0: BEGIN
/*****************************************************************************
Optional parameters:
 p_TranDt	- Date to update, if NULL THEN use (current-date - 1)
 p_Days	- Number of days to load including p_TranDt. If negative number or null then do not refresh partitioning
Description:	Updates LOYALTY_MEMBER_DAY_F
*****************************************************************************/

DECLARE v_TranDt	DATE;
DECLARE v_Days INTEGER;
DECLARE i INTEGER;
-- Global continue-handler for dynamic partitioning creation error 9247
DECLARE T9247 CONDITION FOR SQLSTATE VALUE 'T9247';
DECLARE CONTINUE HANDLER FOR T9247 BEGIN END;

SET v_TranDt = COALESCE(p_TranDt,CURRENT_DATE-1)
;
SET v_Days = CASE WHEN p_TranDt IS NULL OR p_Days IS NULL THEN 0 ELSE ABS(p_Days) END
;

-- Check if dynamic partitioning on target table needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(MONTH FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(MONTH FROM CURRENT_DATE) AND Coalesce(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions and add new
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.LOYALTY_MEMBER_DAY_F TO CURRENT WITH DELETE;')
	;
END IF
;
-- Check if dynamic partitioning on AJI needs to be refreshed (to current)
-- do not refresh if negative p_Days or 0 is used (negative p_Days or 0 is used for loading historical periods)
IF EXTRACT(YEAR FROM Coalesce(p_TranDt, CURRENT_DATE-1)) <> EXTRACT(YEAR FROM CURRENT_DATE) AND Coalesce(p_Days,-1) > 0
THEN -- Monthly partition-boundry (1st day of month), drop old partitions and add new
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_LOYALTY_MEMBER_WEEK_F TO CURRENT;')
	;
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_LOYALTY_MEMBER_MONTH_F TO CURRENT;')
	;
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_LOYALTY_CONTACT_WEEK_F TO CURRENT;')
	;
	CALL DBC.SysExecSQL('ALTER TABLE ${DB_ENV}SemCMNT.AJI1_LOYALTY_CONTACT_MONTH_F TO CURRENT;')
	;
END IF
;
SET i = 0;
WHILE_LOOP:
WHILE i < v_Days OR i = 0
DO
	Insert Into ${DB_ENV}SemCMNT.LOYALTY_MEMBER_DAY_F
	(
		Member_Account_Seq_Num,
		Contact_Account_Seq_Num,
		Loyalty_Program_Seq_Num,
		Calendar_Dt,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Day_Home_Store_Seq_Num,
		Week_Home_Store_Seq_Num,
		Month_Home_Store_Seq_Num,
		Member_Ind,
		New_Member_Ind,
		Active_Member_Ind,
		Lost_Member_Ind,
		ReActived_Member_Ind,
		Day_Member_Tier_Lvl_Seq_Num,
		Week_Member_Tier_Lvl_Seq_Num,
		Month_Member_Tier_Lvl_Seq_Num,
		
		Contact_Ind,
		Active_Contact_Ind,

		Member_Cnt,
		Member_Distinct_Cnt,
		Contact_Cnt,
		Contact_Distinct_Cnt
	)
	Select
		lmf.Member_Account_Seq_Num,
		lmf.Contact_Account_Seq_Num,
		lmf.Loyalty_Program_Seq_Num, 
		lmf.Calendar_Dt, 
		lmf.Calendar_Week_Id, 
		lmf.Calendar_Month_Id, 
		Coalesce(lmf.Day_Home_Store_Seq_Num,-1) as Day_Home_Store_Seq_Num,
		Coalesce(lmf.Week_Home_Store_Seq_Num,-1) as Week_Home_Store_Seq_Num,
		Coalesce(lmf.Month_Home_Store_Seq_Num,-1) as Month_Home_Store_Seq_Num,
		lmf.Member_Ind, 
		lmf.New_Member_Ind, 
		lmf.Active_Member_Ind, 
		lmf.Lost_Member_Ind, 
		lmf.ReActived_Member_Ind, 
		Coalesce(lmf.Day_Member_Tier_Lvl_Seq_Num,-1) as Day_Member_Tier_Lvl_Seq_Num,
		Coalesce(lmf.Week_Member_Tier_Lvl_Seq_Num,-1) as Week_Member_Tier_Lvl_Seq_Num,
		Coalesce(lmf.Month_Member_Tier_Lvl_Seq_Num,-1) as Month_Member_Tier_Lvl_Seq_Num,

		lmf.Contact_Ind, 
		lmf.Active_Contact_Ind, 

		Count(lmf.Member_Account_Seq_Num) as Member_Cnt,
		Count(Distinct lmf.Member_Account_Seq_Num) as Member_Distinct_Cnt,
		Count(lmf.Contact_Account_Seq_Num) as Contact_Cnt,
		Count(Distinct lmf.Contact_Account_Seq_Num) as Contact_Distinct_Cnt

	From 
	--NOTE! This code is a copy from CMNVOUT.LOYALTY_MEMBER_F and parametrized. Home store and Tierlevel have additional joins
	-- Columns not used in the aggregate has been removed
	(
	Select	
	   lpm.Loyalty_Program_Seq_Num
	 , cald.Calendar_Dt 
	 , cald.Calendar_Week_Id 
	 , cald.Calendar_Month_Id 
	 , lma.Member_Account_Seq_Num
	 , lca.Contact_Account_Seq_Num
	 , lmhs.Store_Seq_Num As Day_Home_Store_Seq_Num
	 , lmhs_week.Store_Seq_Num As Week_Home_Store_Seq_Num
	 , lmhs_month.Store_Seq_Num As Month_Home_Store_Seq_Num
	 , mtlh.To_Loyalty_Tier_Lvl_Seq_Num As Day_Member_Tier_Lvl_Seq_Num
	 , mtlh_week.To_Loyalty_Tier_Lvl_Seq_Num As Week_Member_Tier_Lvl_Seq_Num
	 , mtlh_month.To_Loyalty_Tier_Lvl_Seq_Num As Month_Member_Tier_Lvl_Seq_Num
	 , Max( Case When lmasb.Loyalty_Status_Cd = 'Active' or lmasb.Loyalty_Status_Cd = 'Inactive' Then 1 Else 0 End ) As Member_Ind
	 , Max( Case When lcdb.Contact_Status_Val = 'Active' or lcdb.Contact_Status_Val = 'Inactive' Then 1 Else 0 End ) As Contact_Ind
	 , Max
		(
			Case When
				Cast(lmasb.Valid_From_Dttm as Date) = cald.Calendar_Dt And
				lmasb.Loyalty_Status_Cd = 'Active'
			Then 1
			Else 0
			End
		) As New_Member_Ind
	 , Max( Case When POSPeriod.LastKnownTranDt Is Not Null Then 1 Else 0 End ) As Active_Member_Ind
	 , Max
		(
			Case When
				(
					POSperiod.LastKnownTranDt Is Not Null And
					POSperiod.Contact_Account_Seq_Num = lca.Contact_Account_Seq_Num
				)
			Then 1
			Else 0
			End
		) As Active_Contact_Ind
	 , Max
		(
			Case When
				(
					POSperiod.LastKnownTranDt Is Not Null And
					POSperiod.LastKnownTranDtPD Is NULL And
					lmasb.Loyalty_Status_Cd = 'Active' And
					lmasb.Valid_From_Dttm < cald.Calendar_Dt - INTERVAL '91' DAY
				)
				Then 1
				Else 0
			End
		) As ReActived_Member_Ind 
	 , Max
		(
			Case When
				Cast(lmasb.Valid_From_Dttm as Date) = cald.Calendar_Dt And
				lmasb.Loyalty_Status_Cd = 'Cancelled'
				Then 1
				Else 0
			End
		) As Lost_Member_Ind 

	/* Get all contact accounts */
	From	${DB_ENV}TgtVOUT.ACCT As ca 
	Inner Join ${DB_ENV}TgtVOUT.LOYALTY_CONTACT_ACCOUNT As lca
		On	ca.Account_Seq_Num = lca.Contact_Account_Seq_Num 
	/* And the validity time for each contact */
	Inner Join ${DB_ENV}TgtVOUT.ACCOUNT_DETAILS_B As cadb 
		On	ca.Account_Seq_Num = cadb.Account_Seq_Num 
	/* Join in calender in order to get a correct fact by day, from start date */
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As cald
		On	cald.Calendar_Dt >= cadb.Account_Open_Dt
		And	cald.Calendar_Dt_End_Dttm Between cadb.Valid_From_Dttm And cadb.Valid_To_Dttm 
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_WEEK As calw
		On	cald.Calendar_Week_Id = calw.Calendar_Week_Id
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_MONTH As calm
		On	cald.Calendar_Month_Id = calm.Calendar_Month_Id
	/* Join in the membership */
	Inner Join ${DB_ENV}TgtVOUT.ACCT_ACCT_RELATIONSHIP As aar
		On	ca.Account_Seq_num = aar.Account_Seq_Num 
		And	cald.Calendar_Dt_End_Dttm Between aar.Valid_From_Dttm And aar.Valid_To_Dttm 
	Inner Join ${DB_ENV}TgtVOUT.ACCT As ma 
		On	aar.Parent_Account_Seq_Num = ma.Account_Seq_Num 
		And	ma.Account_Type_Cd = 'MEM'
	Inner Join ${DB_ENV}TgtVOUT.LOYALTY_MEMBER_ACCOUNT As lma
		On	ma.Account_Seq_Num =  lma.Member_Account_Seq_Num 
	/* And the validity time for each member */
	/* Join in the program of the member */
	Inner Join ${DB_ENV}TgtVOUT.LOYALTY_PROGRAM_MEMBER As lpm
		On	lma.Member_Account_Seq_Num = lpm.Member_Account_Seq_Num 
		And	cald.Calendar_Dt_End_Dttm Between lpm.Valid_From_Dttm And lpm.Valid_To_Dttm 
	Inner Join ${DB_ENV}TgtVOUT.LOYALTY_PROGRAM_DETAILS_B As lpdb
		On	lpdb.Loyalty_Program_Seq_Num = lpm.Loyalty_Program_Seq_Num 
		And	cald.Calendar_Dt_End_Dttm Between lpdb.Valid_From_Dttm And lpdb.Valid_To_Dttm 

		/* Join in the home store of the member for the day, use left outer since inner join generates huge spool, bug? */
	Left Outer Join ${DB_ENV}TgtVOUT.LOYALTY_MEMBER_HOME_STORE As lmhs
		On	lma.Member_Account_Seq_Num = lmhs.Member_Account_Seq_Num 
		And	cald.Calendar_Dt_End_Dttm Between lmhs.Valid_From_Dttm And lmhs.Valid_To_Dttm 
	/* Join in the home store of the member for the month */
	Left Outer Join ${DB_ENV}TgtVOUT.LOYALTY_MEMBER_HOME_STORE As lmhs_Month
		On	lma.Member_Account_Seq_Num = lmhs_Month.Member_Account_Seq_Num 
		And	calm.Calendar_Month_End_Dttm Between lmhs_Month.Valid_From_Dttm And lmhs_Month.Valid_To_Dttm 
	/* Join in the home store of the member for the week */
	Left Outer Join ${DB_ENV}TgtVOUT.LOYALTY_MEMBER_HOME_STORE As lmhs_Week
		On	lma.Member_Account_Seq_Num = lmhs_Week.Member_Account_Seq_Num 
		And	calw.Calendar_Week_End_Dttm Between lmhs_Week.Valid_From_Dttm And lmhs_Week.Valid_To_Dttm 

	/* Join in the member tier level for the time being, use left outer since inner join generates huge spool, bug? */
	Left Outer Join ${DB_ENV}TgtVOUT.LOY_MEMBER_TIER_LVL_HIST As mtlh
		On	lma.Member_Account_Seq_Num = mtlh.Member_Account_Seq_Num  
		And	cald.Calendar_Dt_End_Dttm Between mtlh.Valid_From_Dttm And mtlh.Valid_To_Dttm 
	/* Join in the member tier level for the month */
	Left Outer Join ${DB_ENV}TgtVOUT.LOY_MEMBER_TIER_LVL_HIST As mtlh_month
		On	lma.Member_Account_Seq_Num = mtlh_month.Member_Account_Seq_Num  
		And	calm.Calendar_Month_End_Dttm Between mtlh_month.Valid_From_Dttm And mtlh_month.Valid_To_Dttm 
	/* Join in the member tier level for the week */
	Left Outer Join ${DB_ENV}TgtVOUT.LOY_MEMBER_TIER_LVL_HIST As mtlh_week
		On	lma.Member_Account_Seq_Num = mtlh_week.Member_Account_Seq_Num  
		And	calw.Calendar_Week_End_Dttm Between mtlh_week.Valid_From_Dttm And mtlh_week.Valid_To_Dttm 

	/* Join in the member status of the member */
	Inner Join ${DB_ENV}TgtVOUT.LOYALTY_MEMBER_STATUS_B As lmasb
		On	lma.Member_Account_Seq_Num = lmasb.Member_Account_Seq_Num 
		And	cald.Calendar_Dt_End_Dttm Between lmasb.Valid_From_Dttm And lmasb.Valid_To_Dttm 

	/* Join in the contact status of the contact */
	Inner Join ${DB_ENV}TgtVOUT.LOY_CONTACT_DETAILS_B As lcdb
		On	lca.Contact_Account_Seq_Num = lcdb.Contact_Account_Seq_Num 
		And	cald.Calendar_Dt_End_Dttm Between lcdb.Valid_From_Dttm And lcdb.Valid_To_Dttm 
	
	/* last known transaction date  per contact*/
	Left Outer Join
	(
		Select
			Member_Account_Seq_Num
			, Contact_Account_Seq_Num
			, Calendar_Dt
			, Max(LastKnownTranDt) As LastKnownTranDt
			, Max(LastKnownTranDtPD) As LastKnownTranDtPD
		From
		(
			/* 
				This sql selects all sales per contact and returns all transaction rows 
				with a max transaction date per week and previous week
				The sql on top returns a single record with the values requested
			*/
			Select 
				caar.Parent_Account_Seq_Num As Member_Account_Seq_Num
			   , cstlc.Contact_Account_Seq_Num
			   , cad.Calendar_Dt
			   , Max(cstlc.Tran_Dt_DD) OVER (PARTITION BY caar.Parent_Account_Seq_Num, cstlc.Contact_Account_Seq_Num, cad.Calendar_Dt ORDER BY  cad.Calendar_Dt DESC )As LastKnownTranDt
			   , Max(Case When cstlc.Tran_Dt_DD<cad.Calendar_Dt Then cstlc.Tran_Dt_DD else Null End ) OVER (PARTITION BY caar.Parent_Account_Seq_Num, cstlc.Contact_Account_Seq_Num, cad.Calendar_Dt ORDER BY  cad.Calendar_Dt DESC ROWS UNBOUNDED PRECEDING)As LastKnownTranDtPD
			From ${DB_ENV}TgtVOUT.SALES_TRANSACTION As cst
			Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT As cstlc
			 On cst.Sales_Tran_Seq_Num = cstlc.Sales_Tran_Seq_Num
			 And cst.Store_Seq_Num = cstlc.Store_Seq_Num
			 And cst.Tran_Dt_DD = cstlc.Tran_Dt_DD
			Inner Join ${DB_ENV}TgtVOUT.ACCT_ACCT_RELATIONSHIP As caar
			 On cstlc.Contact_Account_Seq_Num = caar.Account_Seq_Num 
			Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As cad
			 On cst.Tran_Dt_DD between :v_TranDt - INTERVAL '91' DAY And :v_TranDt
			 And cad.Calendar_Dt_End_Dttm Between caar.Valid_From_Dttm And caar.Valid_To_Dttm 

			Where cst.Training_Mode_Status = 'N'
			 And cst.Tran_Status_Cd in ('CT')
			 And cst.Tran_Type_Cd in ('POS')
			 
			 and cad.Calendar_Dt = :v_TranDt
			 
		) AllSalesPerContact
		Group By Member_Account_Seq_Num
			, Contact_Account_Seq_Num
			, Calendar_Dt
	) As POSperiod
	 On POSperiod.Member_Account_Seq_Num =  lma.Member_Account_Seq_Num
	 And POSPeriod.Calendar_Dt = cald.Calendar_Dt
	/* filter to limit the date multiply And starting account selection*/
	Where	ca.Account_Type_Cd = 'CON'
		And	cald.Calendar_dt Between  '2012-01-01' And  :v_TranDt
		And coalesce(lmasb.Loyalty_Status_Cd,'')  <> 'Pre Alloted' 
	Group by 1,2,3,4,5,6,7,8,9,10,11,12
	) as lmf
	( 
		Loyalty_Program_Seq_Num,
		Calendar_Dt,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Member_Account_Seq_Num, 
		Contact_Account_Seq_Num,
		Day_Home_Store_Seq_Num,
		Week_Home_Store_Seq_Num,
		Month_Home_Store_Seq_Num,
		Day_Member_Tier_Lvl_Seq_Num,
		Week_Member_Tier_Lvl_Seq_Num,
		Month_Member_Tier_Lvl_Seq_Num,
		Member_Ind,
		Contact_Ind,
		New_Member_Ind,
		Active_Member_Ind,
		Active_Contact_Ind,
		ReActived_Member_Ind,
		Lost_Member_Ind
	)
	
	Where lmf.Calendar_Dt = :v_TranDt
	Group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
	;

	SET i = i + 1;
	
	IF i >= v_Days THEN
		LEAVE WHILE_LOOP;
	END IF;

	SET v_TranDt = v_TranDt + 1;

END WHILE WHILE_LOOP
;

END P0
;
