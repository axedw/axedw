/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Upd_DIST_PRODUCTIVITY_F.btq 27451 2019-03-19 14:36:20Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2019-03-19 15:36:20 +0100 (tis, 19 mar 2019) $
# Last Revision    : $Revision: 27451 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/procedure/Upd_DIST_PRODUCTIVITY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}SemCMNVIN.Upd_DIST_PRODUCTIVITY_F() SQL SECURITY INVOKER

P0: BEGIN

-- Global continue-handler for dynamic partitioning creation error 9247
DECLARE T9247 CONDITION FOR SQLSTATE VALUE 'T9247';
DECLARE CONTINUE HANDLER FOR T9247 BEGIN END;


CREATE VOLATILE TABLE DIST_PRODUCTIVITY_F_CAL, NO LOG
as
(
select calendar_dt,Calendar_Week_Id,Calendar_Month_Id 
FROM ${DB_ENV}TgtVOUT.CALENDAR_DATE 
WHERE calendar_dt
Between 
coalesce((select calendar_dt from ${DB_ENV}TgtVOUT.CALENDAR_DATE where calendar_month_id in 
(select calendar_month_id from ${DB_ENV}TgtVOUT.CALENDAR_DATE where Calendar_Dt = (current_date - 50))
and day_of_Month_Num_DD = 1),(current_date-1)) And (current_date - 1)
)
with data
Primary index(calendar_dt)
on commit preserve rows
;

Collect statistics
index(calendar_dt)
On DIST_PRODUCTIVITY_F_CAL
;

DELETE FROM ${DB_ENV}SemCMNT.DIST_PRODUCTIVITY_F_1 where Event_Dt_DD in (select calendar_dt from DIST_PRODUCTIVITY_F_CAL)
;

INSERT INTO ${DB_ENV}SemCMNT.DIST_PRODUCTIVITY_F_1(
Event_Dt_DD
,Location_Seq_Num
,Location_Zone_Seq_Num
,Concept_Cd
,Event_Subtype_Cd
,User_Distribution_Id
,Event_Category_Cd
,Distribution_Event_Subtype_Cd
,To_Storage_Location_Id
,To_Equipment_Id
,From_Storage_Location_Type
,Actual_Qty
,Row_Cnt
,Filter_Row_Cnt
,Weight_Meas
)
SELECT
de1.Event_Dt_DD,
de1.Location_Seq_Num,
de1.Location_Zone_Seq_Num,
de1.Concept_cd,
de1.Event_Subtype_Cd,
de1.User_Distribution_Id,
de1.Event_Category_Cd,
de1.Distribution_Event_Subtype_Cd,
case when Coalesce(pr1.To_Storage_Location_Id,'-1') in ('','PND','HBPD') then pr1.To_Storage_Location_Id else '-1' end As To_Storage_Location_Id,
case when Coalesce(pr1.To_Equipment_Id,'-1') = '' then pr1.To_Equipment_Id else '-1' end As To_Equipment_Id,
case when Coalesce(pr1.From_Storage_Location_Type,'-1') in ('9020') then pr1.From_Storage_Location_Type else '-1' end As From_Storage_Location_Type,
sum(de1.Actual_Qty) as Actual_Qty,
count(1) as Row_Cnt,
sum(case when de1.Actual_Qty <> 0 then 1 else 0 end) as Filter_Row_Cnt,
sum(Coalesce(dep1.Weight_Meas,0)) as Weight_Meas
From ${DB_ENV}TgtVOUT.DISTRIBUTION_EVENT de1

Left Outer Join ${DB_ENV}TgtVOUT.PIECE_ROUTING pr1
On de1.Event_Seq_Num = pr1.Event_Seq_Num
And de1.Distribution_Event_pos = pr1.Distribution_Event_pos
And de1.Event_Dt_DD = pr1.Event_Dt_DD

Left Outer Join (
SELECT 
Event_Seq_Num,Distribution_Event_pos,Event_Dt_DD,
case when coalesce(Valuation_Weight,0) <> 0 then coalesce(Valuation_Weight,0) else coalesce(Weight_Meas,0) end as Weight_Meas
FROM ${DB_ENV}TgtVOUT.DISTRIBUTION_EVENT_PIECE) as dep1
On de1.Event_Seq_Num = dep1.Event_Seq_Num
And de1.Distribution_Event_pos = dep1.Distribution_Event_pos
And de1.Event_Dt_DD = dep1.Event_Dt_DD

Inner Join DIST_PRODUCTIVITY_F_CAL As c
On de1.Event_Dt_DD = c.Calendar_Dt	

--where de1.Event_Dt_DD >= date '2017-01-01'

group by 1,2,3,4,5,6,7,8,9,10,11
;

COLLECT STATISTICS ON ${DB_ENV}SemCMNT.DIST_PRODUCTIVITY_F_1
;

-- 1 TMP
DELETE FROM ${DB_ENV}SemCMNT.TMP_DIST_PRODUCTIVITY_F
;

--event 2 TMP
INSERT INTO ${DB_ENV}SemCMNT.TMP_DIST_PRODUCTIVITY_F(
Location_Seq_Num, 
Location_Zone_Seq_Num, 
Calendar_Dt, 
Hour_Of_Day,
Associate_Seq_Num, 
Associate_Manager_Seq_Num, 
User_Group_Cd,
Operation_Equipment_Id,
Parcel_Qty,
Parcel_Pick_Qty, 
Parcel_Pick_Weighting_Qty, 
Parcel_Pick_Row_Cnt,
Parcel_Pick_Filter_Row_Cnt,
Lift_Pallet_In_Qty, 
Lift_Refill_Qty,
Lift_Pallet_Out_Qty, 
Lift_Pallet_Transfer_Qty, 
Lift_Pallet_HighBay_Out_Qty,
Lift_Mission_Qty, 
Pallet_Qty, 
Pallet_Pick_Qty, 
Pallet_Weighting_Qty,
Undefined_Parcel_Qty, 
Undefined_Pallet_Qty,
Pallet_Weight,
Parcel_Weight,
Lift_Refill_Weight,
Lift_Pallet_In_Weight
)

SELECT 
Location_Seq_Num,
Location_Zone_Seq_Num,
Calendar_Dt,
Hour_Of_Day,
Associate_Seq_Num,
Associate_Manager_Seq_Num,
User_Group_Cd,
Operation_Equipment_Id,
sum(Parcel_Pick_Qty) as Parcel_Qty,
sum(Parcel_Pick_Qty) as Parcel_Pick_Qty,
sum(Parcel_Pick_Weighting_Qty) as Parcel_Pick_Weighting_Qty, 
sum(Parcel_Pick_Row_Cnt) as Parcel_Pick_Row_Cnt,
sum(Parcel_Pick_Filter_Row_Cnt) as Parcel_Pick_Filter_Row_Cnt,
sum(Lift_Pallet_In_Qty) as Lift_Pallet_In_Qty,
sum(Lift_Refill_Qty) as Lift_Refill_Qty,
sum(Lift_Pallet_Out_Qty+Lift_Pallet_Out_Pretran_Qty) as Lift_Pallet_Out_Qty,
sum(Lift_Pallet_Transfer_Qty) as Lift_Pallet_Transfer_Qty,
sum(Lift_Pallet_HighBay_Out_Qty) as Lift_Pallet_HighBay_Out_Qty,
sum(Lift_Pallet_In_Qty+Lift_Pallet_Out_Qty+Lift_Refill_Qty+Lift_Pallet_Transfer_Qty+Lift_Pallet_HighBay_Out_Qty) as Lift_Mission_Qty,
sum(Lift_Pallet_Out_Qty+Lift_Pallet_HighBay_Out_Qty) as Pallet_Qty,
sum(Pallet_Pick_Qty) as Pallet_Pick_Qty,
sum((Lift_Pallet_Out_Qty+Lift_Pallet_HighBay_Out_Qty+Lift_Pallet_Out_Pretran_Qty)*Weighting_Val_Pallet_Qty) as Pallet_Weighting_Qty,
sum(Undefined_Parcel_Qty) as Undefined_Parcel_Qty,
sum(Undefined_Pallet_Qty) as Undefined_Pallet_Qty,
sum(Pallet_Pick_Weight) as Pallet_Weight,
sum(Parcel_Pick_Weight) as Parcel_Weight,
sum(Lift_Refill_Weight) as Lift_Refill_Weight,
sum(Lift_Pallet_In_Weight) as Lift_Pallet_In_Weight
FROM
(SELECT 
    coalesce(mpdz.To_Location_Seq_Num,de.Supply_Location_Seq_Num) as Location_Seq_Num
  , coalesce(mpdz.To_Location_Zone_Seq_Num,de.Supply_Location_Zone_Seq_Num) as Location_Zone_Seq_Num
  , de.Associate_Seq_Num
  , de.Event_Dt as Calendar_Dt
  --, Cast(substr('00'||Cast(de.Event_Hour as Char(2)),Length(Cast(de.Event_Hour as varChar(2)))+1,2)||':00:00' As Time(0)) As Hour_Of_Day
  , cast('00:00:00' as Time(0)) as Hour_Of_Day
  , COALESCE(am.Associate_Manager_Seq_Num,-1) AS Associate_Manager_Seq_Num
  , Case when Trim(Coalesce(deg.User_Group_Cd,'Default')) = '' Then 'Default' Else Trim(Coalesce(deg.User_Group_Cd,'Default')) End As User_Group_Cd
  , COALESCE(de.Operation_Equipment_Id,'-1') AS Operation_Equipment_Id
  
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 0 And de.Event_Subtype_Cd not in ('RETR') And de.Event_Category_Cd in ('2010','2012','2030') Then Actual_Qty Else 0 End) As Parcel_Pick_Qty
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 0 And de.Event_Subtype_Cd not in ('RETR') And de.Event_Category_Cd in ('2010','2012','2030') Then (Actual_Qty * Weighting_Val) Else 0 End) As Parcel_Pick_Weighting_Qty   
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 1 And coalesce(To_Storage_Location_ID,'') <> '' Then Row_Cnt Else 0 End) As Pallet_Pick_Qty
  , (Case When Dist_Mission_Cd = 'INFACKNING' And coalesce(To_Equipment_Id,'') = '' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And User_Distribution_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020' Then Row_Cnt Else 0 End) As Lift_Pallet_In_Qty
  , (Case When Dist_Mission_Cd = 'HELPALL' And coalesce(To_Equipment_Id,'') = '' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And User_Distribution_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020'  Then Row_Cnt Else 0 End) As Lift_Pallet_Out_Qty
  , (Case When Dist_Mission_Cd = 'PAFYLLNING' And coalesce(To_Equipment_Id,'') = '' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And User_Distribution_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020' Then Row_Cnt Else 0 End) As Lift_Refill_Qty
  , (Case When Dist_Mission_Cd = 'FLYTT' And coalesce(To_Equipment_Id,'') = '' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And User_Distribution_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020' Then Row_Cnt Else 0 End) As Lift_Pallet_Transfer_Qty
  , (Case When Dist_Mission_Cd = 'HBAY' And coalesce(To_Storage_Location_ID,'') <> '' Then Row_Cnt Else 0 End) As Lift_Pallet_HighBay_Out_Qty
  , (Case When Dist_Mission_Cd = '' Then Actual_Qty Else 0 End) As Undefined_Parcel_Qty
  , (Case When Dist_Mission_Cd = '' Then Row_Cnt Else 0 End) As Undefined_Pallet_Qty  
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 0 And de.Event_Subtype_Cd not in ('RETR') And de.Event_Category_Cd in ('2010','2012','2030') Then Row_Cnt Else 0 End) As Parcel_Pick_Row_Cnt
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 0 And de.Event_Subtype_Cd not in ('RETR') And de.Event_Category_Cd in ('2010','2012','2030') Then Filter_Row_Cnt Else 0 End) As Parcel_Pick_Filter_Row_Cnt
  , de.Weighting_Val_Pallet_Qty
  , (Case When Dist_Mission_Cd = 'HELPALL' And coalesce(To_Equipment_Id,'') = '' Then Weight_Meas Else 0 End) As Pallet_Pick_Weight
  , (Case When Dist_Mission_Cd = '-1' And Pallet_Ind = 0 And de.Event_Subtype_Cd not in ('RETR') And de.Event_Category_Cd in ('2010','2012','2030') Then Weight_Meas Else 0 End) As Parcel_Pick_Weight
  , (Case When Dist_Mission_Cd = 'PAFYLLNING' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And User_Distribution_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020' Then Weight_Meas Else 0 End) As Lift_Refill_Weight
  , (Case When Dist_Mission_Cd = 'INFACKNING' And coalesce(To_Equipment_Id,'') = '' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And User_Distribution_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020' Then Weight_Meas Else 0 End) As Lift_Pallet_In_Weight
  , (Case When de.Helpall_Pretran_Ind = 1 And coalesce(To_Equipment_Id,'') = '' And coalesce(To_Storage_Location_ID,'') not in ('','PND','HBPD') And User_Distribution_Id <> 'UNIBATCH' And From_Storage_Location_Type <> '9020'  Then Row_Cnt Else 0 End) As Lift_Pallet_Out_Pretran_Qty  
FROM ${DB_ENV}SemCMNVIN.DIST_PRODUCTIVITY_F_1 de

LEFT OUTER JOIN ${DB_ENV}SemMetadataVOUT.MSTR_PRODUCTIVITY_DC_ZONE mpdz
on de.Supply_Location_Seq_Num = mpdz.From_Location_Seq_Num and de.Supply_Location_Zone_Seq_Num = mpdz.From_Location_Zone_Seq_Num

LEFT OUTER JOIN ${DB_ENV}SemCmnVOUT.DIST_EVENT_GROUP_D deg
ON de.Dist_Event_Group_Seq_Num = deg.Dist_Event_Group_Seq_Num
  
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ASSOCIATE_MANAGER am
ON de.Associate_Seq_Num = am.Associate_Seq_Num and am.Valid_To_Dt = '9999-12-31'

LEFT OUTER JOIN (
SELECT	
Loc_Seq_Num,Location_Zone_Seq_Num,max(Calendar_Dt) as max_Calendar_Dt
FROM ${DB_ENV}SemMetadataVOUT.MSTR_DIST_EVENT_MANUAL_M
group by Loc_Seq_Num,Location_Zone_Seq_Num
) as ma1
on de.Supply_Location_Seq_Num = ma1.Loc_Seq_Num and de.Supply_Location_Zone_Seq_Num = ma1.Location_Zone_Seq_Num and de.Event_Dt < ma1.max_Calendar_Dt

where de.Event_Dt >= date '2017-01-01' and ma1.Loc_Seq_Num is null
	
) as x

group by 1,2,3,4,5,6,7,8
;

COLLECT STATISTICS ON ${DB_ENV}SemCMNT.TMP_DIST_PRODUCTIVITY_F
;

--event manual 2b

MERGE INTO ${DB_ENV}SemCMNT.TMP_DIST_PRODUCTIVITY_F as tgt
USING 

(
Select 
coalesce(mpdz.To_Location_Seq_Num,m1.Loc_Seq_Num) as Location_Seq_Num,
coalesce(mpdz.To_Location_Zone_Seq_Num,m1.Location_Zone_Seq_Num) as Location_Zone_Seq_Num,
m1.Calendar_Dt,
cast('00:00:00' as Time(0)) as Hour_Of_Day,
-1 as Associate_Seq_Num,
-1 as Associate_Manager_Seq_Num,
'Default' as User_Group_Cd,
'-1' as Operation_Equipment_Id,
m1.Nbr_of_Parcels as Parcel_Qty,
m1.Nbr_of_Parcels as Parcel_Pick_Qty,
m1.Nbr_of_Parcels as Parcel_Pick_Weighting_Qty,
m1.Nbr_of_Pallet as Lift_Mission_Qty,
m1.Nbr_of_Pallet as Pallet_Qty,
m1.Nbr_of_Pallet as Pallet_Pick_Qty,
(m1.Nbr_of_Pallet * 10) as Pallet_Weighting_Qty,
m1.Nbr_of_Pallet as Lift_Pallet_Out_Qty

FROM ${DB_ENV}SemMetadataVOUT.MSTR_DIST_EVENT_MANUAL_M as m1

LEFT OUTER JOIN ${DB_ENV}SemMetadataVOUT.MSTR_PRODUCTIVITY_DC_ZONE mpdz
on m1.Loc_Seq_Num = mpdz.From_Location_Seq_Num and m1.Location_Zone_Seq_Num = mpdz.From_Location_Zone_Seq_Num

 ) as y
 ON tgt.Location_Seq_Num = y.Location_Seq_Num and tgt.Location_Zone_Seq_Num = y.Location_Zone_Seq_Num 
 and tgt.Calendar_Dt = y.Calendar_Dt and tgt.Hour_Of_Day = y.Hour_Of_Day
 and tgt.Associate_Seq_Num = y.Associate_Seq_Num and tgt.Associate_Manager_Seq_Num = y.Associate_Manager_Seq_Num
 and tgt.User_Group_Cd = y.User_Group_Cd and tgt.Operation_Equipment_Id = y.Operation_Equipment_Id 
 
 WHEN MATCHED THEN UPDATE SET 

 Parcel_Qty = (y.Parcel_Qty + coalesce(tgt.Parcel_Qty,0)),
 Parcel_Pick_Qty = (y.Parcel_Pick_Qty + coalesce(tgt.Parcel_Pick_Qty,0)),
 Parcel_Pick_Weighting_Qty = (y.Parcel_Pick_Weighting_Qty + coalesce(tgt.Parcel_Pick_Weighting_Qty,0)),
 Lift_Mission_Qty = (y.Lift_Mission_Qty + coalesce(tgt.Lift_Mission_Qty,0)),
 Pallet_Qty = (y.Pallet_Qty + coalesce(tgt.Pallet_Qty,0)),
 Pallet_Pick_Qty = (y.Pallet_Pick_Qty + coalesce(tgt.Pallet_Pick_Qty,0)),
 Pallet_Weighting_Qty = (y.Pallet_Weighting_Qty + coalesce(tgt.Pallet_Weighting_Qty,0)),
 Lift_Pallet_Out_Qty = (y.Lift_Pallet_Out_Qty + coalesce(tgt.Lift_Pallet_Out_Qty,0))
  
 WHEN NOT MATCHED THEN INSERT( 
 
Location_Seq_Num, 
Location_Zone_Seq_Num, 
Calendar_Dt, 
Hour_Of_Day,
Associate_Seq_Num, 
Associate_Manager_Seq_Num, 
User_Group_Cd,
Operation_Equipment_Id,
Parcel_Qty,
Parcel_Pick_Qty, 
Parcel_Pick_Weighting_Qty, 
Lift_Mission_Qty, 
Pallet_Qty, 
Pallet_Pick_Qty, 
Pallet_Weighting_Qty,
Lift_Pallet_Out_Qty
 )   
VALUES (

 y.Location_Seq_Num,
 y.Location_Zone_Seq_Num,
 y.Calendar_Dt,
 y.Hour_Of_Day,
 y.Associate_Seq_Num,
 y.Associate_Manager_Seq_Num,
 y.User_Group_Cd,
 y.Operation_Equipment_Id,
 y.Parcel_Qty,
 y.Parcel_Pick_Qty,
 y.Parcel_Pick_Weighting_Qty,
 y.Lift_Mission_Qty,
 y.Pallet_Qty,
 y.Pallet_Pick_Qty,
 y.Pallet_Weighting_Qty,
 y.Lift_Pallet_Out_Qty

 ) 

;

COLLECT STATISTICS ON ${DB_ENV}SemCMNT.TMP_DIST_PRODUCTIVITY_F
;

--labor 3 TMP

MERGE INTO ${DB_ENV}SemCMNT.TMP_DIST_PRODUCTIVITY_F as tgt
USING 

(
SELECT 
Location_Seq_Num,
Location_Zone_Seq_Num,
Calendar_Dt,
Hour_Of_Day,
Associate_Seq_Num,
Associate_Manager_Seq_Num,
User_Group_Cd,
Operation_Equipment_Id,
sum(Total_Actual_Minute_Qty) as Total_Actual_Minute_Qty,
sum(Picking_Actual_Minute_Qty+Lift_Actual_Minute_Qty+Admin_Actual_Minute_Qty) As Actual_Minute_Qty,
sum(Picking_Actual_Minute_Qty) as Picking_Actual_Minute_Qty,
sum(Lift_Actual_Minute_Qty) as Lift_Actual_Minute_Qty,
sum(Undefined_Actual_Minute_Qty) as Undefined_Actual_Minute_Qty
FROM
 (SELECT 
    coalesce(mpdz.To_Location_Seq_Num,lb.Location_Seq_Num) as Location_Seq_Num
  , coalesce(mpdz.To_Location_Zone_Seq_Num,lb.Location_Zone_Seq_Num) as Location_Zone_Seq_Num
  , Case When Shift_Type_Cd = 'Medarbetare' Then lb.Associate_Seq_Num Else -1 End As Associate_Seq_Num
  , lb.Labor_Dt as Calendar_Dt
  --, lb.Labor_Hour_Of_Day_Tm As Hour_Of_Day
  , cast('00:00:00' as Time(0)) as Hour_Of_Day
  , Associate_Manager_Seq_Num
  , Case when Trim(Coalesce(wsc.User_Group_Cd,'Default')) = '' Then 'Default' Else Trim(Coalesce(wsc.User_Group_Cd,'Default')) End As User_Group_Cd
  , '-1' as Operation_Equipment_Id
  , (Actual_Labor_Minute_Qty+Actual_OT_Labor_Minute_Qty+ReAlloc_Actual_Labor_Minute_Qty+ReAlloc_Actual_OT_Labor_Minute_Qty) As Total_Actual_Minute_Qty
  , Case When Shift_Class_Cd = 'Plock' Then (Actual_Labor_Minute_Qty+Actual_OT_Labor_Minute_Qty+ReAlloc_Actual_Labor_Minute_Qty+ReAlloc_Actual_OT_Labor_Minute_Qty) Else 0 End As Picking_Actual_Minute_Qty
  , Case When Shift_Class_Cd = 'Lyft' Then (Actual_Labor_Minute_Qty+Actual_OT_Labor_Minute_Qty+ReAlloc_Actual_Labor_Minute_Qty+ReAlloc_Actual_OT_Labor_Minute_Qty) Else 0 End As Lift_Actual_Minute_Qty
  , Case When Shift_Class_Cd = 'Lager' Then (Actual_Labor_Minute_Qty+Actual_OT_Labor_Minute_Qty+ReAlloc_Actual_Labor_Minute_Qty+ReAlloc_Actual_OT_Labor_Minute_Qty) Else 0 End As Admin_Actual_Minute_Qty
  , Case When Shift_Class_Cd Not In ( 'Lager','Lyft','Plock' ) Then (Actual_Labor_Minute_Qty+Actual_OT_Labor_Minute_Qty+ReAlloc_Actual_Labor_Minute_Qty+ReAlloc_Actual_OT_Labor_Minute_Qty) Else 0 End As Undefined_Actual_Minute_Qty
 FROM ${DB_ENV}SemCMNVIN.ASSOCIATE_LABOR_TASK_QUINYX lb
 
 LEFT OUTER JOIN ${DB_ENV}SemMetadataVOUT.MSTR_PRODUCTIVITY_DC_ZONE mpdz
on lb.Location_Seq_Num = mpdz.From_Location_Seq_Num and lb.Location_Zone_Seq_Num = mpdz.From_Location_Zone_Seq_Num
 
 INNER JOIN ${DB_ENV}SemCmnVOUT.WORK_SHIFT_CLASS_D wsc
  ON lb.Work_Shift_Class_Seq_Num = wsc.Work_Shift_Class_Seq_Num
  
 WHERE lb.Labor_Dt >= date '2017-01-01'
 ) as x
 
 group by 1,2,3,4,5,6,7,8
 ) as y
 ON tgt.Location_Seq_Num = y.Location_Seq_Num and tgt.Location_Zone_Seq_Num = y.Location_Zone_Seq_Num 
 and tgt.Calendar_Dt = y.Calendar_Dt and tgt.Hour_Of_Day = y.Hour_Of_Day
 and tgt.Associate_Seq_Num = y.Associate_Seq_Num and tgt.Associate_Manager_Seq_Num = y.Associate_Manager_Seq_Num
 and tgt.User_Group_Cd = y.User_Group_Cd and tgt.Operation_Equipment_Id = y.Operation_Equipment_Id
 
 WHEN MATCHED THEN UPDATE SET 
 
 Total_Actual_Minute_Qty = (y.Total_Actual_Minute_Qty + coalesce(tgt.Total_Actual_Minute_Qty,0)),
 Actual_Minute_Qty = (y.Actual_Minute_Qty + coalesce(tgt.Actual_Minute_Qty,0)),
 Picking_Actual_Minute_Qty = (y.Picking_Actual_Minute_Qty + coalesce(tgt.Picking_Actual_Minute_Qty,0)),
 Lift_Actual_Minute_Qty = (y.Lift_Actual_Minute_Qty + coalesce(tgt.Lift_Actual_Minute_Qty,0)),
 Undefined_Actual_Minute_Qty = (y.Undefined_Actual_Minute_Qty + coalesce(tgt.Undefined_Actual_Minute_Qty,0))
 
 WHEN NOT MATCHED THEN INSERT( 
 
 Location_Seq_Num,
 Location_Zone_Seq_Num,
 Calendar_Dt,
 Hour_Of_Day,
 Associate_Seq_Num,
 Associate_Manager_Seq_Num,
 User_Group_Cd,
 Operation_Equipment_Id,
 Total_Actual_Minute_Qty,
 Actual_Minute_Qty,
 Picking_Actual_Minute_Qty,
 Lift_Actual_Minute_Qty,
 Undefined_Actual_Minute_Qty
 )   
VALUES (

 y.Location_Seq_Num,
 y.Location_Zone_Seq_Num,
 y.Calendar_Dt,
 y.Hour_Of_Day,
 y.Associate_Seq_Num,
 y.Associate_Manager_Seq_Num,
 y.User_Group_Cd,
 y.Operation_Equipment_Id,
 y.Total_Actual_Minute_Qty,
 y.Actual_Minute_Qty,
 y.Picking_Actual_Minute_Qty,
 y.Lift_Actual_Minute_Qty,
 y.Undefined_Actual_Minute_Qty

 )
;

COLLECT STATISTICS ON ${DB_ENV}SemCMNT.TMP_DIST_PRODUCTIVITY_F
;

--labor 4 TMP

MERGE INTO ${DB_ENV}SemCMNT.TMP_DIST_PRODUCTIVITY_F as tgt
   USING 
(SELECT 
Location_Seq_Num,
Location_Zone_Seq_Num,
Calendar_Dt,
Hour_Of_Day,
Associate_Seq_Num,
Associate_Manager_Seq_Num,
User_Group_Cd,
Operation_Equipment_Id,
sum(Total_Actual_Minute_Qty) as Total_Actual_Minute_Qty,
sum(Picking_Actual_Minute_Qty+Lift_Actual_Minute_Qty+Admin_Actual_Minute_Qty) As Actual_Minute_Qty,
sum(Picking_Actual_Minute_Qty) as Picking_Actual_Minute_Qty,
sum(Lift_Actual_Minute_Qty) as Lift_Actual_Minute_Qty,
sum(Undefined_Actual_Minute_Qty) as Undefined_Actual_Minute_Qty
FROM
 (SELECT m3.Loc_Seq_Num as Location_Seq_Num,
m2.Location_Zone_Seq_Num,
d1.Calendar_Dt, 
cast('00:00:00' as Time(0)) as Hour_Of_Day,
-1 as Associate_Seq_Num,
-1 as Associate_Manager_Seq_Num,
Case when Trim(Coalesce(wsc.User_Group_Cd,'Default')) = '' Then 'Default' Else Trim(Coalesce(wsc.User_Group_Cd,'Default')) End As User_Group_Cd,
'-1' as Operation_Equipment_Id,
  (Reallocation_Value*60) As Total_Actual_Minute_Qty,
   Case When wsc.Shift_Class_Cd = 'Plock' Then (Reallocation_Value*60) Else 0 End As Picking_Actual_Minute_Qty,
   Case When wsc.Shift_Class_Cd = 'Lyft' Then (Reallocation_Value*60) Else 0 End As Lift_Actual_Minute_Qty,
   Case When wsc.Shift_Class_Cd = 'Lager' Then (Reallocation_Value*60) Else 0 End As Admin_Actual_Minute_Qty,
  Case When wsc.Shift_Class_Cd Not In ( 'Lager','Lyft','Plock' ) Then (Reallocation_Value*60) Else 0 End As Undefined_Actual_Minute_Qty
FROM ${DB_ENV}SemMetadataVOUT.MSTR_COST_REALLOCATION_RULES_M as r1
INNER JOIN ${DB_ENV}SemCMNVOUT.CALENDAR_DAY_D as d1
on d1.Calendar_Dt between r1.Valid_From_Dt and r1.Valid_To_Dt
INNER JOIN ${DB_ENV}MetaDataVOUT.REF_COST_CENTER_TO_LOCATION_ZONE as m1
on r1.To_N_Cost_Center_Id = m1.N_Cost_Center_Id
INNER JOIN ${DB_ENV}MetaDataVOUT.MAP_LOCATION_ZONE as m2
on m1.N_Location_Id = m2.N_Loc_Id and m2.N_Location_Zone_Id = m1.N_Location_Zone_Id
INNER JOIN ${DB_ENV}MetaDataVOUT.MAP_LOC as m3
on m1.N_Location_Id = m3.N_Loc_Id
INNER JOIN ${DB_ENV}SemCMNVOUT.WORK_SHIFT_CLASS_D as wsc
on r1.To_N_Cost_Center_Id = wsc.Cost_Center_Id and r1.To_Work_Shift_Cd = wsc.Work_Shift_Cd

where r1.To_Work_Shift_Cd = 'Administration'  and Apply_On_Subject_Area = 'ASSOCIATE_LABOR'
and d1.Calendar_Dt between date '2017-01-01' and  (current_date -1)
and d1.Day_Of_Week_Num between 1 and 5
) as x
 group by 1,2,3,4,5,6,7,8
) as y
 ON tgt.Location_Seq_Num = y.Location_Seq_Num and tgt.Location_Zone_Seq_Num = y.Location_Zone_Seq_Num 
 and tgt.Calendar_Dt = y.Calendar_Dt and tgt.Hour_Of_Day = y.Hour_Of_Day
 and tgt.Associate_Seq_Num = y.Associate_Seq_Num and tgt.Associate_Manager_Seq_Num = y.Associate_Manager_Seq_Num
 and tgt.User_Group_Cd = y.User_Group_Cd and tgt.Operation_Equipment_Id = y.Operation_Equipment_Id
 
 WHEN MATCHED THEN UPDATE SET 

 Total_Actual_Minute_Qty = (y.Total_Actual_Minute_Qty + coalesce(tgt.Total_Actual_Minute_Qty,0)),
 Actual_Minute_Qty = (y.Actual_Minute_Qty + coalesce(tgt.Actual_Minute_Qty,0)),
 Picking_Actual_Minute_Qty = (y.Picking_Actual_Minute_Qty + coalesce(tgt.Picking_Actual_Minute_Qty,0)),
 Lift_Actual_Minute_Qty = (y.Lift_Actual_Minute_Qty + coalesce(tgt.Lift_Actual_Minute_Qty,0)),
 Undefined_Actual_Minute_Qty = (y.Undefined_Actual_Minute_Qty + coalesce(tgt.Undefined_Actual_Minute_Qty,0))
 
 WHEN NOT MATCHED THEN INSERT( 
 
 Location_Seq_Num,
 Location_Zone_Seq_Num,
 Calendar_Dt,
 Hour_Of_Day,
 Associate_Seq_Num,
 Associate_Manager_Seq_Num,
 User_Group_Cd,
 Operation_Equipment_Id,
 Total_Actual_Minute_Qty,
 Actual_Minute_Qty,
 Picking_Actual_Minute_Qty,
 Lift_Actual_Minute_Qty,
 Undefined_Actual_Minute_Qty
 )   
VALUES (

 y.Location_Seq_Num,
 y.Location_Zone_Seq_Num,
 y.Calendar_Dt,
 y.Hour_Of_Day,
 y.Associate_Seq_Num,
 y.Associate_Manager_Seq_Num,
 y.User_Group_Cd,
 y.Operation_Equipment_Id,
 y.Total_Actual_Minute_Qty,
 y.Actual_Minute_Qty,
 y.Picking_Actual_Minute_Qty,
 y.Lift_Actual_Minute_Qty,
 y.Undefined_Actual_Minute_Qty

 )
 ;

COLLECT STATISTICS ON ${DB_ENV}SemCMNT.TMP_DIST_PRODUCTIVITY_F
;
 
END P0
;

