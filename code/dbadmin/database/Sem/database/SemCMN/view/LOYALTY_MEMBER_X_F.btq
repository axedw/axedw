/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOYALTY_MEMBER_X_F.btq 13764 2014-09-08 15:03:42Z K9105286 $
# Last Changed By  : $Author: K9105286 $
# Last Change Date : $Date: 2014-09-08 17:03:42 +0200 (mån, 08 sep 2014) $
# Last Revision    : $Revision: 13764 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/LOYALTY_MEMBER_X_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/


-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


Replace	View ${DB_ENV}SemCMNVOUT.LOYALTY_MEMBER_X_F 
( 
	Loyalty_Program_Seq_Num,
	Calendar_Dt,
	Member_Account_Seq_Num, 
	Current_Member_Status_Cd,
	Member_Ind,
--	Active_Member_Ind,
	Member_Cnt
)
As
Lock row for access
Select	
	lpm.Loyalty_Program_Seq_Num
	,cald.Calendar_Dt 
	,lma.Member_Account_Seq_Num
	,lmasb.Loyalty_Status_Cd As Current_Member_Status_Cd
	,Case 
		When lmasb_hist.Loyalty_Status_Cd = 'Active' 
		Or lmasb_hist.Loyalty_Status_Cd = 'Inactive' 
		Then 1 
		Else 0 
	End As Member_Ind
--	, lmdf.Active_Member_Ind
	,1 (SMALLINT) As Member_Cnt
/* Get all member accounts */
From	${DB_ENV}TgtVOUT.ACCT As ma 
Inner Join ${DB_ENV}TgtVOUT.LOYALTY_MEMBER_ACCOUNT As lma
	On	ma.Account_Seq_Num =  lma.Member_Account_Seq_Num 
/* And the validity time for each contact */
Inner Join ${DB_ENV}TgtVOUT.ACCOUNT_DETAILS_B As madb 
	On	ma.Account_Seq_Num = madb.Account_Seq_Num 
	And	madb.Valid_To_Dttm = SYSLIB.HighTSVal()
/* Join in calender in order to get a correct fact by day, from start date - 7 days (to be on the safe side) until current day */
Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As cald
	On	cald.Calendar_Dt Between madb.Account_Open_Dt - INTERVAL '7' DAY And CURRENT_DATE
/* And the validity time for each member */
/* Join in the program of the member */
Inner Join ${DB_ENV}TgtVOUT.LOYALTY_PROGRAM_MEMBER As lpm
	On	lma.Member_Account_Seq_Num = lpm.Member_Account_Seq_Num 
	And	cald.Calendar_Dt_End_Dttm Between lpm.Valid_From_Dttm And lpm.Valid_To_Dttm 
/* Join in the member status of the member */
Inner Join ${DB_ENV}TgtVOUT.LOYALTY_MEMBER_STATUS_B As lmasb_hist
	On	lma.Member_Account_Seq_Num = lmasb_hist.Member_Account_Seq_Num 
	And	cald.Calendar_Dt_End_Dttm Between lmasb_hist.Valid_From_Dttm And lmasb_hist.Valid_To_Dttm 
Inner Join ${DB_ENV}TgtVOUT.LOYALTY_MEMBER_STATUS_B As lmasb
	On	lma.Member_Account_Seq_Num = lmasb.Member_Account_Seq_Num 
	And	lmasb.Valid_To_Dttm = SYSLIB.HighTSVal()
/* last known transactions  per member */
/* Short term solution: Re-use semantic table for Active contact resulted in bad performance
Left Outer Join 
	( 
	Select Member_Account_Seq_Num
	, Calendar_Dt
	, Max(Active_Member_Ind) As Active_Member_Ind
	From ${DB_ENV}SemCMNT.LOYALTY_MEMBER_DAY_F
	Group by Member_Account_Seq_Num
	,Calendar_Dt) As lmdf
	On lma.Member_Account_Seq_Num = lmdf.Member_Account_Seq_Num
	And cald.Calendar_Dt = lmdf.Calendar_Dt */
/* filter to limit the date multiply And starting account selection*/
Where	ma.Account_Type_Cd = 'MEM'
	And	cald.Calendar_dt Between  '2012-01-01' And  CURRENT_DATE
    And coalesce(lmasb.Loyalty_Status_Cd,'')  <> 'Pre Alloted' 
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.LOYALTY_MEMBER_X_F IS '$Revision: 13764 $ - $Date: 2014-09-08 17:03:42 +0200 (mån, 08 sep 2014) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE