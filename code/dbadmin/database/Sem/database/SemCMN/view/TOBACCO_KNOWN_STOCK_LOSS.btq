/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: TOBACCO_KNOWN_STOCK_LOSS.btq  2020-02-28  a97884 $
# Last Changed By  : $Author: a97884 $
# Last Change Date : $Date: 2020-02-28  $
# Last Revision    : $Revision: 1 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/TOBACCO_KNOWN_STOCK_LOSS.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}SemCMNVOUT
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.TOBACCO_KNOWN_STOCK_LOSS
(
Scan_Code_Seq_Num ,
N_Store_Id        ,
Inv_Reg_Batch_Id    ,
Known_Stock_Loss_Adj_Reason_Cd,
eoId, 
FId ,
EventTime,
deactReason1,
gtin,
serialNumber
)
As
select
 kl.Scan_Code_Seq_Num                 AS Scan_Code_Seq_Num,
 s1.N_Store_Id                        AS N_Store_Id,
 kl.Inv_Reg_Batch_Id                  AS Inv_Reg_Batch_Id,
 kl.Known_Stock_Loss_Adj_Reason_Cd    AS Known_Stock_Loss_Adj_Reason_Cd,
 bdb1.Tobacco_Economic_Operator_Id    AS eoId ,
 bdb1.Tobacco_Facility_Id             AS FId ,
 kl.Adjustment_Dt                     AS eventTime,
 '3'                                  AS deactReason1,
 sc1.N_Scan_Cd                       AS gtin,
 kl.Traceability_Id                  AS serialNumber
FROM  ${DB_ENV}TgtVOUT.KNOWN_STOCK_LOSS  AS kl

INNER JOIN ${DB_ENV}TgtVOUT.STORE s1
        ON s1.Store_Seq_Num = kl.Location_Seq_Num
              
INNER JOIN ${DB_ENV}Tgtvout.LOCATION_ORGANIZATION     AS lo
        ON lo.Location_Seq_Num       = kl.Location_Seq_Num
       AND lo.Valid_To_Dttm        = syslib.hightsval()
              
INNER JOIN ${DB_ENV}TgtVOUT.BUSINESS_DETAILS_B         AS bdb1
        ON  bdb1.Org_Party_Seq_Num = lo.Org_Party_Seq_Num
       AND bdb1.valid_to_dttm = syslib.hightsval()
       AND bdb1.Tobacco_Purchase_Permit_Cd <> '-1'
 
INNER JOIN ${DB_ENV}TgtVOUT.MU_SCAN_CODE              AS sc1
        ON sc1.Scan_Code_Seq_num = kl.Scan_Code_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.SCAN_CODE_D             AS s2
        ON s2.Scan_Code_Seq_Num   = kl.Scan_Code_Seq_Num

INNER JOIN ${DB_ENV}SemCMNVOUT.ARTICLE_D               AS a1
        ON a1.ARTICLE_SEQ_NUM     = s2.ARTICLE_SEQ_NUM

INNER JOIN ${DB_ENV}TgtVOUT.ARTICLE_TRACEABILITY       AS atm
        ON atm.article_seq_num      = a1.article_seq_num
       AND atm.Valid_To_Dttm        = syslib.hightsval()
       AND atm.Traceability_Type_Cd = 'TO'

WHERE kl.Traceability_Id  <>'-1' ;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.TOBACCO_KNOWN_STOCK_LOSS IS '$Revision: 1 $ - $Date: 2020-05-08  $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
