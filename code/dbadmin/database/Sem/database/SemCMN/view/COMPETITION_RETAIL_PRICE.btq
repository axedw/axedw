/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: COMPETITION_RETAIL_PRICE.btq 32349 2020-06-12 09:23:12Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-06-12 11:23:12 +0200 (fre, 12 jun 2020) $
# Last Revision    : $Revision: 32349 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/COMPETITION_RETAIL_PRICE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
Database ${DB_ENV}SemCMNVOUT
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- We create the view

REPLACE VIEW ${DB_ENV}SemCMNVOUT.COMPETITION_RETAIL_PRICE

(
	Scan_Code_Seq_Num,
	Competition_Store_Seq_Num,
	Transaction_Dttm,
	Transaction_Dt,
	N_Scan_Cd,
	Retail_Item_Price,
	Retail_Item_Qty,
	UOM_Cd,
	UOM_Name,
	UOM_Category_Cd,
	UOM_Category_Name,
	UOM_Category_Desc,
	Discount_Item_Price,
	Discount_Item_Qty,
	ImageUrl,
	N_CategoryId,
	Discounted_UOM_Cd,
	Discounted_UOM_Name,
	Discounted_UOM_Category_Cd,
	CategoryDescription,
	Promotional,
	N_DiscountId,
	DiscountReason,
	ProductName,
	Barcoded,
	SoftDelete,
	Status,
	"UserName",
	SourceSystem,
	OpenJobRunId,
	CloseJobRunId,
	InsertDttm	
)
AS
SELECT
	CRP.Scan_Code_Seq_Num,
	CRP.Competition_Store_Seq_Num,
	CRP.Transaction_Dttm,
	CRP.Transaction_Dt,
	Coalesce(MSC.N_Scan_Cd,'-1'),
	CRP.Retail_Item_Price,
	CRP.Retail_Item_Qty,
	CRP.UOM_Cd,
	Coalesce(UOM.UOM_Name,'Saknas'),
	CRP.UOM_Category_Cd,
	Coalesce(UCA.UOM_Category_Name,'Saknas'),
	Coalesce(UCA.UOM_Category_Desc,'Saknas'),
	CRP.Discount_Item_Price,
	CRP.Discount_Item_Qty,
	CRP.ImageUrl,
	CRP.N_CategoryId,
	CRP.Discounted_UOM_Cd,
	Coalesce(DUOM.UOM_Name,'Saknas'),
	CRP.Discounted_UOM_Category_Cd,
	CRP.CategoryDescription,
	CRP.Promotional,
	CRP.N_DiscountId,
	CRP.DiscountReason,
	CRP.ProductName,
	CRP.Barcoded,
	CRP.SoftDelete,
	CRP.Status,
	CRP."UserName",
	Coalesce(J.FolderName,'-1') AS SourceSystem,
	CRP.OpenJobRunId,
	CRP.CloseJobRunId,
	CRP.InsertDttm
FROM ${DB_ENV}TgtVOUT.COMPETITION_RETAIL_PRICE CRP
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.MU_SCAN_CODE MSC ON MSC.Scan_Code_Seq_Num = CRP.Scan_Code_Seq_Num
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.UOM UOM ON UOM.UOM_Cd = CRP.UOM_Cd
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.UOM DUOM ON DUOM.UOM_Cd = CRP.Discounted_UOM_Cd
LEFT OUTER JOIN ${DB_ENV}TgtVOUT.UOM_CATEGORY UCA ON UCA.UOM_Category_Cd = CRP.UOM_Category_Cd
LEFT OUTER JOIN ${DB_ENV}MetadataVOUT.JobRun AS J ON CRP.OpenJobRunId = J.JobRunID
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.COMPETITION_RETAIL_PRICE IS '$Revision: 32349 $ - $Date: 2020-06-12 11:23:12 +0200 (fre, 12 jun 2020) $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
