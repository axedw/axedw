/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ANNUAL_SEASON_D.btq 22620 2017-08-08 12:52:19Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-08-08 14:52:19 +0200 (tis, 08 aug 2017) $
# Last Revision    : $Revision: 22620 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/ANNUAL_SEASON_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

--- The default database is set.--
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVOUT.ANNUAL_SEASON_D
(
	Annual_Season_Seq_Num
	, Season_Cd
	, Season_Year_Id
	, Season_Rollout_Period
	, Season_Name
	, Store_Calendar_Start_Dt 
	, Store_Calendar_End_Dt 
	, Warehouse_Calendar_Start_Dt 
	, Warehouse_Calendar_End_Dt
	, Season_Ind
) 
As
Locking Row For Access
Select 
	Anns.Annual_Season_Seq_Num
	, Annsdb.Season_Cd As Season_Cd
	, Anns.N_Calendar_Year_Id As Season_Year_Id
	, Anns.N_Rollout_Period_Id As Season_Rollout_Period
	, s.Season_Name
	, Annsdb.Store_Calendar_Start_Dt 
	, Annsdb.Store_Calendar_End_Dt 
	, Annsdb.Warehouse_Calendar_Start_Dt 
	, Annsdb.Warehouse_Calendar_End_Dt
	,Case When Anns.Annual_Season_Seq_Num In (-1) Then 'Nej' Else 'Ja' End As Season_Ind

From ${DB_ENV}TgtVOUT.ANNUAL_SEASON Anns
Inner Join ${DB_ENV}TgtVOUT.ANNUAL_SEASON_DETAILS_B Annsdb
On Annsdb.Annual_Season_Seq_Num = Anns.Annual_Season_Seq_Num
And Annsdb.Valid_To_Dttm = SYSLIB.HighTSVal()
Inner Join ${DB_ENV}TgtVOUT.SEASON s
On s.Season_Cd = Annsdb.Season_Cd

Union All -- Add record for missing masterdata
Select d0.Dummy_Seq_Num As Annual_Season_Seq_Num,
	'' As Season_Cd,
	null As Season_Year_Id,
	'' As Season_Rollout_Period,	
	'' As Season_Name,
	null As Store_Calendar_Start_Dt,
	null As Store_Calendar_End_Dt,
	null As Warehouse_Calendar_Start_Dt,
	null As Warehouse_Calendar_End_Dt,
	'Nej' as Season_Ind
From ${DB_ENV}SemCMNT.DUMMY As d0
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.ANNUAL_SEASON_D IS '$Revision: 22620 $ - $Date: 2017-08-08 14:52:19 +0200 (tis, 08 aug 2017) $'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
