/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ASSOCIATE_LABOR_TASK_TILTID.btq 24976 2018-05-29 11:23:44Z a54632 $
# Last Changed By  : $Author: a54632 $
# Last Change Date : $Date: 2018-05-29 13:23:44 +0200 (tis, 29 maj 2018) $
# Last Revision    : $Revision: 24976 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/ASSOCIATE_LABOR_TASK_TILTID.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVIN	 -- Change db name if needed
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVIN.ASSOCIATE_LABOR_TASK_TILTID
(
	Associate_Seq_Num
	, Location_Seq_Num
	, Location_Zone_Seq_Num 
	, Store_Seq_Num
	, DC_Seq_Num
	, Cost_Center_Seq_Num
	, Work_Shift_Cd
	, Labor_Dt
	, Labor_Hour_Of_Day_Tm
	, Work_Shift_Class_Seq_Num 
	, Associate_Manager_Seq_Num
	, Actual_Labor_Minute_Qty
	, Actual_Labor_Cost_Amt
	, Actual_OT_Labor_Cost_Amt
	, Actual_OT_Labor_Minute_Qty	
	, Actual_QOT_Labor_Minute_Qty
	, ReAlloc_Actual_Labor_Minute_Qty
	, ReAlloc_Actual_Labor_Cost_Amt
	, ReAlloc_Actual_OT_Labor_Cost_Amt
	, ReAlloc_Actual_OT_Labor_Minute_Qty	
	, ReAlloc_Actual_QOT_Labor_Minute_Qty
	, OB_Minute_Qty
	, ReAlloc_OB_Minute_Qty	
	, Wage_Rate_Type_Cd
	, Overtime_Wage_Rate_Type_Cd
	, Labor_Task_Status_Cd
	, Source_System_Cd 	
) As
    LOCK ROW FOR ACCESS 
SELECT 
        labortask.Associate_Seq_Num,
        labortask.Location_Seq_Num,
        labortask.Location_Zone_Seq_Num,
        labortask.Location_Seq_Num AS Store_Seq_Num,
        labortask.Location_Seq_Num AS DC_Seq_Num,
        labortask.Cost_Center_Seq_Num,
        labortask.Work_Shift_Cd,
        labortask.Labor_Dt,
        labortask.Labor_Hour_Of_Day_Tm,
        labortask.Dummy_Seq_Num AS Work_Shift_Class_Seq_Num,
        COALESCE(am.Associate_Manager_Seq_Num,-1) AS Associate_Manager_Seq_Num,
            
        CASE
            WHEN labortask.Overtime_Wage_Rate_Type_Cd = '-1' OR labortask.Overtime_Wage_Rate_Type_Cd IS NULL THEN labortask.Act_Labor_Min_Qty ELSE 0
        END AS Actual_Labor_Minute_Qty,
        
         labortask.Act_Labor_Min_Qty*labortask.CostPerMinute AS Actual_Labor_Cost_Amt,
         labortask.Act_Labor_Min_Qty*labortask.OTCostPerMinute AS Actual_OT_Labor_Cost_Amt,
                
        CASE
            WHEN labortask.Overtime_Wage_Rate_Type_Cd <> '-1' AND labortask.Overtime_Wage_Rate_Type_Cd IS NOT NULL THEN labortask.Act_Labor_Min_Qty ELSE 0
        END AS Actual_OT_Labor_Minute_Qty,
        
        CAST((COALESCE(wrt.Wage_Rate_Type_Percent,0)/100)*(
            CASE
                WHEN labortask.Overtime_Wage_Rate_Type_Cd <> '-1' AND labortask.Overtime_Wage_Rate_Type_Cd IS NOT NULL THEN labortask.Act_Labor_Min_Qty ELSE 0
            END) AS DECIMAL(9,3)) AS Actual_QOT_Labor_Minute_Qty,
            
            		/* Mått för omfördelning, Används ej av TILTID/Butik */
         0 AS ReAlloc_Actual_Labor_Minute_Qty,
		 0 AS ReAlloc_Actual_Labor_Cost_Amt,
         0 AS ReAlloc_Actual_OT_Labor_Cost_Amt,
	     0 AS ReAlloc_Actual_OT_Labor_Minute_Qty,
	     0 AS ReAlloc_Actual_QOT_Labor_Minute_Qty,
		 0 AS OB_Minute_Qty,
		 0 AS ReAlloc_OB_Minute_Qty,		 
		
         labortask.Wage_Rate_Type_Cd,
        COALESCE(labortask.Overtime_Wage_Rate_Type_Cd,'-1') AS Overtime_Wage_Rate_Type_Cd,
                
        CASE
            WHEN labortask.Labor_Task_Status_Cd = -1 THEN -2 ELSE 10+labortask.Labor_Task_Status_Cd
        END AS Labor_Task_Status_Cd,
		'TILTID' AS Source_System_Cd
		
        FROM ( /* Skapa grundmått per timme */
    SELECT 
        Associate_Seq_Num,
		alta.Location_Seq_Num AS Location_Seq_Num,
		alta.Location_Zone_Seq_Num AS Location_Zone_Seq_Num,
		alta.Cost_Center_Seq_Num,
		alta.Work_Shift_Cd,
		alta.Actual_Task_Status_Cd AS Labor_Task_Status_Cd,
		Actual_Task_Dt_DD AS Labor_Dt,
		hod.Time_Of_Day_Tm AS Labor_Hour_Of_Day_Tm,
		dummy.Dummy_Seq_Num,
		
		SUM(CASE
			WHEN CAST(alta.Actual_Task_Start_Dttm AS TIME) >=  hod.Time_Of_Day_Tm AND EXTRACT(HOUR FROM (alta.Actual_Task_End_Dttm)) =  EXTRACT(HOUR	FROM (hod.Time_Of_Day_Tm))  
				THEN (CAST(EXTRACT(MINUTE FROM (alta.Actual_Task_End_Dttm)) AS INTEGER) - CAST(EXTRACT(MINUTE	FROM (alta.Actual_Task_Start_Dttm)) AS INTEGER))
			WHEN CAST(alta.Actual_Task_Start_Dttm AS TIME) >=  hod.Time_Of_Day_Tm AND EXTRACT(HOUR FROM (alta.Actual_Task_End_Dttm)) <>  EXTRACT(HOUR FROM (hod.Time_Of_Day_Tm)) 
				THEN CAST(60 AS INTEGER) - CAST(EXTRACT(MINUTE FROM (alta.Actual_Task_Start_Dttm)) AS INTEGER)
			WHEN CAST(alta.Actual_Task_Start_Dttm AS TIME) <  hod.Time_Of_Day_Tm AND EXTRACT(HOUR FROM (hod.Time_Of_Day_Tm)) < EXTRACT(HOUR	FROM (alta.Actual_Task_End_Dttm)) 
				THEN CAST(60 AS INTEGER)
			WHEN CAST(alta.Actual_Task_Start_Dttm AS TIME) <=  hod.Time_Of_Day_Tm AND EXTRACT(HOUR	FROM (hod.Time_Of_Day_Tm)) = EXTRACT(HOUR	FROM (alta.Actual_Task_End_Dttm)) 
			THEN CAST(EXTRACT(MINUTE
			FROM (alta.Actual_Task_End_Dttm)) AS INTEGER)
                ELSE CAST(0 AS INTEGER)
		END ) AS Act_Labor_Min_Qty,
		
		SUM(Actual_Task_Cost_Amt) AS Actual_Labor_Task_Cost_Amt,
        SUM(Actual_Task_OT_Cost_Amt) AS Actual_Labor_Task_Overtime_Cost_Amt,
        Wage_Rate_Type_Cd AS Wage_Rate_Type_Cd,
        OT_Wage_Rate_Type_Cd AS Overtime_Wage_Rate_Type_Cd,
        
		SUM(((Actual_Task_Cost_Amt) /
		CASE
			WHEN alta.Actual_Task_End_Dttm > alta.Actual_Task_Start_Dttm 
			THEN ((EXTRACT(HOUR	FROM (alta.Actual_Task_End_Dttm))*60+EXTRACT(MINUTE FROM (alta.Actual_Task_End_Dttm)))- (EXTRACT(HOUR FROM (alta.Actual_Task_Start_Dttm))*60+EXTRACT(MINUTE FROM (alta.Actual_Task_Start_Dttm)))) 
			ELSE 1
		END)) / COUNT(*) AS CostPerMinute,
                
        SUM(((Actual_Task_OT_Cost_Amt) /
		CASE
			WHEN alta.Actual_Task_End_Dttm > alta.Actual_Task_Start_Dttm 
			THEN ((EXTRACT(HOUR	FROM (alta.Actual_Task_End_Dttm))*60+EXTRACT(MINUTE FROM (alta.Actual_Task_End_Dttm)))- (EXTRACT(HOUR	FROM (alta.Actual_Task_Start_Dttm))*60+EXTRACT(MINUTE	FROM (alta.Actual_Task_Start_Dttm)))) 
			ELSE 1
		END)) /COUNT(*)   AS OTCostPerMinute
		
		FROM  ${DB_ENV}TgtVOUT.ASSOCIATE_LABOR_TASK_ACTUAL alta
            
        JOIN ${DB_ENV}TgtVOUT.HOUR_OF_DAY hod ON EXTRACT(HOUR
			FROM (hod.Time_Of_Day_Tm)) BETWEEN EXTRACT(HOUR
    		FROM (alta.Actual_Task_Start_Dttm)) AND EXTRACT(HOUR
    		FROM (alta.Actual_Task_End_Dttm))
		JOIN ${DB_ENV}SemCMNT.DUMMY dummy
		ON Dummy_Seq_Num = -1
    GROUP BY 
        Associate_Seq_Num,
        alta.Location_Seq_Num,
        alta.Location_Zone_Seq_Num,
        alta.Cost_Center_Seq_Num,
        alta.Work_Shift_Cd,
        alta.Actual_Task_Status_Cd,
        Actual_Task_Dt_DD,
        hod.Time_Of_Day_Tm,
		dummy.Dummy_Seq_Num,
        Wage_Rate_Type_Cd,
        OT_Wage_Rate_Type_Cd
            ) labortask
            
    LEFT OUTER JOIN ${DB_ENV}TgtVOUT.WAGE_RATE_TYPE wrt 
		ON wrt.Wage_Rate_Type_Cd = labortask.Overtime_Wage_Rate_Type_Cd
    LEFT OUTER JOIN ${DB_ENV}TgtVOUT.COST_CENTER cc 
		ON cc.Cost_Center_Seq_Num = labortask.Cost_Center_Seq_Num
    LEFT OUTER JOIN ${DB_ENV}TgtVOUT.ASSOCIATE_MANAGER am
        ON labortask.Associate_Seq_Num = am.Associate_Seq_Num 
        AND labortask.Labor_Dt BETWEEN am.Valid_From_Dt AND am.Valid_To_Dt
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVIN.ASSOCIATE_LABOR_TASK_TILTID IS '$Revision: 24976 $ - $Date: 2018-05-29 13:23:44 +0200 (tis, 29 maj 2018) $ '
;

--SELECT TOP 1 * FROM ${DB_ENV}SemCMNVIN.ASSOCIATE_LABOR_TASK_TILTID;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
