/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOYALTY_MEMBER_WEEK_F.btq 11817 2013-12-15 19:14:54Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2013-12-15 20:14:54 +0100 (sön, 15 dec 2013) $
# Last Revision    : $Revision: 11817 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/LOYALTY_MEMBER_WEEK_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.LOYALTY_MEMBER_WEEK_F
(
	Member_Account_Seq_Num,
	Loyalty_Program_Seq_Num,
	Calendar_Week_Id,
	Member_Ind,
	New_Member_Ind,
	Active_Member_Ind,
	Contact_Ind,
	Active_Contact_Ind,
	Lost_Member_Ind,
	ReActived_Member_Ind,
	Member_Cnt,
	Member_Sum_Cnt,
	Member_Ord_Cnt,
	Member_Period_Cnt
)
as
Lock row for access
Select 
	Member_Account_Seq_Num,
	Loyalty_Program_Seq_Num,
	Calendar_Week_Id,

	Max(Member_Ind) as Member_Ind,
	Max(New_Member_Ind) as New_Member_Ind,
	Max(Active_Member_Ind) as Active_Member_Ind,
	Max(Contact_Ind) as Contact_Ind,
	Max(Active_Contact_Ind) as Active_Contact_Ind,
	Max(Lost_Member_Ind) as Lost_Member_Ind,
	Max(ReActived_Member_Ind) as ReActived_Member_Ind,

 	Member_Cnt,
	Member_Sum_Cnt,
	Member_Ord_Cnt_loc,
	Member_Period_Cnt_loc
	
From
(
	Select
		lmd.Member_Account_Seq_Num,
		lmd.Loyalty_Program_Seq_Num,
		lmd.Calendar_Week_Id,

		lmd.Member_Ind,
		lmd.New_Member_Ind,
		lmd.Active_Member_Ind,
		lmd.Contact_Ind,
		lmd.Active_Contact_Ind,
		lmd.Lost_Member_Ind,
		lmd.ReActived_Member_Ind,

		Count(lmd.Member_Account_Seq_Num) as Member_Cnt,
		Cast(1 as INTEGER) as Member_Sum_Cnt,
		
		Rank() over (partition by 
			lmd.Member_Account_Seq_Num,
			lmd.Calendar_Week_Id
		order by
			lmd.Member_Account_Seq_Num +
			lmd.Loyalty_Program_Seq_Num +
			lmd.Calendar_Week_Id +
			lmd.Member_Ind +
			lmd.New_Member_Ind +
			lmd.Active_Member_Ind +
			lmd.Contact_Ind +
			lmd.Active_Contact_Ind +
			lmd.Lost_Member_Ind +
			lmd.ReActived_Member_Ind
		desc
		) as Member_Ord_Cnt_loc,
		Case when Member_Ord_Cnt_loc = 1 Then 1 else 0 end as Member_Period_Cnt_loc

	from ${DB_ENV}SemCMNT.LOYALTY_MEMBER_DAY_F as lmd

	group by 
		lmd.Member_Account_Seq_Num,
		lmd.Loyalty_Program_Seq_Num,
		lmd.Calendar_Week_Id,
		lmd.Member_Ind,
		lmd.New_Member_Ind,
		lmd.Active_Member_Ind,
		lmd.Contact_Ind,
		lmd.Active_Contact_Ind,
		lmd.Lost_Member_Ind,
		lmd.ReActived_Member_Ind
) as a
Group by
	Member_Account_Seq_Num,
	Loyalty_Program_Seq_Num,
	Calendar_Week_Id,

 	Member_Cnt,
	Member_Sum_Cnt,
	Member_Ord_Cnt_loc,
	Member_Period_Cnt_loc
Where Member_Ord_Cnt_loc = 1 -- show only the row with most ones
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','LOYALTY_MEMBER_WEEK_F','POST','I',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.LOYALTY_MEMBER_WEEK_F IS '$Revision: 11817 $ - $Date: 2013-12-15 20:14:54 +0100 (sön, 15 dec 2013) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
