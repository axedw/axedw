/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRANSACTION_TOTALS_F.btq 29912 2020-01-16 16:39:25Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-01-16 17:39:25 +0100 (tor, 16 jan 2020) $
# Last Revision    : $Revision: 29912 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/SALES_TRANSACTION_TOTALS_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
--- The default database is set.--
Database	${DB_ENV}SemCMNVOUT;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_TOTALS_F 
( 
	Sales_Tran_Seq_Num,
	Sales_Tran_Id,
	Store_Seq_Num,
	Tran_Dt,
	Tran_End_Dttm,
	Tran_Tm,
	Hour_Of_Day,
	Store_Department_Seq_Num,
	Customer_Seq_Num,
	Loyalty_Account_Id,
	Contact_Account_Seq_Num,
	Loyalty_Identifier_Seq_Num,
	Receipt_Type_Cd,
	Delivery_Method_Cd,
	POS_Register_Seq_Num,
	Sales_Associate_Id,
	Junk_Seq_Num,
	Empl_Sales_Ind,
	Target_Ind,
	Return_Ind,
	Suspended_Ind,
	Suspended_EOD_Ind,
	Resumed_Ind,
	Resumed_Voided_Ind,
	Voided_Ind,
	Loyalty_Based_Amt,
	Personnel_Discount_Amt,
	Personnel_Discnt_Eligblty_Amt,
	Sales_Amt,
	Taxable_Amt,
	Tax_Amt,
	Grand_Amt,
	Total_Cost_Amt,
	Gross_Profit_Amt,
	Item_Cnt,
	Scanned_Item_Cnt,
	Tot_Discount_Amt,
	Tot_Loy_Discount_Amt,
	Post_Registered_Bonus_Amt,
	Receipt_Cnt,
	OpenJobRunId,
	Insert_Dttm
) As
Select 
	st1.Sales_Tran_Seq_Num,
	st1.N_Sales_Tran_Id Sales_Tran_Id,
	st1.Store_Seq_Num,
	st1.Tran_Dt_DD As Tran_Dt,
	st1.Tran_End_Dttm_DD As Tran_End_Dttm,
	CAST(st1.Tran_End_Dttm_DD AS TIME(6)) (FORMAT 'HH:MI:SS') As Tran_Tm,
	CAST(CAST(Tran_Tm as Char(2))||':00:00' as Time(0)) as Hour_Of_Day,
	st1.Store_Department_Seq_Num,
	Coalesce(stp.Party_Seq_Num,-1) As Customer_Seq_Num,
	Coalesce(stla.Loyalty_Account_Id,'-1') As Loyalty_Account_Id,
	Coalesce(stlc.Contact_Account_Seq_Num,-1) As Contact_Account_Seq_Num,
	Coalesce(stlim.Loyalty_Identifer_Seq_Num,-1) AS Loyalty_Identifier_Seq_Num, --FELSTAVAT Identifer
	Coalesce(stc.Circumstance_Value_Cd,'-1')	As Receipt_Type_Cd, 
	Coalesce(stdlmd.Circumstance_Value_Cd,'-1') As Delivery_Method_Cd,
	st1.POS_Register_Seq_Num,
	st1.Sales_Associate_Id,
	Coalesce(j1.Junk_Seq_Num, -1) As Junk_Seq_Num,
	Case  
		When Personnel_Discnt_Eligblty_Amt <> 0 Then 1
		Else 0 
	End As Empl_Sales_Ind,
	1 (SMALLINT) As Target_Ind,
	Case	
		When stl2.Sales_Tran_Seq_Num Is Not Null Then 1
		Else 0 
	End As Return_Ind	/* De kvitton som har returer */,
	0 (SMALLINT) As Suspended_Ind			/*  Alla kvitton hör till ej Parkerade kvitton*/,
	0 (SMALLINT) As Suspended_EOD_Ind		/*  All kvitton hör till ej Parkerade kvitton*/,
	Case	
		When  st1.Tran_Status_Cd = 'CT' 
		And st1.Related_Sales_Tran_Seq_Num Is Not Null Then 1
		Else 0 
	End As Resumed_Ind				/* återupptaget kvitto*. Tillagd av Fraud */,
	Case 
		When st1.Tran_Status_Cd = 'CT' 
			And st1.Related_Sales_Tran_Seq_Num Is Not Null 
			And stlc2.Sales_Tran_Seq_Num Is Not Null Then 1
		Else 0 
	End As Resumed_Voided_Ind				/* Parkerade kvitton med korrigeringar */,
	Case	
		When stlc2.Sales_Tran_Seq_Num Is Not Null
		Then 1
		Else 0 
	End As Voided_Ind				/* Kvitton med korrigeringar. Tillagd av Fraud */,
	Coalesce(sttla.Total_Amt , 0) As Loyalty_Based_Amt,
	Coalesce(sttpa.Total_Amt , 0) As Personnel_Discount_Amt,
	Coalesce(sttpd.Total_Amt , 0) As Personnel_Discnt_Eligblty_Amt,
	Coalesce(sttsa.Total_Amt , 0) As Sales_Amt,
	Coalesce(stttt.Total_Amt , 0) As Taxable_Amt,
	Coalesce(sttta.Total_Amt , 0) As Tax_Amt,
	Coalesce(sttga.Total_Amt , 0) As Grand_Amt,
	Coalesce(sttca.Total_Amt , 0) As Total_Cost_Amt,
	Coalesce(sttcs.Total_Amt , 0) As Gross_Profit_Amt,
	Coalesce(sttic.Total_Amt , 0) As Item_Cnt,
	Coalesce(sttsc.Total_Amt , 0) As Scanned_Item_Cnt,
	Coalesce(stttd.Total_Amt , 0) As Tot_Discount_Amt,
	Coalesce(sttld.Total_Amt , 0) As Tot_Loy_Discount_Amt,
	Coalesce(sttpr.Total_Amt , 0) As Post_Registered_Bonus_Amt,
	Case
		When Tran_Status_Cd = 'VO' Then -1 
		When Tran_Status_Cd = 'CT' Then 1
		Else 0 
	End	As Receipt_Cnt,
	st1.OpenJobRunID,
	st1.InsertDttm
From ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_VALIDATED As st1
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttla
	On st1.Sales_Tran_Seq_Num = sttla.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttla.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttla.Tran_Dt_DD
	And sttla.Tran_Type_Cd = 'LA' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttpa
	On st1.Sales_Tran_Seq_Num = sttpa.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttpa.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttpa.Tran_Dt_DD
	And sttpa.Tran_Type_Cd = 'PA' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttpd
	On st1.Sales_Tran_Seq_Num = sttpd.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttpd.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttpd.Tran_Dt_DD
	And sttpd.Tran_Type_Cd = 'PD' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttsa
	On st1.Sales_Tran_Seq_Num = sttsa.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttsa.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttsa.Tran_Dt_DD
	And sttsa.Tran_Type_Cd = 'SA' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As stttt
	On st1.Sales_Tran_Seq_Num = stttt.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = stttt.Store_Seq_Num 
	And st1.Tran_Dt_DD = stttt.Tran_Dt_DD
	And stttt.Tran_Type_Cd = 'TT' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttta
	On st1.Sales_Tran_Seq_Num = sttta.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttta.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttta.Tran_Dt_DD
	And sttta.Tran_Type_Cd = 'TA' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttga
	On st1.Sales_Tran_Seq_Num = sttga.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttga.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttga.Tran_Dt_DD
	And sttga.Tran_Type_Cd = 'GA' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttca
	On st1.Sales_Tran_Seq_Num = sttca.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttca.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttca.Tran_Dt_DD
	And sttca.Tran_Type_Cd = 'CA' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttcs
	On st1.Sales_Tran_Seq_Num = sttcs.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttcs.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttcs.Tran_Dt_DD
	And sttcs.Tran_Type_Cd = 'CS' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttic
	On st1.Sales_Tran_Seq_Num = sttic.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttic.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttic.Tran_Dt_DD
	And sttic.Tran_Type_Cd = 'IC' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttsc
	On st1.Sales_Tran_Seq_Num = sttsc.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttsc.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttsc.Tran_Dt_DD
	And sttsc.Tran_Type_Cd = 'SC' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As stttd
	On st1.Sales_Tran_Seq_Num = stttd.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = stttd.Store_Seq_Num 
	And st1.Tran_Dt_DD = stttd.Tran_Dt_DD
	And stttd.Tran_Type_Cd = 'TD' 
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttld
	On st1.Sales_Tran_Seq_Num = sttld.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttld.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttld.Tran_Dt_DD
	And sttld.Tran_Type_Cd = 'LD'  
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttpr
	On st1.Sales_Tran_Seq_Num = sttpr.Sales_Tran_Seq_Num 
	And st1.Store_Seq_Num = sttpr.Store_Seq_Num 
	And st1.Tran_Dt_DD = sttpr.Tran_Dt_DD
	And sttpr.Tran_Type_Cd = 'PLA'   
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT As stlc 
	On st1.Sales_Tran_Seq_Num = stlc.Sales_Tran_Seq_Num
	And st1.Store_Seq_Num = stlc.Store_Seq_Num 
	And st1.Tran_Dt_DD = stlc.Tran_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_ID_METHOD As stlim 
	On st1.Sales_Tran_Seq_Num = stlim.Sales_Tran_Seq_Num
	And st1.Store_Seq_Num = stlim.Store_Seq_Num 
	And st1.Tran_Dt_DD = stlim.Tran_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_PARTY As stp
 	On st1.Sales_Tran_Seq_Num = stp.Sales_Tran_Seq_Num
 	And st1.Store_Seq_Num = stp.Store_Seq_Num 
 	And st1.Tran_Dt_DD = stp.Tran_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOYALTY_ACCOUNT As stla
 	On st1.Sales_Tran_Seq_Num = stla.Sales_Tran_Seq_Num
 	And st1.Store_Seq_Num = stla.Store_Seq_Num 
 	And st1.Tran_Dt_DD = stla.Tran_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_CIRCUMSTANCE As stc
 	On st1.Sales_Tran_Seq_Num = stc.Sales_Tran_Seq_Num
 	And st1.Store_Seq_Num = stc.Store_Seq_Num 
 	And st1.Tran_Dt_DD = stc.Tran_Dt_DD
 	And stc.Circumstance_Cd = 'COTP'
Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_CIRCUMSTANCE As stdlmd
 	On st1.Sales_Tran_Seq_Num = stdlmd.Sales_Tran_Seq_Num
 	And st1.Store_Seq_Num = stdlmd.Store_Seq_Num 
 	And st1.Tran_Dt_DD = stdlmd.Tran_Dt_DD
 	And stdlmd.Circumstance_Cd = 'DLMD'	
Left Outer Join (
	Select 
		stlc1.Sales_Tran_Seq_Num,
		stlc1.Store_Seq_Num,
		stlc1.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRAN_LINE_CIRCUMSTANCE As stlc1
		Where 	stlc1.Circumstance_Cd='VOID' 
		And	stlc1.Circumstance_Value_Cd In ('PA','LI') 
		Group by 
		stlc1.Sales_Tran_Seq_Num,
		stlc1.Store_Seq_Num,
		stlc1.Tran_Dt_DD
		) 	stlc2
	On	st1.Sales_Tran_Seq_Num = stlc2.Sales_Tran_Seq_Num
	And	st1.Store_Seq_Num =  stlc2.Store_Seq_Num
	And	st1.Tran_Dt_DD = stlc2.Tran_Dt_DD
Left Outer Join (
	Select 
		stl1.Sales_Tran_Seq_Num,
		stl1.Store_Seq_Num,
		stl1.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
	Where stl1.Tran_Line_Status_Cd = 'CT'
	And stl1.Return_Reason_Cd <> '-1'
	Group by 
		stl1.Sales_Tran_Seq_Num,
		stl1.Store_Seq_Num,
		stl1.Tran_Dt_DD) stl2		
	On st1.Sales_Tran_Seq_Num = stl2.Sales_Tran_Seq_Num
	And st1.Store_Seq_Num =  stl2.Store_Seq_Num
	And st1.Tran_Dt_DD = stl2.Tran_Dt_DD

LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
on st1.Store_Seq_Num = std1.Store_Seq_Num and st1.Tran_Dt_DD = std1.Calendar_Dt
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') and j1.Loyalty_Ind = 0 	
	
Where st1.Tran_Status_Cd in ( 'CT', 'VO')
 And st1.Tran_Type_Cd = 'POS'
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_TOTALS_F IS '$Revision: 29912 $ - $Date: 2020-01-16 17:39:25 +0100 (tor, 16 jan 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
