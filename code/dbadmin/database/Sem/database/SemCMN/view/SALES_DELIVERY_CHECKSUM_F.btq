/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_DELIVERY_CHECKSUM_F.btq 29891 2020-01-16 13:28:53Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-01-16 14:28:53 +0100 (tor, 16 jan 2020) $
# Last Revision    : $Revision: 29891 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/SALES_DELIVERY_CHECKSUM_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database	${DB_ENV}SemCMNVOUT;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- We create the view

Replace	View ${DB_ENV}SemCMNVOUT.SALES_DELIVERY_CHECKSUM_F
(
Store_Seq_Num,
Store_Id,
Sales_Date,
Source_Delivery_Dttm,
Delivery_Ind,
Delivery_Rank,
Nr_Of_Sales_Rows,
Daily_Sale_Total
)
As
Select	
	s1.Store_Seq_Num,
	s1.N_Store_Id As Store_Id,
	cal1.Calendar_Dt As Sales_Date,
	sds.SourceDeliveryDtTm As Source_Delivery_Dttm,
	Case	
		When sds.SourceDeliveryAmountSum is Not Null Then 1
		Else	0
	End	As Delivery_Ind,
	Rank() Over (Partition By sds.N_Store_Id, sds.SourceDeliveryBusinessDay 
		Order By sds.SourceDeliveryDtTm Desc) As Delivery_Rank,
	sds.SourceDeliveryNrRows As Nr_Of_Sales_Rows,
	sds.SourceDeliveryAmountSum As Daily_Sale_Total
From  ${DB_ENV}TgtVOUT.STORE_TRAIT_VALUE_B As stv1
-- Generate a row for each valid date
Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As cal1
	On cal1.Calendar_Dt_End_Dttm  Between stv1.Valid_From_Dttm And stv1.Valid_To_Dttm 
	And cal1.Calendar_Dt < CURRENT_DATE
Inner Join ${DB_ENV}TgtVOUT.STORE As s1
	On s1.Store_Seq_Num = stv1.Store_Seq_Num
Left Outer Join ${DB_ENV}SemCMNVOUT.SourceDeliveryStatisticsValidated As sds
	On sds.N_Store_Id = s1.N_Store_Id
	And sds.SourceDeliveryBusinessDay = cal1.Calendar_Dt
	And sds.SourceID = 5
Where stv1.Location_Trait_Cd = 'ACT'

UNION ALL

Select	
	s3.Store_Seq_Num,
	s3.N_Store_Id As Store_Id,
	sds3.SourceDeliveryBusinessDay As Sales_Date,
	sds3.SourceDeliveryDtTm As Source_Delivery_Dttm,
	Case	
		When sds3.SourceDeliveryAmountSum is Not Null Then 1
		Else	0
	End	As Delivery_Ind,
	Rank() Over (Partition By sds3.N_Store_Id, sds3.SourceDeliveryBusinessDay 
		Order By sds3.SourceDeliveryDtTm Desc) As Delivery_Rank,
	sds3.SourceDeliveryNrRows As Nr_Of_Sales_Rows,
	sds3.SourceDeliveryAmountSum As Daily_Sale_Total
From ${DB_ENV}MetadataVOUT.SourceDeliveryStatistics As sds3
Inner Join ${DB_ENV}TgtVOUT.STORE As s3
	On s3.N_Store_Id = sds3.N_Store_Id
Where sds3.SourceID = 5 And	(s3.Store_Seq_Num, sds3.SourceDeliveryBusinessDay)
Not In
(
Select	
	s2.Store_Seq_Num,
	cal2.Calendar_Dt As Sales_Date
From  ${DB_ENV}TgtVOUT.STORE_TRAIT_VALUE_B As stv2
-- Generate a row for each valid date
Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As cal2
	On cal2.Calendar_Dt_End_Dttm  Between stv2.Valid_From_Dttm And stv2.Valid_To_Dttm 
Inner Join ${DB_ENV}TgtVOUT.STORE As s2
	On s2.Store_Seq_Num = stv2.Store_Seq_Num
Left Outer Join ${DB_ENV}SemCMNVOUT.SourceDeliveryStatisticsValidated As sds2
	On sds2.N_Store_Id = s2.N_Store_Id
	And sds2.SourceDeliveryBusinessDay = cal2.Calendar_Dt
	And sds2.SourceID = 5
Where stv2.Location_Trait_Cd = 'ACT');

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.SALES_DELIVERY_CHECKSUM_F IS '$Revision: 29891 $ - $Date: 2020-01-16 14:28:53 +0100 (tor, 16 jan 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE