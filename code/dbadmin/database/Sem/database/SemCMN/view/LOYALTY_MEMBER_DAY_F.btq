/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOYALTY_MEMBER_DAY_F.btq 12024 2014-01-08 12:54:32Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2014-01-08 13:54:32 +0100 (ons, 08 jan 2014) $
# Last Revision    : $Revision: 12024 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/LOYALTY_MEMBER_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.LOYALTY_MEMBER_DAY_F 
(
	Member_Account_Seq_Num,
	Loyalty_Program_Seq_Num,
	Calendar_Dt,
	Calendar_Week_Id,
	Calendar_Month_Id,
	Home_Store_Seq_Num,
	Week_Home_Store_Seq_Num,
	Month_Home_Store_Seq_Num,
	
	Member_Ind,
	New_Member_Ind,
	Active_Member_Ind,
	Contact_Ind,
	Active_Contact_Ind,
	Lost_Member_Ind,
	ReActived_Member_Ind,
	
	Member_Tier_Lvl_Seq_Num,
	Week_Member_Tier_Lvl_Seq_Num,
	Month_Member_Tier_Lvl_Seq_Num,
	
	Member_Cnt,
	Member_Sum_Cnt,
	Member_Ord_Cnt,
	Member_Period_Cnt,
	Member_Distinct_Cnt
)
as
Lock row for access
select 
	Member_Account_Seq_Num,
	Loyalty_Program_Seq_Num,
	Calendar_Dt,
	Calendar_Week_Id,
	Calendar_Month_Id,
	Day_Home_Store_Seq_Num,
	Week_Home_Store_Seq_Num,
	Month_Home_Store_Seq_Num,

	Max(Member_Ind) as Member_Ind,
	Max(New_Member_Ind) as New_Member_Ind,
	Max(Active_Member_Ind) as Active_Member_Ind,
	Max(Contact_Ind) as Contact_Ind,
	Max(Active_Contact_Ind) as Active_Contact_Ind,
	Max(Lost_Member_Ind) as Lost_Member_Ind,
	Max(ReActived_Member_Ind) as ReActived_Member_Ind,

	Day_Member_Tier_Lvl_Seq_Num,
	Week_Member_Tier_Lvl_Seq_Num,
	Month_Member_Tier_Lvl_Seq_Num,

 	Member_Cnt,
	Member_Sum_Cnt,
	Member_Ord_Cnt_loc,
	Member_Period_Cnt_loc,
	Member_Distinct_Cnt
	
From
(
	select 
		a11.Member_Account_Seq_Num,
		a11.Loyalty_Program_Seq_Num,
		a11.Calendar_Dt,
		a11.Calendar_Week_Id,
		a11.Calendar_Month_Id,
		a11.Day_Home_Store_Seq_Num,
		a11.Week_Home_Store_Seq_Num,
		a11.Month_Home_Store_Seq_Num,
		
		a11.Member_Ind,
		a11.New_Member_Ind,
		a11.Active_Member_Ind,
		a11.Contact_Ind,
		a11.Active_Contact_Ind,
		a11.Lost_Member_Ind,
		a11.ReActived_Member_Ind,
		
		a11.Day_Member_Tier_Lvl_Seq_Num,
		a11.Week_Member_Tier_Lvl_Seq_Num,
		a11.Month_Member_Tier_Lvl_Seq_Num,
		
		a11.Member_Cnt,
		Cast(1 as INTEGER) as Member_Sum_Cnt,
		Rank() over (partition by 
				a11.Member_Account_Seq_Num,
				a11.Calendar_Dt
			order by
				a11.Member_Account_Seq_Num +
				a11.Loyalty_Program_Seq_Num +
				Cast(a11.Calendar_Dt as INTEGER) +
				a11.Day_Home_Store_Seq_Num +
				a11.Week_Home_Store_Seq_Num +
				a11.Month_Home_Store_Seq_Num +
				a11.Member_Ind +
				a11.New_Member_Ind +
				a11.Active_Member_Ind +
				a11.Contact_Ind +
				a11.Active_Contact_Ind +
				a11.Lost_Member_Ind +
				a11.ReActived_Member_Ind +
				a11.Day_Member_Tier_Lvl_Seq_Num +
				a11.Week_Member_Tier_Lvl_Seq_Num +
				a11.Month_Member_Tier_Lvl_Seq_Num
			desc
			) as Member_Ord_Cnt_loc,
		Case when Member_Ord_Cnt_loc = 1 Then 1 else 0 end as Member_Period_Cnt_loc,
		a11.Member_Distinct_Cnt

	from ${DB_ENV}SemCMNT.LOYALTY_MEMBER_DAY_F as a11
	group by 
		a11.Member_Account_Seq_Num,
		a11.Loyalty_Program_Seq_Num,
		a11.Calendar_Dt,
		a11.Calendar_Week_Id,
		a11.Calendar_Month_Id,
		a11.Day_Home_Store_Seq_Num,
		a11.Week_Home_Store_Seq_Num,
		a11.Month_Home_Store_Seq_Num,

		a11.Member_Ind,
		a11.New_Member_Ind,
		a11.Active_Member_Ind,
		a11.Contact_Ind,
		a11.Active_Contact_Ind,
		a11.Lost_Member_Ind,
		a11.ReActived_Member_Ind,
		
		a11.Day_Member_Tier_Lvl_Seq_Num,
		a11.Week_Member_Tier_Lvl_Seq_Num,
		a11.Month_Member_Tier_Lvl_Seq_Num,

		a11.Member_Cnt,
		a11.Member_Distinct_Cnt
) as a
Group by
	Member_Account_Seq_Num,
	Loyalty_Program_Seq_Num,
	Calendar_Dt,
	Calendar_Week_Id,
	Calendar_Month_Id,
	Day_Home_Store_Seq_Num,
	Week_Home_Store_Seq_Num,
	Month_Home_Store_Seq_Num,

	Day_Member_Tier_Lvl_Seq_Num,
	Week_Member_Tier_Lvl_Seq_Num,
	Month_Member_Tier_Lvl_Seq_Num,

 	Member_Cnt,
	Member_Sum_Cnt,
	Member_Ord_Cnt_loc,
	Member_Period_Cnt_loc,
	Member_Distinct_Cnt
Where Member_Ord_Cnt_loc = 1 -- show only the row with most ones
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.LOYALTY_MEMBER_DAY_F IS '$Revision: 12024 $ - $Date: 2014-01-08 13:54:32 +0100 (ons, 08 jan 2014) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
