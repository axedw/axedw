/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: DIST_EVENT_F.btq 29329 2019-10-23 11:06:33Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-10-23 13:06:33 +0200 (ons, 23 okt 2019) $
# Last Revision    : $Revision: 29329 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/DIST_EVENT_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

Database ${DB_ENV}SemCMNVOUT
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


Replace	View ${DB_ENV}SemCMNVOUT.DIST_EVENT_F
(
	Event_Seq_Num
	,Dist_Event_Pos
	,Reporting_Dt
	,Event_Dt
	,Event_Hour
	,Event_End_Dttm
	,DC_Seq_Num
	,Supply_Location_Seq_Num
	,Supply_Location_Zone_Seq_Num
	,Req_Customer_Seq_Num
	,Req_Concept_Cd
	,Article_Seq_Num
	,Measuring_Unit_Seq_Num
	,Base_UOM_Meas_Unit_Seq_Num
	,Associate_Seq_Num
	,Operation_Equipment_Id
	,User_Distribution_Id
	,Dist_Mission_Cd
	,Event_Category_Cd
	,Event_Subtype_Cd
	,Dist_Event_Subtype_Cd
	,Dist_Event_Group_Seq_Num
	,Exception_Cd
	,Pallet_Ind
	,Actual_Qty
	,Actual_Base_UOM_Qty
	,Ordered_Qty
	,Ordered_Base_UOM_Qty
	,From_Equipment_Id
	,From_Storage_Location_Id
	,From_Storage_Location_Type
	,To_Equipment_Id
	,To_Storage_Location_Id
	,To_Storage_Location_Type	
	,Row_Cnt
	,Weighting_Val
	,Weighting_Val_Pallet_Qty
	,Weight_Meas
	,Weight_Meas_UOM_Cd
	,Req_Store_Seq_Num
) As
Lock Row For Access
select 
	de1.Event_Seq_Num
	,de1.Distribution_Event_Pos As Dist_Event_Pos
	, Case When de1.Event_End_Hour_DD Between 0 And 2 Then de1.Event_Dt_DD - INTERVAL '1' DAY Else de1.Event_Dt_DD End As Reporting_Dt
	,de1.Event_Dt_DD As Event_Dt
	,de1.Event_End_Hour_DD As Event_Hour
	,de1.Event_End_Dttm 
	,coalesce(mdelz1.Location_Seq_Num,de1.Location_Seq_Num) As DC_Seq_Num
	,coalesce(mdelz1.Location_Seq_Num,de1.Location_Seq_Num) As Supply_Location_Seq_Num
	,coalesce(mdelz1.Location_Zone_Seq_Num,de1.Location_Zone_Seq_Num) As Supply_Location_Zone_Seq_Num
	,coalesce(cu1.Customer_Seq_Num,-1) as Req_Customer_Seq_Num
	,de1.Concept_Cd As Req_Concept_Cd
	,de1.Article_Seq_Num 
	,de1.Measuring_Unit_Seq_Num
	,de1.Base_UOM_Meas_Unit_Seq_Num
	,Coalesce(au1.Associate_Seq_Num,a1.Associate_Seq_Num,-1) As Associate_Seq_Num
	,coalesce(oe.Equipment_Id,'-1') as Operation_Equipment_Id
	,de1.User_Distribution_Id
	,Coalesce(dmetm1.Dist_Mission_Cd,'-1') As Dist_Mission_Cd
	,de1.Event_Category_Cd
	,de1.Event_Subtype_Cd
	,de1.Distribution_Event_Subtype_Cd As Dist_Event_Subtype_Cd
	,mespg1.M_Seq_Num As Dist_Event_Group_Seq_Num
	,de1.Exception_Cd
	,Coalesce(mdet1.Pallet_Ind,0) As Pallet_Ind	
	,de1.Actual_Qty
	,de1.Actual_Base_UOM_Qty
	,de1.Ordered_Qty
	,de1.Ordered_Base_UOM_Qty
	,Coalesce(pr1.From_Equipment_Id,'-1') As From_Equipment_Id
	,Coalesce(pr1.From_Storage_Location_Id,'-1') As From_Storage_Location_Id
	,Coalesce(pr1.From_Storage_Location_Type,'-1') As From_Storage_Location_Type
	,Coalesce(pr1.To_Equipment_Id,'-1') As To_Equipment_Id
	,Coalesce(pr1.To_Storage_Location_Id,'-1') As To_Storage_Location_Id
	,Coalesce(pr1.To_Storage_Location_Type,'-1') As To_Storage_Location_Type	
	, 1 As Row_Cnt
	, Coalesce(mpwr1.Weighting_Val,mpwr2.Weighting_Val,1) as Weighting_Val
    , Coalesce(mpwr3.Weighting_Val,10) as Weighting_Val_Pallet_Qty
	--, dep1.Weight_Meas
    , case when coalesce(dep1.Valuation_Weight,0) <> 0 then coalesce(dep1.Valuation_Weight,0) else coalesce(dep1.Weight_Meas,0) end as Weight_Meas	
	, dep1.Weight_Meas_UOM_Cd
	, coalesce(sto1.store_seq_num,-1) as Req_Store_Seq_Num
From ${DB_ENV}TgtVOUT.EVENT e1
Inner Join ${DB_ENV}TgtVOUT.DISTRIBUTION_EVENT de1
	On de1.Event_Seq_Num = e1.Event_Seq_Num
	--And de1.Event_Dt_DD = e1.Event_Dt_DD  --REMOVED 2017-03-15 DUE TO INCORRECT DEFINTION OF EVENT_DT!
/* Added to remove movements from and to only to include to! */
Left Outer Join ${DB_ENV}TgtVOUT.PIECE_ROUTING pr1
	On de1.Event_Seq_Num = pr1.Event_Seq_Num
	And de1.Distribution_Event_pos = pr1.Distribution_Event_pos
	And de1.Event_Dt_DD = pr1.Event_Dt_DD
/* END OF FIX */
Left Outer Join ${DB_ENV}TgtVOUT.DISTRIBUTION_EVENT_PIECE dep1
	On de1.Event_Seq_Num = dep1.Event_Seq_Num
	And de1.Distribution_Event_pos = dep1.Distribution_Event_pos
	And de1.Event_Dt_DD = dep1.Event_Dt_DD
Left Outer Join ${DB_ENV}TgtVOUT.LOCATION_ZONE lz1 
	On de1.Location_Zone_Seq_Num = lz1.Location_Zone_Seq_Num
Left Outer Join ${DB_ENV}TgtVOUT.ASSOCIATE au1
	On de1.User_Distribution_Id = au1.N_Associate_Id
	And au1.N_Location_Id <= 0  --AXFOOD CORPORATE MASTER
Left Outer Join ${DB_ENV}SemMetadataVOUT.MSTR_USERID_EMPLOYEE_M muem1
	On muem1.N_User_Id = de1.User_Distribution_Id
Left Outer Join ${DB_ENV}TgtVOUT.ASSOCIATE a1 
	On a1.N_Associate_Id = muem1.N_Associate_Id
	And a1.N_Location_Id <= 0  --AXFOOD CORPORATE MASTER
Left Outer Join ${DB_ENV}TgtVOUT.OPERATIONS_EQUIPMENT as oe
on de1.User_Distribution_Id = oe.Equipment_Id
Left Outer Join ${DB_ENV}SemMetadataVOUT.MSTR_DIST_EVENT_TP_MISSION_M dmetm1
	On dmetm1.Event_Category_Cd = de1.Event_Category_Cd
	And dmetm1.Distribution_Event_Subtype_Cd = de1.Distribution_Event_Subtype_Cd
Left Outer Join ${DB_ENV}SemMetadataVOUT.MSTR_EVENT_SUB_TP_GROUP_M mespg1
	On mespg1.N_Location_Zone_Id = lz1.N_Location_Zone_Id
	And mespg1.Event_Subtype_Cd = de1.Event_Subtype_Cd
Left Outer Join ${DB_ENV}SemMetadataVOUT.MSTR_DIST_EVENT_TYPE_M mdet1
	On mdet1.Distribution_Event_Subtype_Cd = de1.Distribution_Event_Subtype_Cd
Left Outer Join ${DB_ENV}SemMetadataVOUT.MSTR_PRODUCTIVITY_WEIGHTING_RANK as mpwr1
	on mpwr1.Weighting_type = 1 And mpwr1.Location_Seq_Num = de1.Location_Seq_Num
	And mpwr1.Location_Zone_Seq_Num = de1.Location_Zone_Seq_Num And mpwr1.Concept_cd = de1.Concept_cd
	And de1.Event_Dt_DD = mpwr1.Calendar_Dt
Left Outer Join ${DB_ENV}SemMetadataVOUT.MSTR_PRODUCTIVITY_WEIGHTING_RANK as mpwr2
	on mpwr2.Weighting_type = 2 And mpwr2.Location_Seq_Num = de1.Location_Seq_Num
	And mpwr2.Location_Zone_Seq_Num = de1.Location_Zone_Seq_Num And mpwr2.Event_Subtype_Cd = de1.Event_Subtype_Cd
	And de1.Event_Dt_DD = mpwr2.Calendar_Dt
Left Outer Join ${DB_ENV}SemMetadataVOUT.MSTR_PRODUCTIVITY_WEIGHTING_RANK as mpwr3
 on mpwr3.Weighting_type = 3 And mpwr3.Location_Zone_Seq_Num = de1.Location_Zone_Seq_Num
 And de1.Event_Dt_DD = mpwr3.Calendar_Dt
Left Outer Join ${DB_ENV}SemMetadataVOUT.MSTR_DIST_EVENT_LOCATION_ZONE as mdelz1
on mdelz1.Location_Zone_Id = lz1.N_Location_Zone_Id --and lz1.N_Location_Id <= 0

Left Outer Join ${DB_ENV}SemCMNVOUT.STORE_D as sto1
on sto1.store_seq_num = de1.Ordering_Location_Seq_Num

Left Outer Join (
select p.Party_Seq_Num As Customer_Seq_Num,
Coalesce(p.N_Party_Id ,'-1') as Customer_Id
From ${DB_ENV}TgtVOUT.PARTY As p
Inner Join ${DB_ENV}TgtVOUT.ORGANIZATION As org
On p.Party_Seq_Num = org.Org_Party_Seq_Num
And org.Org_Is_Business_Ind = 1
And org.Org_Type_Cd = 'EXT'
Where p.Party_Type_Cd = 'ORG') as cu1
on cu1.customer_seq_num = de1.Ordering_party_Seq_Num

;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.DIST_EVENT_F IS '$Revision: 29329 $ - $Date: 2019-10-23 13:06:33 +0200 (ons, 23 okt 2019) $ '
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
