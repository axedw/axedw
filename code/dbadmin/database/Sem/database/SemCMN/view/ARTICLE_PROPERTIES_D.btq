/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ARTICLE_PROPERTIES_D.btq 21813 2017-03-03 08:25:04Z k9107640 $
# Last Changed By  : $Author: k9107640 $
# Last Change Date : $Date: 2017-03-03 09:25:04 +0100 (fre, 03 mar 2017) $
# Last Revision    : $Revision: 21813 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/ARTICLE_PROPERTIES_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

--- The default database is set.--
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace View ${DB_ENV}SemCMNVOUT.ARTICLE_PROPERTIES_D
(
	Article_Seq_Num
	
	,Age_Limit
 	,Warranty
	
 	,Trend_Global_MENA_Ind
 	,Trend_Global_Asia_Ind
 	,Trend_Global_Balkans_Ind
 	,Trend_Global_Baltics_Ind
	
	--Miljömärken
 	,Env_Z01_Falken_Ind
 	,Env_Z02_Svanen_Ind
 	,Env_Z03_EU_Blomman_Ind
 	,Env_Z04_Krav_Ind
 	,Env_Z18_GMO_Ind
 	,Env_Z38_EU_Eko_Ind
 	,Env_Z40_MSC_Ind
 	,Env_Z49_Rainforest_Ind
 	,Env_Z50_SvensktSigill_Ind
 	,Env_Z51_FSC_Ind
 	,Env_Z52_PEFC_Ind
 	,Env_Z53_USDA_Ind
 	,Env_Z57_Soil_Ass_Org_Ind
 	
 	--Allergi/dietmärken
 	,AllergyDiet_Z05_Nyckelhalet_Ind
 	,AllergyDiet_Z17_RekAstmaAllergi_Ind
 	,AllergyDiet_Z21_Light_Ind
 	,AllergyDiet_Z25_SLV_Diet_Ind
 	,AllergyDiet_Z26_SLV_Sup_Milk_Ind
 	,AllergyDiet_Z27_SLV_Sup_Ind
 	,AllergyDiet_Z28_SLV_Probe_Ind
 	,AllergyDiet_Z29_SLV_Diet_Ind
 	,AllergyDiet_Z35_No_Lactase_Enzymes_Ind
 	,AllergyDiet_Z37_LowOn_Fenylanalin_Ind
 	,AllergyDiet_Z44_Contains_PVC_Ind
 	,AllergyDiet_Z45_Contains_PVC_No_Phthalate_Ind
 	,AllergyDiet_Z46_Contains_PVC_Ind
 	,AllergyDiet_Z48_Contains_Latex_Ind
 	,AllergyDiet_Z55_Diet_450_800_kcal_Ind
 	
	 --fri från
 	,FreeFrom_Z06_Sugar_Ind
	,Reduced_Z07_Lactose_Ind
	,FreeFrom_Z08_Gluten_Ind
	,FreeFrom_Z09_Lactose_Ind
	,FreeFrom_Z10_Milkprotein_Ind
	,FreeFrom_Z11_Soy_Ind
	,FreeFrom_Z12_Egg_Ind
	,NoAdded_Z19_Sugar_Ind
	,NoAdded_Z20_Sweetener_Ind
	,FreeFrom_Z22_Milk_Ind
	,FreeFrom_Z23_Pea_Protein_Ind
	,Reduced_Z24_Protein_Ind	
	,FreeFrom_Z34_Nat_Gluten_Ind
	,FreeFrom_Z36_Protein_Ind
	,FreeFrom_Z39_Fish_Ind
	,FreeFrom_Z43_PVC_Ind
	,FreeFrom_Z47_Latex_Ind
	,VeryLowOn_Z54_Gluten_Ind
	,FreeFrom_Z58_Peanuts_Ind
	
	--Etisk
	,Ethical_987_No_Racketing_Ind
	,Ethical_Z15_Fairtrade_Ind
	,Ethical_Z56_UTZ_Ind
	 
	  --Övriga
	 ,Env_Z59_Svenskt_Kott_Ind
	 ,Env_Z60_Svensk_Fagel_Ind
	 ,Env_Z61_Cosmos_Org_Ind
  	 ,Env_Z63_ASC_Ind
 	 ,Env_Z64_From_Sweden_Ind
	 ,Env_Z65_Meat_From_Sweden_Ind
) as 

LOCKING ROW FOR ACCESS --Dirty read needed as table db is used
--select Article_Seq_Num, Trend_Global_Mid_East_Ind, Env_Z02_Svanen_Ind, AllergyDiet_Z05_Nyckelhalet_Ind, FreeFrom_Z11_Soy_Ind, Ethical_Z15_Fairtrade_Ind
Select 
	a.Article_Seq_Num
	
 	,Coalesce(age1.Article_Trait_Val_Desc,'Nej') as Age_Limit
 	,Coalesce(war1.Article_Trait_Val_Desc,'Saknas') as Warranty
 	
 	--Egenskap : Trend -Global
 	,case when glob1.Article_Attribute_Val_Desc is not null then 1 else 0 end as Trend_Global_MENA_Ind
 	,case when glob2.Article_Attribute_Val_Desc is not null then 1 else 0 end as Trend_Global_Asia_Ind
 	,case when glob3.Article_Attribute_Val_Desc is not null then 1 else 0 end as Trend_Global_Balkans_Ind
 	,case when glob4.Article_Attribute_Val_Desc is not null then 1 else 0 end as Trend_Global_Baltics_Ind
 	
 	--Miljömärken
 	,case when z01.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z01_Falken_Ind
 	,case when z02.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z02_Svanen_Ind
 	,case when z03.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z03_EU_Blomman_Ind
 	,case when z04.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z04_Krav_Ind
 	,case when z18.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z18_GMO_Ind
 	,case when z38.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z38_EU_Eko_Ind
 	,case when z40.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z40_MSC_Ind
 	,case when z49.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z49_Rainforest_Ind
 	,case when z50.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z50_SvensktSigill_Ind
 	,case when z51.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z51_FSC_Ind
 	,case when z52.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z52_PEFC_Ind
 	,case when z53.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z53_USDA_Ind
 	,case when z57.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z57_Soil_Ass_Org_Ind
 	
 	--Allergi/dietmärken
 	,case when z05.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z05_Nyckelhalet_Ind
 	,case when z17.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z17_RekAstmaAllergi_Ind
 	,case when z21.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z21_Light_Ind
 	,case when z25.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z25_SLV_Diet_Ind
 	,case when z26.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z26_SLV_Sup_Milk_Ind
 	,case when z27.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z27_SLV_Sup_Ind
 	,case when z28.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z28_SLV_Probe_Ind
 	,case when z29.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z29_SLV_Diet_Ind
 	,case when z35.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z35_No_Lactase_Enzymes_Ind
 	,case when z37.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z37_LowOn_Fenylanalin_Ind
 	,case when z44.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z44_Contains_PVC_Ind
 	,case when z45.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z45_Contains_PVC_No_Phthalate_Ind
 	,case when z46.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z46_Contains_PVC_Ind
 	,case when z48.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z48_Contains_Latex_Ind
 	,case when z55.Article_Symbol_Cd is not null then 1 else 0 end as AllergyDiet_Z55_Diet_450_800_kcal_Ind
 	
 	--fri från
	,case when z06.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z06_Sugar_Ind
	,case when z07.Article_Symbol_Cd is not null then 1 else 0 end as Reduced_Z07_Lactose_Ind
	,case when z08.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z08_Gluten_Ind
	,case when z09.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z09_Lactose_Ind
	,case when z10.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z10_Milk_Protein_Ind
	,case when z11.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z11_Soy_Ind
	,case when z12.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z12_Egg_Ind
	,case when z19.Article_Symbol_Cd is not null then 1 else 0 end as NoAdded_Z19_Sugar_Ind
	,case when z20.Article_Symbol_Cd is not null then 1 else 0 end as NoAdded_Z20_Sweetener_Ind
	,case when z22.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z22_Milk_Ind
	,case when z23.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z23_Pea_Protein_Ind
	,case when z24.Article_Symbol_Cd is not null then 1 else 0 end as Reduced_Z24_Protein_Ind
	,case when z34.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z34_Nat_Gluten_Ind
	,case when z36.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z36_Protein_Ind
	,case when z39.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z39_Fish_Ind
	,case when z43.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z43_PVC_Ind
	,case when z47.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z47_Latex_Ind
	,case when z54.Article_Symbol_Cd is not null then 1 else 0 end as VeryLowOn_Z54_Gluten_Ind
	,case when z58.Article_Symbol_Cd is not null then 1 else 0 end as FreeFrom_Z58_Peanuts_Ind
	
	--Etisk
	,case when m987.Article_Symbol_Cd is not null then 1 else 0 end as Ethical_987_No_Racketing_Ind
	,case when z15.Article_Symbol_Cd is not null then 1 else 0 end as Ethical_Z15_Fairtrade_Ind
	,case when z56.Article_Symbol_Cd is not null then 1 else 0 end as Ethical_Z56_UTZ_Ind

	--Övriga
	,case when z59.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z59_Svenskt_Kott_Ind
	,case when z60.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z60_Svensk_Fagel_Ind
	,case when z61.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z61_Cosmos_Org_Ind
	,case when z63.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z63_ASC_Ind
	,case when z64.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z64_From_Sweden_Ind
	,case when z65.Article_Symbol_Cd is not null then 1 else 0 end as Env_Z65_Meat_From_Sweden_Ind
 	
From ${DB_ENV}TgtVOUT.ARTICLE As a
-- Blandade egenskaper
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_TRAIT_VALUE_B as age1
 On  a.Article_Seq_Num = age1.Article_Seq_Num
 And age1.Article_Trait_Cd='Z_ALDERSGRANS'
 And age1.Valid_To_Dttm = SYSLIB.HighTSVal()
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_TRAIT_VALUE_B as war1
 On  a.Article_Seq_Num = war1.Article_Seq_Num
 And war1.Article_Trait_Cd='Z_GARANTI'
 And war1.Valid_To_Dttm = SYSLIB.HighTSVal()
 
--Egenskap : Trend -Global
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_ATTRIBUTES_B as glob1
 On  a.Article_Seq_Num = glob1.Article_Seq_Num
 And glob1.Article_Attribute_Cd='Z_ARTIKELATTRIBUT_TREND'
 And glob1.Article_Attribute_Val = '1'
 And glob1.Valid_To_Dttm = SYSLIB.HighTSVal()
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_ATTRIBUTES_B as glob2
 On  a.Article_Seq_Num = glob2.Article_Seq_Num
 And glob2.Article_Attribute_Cd='Z_ARTIKELATTRIBUT_TREND'
 And glob2.Article_Attribute_Val = '2'
 And glob2.Valid_To_Dttm = SYSLIB.HighTSVal()
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_ATTRIBUTES_B as glob3
 On  a.Article_Seq_Num = glob3.Article_Seq_Num
 And glob3.Article_Attribute_Cd='Z_ARTIKELATTRIBUT_TREND'
 And glob3.Article_Attribute_Val = '3'
 And glob3.Valid_To_Dttm = SYSLIB.HighTSVal()
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_ATTRIBUTES_B as glob4
 On  a.Article_Seq_Num = glob4.Article_Seq_Num
 And glob4.Article_Attribute_Cd='Z_ARTIKELATTRIBUT_TREND'
 And glob4.Article_Attribute_Val = '4'
 And glob4.Valid_To_Dttm = SYSLIB.HighTSVal()  
 
--Miljömärken
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z01
 On  a.Article_Seq_Num = z01.Article_Seq_Num
 And z01.Article_Symbol_Cd='Z01'
 And z01.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z02
 On  a.Article_Seq_Num = z02.Article_Seq_Num
 And z02.Article_Symbol_Cd='Z02'
 And z02.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z03
 On  a.Article_Seq_Num = z03.Article_Seq_Num
 And z03.Article_Symbol_Cd='Z03'
 And z03.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z04
 On  a.Article_Seq_Num = z04.Article_Seq_Num
 And z04.Article_Symbol_Cd='Z04'
 And z04.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z18
 On  a.Article_Seq_Num = z18.Article_Seq_Num
 And z18.Article_Symbol_Cd='Z18'
 And z18.Valid_To_Dttm = SYSLIB.HighTSVal()
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z38
 On  a.Article_Seq_Num = z38.Article_Seq_Num
 And z38.Article_Symbol_Cd='Z38'
 And z38.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z40
 On  a.Article_Seq_Num = z40.Article_Seq_Num
 And z40.Article_Symbol_Cd='Z40'
 And z40.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z49
 On  a.Article_Seq_Num = z49.Article_Seq_Num
 And z49.Article_Symbol_Cd='Z49'
 And z49.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z50
 On  a.Article_Seq_Num = z50.Article_Seq_Num
 And z50.Article_Symbol_Cd='Z50'
 And z50.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z51
 On  a.Article_Seq_Num = z51.Article_Seq_Num
 And z51.Article_Symbol_Cd='Z51'
 And z51.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z52
 On  a.Article_Seq_Num = z52.Article_Seq_Num
 And z52.Article_Symbol_Cd='Z52'
 And z52.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z53
 On  a.Article_Seq_Num = z53.Article_Seq_Num
 And z53.Article_Symbol_Cd='Z53'
 And z53.Valid_To_Dttm = SYSLIB.HighTSVal()   
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z57
 On  a.Article_Seq_Num = z57.Article_Seq_Num
 And z57.Article_Symbol_Cd='Z57'
 And z57.Valid_To_Dttm = SYSLIB.HighTSVal()   
 
 --Allergi/dietmärken
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z05
 On  a.Article_Seq_Num = z05.Article_Seq_Num
 And z05.Article_Symbol_Cd='Z05'
 And z05.Valid_To_Dttm = SYSLIB.HighTSVal()    
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z17
 On  a.Article_Seq_Num = z17.Article_Seq_Num
 And z17.Article_Symbol_Cd='Z17'
 And z17.Valid_To_Dttm = SYSLIB.HighTSVal()    
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z21
 On  a.Article_Seq_Num = z21.Article_Seq_Num
 And z21.Article_Symbol_Cd='Z21'
 And z21.Valid_To_Dttm = SYSLIB.HighTSVal()    
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z25
 On  a.Article_Seq_Num = z25.Article_Seq_Num
 And z25.Article_Symbol_Cd='Z25'
 And z25.Valid_To_Dttm = SYSLIB.HighTSVal()    
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z26
 On  a.Article_Seq_Num = z26.Article_Seq_Num
 And z26.Article_Symbol_Cd='Z26'
 And z26.Valid_To_Dttm = SYSLIB.HighTSVal()     
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z27
 On  a.Article_Seq_Num = z27.Article_Seq_Num
 And z27.Article_Symbol_Cd='Z27'
 And z27.Valid_To_Dttm = SYSLIB.HighTSVal()     
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z28
 On  a.Article_Seq_Num = z28.Article_Seq_Num
 And z28.Article_Symbol_Cd='Z28'
 And z28.Valid_To_Dttm = SYSLIB.HighTSVal()     
Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z29
 On  a.Article_Seq_Num = z29.Article_Seq_Num
 And z29.Article_Symbol_Cd='Z29'
 And z29.Valid_To_Dttm = SYSLIB.HighTSVal()     
  Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z35
 On  a.Article_Seq_Num = z35.Article_Seq_Num
 And z35.Article_Symbol_Cd='Z35'
 And z35.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z37
 On  a.Article_Seq_Num = z37.Article_Seq_Num
 And z37.Article_Symbol_Cd='Z37'
 And z37.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z44
 On  a.Article_Seq_Num = z44.Article_Seq_Num
 And z44.Article_Symbol_Cd='Z44'
 And z44.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z45
 On  a.Article_Seq_Num = z45.Article_Seq_Num
 And z45.Article_Symbol_Cd='Z45'
 And z45.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z46
 On  a.Article_Seq_Num = z46.Article_Seq_Num
 And z46.Article_Symbol_Cd='Z46'
 And z46.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z48
 On  a.Article_Seq_Num = z48.Article_Seq_Num
 And z48.Article_Symbol_Cd='Z48'
 And z48.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z55
 On  a.Article_Seq_Num = z55.Article_Seq_Num
 And z55.Article_Symbol_Cd='Z55'
 And z55.Valid_To_Dttm = SYSLIB.HighTSVal()    
 
 --fri från
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z06
 On  a.Article_Seq_Num = z06.Article_Seq_Num
 And z06.Article_Symbol_Cd='Z06'
 And z06.Valid_To_Dttm = SYSLIB.HighTSVal()    
  Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z07
 On  a.Article_Seq_Num = z07.Article_Seq_Num
 And z07.Article_Symbol_Cd='Z07'
 And z07.Valid_To_Dttm = SYSLIB.HighTSVal()    
  Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z08
 On  a.Article_Seq_Num = z08.Article_Seq_Num
 And z08.Article_Symbol_Cd='Z08'
 And z08.Valid_To_Dttm = SYSLIB.HighTSVal()    
  Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z09
 On  a.Article_Seq_Num = z09.Article_Seq_Num
 And z09.Article_Symbol_Cd='Z09'
 And z09.Valid_To_Dttm = SYSLIB.HighTSVal()    
  Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z10
 On  a.Article_Seq_Num = z10.Article_Seq_Num
 And z10.Article_Symbol_Cd='Z10'
 And z10.Valid_To_Dttm = SYSLIB.HighTSVal()    
   Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z11
 On  a.Article_Seq_Num = z11.Article_Seq_Num
 And z11.Article_Symbol_Cd='Z11'
 And z11.Valid_To_Dttm = SYSLIB.HighTSVal()    
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z12
 On  a.Article_Seq_Num = z12.Article_Seq_Num
 And z12.Article_Symbol_Cd='Z12'
 And z12.Valid_To_Dttm = SYSLIB.HighTSVal()    
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z19
 On  a.Article_Seq_Num = z19.Article_Seq_Num
 And z19.Article_Symbol_Cd='Z19'
 And z19.Valid_To_Dttm = SYSLIB.HighTSVal()    
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z20
 On  a.Article_Seq_Num = z20.Article_Seq_Num
 And z20.Article_Symbol_Cd='Z20'
 And z20.Valid_To_Dttm = SYSLIB.HighTSVal()    
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z22
 On  a.Article_Seq_Num = z22.Article_Seq_Num
 And z22.Article_Symbol_Cd='Z22'
 And z22.Valid_To_Dttm = SYSLIB.HighTSVal()    
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z23
 On  a.Article_Seq_Num = z23.Article_Seq_Num
 And z23.Article_Symbol_Cd='Z23'
 And z23.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z24
 On  a.Article_Seq_Num = z24.Article_Seq_Num
 And z24.Article_Symbol_Cd='Z24'
 And z24.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z34
 On  a.Article_Seq_Num = z34.Article_Seq_Num
 And z34.Article_Symbol_Cd='Z34'
 And z34.Valid_To_Dttm = SYSLIB.HighTSVal()        
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z36
 On  a.Article_Seq_Num = z36.Article_Seq_Num
 And z36.Article_Symbol_Cd='Z36'
 And z36.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z39
 On  a.Article_Seq_Num = z39.Article_Seq_Num
 And z39.Article_Symbol_Cd='Z39'
 And z39.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z43
 On  a.Article_Seq_Num = z43.Article_Seq_Num
 And z43.Article_Symbol_Cd='Z43'
 And z43.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z47
 On  a.Article_Seq_Num = z47.Article_Seq_Num
 And z47.Article_Symbol_Cd='Z47'
 And z47.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z54
 On  a.Article_Seq_Num = z54.Article_Seq_Num
 And z54.Article_Symbol_Cd='Z54'
 And z54.Valid_To_Dttm = SYSLIB.HighTSVal()     
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z58
 On  a.Article_Seq_Num = z58.Article_Seq_Num
 And z58.Article_Symbol_Cd='Z58'
 And z58.Valid_To_Dttm = SYSLIB.HighTSVal()     

 --Etisk
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as m987
 On  a.Article_Seq_Num = m987.Article_Seq_Num
 And m987.Article_Symbol_Cd='987'
 And m987.Valid_To_Dttm = SYSLIB.HighTSVal()
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z15
 On  a.Article_Seq_Num = z15.Article_Seq_Num
 And z15.Article_Symbol_Cd='Z15'
 And z15.Valid_To_Dttm = SYSLIB.HighTSVal()
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z56
 On  a.Article_Seq_Num = z56.Article_Seq_Num
 And z56.Article_Symbol_Cd='Z56'
 And z56.Valid_To_Dttm = SYSLIB.HighTSVal()
 
 --Övriga
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z59
 On  a.Article_Seq_Num = z59.Article_Seq_Num
 And z59.Article_Symbol_Cd='Z59'
 And z59.Valid_To_Dttm = SYSLIB.HighTSVal()  
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z60
 On  a.Article_Seq_Num = z60.Article_Seq_Num
 And z60.Article_Symbol_Cd='Z60'
 And z60.Valid_To_Dttm = SYSLIB.HighTSVal()  
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z61
 On  a.Article_Seq_Num = z61.Article_Seq_Num
 And z61.Article_Symbol_Cd='Z61'
 And z61.Valid_To_Dttm = SYSLIB.HighTSVal()  
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z63
 On  a.Article_Seq_Num = z63.Article_Seq_Num
 And z63.Article_Symbol_Cd='Z63'
 And z63.Valid_To_Dttm = SYSLIB.HighTSVal()
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z64
 On  a.Article_Seq_Num = z64.Article_Seq_Num
 And z64.Article_Symbol_Cd='Z64'
 And z64.Valid_To_Dttm = SYSLIB.HighTSVal()
 Left Outer Join ${DB_ENV}TgtVOUT.ARTICLE_SYMBOL_CODE_B as z65
 On  a.Article_Seq_Num = z65.Article_Seq_Num
 And z65.Article_Symbol_Cd='Z65'
 And z65.Valid_To_Dttm = SYSLIB.HighTSVal()         
      
Union All -- Add record for missing masterdata

Select 
	d0.Dummy_Seq_Num As Article_Seq_Num
	
	,d0.Dummy_Desc as Age_Limit
 	,d0.Dummy_Desc as Warranty
	
 	,0 as Trend_Global_Mid_East_Ind
 	,0 as Trend_Global_Asia_Ind
 	,0 as Trend_Global_Balkans_Ind
 	,0 as Trend_Global_Baltics_Ind
	
	--Miljömärken
 	,0 as Env_Z01_Falken_Ind
 	,0 as Env_Z02_Svanen_Ind
 	,0 as Env_Z03_EU_Blomman_Ind
 	,0 as Env_Z04_Krav_Ind
 	,0 as Env_Z18_GMO_Ind
 	,0 as Env_Z38_EU_Eko_Ind
 	,0 as Env_Z40_MSC_Ind
 	,0 as Env_Z49_Rainforest_Ind
 	,0 as Env_Z50_SvensktSigill_Ind
 	,0 as Env_Z51_FSC_Ind
 	,0 as Env_Z52_PEFC_Ind
 	,0 as Env_Z53_USDA_Ind
 	,0 as Env_Z57_Soil_Ass_Org_Ind
 	
 	--Allergi/dietmärken
 	,0 as AllergyDiet_Z05_Nyckelhalet_Ind
 	,0 as AllergyDiet_Z17_RekAstmaAllergi_Ind
 	,0 as AllergyDiet_Z21_Light_Ind
 	,0 as AllergyDiet_Z25_SLV_Diet_Ind
 	,0 as AllergyDiet_Z26_SLV_Sup_Milk_Ind
 	,0 as AllergyDiet_Z27_SLV_Sup_Ind
 	,0 as AllergyDiet_Z28_SLV_Probe_Ind
 	,0 as AllergyDiet_Z29_SLV_Diet_Ind
 	,0 as AllergyDiet_Z35_No_Lactase_Enzymes_Ind
 	,0 as AllergyDiet_Z37_LowOn_Fenylanalin_Ind
 	,0 as AllergyDiet_Z44_Contains_PVC_Ind
 	,0 as AllergyDiet_Z45_Contains_PVC_No_Phthalate_Ind
 	,0 as AllergyDiet_Z46_Contains_PVC_Ind
 	,0 as AllergyDiet_Z48_Contains_Latex_Ind
 	,0 as AllergyDiet_Z55_Diet_450_800_kcal_Ind
 	
	 --fri från
 	,0 as FreeFrom_Z06_Sugar_Ind
	,0 as Reduced_Z07_Lactose_Ind
	,0 as FreeFrom_Z08_Gluten_Ind
	,0 as FreeFrom_Z09_Lactose_Ind
	,0 as FreeFrom_Z10_Milkprotein_Ind
	,0 as FreeFrom_Z11_Soy_Ind
	,0 as FreeFrom_Z12_Egg_Ind
	,0 as NoAdded_Z19_Sugar_Ind
	,0 as NoAdded_Z20_Sweetener_Ind
	,0 as FreeFrom_Z22_Milk_Ind
	,0 as FreeFrom_Z23_Pea_Protein_Ind
	,0 as Reduced_Z24_Protein_Ind	
	,0 as FreeFrom_Z34_Nat_Gluten_Ind
	,0 as FreeFrom_Z36_Protein_Ind
	,0 as FreeFrom_Z39_Fish_Ind
	,0 as FreeFrom_Z43_PVC_Ind
	,0 as FreeFrom_Z47_Latex_Ind
	,0 as VeryLowOn_Z54_Gluten_Ind
	,0 as FreeFrom_Z58_Peanuts_Ind
	
	--Etisk
	,0 as Ethical_987_No_Racketing_Ind
	,0 as Ethical_Z15_Fairtrade_Ind
	,0 as Ethical_Z56_UTZ_Ind
	 
	  --Övriga
	 ,0 as Env_Z59_Svenskt_Kott_Ind
	 ,0 as Env_Z60_Svensk_Fagel_Ind
	 ,0 as Env_Z61_Cosmos_Org_Ind
  	 ,0 as Env_Z63_ASC_Ind
 	 ,0 as Env_Z64_From_Sweden_Ind
	 ,0 as Env_Z65_Meat_From_Sweden_Ind
	 
From ${DB_ENV}SemCMNT.DUMMY As d0

;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.ARTICLE_PROPERTIES_D IS '$Revision: 21813 $ - $Date: 2017-03-03 09:25:04 +0100 (fre, 03 mar 2017) $'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
