/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ARTICLE_RETAIL_PRICE_F.btq 28256 2019-05-08 10:57:39Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-05-08 12:57:39 +0200 (ons, 08 maj 2019) $
# Last Revision    : $Revision: 28256 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/view/ARTICLE_RETAIL_PRICE_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- The default database is set.
Database ${DB_ENV}SemCMNVOUT	 -- Change db name as needed
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

Replace	View ${DB_ENV}SemCMNVOUT.ARTICLE_RETAIL_PRICE_F
(
Location_Seq_Num,
Article_Seq_Num,
Calendar_Dt,
Retail_Price_List_Cd,
Retail_Pricing_Lvl_Name,
Currency_Cd,
Calc_Cost_Price_Amt_Excl_VAT,
Retail_Price_Amt_Excl_VAT,
Retail_Price_Amt_Incl_VAT
)
As
Select 
	price.Location_Seq_Num,
	price.Article_Seq_Num,
	Cal.Calendar_Dt, 
	price.Retail_Price_List_Cd,
	price.Retail_Pricing_Lvl_Name,
	price.Currency_Cd,
	price.Applied_Cost_Price_Amt As Calc_Cost_Price_Amt_Excl_VAT, 
	price.Net_Retail_Price_Amt As Retail_Price_Amt_Excl_VAT,
	price.Final_Retail_Price_Amt As Retail_Price_Amt_Incl_VAT
/* For each day */
From ${DB_ENV}TgtVOUT.CALENDAR_DATE As cal
/* fetch the price document */	
Inner Join ${DB_ENV}TgtVOUT.ACTUAL_RETAIL_PRICE As price
On cal.Calendar_Dt between Cast(price.Valid_From_Dttm As Date) And Cast(price.Valid_To_Dttm As Date)
/* Fetch price documents from analytical day 1 uptil now */	
Where cal.Calendar_Dt between DATE '2011-01-01' and ADD_MONTHS(CURRENT_DATE,13)
/* get the last changed row, for backwards compatibility ignoring Retail_Price_UOM_Cd */
Qualify  Rank() Over(Partition by cal.Calendar_Dt, price.Location_Seq_Num,price.Retail_Price_List_Cd, price.Article_Seq_Num Order by price.Valid_From_Dttm Desc) = 1
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNVOUT.ARTICLE_RETAIL_PRICE_F IS '$Revision: 28256 $ - $Date: 2019-05-08 12:57:39 +0200 (ons, 08 maj 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
