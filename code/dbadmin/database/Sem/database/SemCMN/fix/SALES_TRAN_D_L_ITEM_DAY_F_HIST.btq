/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRAN_D_L_ITEM_DAY_F_HIST.btq 21731 2017-02-27 09:50:01Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-02-27 10:50:01 +0100 (mån, 27 feb 2017) $
# Last Revision    : $Revision: 21731 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/fix/SALES_TRAN_D_L_ITEM_DAY_F_HIST.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/


DATABASE ${DB_ENV}SemCMNT
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2017-01-01' and date '2017-01-31'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2016-01-01' and date '2016-03-31'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2016-04-01' and date '2016-06-30'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2016-07-01' and date '2016-09-30'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2016-10-01' and date '2016-12-31'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2015-01-01' and date '2015-03-31'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2015-04-01' and date '2015-06-30'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2015-07-01' and date '2015-09-30'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2015-10-01' and date '2015-12-31'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2014-01-01' and date '2014-03-31'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2014-04-01' and date '2014-06-30'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2014-07-01' and date '2014-09-30'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F_A20841
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X_A20841 As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2014-10-01' and date '2014-12-31'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

