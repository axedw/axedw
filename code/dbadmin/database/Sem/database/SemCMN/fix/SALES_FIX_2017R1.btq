/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_FIX_2017R1.btq 21727 2017-02-27 09:11:31Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-02-27 10:11:31 +0100 (mån, 27 feb 2017) $
# Last Revision    : $Revision: 21727 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/fix/SALES_FIX_2017R1.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}SemCMNT;

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRANSACTION_LINE_X(
Sales_Tran_Seq_Num, Sales_Tran_Line_Num, Store_Seq_Num,Tran_Dt, Tran_End_Dttm, 
Scan_Code_Seq_Num, Store_Department_Seq_Num,Sold_To_Party_Seq_Num, Receipt_Type_Cd, Loyalty_Identifier_Seq_Num,
Sales_Location_Cd, Campaign_Sales_Type_Cd, Preferred_Activity_Code_Id,Preferred_Discount_Type_Cd, Pref_Promotion_Offer_Seq_Num, 
RankDiscountType,Hist_Article_Seq_Num, UOM_Category_Cd, AO_Ind,Junk_Seq_Num,InsertDttm)
SELECT	s1.Sales_Tran_Seq_Num, s1.Sales_Tran_Line_Num, s1.Store_Seq_Num,s1.Tran_Dt, s1.Tran_End_Dttm, 
s1.Scan_Code_Seq_Num, s1.Store_Department_Seq_Num,s1.Sold_To_Party_Seq_Num, s1.Receipt_Type_Cd, s1.Loyalty_Identifier_Seq_Num,
s1.Sales_Location_Cd, s1.Campaign_Sales_Type_Cd, s1.Preferred_Activity_Code_Id,s1.Preferred_Discount_Type_Cd, s1.Pref_Promotion_Offer_Seq_Num, 
s1.RankDiscountType,
Case When s3.Scan_Code_Seq_Num is not null Then -1 Else s1.Hist_Article_Seq_Num End As Hist_Article_Seq_Num,
s1.UOM_Category_Cd, s1.AO_Ind,coalesce(j1.Junk_Seq_Num,-1) as Junk_Seq_Num,s1.InsertDttm
FROM ${DB_ENV}SemCMNT.#SALES_TRANSACTION_LINE_X#01 as s1
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as s2
on s1.Store_Seq_Num = s2.Store_Seq_Num and s1.Tran_Dt = s2.Calendar_Dt
LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
on j1.Concept_Cd = coalesce(s2.Concept_Cd,'-1') and j1.Corp_Affiliation_Type_Cd = coalesce(s2.Corp_Affiliation_Type_Cd,'-1')
and j1.Retail_Region_Cd = coalesce(s2.Retail_Region_Cd,'-1') and j1.Loyalty_Ind = 0
LEFT OUTER JOIN (
SELECT s12.Scan_Code_Seq_Num 
FROM ${DB_ENV}SemCMNT.EXCLUDED_SCAN_CODE as s11
INNER JOIN ${DB_ENV}MetaDataVOUT.MAP_MU_SCAN_CODE as s12
on s11.Scan_Cd = s12.N_Scan_Cd
) As s3
ON s3.Scan_Code_Seq_num = s1.Scan_Code_Seq_Num
where s1.Tran_Dt between date '2017-02-01' and date '2017-03-31'
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_LINE_DAY_F
(
	Scan_Code_Seq_Num,
	Sales_Tran_Dt,
	Store_Seq_Num,
	Day_Of_Week_Num,
	Calendar_Week_Id,
	Calendar_Month_Id,
	Receipt_Type_Cd,
	Sales_Location_Cd,
	Store_Department_Seq_Num,
	Campaign_Sales_Type_Cd,
	Contact_Account_Seq_Num,
	Customer_Seq_Num,
	AO_Ind,
	UOM_Category_Cd,
	Preferred_Activity_Code_Id, 
	Pref_Promotion_Offer_Seq_Num, 
	Article_Seq_Num,
	Junk_Seq_Num,
	GP_Unit_Selling_Price_Amt,
	Unit_Cost_Amt,
	Unit_Selling_Price_Amt,
	Item_Qty,
	Receipt_Line_Cnt,
	Tax_Amt,
	Additional_Tax_Amt
)
	Select
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt as Sales_Tran_Dt,
		stl0.Store_Seq_Num,
		c.Day_Of_Week_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.Receipt_Type_Cd,
		stl0.Sales_Location_Cd,
		stl0.Store_Department_Seq_Num,
		stl0.Campaign_Sales_Type_Cd,
		Coalesce(stlc.Contact_Account_Seq_Num,-1) As Contact_Account_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		stl0.AO_Ind,
		stl0.UOM_Category_Cd,
		(case when coalesce(stl0.preferred_activity_code_id,'-1') = '' then '-1' else coalesce(stl0.preferred_activity_code_id,'-1') end)  As Preferred_Activity_Code_Id,
		(case when coalesce(stl0.Pref_Promotion_Offer_Seq_Num,-1) = '' then -1 else coalesce(stl0.Pref_Promotion_Offer_Seq_Num,-1) end)  As Pref_Promotion_Offer_Seq_Num,
		stl0.Hist_Article_Seq_Num,
		stl0.Junk_Seq_Num,
		Sum(
			Case	When stl1.Unit_Cost_Amt_DD = 0  
			Then 0
			Else stl1.Unit_Selling_Price_Amt
			End
			)	As GP_Unit_Selling_Price_Amt,
		Sum(stl1.Unit_Cost_Amt_DD) As Unit_Cost_Amt,
		Sum(stl1.Unit_Selling_Price_Amt) As Unit_Selling_Price_Amt,
		Sum(Case When stla1.Actual_Item_Qty Is Not Null 
			Then stla1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
			End
			)	As Item_Qty,
		Sum(1) As Receipt_Line_Cnt,
		Sum(stl1.Tax_Amt) As Tax_Amt,
		Sum(Coalesce(stcl1.Charge_Amt,0)) As Additional_Tax_Amt
	
	From ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_LINE_X As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
	  On stl0.Tran_Dt = c.Calendar_Dt

	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As stla1
		On	stla1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stla1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stla1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stla1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_CHARGE_LINE As stcl1
		On	stcl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stcl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stcl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stcl1.Tran_Dt_DD = stl0.Tran_Dt
		And	stcl1.Charge_Seq_Num = 1 -- Qualify PK (ie bad pk on this table)
		And	stcl1.Charge_Type_Cd = 'LUX'
	Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT As stlc 
		On stl0.Sales_Tran_Seq_Num = stlc.Sales_Tran_Seq_Num
		And stl0.Store_Seq_Num = stlc.Store_Seq_Num 
		And stl0.Tran_Dt = stlc.Tran_Dt_DD

	Where stl0.Tran_Dt between date '2017-02-01' and date '2017-03-31'
	
	Group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F
(
		Scan_Code_Seq_Num,
		Tran_Dt,
		Store_Seq_Num,
		Customer_Seq_Num,
		Calendar_Week_Id,
		Calendar_Month_Id,
		Discount_Type_Cd,
		AO_Ind,
		Article_Seq_Num,
		Junk_Seq_Num,		
		Disc_Item_Qty,
		Discount_Amt
)
	Select	
		st.Scan_Code_Seq_Num as Scan_Code_Seq_Num,
		st.Tran_Dt as Tran_Dt,
		st.Store_Seq_Num as Store_Seq_Num,
		st.Customer_Seq_Num as Customer_Seq_Num,
		st.Calendar_Week_Id as Calendar_Week_Id,
		st.Calendar_Month_Id as Calendar_Month_Id,
		st.Discount_Type_Cd as Discount_Type_Cd,
		st.AO_Ind as AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num,			
		SUM(st.Disc_Item_Qty) as Disc_Item_Qty,
		SUM(st.Discount_Amt) as Discount_Amt

		From
	(
		Select	
		stl0.Scan_Code_Seq_Num,
		stl0.Tran_Dt,
		stl0.Store_Seq_Num,
		stl0.Sold_To_Party_Seq_Num As Customer_Seq_Num,
		c.Calendar_Week_Id,
		c.Calendar_Month_Id,
		stl0.AO_Ind,
		stl0.Hist_Article_Seq_Num as Article_Seq_Num,
		stl0.Junk_Seq_Num,			
		stdl1.Discount_Type_Cd As Discount_Type_Cd,
		Case 
			When sta1.Actual_Item_Qty Is Not Null 
			Then sta1.Actual_Item_Qty * stl1.Item_Qty
			Else stl1.Item_Qty
		End	As Disc_Item_Qty,
		stdl1.Discount_Amt_Excl_Vat as Discount_Amt,
		stl0.InsertDttm 
	From ${DB_ENV}SemCMNVOUT.SALES_TRANSACTION_LINE_X As stl0
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On stl0.Tran_Dt = c.Calendar_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_DISCOUNT_LINE As stdl1
		On	stdl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	stdl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stdl1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	stdl1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st1
		On	st1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	st1.Store_Seq_Num = stl0.Store_Seq_Num
		And	st1.Tran_Dt_DD = stl0.Tran_Dt
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE As stl1
		On	stl1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	Stl1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	stl1.Store_Seq_Num = stl0.Store_Seq_Num
		And	stl1.Tran_Dt_DD = stl0.Tran_Dt
	Left Outer Join  ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE_ACTUAL As sta1
		On	sta1.Sales_Tran_Seq_Num = stl0.Sales_Tran_Seq_Num
		And	sta1.Sales_Tran_Line_Num = stl0.Sales_Tran_Line_Num
		And	sta1.Store_Seq_Num = stl0.Store_Seq_Num 
		And	sta1.Tran_Dt_DD = stl0.Tran_Dt
	)As st

	Where st.Tran_Dt between date '2017-02-01' and date '2017-03-31'
		
		Group by 
		st.Scan_Code_Seq_Num,
		st.Tran_Dt,
		st.Store_Seq_Num,
		st.Customer_Seq_Num,
		st.Calendar_Week_Id,
		st.Calendar_Month_Id,
		st.Discount_Type_Cd,
		st.AO_Ind,
		st.Article_Seq_Num,
		st.Junk_Seq_Num		
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRAN_TOTALS_DAY_F
(
	Contact_Account_Seq_Num,
	Store_Seq_Num,
	Tran_Dt,
	Calendar_Week_Id,
	Calendar_Month_Id,
	Junk_Seq_Num,
	Grand_Amt,
	Tot_Discount_Amt,
	Employee_Discount_Based_Amt,
	Tot_Loy_Discount_Amt
)   
    Select	
	st1.Contact_Account_Seq_Num as Contact_Account_Seq_Num,
	st1.Store_Seq_Num as Store_Seq_Num,
	st1.Tran_Dt as Tran_Dt,
	st1.Calendar_Week_Id as Calendar_Week_Id,
	st1.Calendar_Month_Id as Calendar_Month_Id,
	st1.Junk_seq_num,
	SUM(st1.Grand_Amt) as Grand_Amt,
	SUM(st1.Tot_Discount_Amt) as Tot_Discount_Amt,
	SUM(st1.Employee_Discount_Based_Amt) as Employee_Discount_Based_Amt,
	SUM(st1.Tot_Loy_Discount_Amt) as Tot_Loy_Discount_Amt
	
	From
	(
		Select 
			st.Sales_Tran_Seq_Num,
			st.N_Sales_Tran_Id Sales_Tran_Id,
			st.Store_Seq_Num,
			st.Tran_Dt_DD As Tran_Dt,
			st.Tran_End_Dttm_DD As Tran_End_Dttm,
			st.Store_Department_Seq_Num,
			Coalesce(stp.Sold_To_Party_Id,'-1') As Customer_Id,
			Coalesce(stp.Party_Seq_Num,'-1') As Customer_Seq_Num,
			Coalesce(stla.Loyalty_Account_Id,'-1') As Loyalty_Account_Id,
			Coalesce(stlc.Contact_Account_Seq_Num,-1) As Contact_Account_Seq_Num,
			Coalesce(stlim.Loyalty_Identifer_Seq_Num,-1) AS Loyalty_Identifier_Seq_Num, 
			stc.Circumstance_Value_Cd As Receipt_Type_Cd,
			st.POS_Register_Seq_Num,
			st.Sales_Associate_Id,
			Coalesce(j1.Junk_Seq_Num,-1) as Junk_Seq_Num,
			Coalesce(sttpd.Total_Amt , 0) As Employee_Discount_Based_Amt,
			Coalesce(sttga.Total_Amt , 0) As Grand_Amt,
			Coalesce(stttd.Total_Amt , 0) As Tot_Discount_Amt,
			Coalesce(sttld.Total_Amt , 0) As Tot_Loy_Discount_Amt,

			st.OpenJobRunID,
			st.InsertDttm,
			c.Calendar_Week_Id,
			c.Calendar_Month_Id 
		From ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st
		Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
		  On st.Tran_Dt_DD = c.Calendar_Dt

		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttpd
		 On st.Sales_Tran_Seq_Num = sttpd.Sales_Tran_Seq_Num 
		 And st.Store_Seq_Num = sttpd.Store_Seq_Num 
		 And st.Tran_Dt_DD = sttpd.Tran_Dt_DD
		 And sttpd.Tran_Type_Cd = 'PD' 

		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttga
		 On st.Sales_Tran_Seq_Num = sttga.Sales_Tran_Seq_Num 
		 And st.Store_Seq_Num = sttga.Store_Seq_Num 
		 And st.Tran_Dt_DD = sttga.Tran_Dt_DD
		 And sttga.Tran_Type_Cd = 'GA' 

		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As stttd
		 On st.Sales_Tran_Seq_Num = stttd.Sales_Tran_Seq_Num 
		 And st.Store_Seq_Num = stttd.Store_Seq_Num 
		 And st.Tran_Dt_DD = stttd.Tran_Dt_DD
		 And stttd.Tran_Type_Cd = 'TD' 
		 
		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_TOTALS As sttld
		 On st.Sales_Tran_Seq_Num = sttld.Sales_Tran_Seq_Num 
		 And st.Store_Seq_Num = sttld.Store_Seq_Num 
		 And st.Tran_Dt_DD = sttld.Tran_Dt_DD
		 And sttld.Tran_Type_Cd = 'LD'  

		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT As stlc 
			On st.Sales_Tran_Seq_Num = stlc.Sales_Tran_Seq_Num
			And st.Store_Seq_Num = stlc.Store_Seq_Num 
			And st.Tran_Dt_DD = stlc.Tran_Dt_DD

		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_ID_METHOD As stlim 
			On st.Sales_Tran_Seq_Num = stlim.Sales_Tran_Seq_Num
			And st.Store_Seq_Num = stlim.Store_Seq_Num 
			And st.Tran_Dt_DD = stlim.Tran_Dt_DD
		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_PARTY As stp
			On st.Sales_Tran_Seq_Num = stp.Sales_Tran_Seq_Num
			And st.Store_Seq_Num = stp.Store_Seq_Num 
			And st.Tran_Dt_DD = stp.Tran_Dt_DD
		Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOYALTY_ACCOUNT As stla
			On st.Sales_Tran_Seq_Num = stla.Sales_Tran_Seq_Num
			And st.Store_Seq_Num = stla.Store_Seq_Num 
			And st.Tran_Dt_DD = stla.Tran_Dt_DD
		Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_CIRCUMSTANCE As stc
			On st.Sales_Tran_Seq_Num = stc.Sales_Tran_Seq_Num
			And st.Store_Seq_Num = stc.Store_Seq_Num 
			And st.Tran_Dt_DD = stc.Tran_Dt_DD
			And stc.Circumstance_Cd = 'COTP'

		LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
			On st.Store_Seq_Num = std1.Store_Seq_Num and st.Tran_Dt_DD = std1.Calendar_Dt
		LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
			on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') 
			and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
			and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') 
			and j1.Loyalty_Ind = 0
			
		Where st.Tran_Status_Cd in ( 'CT', 'VO')
		And st.Tran_Type_Cd = 'POS'
	) As st1
	
	WHERE st1.Tran_Dt between date '2017-02-01' and date '2017-03-31'
	
	Group by
		st1.Contact_Account_Seq_Num,
		st1.Store_Seq_Num,
		st1.Tran_Dt,
		st1.Calendar_Week_Id,
		st1.Calendar_Month_Id,
		st1.Junk_seq_num
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO ${DB_ENV}SemCMNT.SALES_TRANSACTION_DAY_F
(
	Store_Seq_Num,
	Store_Department_Seq_Num,
	Tran_Dt,
	Contact_Account_Seq_Num,
	Customer_Seq_Num,
	Calendar_Week_Id,
    Calendar_Month_Id,
	Junk_Seq_Num,
	Receipt_Cnt
)   
	Select 
		st.Store_Seq_Num as Store_Seq_Num,
		st.Store_Department_Seq_Num as Store_Department_Seq_Num,
		st.Tran_Dt_DD as Tran_Dt,
		Coalesce(stlc.Contact_Account_Seq_Num,-1) as Contact_Account_Seq_Num,
		Coalesce(stp.Party_Seq_Num,-1) as Customer_Seq_Num,
		c.Calendar_Week_Id as Calendar_Week_Id,
		c.Calendar_Month_Id as Calendar_Month_Id,
		Coalesce(j1.Junk_Seq_Num,-1) as Junk_Seq_Num,
		SUM(
			Case When st.Tran_Status_Cd = 'VO' Then -1 
			When st.Tran_Status_Cd = 'CT' Then 1
			Else 0 
			End
		) as Receipt_Cnt
		
	From ${DB_ENV}TgtVOUT.SALES_TRANSACTION As st
	Inner Join ${DB_ENV}TgtVOUT.CALENDAR_DATE As c
	  On st.Tran_Dt_DD = c.Calendar_Dt
	Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRANSACTION_PARTY As stp
		On st.Sales_Tran_Seq_Num = stp.Sales_Tran_Seq_Num
		And st.Store_Seq_Num = stp.Store_Seq_Num 
		And st.Tran_Dt_DD = stp.Tran_Dt_DD
	Left Outer Join ${DB_ENV}TgtVOUT.SALES_TRAN_LOY_CONTACT As stlc 
		On st.Sales_Tran_Seq_Num = stlc.Sales_Tran_Seq_Num
		And st.Store_Seq_Num = stlc.Store_Seq_Num 
		And st.Tran_Dt_DD = stlc.Tran_Dt_DD
	Inner Join ${DB_ENV}TgtVOUT.SALES_TRAN_CIRCUMSTANCE As stc
		On st.Sales_Tran_Seq_Num = stc.Sales_Tran_Seq_Num
		And st.Store_Seq_Num = stc.Store_Seq_Num 
		And st.Tran_Dt_DD = stc.Tran_Dt_DD
		And stc.Circumstance_Cd = 'COTP'
	LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.STORE_DETAILS_DAY_D as std1
		On st.Store_Seq_Num = std1.Store_Seq_Num and st.Tran_Dt_DD = std1.Calendar_Dt
	LEFT OUTER JOIN ${DB_ENV}SemCMNVOUT.JUNK_D as j1
		on j1.Concept_Cd = coalesce(std1.Concept_Cd,'-1') 
		and j1.Corp_Affiliation_Type_Cd = coalesce(std1.Corp_Affiliation_Type_Cd,'-1')
		and j1.Retail_Region_Cd = coalesce(std1.Retail_Region_Cd,'-1') 
		and j1.Loyalty_Ind = 0

	Where st.Tran_Dt_DD between date '2017-02-01' and date '2017-03-31' And
	st.Tran_Type_Cd = 'POS'
	And st.Tran_Status_Cd in ( 'CT', 'VO')
	And (st.Sales_Tran_Seq_Num, st.Store_Seq_Num, st.Tran_Dt_DD) In
	(Select 
		stl2.Sales_Tran_Seq_Num
		,stl2.Store_Seq_Num 
		,stl2.Tran_Dt_DD
	From ${DB_ENV}TgtVOUT.SALES_TRANSACTION_LINE stl2
	Where stl2.Tran_Line_Status_Cd = 'CT')

	Group by 1,2,3,4,5,6,7,8
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.LABEL NoFix1
