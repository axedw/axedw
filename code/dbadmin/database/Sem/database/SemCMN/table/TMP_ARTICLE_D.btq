/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: TMP_ARTICLE_D.btq 23027 2017-09-06 15:09:10Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2017-09-06 17:09:10 +0200 (ons, 06 sep 2017) $
# Last Revision    : $Revision: 23027 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/table/TMP_ARTICLE_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}SemCMNT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','TMP_ARTICLE_D','PRE','IO',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}SemCMNT.TMP_ARTICLE_D
(	Article_Seq_Num	INTEGER NOT NULL,
	Article_Id	VARCHAR(20) NOT NULL,
	Article_Desc	VARCHAR(250) NOT NULL,
	Article_Business_Expiry_Dt	DATE NOT NULL COMPRESS (DATE '9999-12-31'),
	Article_Available_From_Dt	DATE NOT NULL,
	Article_Logical_Delete_Dt	DATE NOT NULL COMPRESS (DATE '9999-12-31'),
	Central_Assortment_Ind	CHAR(1) NOT NULL COMPRESS 'J',
	Package_Name	VARCHAR(100),
	Brand_Name	VARCHAR(100),
	Vendor_Seq_Num	INTEGER NOT NULL,
	Pref_Vendor_Seq_Num	INTEGER NOT NULL,
	Tax_Group_Cd	CHAR(2) NOT NULL COMPRESS('-1','1','2'),
	Assort_Mtrx_Row_Cd	BYTEINT NOT NULL DEFAULT -1,
	Assort_Mtrx_Col_Cd	BYTEINT NOT NULL DEFAULT -1,
	Out_Of_Stock_Ind	BYTEINT COMPRESS (0,1),
	Ordering_Unit_Qty 	DECIMAL(18,4),
	Ordering_Unit_UOM_Cd	CHAR(4) NOT NULL,
	Master_Scan_Cd_Ordering_UOM VARCHAR(20),
	Net_Qty	DECIMAL(18,8),
	Gross_Qty DECIMAL(18,8),
	Net_Qty_UOM_Cd		CHAR(4) NOT NULL,
	Comparison_Unit		VARCHAR(100),
	Article_Type_Cd		CHAR(4) NOT NULL,
	Article_Property_1	VARCHAR(100),
	Article_Property_2	VARCHAR(100),
	Art_Hier_Lvl_8_Seq_Num	INTEGER NOT NULL,
	Art_Hier_Lvl_4_Seq_Num	INTEGER NOT NULL,
	Art_Hier_Lvl_3_Seq_Num	INTEGER NOT NULL,
	Art_Hier_Lvl_2_Seq_Num	INTEGER NOT NULL,
	Prod_Hier_Lvl3_Seq_Num	INTEGER NOT NULL,
	SAP_Article_Id VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
	Base_Unit_UOM_Cd CHAR(4) CHARACTER SET LATIN NOT CASESPECIFIC,
	Master_Scan_Cd_Base_UOM VARCHAR(20),
	Annual_Season_Seq_Num INTEGER NOT NULL,
	Transportation_Group_Cd	VARCHAR(50)  DEFAULT '-1' ,
	Packaging_Type_Cd CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
	Vendor_Article_Id VARCHAR(20),
	Article_Class_Cd CHAR(6) CHARACTER SET LATIN NOT CASESPECIFIC,
	Unit_Weight_Ind BYTEINT COMPRESS (0,1),	
	CONSTRAINT PKARTICLE_TMP PRIMARY KEY (Article_Seq_Num),
	CONSTRAINT AKARTICLE_TMP UNIQUE(Article_Id)
)
 INDEX S1ARTICLE_TMP (Art_Hier_Lvl_8_Seq_Num)
,INDEX S2ARTICLE_TMP (Prod_Hier_Lvl3_Seq_Num)
,INDEX S3ARTICLE_TMP (Art_Hier_Lvl_4_Seq_Num)
,INDEX S4ARTICLE_TMP (Art_Hier_Lvl_3_Seq_Num)
,INDEX S5ARTICLE_TMP (Art_Hier_Lvl_2_Seq_Num)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','TMP_ARTICLE_D','POST','IO',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNT.TMP_ARTICLE_D IS '$Revision: 23027 $ - $Date: 2017-09-06 17:09:10 +0200 (ons, 06 sep 2017) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- Define additional/sample Statistics 
COLLECT STATISTICS COLUMN PARTITION
ON ${DB_ENV}SemCMNT.TMP_ARTICLE_D
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
