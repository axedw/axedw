/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SALES_TRAN_D_L_ITEM_DAY_F.btq 24462 2018-03-10 10:24:59Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2018-03-10 11:24:59 +0100 (lör, 10 mar 2018) $
# Last Revision    : $Revision: 24462 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/table/SALES_TRAN_D_L_ITEM_DAY_F.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}SemCMNT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','SALES_TRAN_D_L_ITEM_DAY_F','PRE','IO',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F
(
	Scan_Code_Seq_Num INTEGER NOT NULL DEFAULT -1,
	Tran_Dt DATE FORMAT 'yyyy-mm-dd' NOT NULL DEFAULT DATE '0001-01-01',
	Store_Seq_Num INTEGER NOT NULL DEFAULT -1,
	Customer_Seq_Num INTEGER NOT NULL DEFAULT -1,
	Calendar_Week_Id INTEGER NOT NULL DEFAULT -1,
	Calendar_Month_Id INTEGER NOT NULL DEFAULT -1,
	Discount_Type_Cd CHAR(2) DEFAULT '-1' COMPRESS ('CO','MA','PO'),
	AO_Ind BYTEINT NOT NULL DEFAULT 0 COMPRESS (0,1),
	Article_Seq_Num INTEGER NOT NULL,
	Junk_Seq_Num INTEGER NOT NULL,	
	Disc_Item_Qty DECIMAL(18,4) COMPRESS (0.0000,1.0000,2.0000),
	Discount_Amt DECIMAL(18,4) COMPRESS 0.0000,
	SemInsertDttm TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
	Agreement_Seq_Num INTEGER NOT NULL DEFAULT -1 COMPRESS -1
)
PRIMARY INDEX PISALES_TRAN_D_L_ITEM_DAY_F ( Scan_Code_Seq_Num, Tran_Dt, Store_Seq_Num )
PARTITION BY (RANGE_N ( Tran_Dt 
	-- Dynamic monthly partitioning moving 36 months (from -36 to +1)
	-- Run alter to current on 1st of month
	BETWEEN  -- Always set start range to 1st of month
    ADD_MONTHS((DATE - (CAST(((EXTRACT(DAY FROM (DATE )))- 1 ) AS INTERVAL DAY(2)))),(-36 )) 
    AND 
    (ADD_MONTHS((DATE - (CAST(((EXTRACT(DAY FROM (DATE )))- 1 ) AS INTERVAL DAY(2)))),(1 )))- INTERVAL '1' DAY 
    EACH INTERVAL '1' MONTH ) 
    ,RANGE_N(Store_Seq_Num BETWEEN 1 AND 1000 EACH 1 , NO RANGE OR UNKNOWN)
)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','SALES_TRAN_D_L_ITEM_DAY_F','POST','IO',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNT.SALES_TRAN_D_L_ITEM_DAY_F IS '$Revision: 24462 $ - $Date: 2018-03-10 11:24:59 +0100 (lör, 10 mar 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
