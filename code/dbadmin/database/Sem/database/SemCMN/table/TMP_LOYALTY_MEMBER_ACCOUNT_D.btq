/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: TMP_LOYALTY_MEMBER_ACCOUNT_D.btq 20280 2016-11-04 07:38:29Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2016-11-04 08:38:29 +0100 (fre, 04 nov 2016) $
# Last Revision    : $Revision: 20280 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemCMN/table/TMP_LOYALTY_MEMBER_ACCOUNT_D.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0
;

DATABASE ${DB_ENV}SemCMNT
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','TMP_LOYALTY_MEMBER_ACCOUNT_D','PRE','IO',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}SemCMNT.TMP_LOYALTY_MEMBER_ACCOUNT_D
(	Member_Account_Seq_Num INTEGER NOT NULL DEFAULT -1
	,Member_Account_Id VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC
	,Loyalty_Program_Seq_Num BYTEINT NOT NULL DEFAULT -1
	,Issuing_Party_Seq_Num INTEGER NOT NULL DEFAULT -1
	,Issuing_Org_Party_Id VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC NOT NULL DEFAULT '-1'
	,Member_Account_Open_Dttm TIMESTAMP(6)
	,Member_Account_Close_Dttm TIMESTAMP(6)
	,Member_Account_Open_Dt DATE FORMAT 'YYYY-MM-DD'
	,Member_Account_Close_Dt DATE FORMAT 'YYYY-MM-DD' COMPRESS(DATE '9999-12-31')
	,Last_Status_Update_Dt TIMESTAMP(6)
	,Last_Statement_Dt DATE FORMAT 'YYYY-MM-DD'
	,Member_Enrollment_Channel_Cd VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC NOT NULL DEFAULT '-1' COMPRESS (
		'Paper Form', 'Paper Account', 'Web','MSR','STAFF_WEB','SMS','IPHONE','ANDROID','-1','UNKNOWN','SELFSCAN')
	,Home_Store_Seq_Num INTEGER
	,Loyalty_Status_Cd VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC COMPRESS(
		'Active','Pre Alloted','Cancelled','Inactive','Merged','Deleted','-1')
	,Loyalty_Member_Type VARCHAR(10) CHARACTER SET LATIN NOT CASESPECIFIC COMPRESS(
		'Lojalitet','VISA','Saknas')
	,Membership_Num VARCHAR(80) CHARACTER SET LATIN NOT CASESPECIFIC
	,Membership_Name VARCHAR(100) CHARACTER SET LATIN NOT CASESPECIFIC
	,HemkopBonusPoint DECIMAL(18,4) COMPRESS(0.0000)
	,YearlyAccumulatedDiscount DECIMAL(18,4) COMPRESS(0.0000)
	,MonthlyAccumulatedDiscount DECIMAL(18,4) COMPRESS(0.0000)
	,HasEmail_Ind BYTEINT NOT NULL DEFAULT 0 COMPRESS(0,1)
	,HasMobile_Ind BYTEINT NOT NULL DEFAULT 0 COMPRESS(0,1)
	,Physical_Card_Ind BYTEINT NOT NULL DEFAULT 0 COMPRESS(0,1)
	,Employee_Discount_Ind BYTEINT NOT NULL  COMPRESS(0,1)
	,Digital_Coupon_Ind BYTEINT NOT NULL COMPRESS(0,1)
	,CNA_Ind BYTEINT NOT NULL DEFAULT 0 COMPRESS(0,1)
	,Last_Tran_Dt DATE FORMAT 'YYYY-MM-DD')
	UNIQUE PRIMARY INDEX ( Member_Account_Seq_Num )
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemCMNT','TMP_LOYALTY_MEMBER_ACCOUNT_D','POST','IO',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemCMNT.TMP_LOYALTY_MEMBER_ACCOUNT_D IS '$Revision: 20280 $ - $Date: 2016-11-04 08:38:29 +0100 (fre, 04 nov 2016) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
