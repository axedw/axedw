/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: MSTR_PRODUCTIVITY_DC.btq 24542 2018-03-29 09:16:54Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-03-29 11:16:54 +0200 (tor, 29 mar 2018) $
# Last Revision    : $Revision: 24542 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemMetadata/table/MSTR_PRODUCTIVITY_DC.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

.SET MAXERROR 0;

DATABASE ${DB_ENV}SemMetadataT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemMetadataT','MSTR_PRODUCTIVITY_DC','PRE','IO',1)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}SemMetadataT.MSTR_PRODUCTIVITY_DC
     (
      From_DC_Id INTEGER NOT NULL DEFAULT -1,
      To_DC_Id INTEGER NOT NULL DEFAULT -1,
	  CONSTRAINT PKMSTR_PRODUCTIVITY_DC PRIMARY KEY (From_DC_Id)
	  )
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemMetadataT','MSTR_PRODUCTIVITY_DC','POST','IO',1)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemMetadataT.MSTR_PRODUCTIVITY_DC IS '$Revision: 24542 $ - $Date: 2018-03-29 11:16:54 +0200 (tor, 29 mar 2018) $ '
;

REPLACE VIEW ${DB_ENV}SemMetadataVOUT.MSTR_PRODUCTIVITY_DC
AS LOCKING ROW FOR ACCESS 
SELECT	
From_DC_Id,
To_DC_Id, 
coalesce(d1.Loc_Seq_Num,-1) as From_Location_Seq_Num,
coalesce(d2.Loc_Seq_Num,-1) as To_Location_Seq_Num
FROM ${DB_ENV}SemMetadataT.MSTR_PRODUCTIVITY_DC m1

INNER JOIN ${DB_ENV}MetaDataVOUT.MAP_LOC as d1
on m1.From_DC_Id = d1.N_Loc_Id

INNER JOIN ${DB_ENV}MetaDataVOUT.MAP_LOC as d2
on m1.To_DC_Id = d2.N_Loc_Id
;

REPLACE VIEW ${DB_ENV}SemMetadataVOUT.MSTR_PRODUCTIVITY_DC_ZONE
AS LOCKING ROW FOR ACCESS 
SELECT	
From_DC_Id,
To_DC_Id, 
d1.Loc_Seq_Num as From_Location_Seq_Num,
d2.Loc_Seq_Num as To_Location_Seq_Num,
dz1.Location_Zone_Seq_Num as From_Location_Zone_Seq_Num,
dz2.Location_Zone_Seq_Num as To_Location_Zone_Seq_Num

FROM ${DB_ENV}SemMetadataT.MSTR_PRODUCTIVITY_DC m1

INNER JOIN ${DB_ENV}MetaDataVOUT.MAP_LOC as d1
on m1.From_DC_Id = d1.N_Loc_Id

INNER JOIN ${DB_ENV}MetaDataVOUT.MAP_LOC as d2
on m1.To_DC_Id = d2.N_Loc_Id

INNER JOIN ${DB_ENV}MetaDataVOUT.MAP_LOCATION_ZONE as dz1
on m1.From_DC_Id = dz1.N_Loc_Id

INNER JOIN ${DB_ENV}MetaDataVOUT.MAP_LOCATION_ZONE as dz2
on m1.To_DC_Id = dz2.N_Loc_Id and dz1.N_Location_Zone_Id = dz2.N_Location_Zone_Id
;

COLLECT STATISTICS 
	COLUMN(From_DC_Id),
	COLUMN(To_DC_Id),
	COLUMN(From_DC_Id,To_DC_Id)
ON ${DB_ENV}SemMetadataT.MSTR_PRODUCTIVITY_DC
;


.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
