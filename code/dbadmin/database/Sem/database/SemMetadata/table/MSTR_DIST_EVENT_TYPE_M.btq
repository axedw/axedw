/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: MSTR_DIST_EVENT_TYPE_M.btq 24629 2018-04-05 12:01:39Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-04-05 14:01:39 +0200 (tor, 05 apr 2018) $
# Last Revision    : $Revision: 24629 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Sem/database/SemMetadata/table/MSTR_DIST_EVENT_TYPE_M.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

.SET MAXERROR 0;

DATABASE ${DB_ENV}SemMetadataT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemMetadataT','MSTR_DIST_EVENT_TYPE_M','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


CREATE TABLE ${DB_ENV}SemMetadataT.MSTR_DIST_EVENT_TYPE_M
(
	M_Seq_Num INTEGER NOT NULL, 
	Distribution_Event_Subtype_Cd	VARCHAR(50) NOT NULL,
	Pallet_Ind	BYTEINT NOT NULL,
	ModifiedBy_User	VARCHAR(50) NOT NULL,
	Valid_From_Dttm	TIMESTAMP(6)  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT current_timestamp NOT NULL ,
	Discontinued_Ind SMALLINT NOT NULL DEFAULT 0,
	CONSTRAINT PKMSTR_DIST_EVENT_TYPE_M PRIMARY KEY (M_Seq_Num),
	CONSTRAINT XAK1MSTR_DIST_EVENT_TYPE_M  UNIQUE (Distribution_Event_Subtype_Cd,Valid_From_Dttm)
)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}SemMetadataT','MSTR_DIST_EVENT_TYPE_M','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}SemMetadataT.MSTR_DIST_EVENT_TYPE_M IS '$Revision: 24629 $ - $Date: 2018-04-05 14:01:39 +0200 (tor, 05 apr 2018) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

-- Define additional/sample Statistics 
COLLECT STATISTICS 
COLUMN PARTITION
,COLUMN (Distribution_Event_Subtype_Cd,Valid_From_Dttm)
,COLUMN M_Seq_Num
,COLUMN (DISTRIBUTION_EVENT_SUBTYPE_CD)
,COLUMN (DISCONTINUED_IND)
ON ${DB_ENV}SemMetadataT.MSTR_DIST_EVENT_TYPE_M
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}SemMetadataVOUT.MSTR_DIST_EVENT_TYPE_M
 AS LOCKING ROW FOR ACCESS 
 SELECT  m1.M_Seq_Num
	, m1.Distribution_Event_Subtype_Cd
	, m1.Pallet_Ind
 FROM ${DB_ENV}SemMetadataT.MSTR_DIST_EVENT_TYPE_M m1
 JOIN (SELECT Trim(Distribution_Event_Subtype_Cd) AS Distribution_Event_Subtype_Cd, Max(Valid_From_Dttm) AS Max_Valid_From_Dttm FROM ${DB_ENV}SemMetadataT.MSTR_DIST_EVENT_TYPE_M GROUP BY  Trim(Distribution_Event_Subtype_Cd) ) m2
 ON Trim(m1.Distribution_Event_Subtype_Cd) = m2.Distribution_Event_Subtype_Cd AND m1.Valid_From_Dttm = m2.Max_Valid_From_Dttm 
 WHERE m1.Discontinued_Ind = 0
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE




