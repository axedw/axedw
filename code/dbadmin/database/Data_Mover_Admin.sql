-- ----------------------------------------------------------------------------
-- User: PR_Data_Mover_Admin' (Production)
-- ----------------------------------------------------------------------------
CREATE USER PR_Data_Mover_Admin
FROM PRdbadmin
AS	PERM = 0
	PASSWORD = ******
	PROFILE = DBAdminP
	DEFAULT ROLE = ALL;

COMMENT ON USER PR_Data_Mover_Admin IS 'Admin user for Teradata Data Mover';

GRANT ALL ON PR_Data_Mover_Admin
	TO PR_Data_Mover_Admin,PRdbadmin,DBADMIN,DBC
	WITH GRANT OPTION;

SELECT * FROM DBC.Databases WHERE DatabaseName = 'PR_Data_Mover_Admin' AND OwnerName <> 'PRLoadUsers';

GIVE PR_Data_Mover_Admin TO PRLoadUsers;

GRANT PRreadR TO PR_Data_Mover_Admin;
GRANT ALL ON TDdbadmin TO PR_Data_Mover_Admin;

-- ----------------------------------------------------------------------------
-- User: IT_Data_Mover_Admin' (Test)
-- ----------------------------------------------------------------------------
CREATE USER IT_Data_Mover_Admin
FROM ITdbadmin
AS	PERM = 0
	PASSWORD = ******
	PROFILE = DBAdminP
	DEFAULT ROLE = ALL
;

COMMENT ON USER IT_Data_Mover_Admin IS 'Admin user for Teradata Data Mover';

GRANT ALL ON IT_Data_Mover_Admin
	TO IT_Data_Mover_Admin,ITdbadmin,DBADMIN,DBC
	WITH GRANT OPTION;

GIVE IT_Data_Mover_Admin TO ITLoadUsers;

GRANT ALL ON ITADST TO IT_Data_Mover_Admin;
GRANT ALL ON ITMetaDataT TO IT_Data_Mover_Admin;
GRANT ALL ON ITSemCMNT TO IT_Data_Mover_Admin;
GRANT ALL ON ITSemEXPT TO IT_Data_Mover_Admin;
GRANT ALL ON ITTgtT TO IT_Data_Mover_Admin;
GRANT ITdeveloperR TO  IT_Data_Mover_Admin;

GRANT ALL ON ITDMRes TO IT_Data_Mover_Admin;
GRANT ALL ON DMRestoreADST TO IT_Data_Mover_Admin;
GRANT ALL ON DMRestoreAxCRMBI TO IT_Data_Mover_Admin;
GRANT ALL ON DMRestoreFilteredTables TO IT_Data_Mover_Admin;
GRANT ALL ON DMRestoreMetaDataT TO IT_Data_Mover_Admin;
GRANT ALL ON DMRestoreSemCMNT TO IT_Data_Mover_Admin;
GRANT ALL ON DMRestoreSemEXPT TO IT_Data_Mover_Admin;
GRANT ALL ON DMRestoreTgtT TO IT_Data_Mover_Admin;
	
