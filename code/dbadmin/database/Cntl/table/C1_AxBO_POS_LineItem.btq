/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: C1_AxBO_POS_LineItem.btq 29765 2019-12-19 12:48:14Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-12-19 13:48:14 +0100 (tor, 19 dec 2019) $
# Last Revision    : $Revision: 29765 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Cntl/table/C1_AxBO_POS_LineItem.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
--
.SET MAXERROR 0;

DATABASE ${DB_ENV}CntlCleanseT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}CntlCleanseT','C1_AxBO_POS_LineItem','PRE','V',1)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE SET TABLE ${DB_ENV}CntlCleanseT.C1_AxBO_POS_LineItem
     (
      XPK_in_LineItem BIGINT NOT NULL,
      FK_in_BOArchiveMsg BIGINT NOT NULL,
      LineItem_EntryMethod CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      LineItem_VoidFlag VARCHAR(5) CHARACTER SET LATIN NOT CASESPECIFIC,
      LineItem_VoidTypeCode CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      LineItem_SequenceNr INTEGER,
      ReceiptLineItemSequenceNr INTEGER,
      LineItem_EndDateTimestamp TIMESTAMP(6),
      CustomerID VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      SAPCustomerID VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      Customer_AgreementID VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      Customer_DiscountAmount DECIMAL(18,4),
      Sale_ItemType CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      Sale_BulkItemFlag VARCHAR(5) CHARACTER SET LATIN NOT CASESPECIFIC,
      Sale_ReturnFlag VARCHAR(5) CHARACTER SET LATIN NOT CASESPECIFIC,
      Sale_ReasonCode CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      MerchandiseStructureID VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      SumMerchandiseHierarchyGroupID VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      TaxGroupID1 VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      LineItem_Description VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
      LineItem_ActualUnitPrice DECIMAL(18,4),
	  LineItem_RegularItemPrice DECIMAL(18,4),
      LineItem_AdditionalTaxAmount DECIMAL(18,4),
      LineItem_CostUnitPrice DECIMAL(18,4),
      LineItem_OriginalCostUnitPrice DECIMAL(18,4),
      LineItem_Quantity DECIMAL(18,4),
      LineItem_Units DECIMAL(18,4),
      POSIdentityItemCount DECIMAL(18,4),
      ItemID VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      POSIdentityID VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      SalesLocation VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      TaxGroupID VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      TaxableAmount DECIMAL(18,4),
      TaxAmount DECIMAL(18,4),
      TotalTaxGroupID VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      TotalTaxableAmount DECIMAL(18,4),
      TotalTaxAmount DECIMAL(18,4),
      TenderType_ReturnFlag VARCHAR(5) CHARACTER SET LATIN NOT CASESPECIFIC,
      TenderID VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      TenderTypeCode CHAR(2) CHARACTER SET LATIN NOT CASESPECIFIC,
      Tender_Amount DECIMAL(18,4),
      ExchangeRate DECIMAL(18,4),
      ForeginCurrencyAmount DECIMAL(18,4),
      CouponNr VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
	  UniqueCouponNumber VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      CouponTypeCode VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      CheckAccountNr VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      AccountNr VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      SerialNr VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      Credit_CustomerAccountNr VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      Credit_CustomerIdentification VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      Credit_RequisitionNr VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
      CustomerAccountCardID VARCHAR(20) CHARACTER SET LATIN NOT CASESPECIFIC,
      MemberCardNumber VARCHAR(80) CHARACTER SET LATIN NOT CASESPECIFIC,
      Identifier VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
      IdentifierType VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
	  Coupon_TypeCode VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
	  Coupon_Number VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
	  Coupon_UniqueNumber VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
	  Coupon_Amount DECIMAL(18,4),
	  Coupon_TypeCode_rank VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
	  Coupon_Number_rank VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,	  
      LineItem_LineType VARCHAR(255) CHARACTER SET LATIN NOT CASESPECIFIC,
	  DigitalReceipt VARCHAR(10) CHARACTER SET LATIN NOT CASESPECIFIC,
Traceability_Id VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
	  BatchID VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
	  SerialNumber VARCHAR(50) CHARACTER SET LATIN NOT CASESPECIFIC,
	  SourceVersion VARCHAR(10),
      LastModificationDttm TIMESTAMP(6) FORMAT 'YYYY-MM-DDHH:MI:SS.S(6)' NOT NULL DEFAULT CURRENT_TIMESTAMP(6))
PRIMARY INDEX ( FK_in_BOArchiveMsg );

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}CntlCleanseT','C1_AxBO_POS_LineItem','POST','V',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}CntlCleanseT.C1_AxBO_POS_LineItem IS '$Revision: 29765 $ - $Date: 2019-12-19 13:48:14 +0100 (tor, 19 dec 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
