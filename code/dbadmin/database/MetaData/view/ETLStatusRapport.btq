/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ETLStatusRapport.btq 24959 2018-05-24 10:43:12Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2018-05-24 12:43:12 +0200 (tor, 24 maj 2018) $
# Last Revision    : $Revision: 24959 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/MetaData/view/ETLStatusRapport.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

DATABASE ${DB_ENV}MetaDataVOUT
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

REPLACE VIEW ${DB_ENV}MetaDataVOUT.ETLStatusRapport
 AS LOCKING ROW FOR ACCESS
SELECT
Case 
When batch_Name like 'LoadToTarget%' Then 'LoadToTarget' 
when batch_Name like 'Semantisk%' Then 'Semantisk' 
else 'x_Ovrigt' end as batch_type, 
batch_Name as Name,
LatestRunTime,
LatestRunStartTime

FROM
(select 
batch_Name,
LatestRunTime,
LatestRunStartTime
from
(SELECT   
Case 
When FolderName = 'LoadToTarget' and streamname = 'wf_APP1_80_T1_POS' Then 'LoadToTarget - daglig POS' 
when FolderName = 'LoadToTarget' and streamname = 'wf_APP_80_T1_LoadToTarget' Then 'LoadToTarget - �vrigt' 
when FolderName = 'LoadToTarget' and streamname = 'wf_APP_90_T1_LoadToTarget' Then 'LoadToTarget - CRM1' 
when FolderName = 'LoadToTarget' and streamname = 'wf_APP_95_T1_LoadToTarget' Then 'LoadToTarget - CRM2'
when FolderName = 'LoadToTarget' and streamname = 'wf_APP1_80_T1_SAPPromotion' Then 'LoadToTarget - PMR'             
when FolderName = 'LoadToTarget' and streamname = 'wf_APP7_80_T1_ACN' Then 'LoadToTarget - AC Nielsen'
when FolderName = 'LoadToTarget' and streamname = 'wf_APP8_80_T1_FinancialInformation' Then 'LoadToTarget - Financial data'    
when FolderName = 'LoadToTarget' and streamname = 'wf_APP_96_T1_LoadBudgetToTarget' Then 'LoadToTarget - Budget'
when FolderName = 'LoadToTarget' and streamname = 'wf_APP10_81_T1_SuccessFactors' Then 'LoadToTarget - SuccessFactors'
when FolderName = 'LoadToTarget' and streamname = 'wf_APP11_81_T1_TimeTracker' Then 'LoadToTarget - TimeTracker'
when FolderName = 'LoadToTarget' and streamname = 'wf_APP12_80_T1_Autoorder' Then 'LoadToTarget - Autoorder'
when FolderName = 'LoadToTarget' and streamname = 'wf_APP13_81_T1_SAPAgreement' Then 'LoadToTarget - SAPAgreement'
when FolderName = 'LoadToTarget' and streamname = 'wf_APP9_81_T1_DistributionEvent' Then 'LoadToTarget - DistributionEvent'
when FolderName = 'load_edw_semantic' and streamname = 'wf_semantic_110_T4_ArticleHierarchyStore' Then 'Semantisk F�rs�ljning' 
when FolderName = 'load_edw_semantic' and streamname = 'wf_semantic_100_T4_PerpetualInventorySalesTransaction' Then 'Semantisk Autoorder' 
when FolderName = 'load_edw_semantic' and streamname = 'wf_semantic_120_T4_UpdLoyaltySemantic' Then 'Semantisk Lojalitet' 
when FolderName = 'load_edw_semantic' and streamname = 'wf_semantic15_150_T4_Autoorder' Then 'Semantisk Autoorder Saldokontroller' 
when FolderName = 'load_edw_semantic' and streamname = 'wf_semantic_140_T4_UpdFinancialInformationHierarchies' Then 'Semantisk Finans'
when FolderName = 'load_edw_semantic' and streamname = 'wf_semantic16_160_T4_DistProductivity' Then 'Semantisk DistProductivity' 
else streamname end as batch_Name,
max(EndDttm) as LatestRunTime,
max(StartDttm) as LatestRunStartTime
FROM ${DB_ENV}MetadataVout.StreamRun 
where streamstatus = 'F'
and foldername not like 'load_archive%'
and foldername not like 'load_datalab%'
and foldername not in (
'dq_control',
'extract_sap_dm_initload',
'load_axbo_archive',
'load_man_sources'
)
group by 1
) as x

UNION

SELECT
Case 
When WorkflowName = 'wf_semantic16_160_T4_DistProductivity' Then 'Semantisk Produktivitet' 
else WorkflowName end as batch_Name,
max(JobEnd) as LatestRunTime,
max(JobStart) as LatestRunStartTime
FROM ${DB_ENV}MetaDataVOUT.JobRun
where WorkflowName in ('wf_semantic16_160_T4_DistProductivity')
and jobend is not null
group by 1

) as z

;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}MetaDataVOUT.ETLStatusRapport IS '$Revision: 24959 $ - $Date: 2018-05-24 12:43:12 +0200 (tor, 24 maj 2018) $ '
;


.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

