/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: LOCATION_SPEC_RETAIL_PRICE.btq 28029 2019-04-24 10:55:30Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2019-04-24 12:55:30 +0200 (ons, 24 apr 2019) $
# Last Revision    : $Revision: 28029 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/LOCATION_SPEC_RETAIL_PRICE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','LOCATION_SPEC_RETAIL_PRICE','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.LOCATION_SPEC_RETAIL_PRICE
(
	Location_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Article_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Retail_Price_UOM_Cd	CHAR(4) DEFAULT '-1' NOT NULL,
	Valid_From_Dt	DATE FORMAT 'YYYY-MM-DD' NOT NULL,
	Retail_Price_Doc_Version_Num	INTEGER DEFAULT -1 NOT NULL,
	Retail_Price_Doc_Num	INTEGER,
	Currency_Cd	CHAR(3) DEFAULT '-1' NOT NULL,
	Applied_Cost_Price_Amt	DECIMAL(9,2),
	Net_Retail_Price_Amt	DECIMAL(9,2),
	Final_Retail_Price_Amt	DECIMAL(9,2),
	Valid_To_Dt	DATE FORMAT 'YYYY-MM-DD',
	OpenJobRunId	INTEGER,
	CloseJobRunId	INTEGER,
	InsertDttm	TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT PKLOCATION_SPEC_RETAIL_PRICE PRIMARY KEY (Location_Seq_Num,Article_Seq_Num,Retail_Price_UOM_Cd,Valid_From_Dt,Retail_Price_Doc_Version_Num)
)
PRIMARY INDEX PPILOCATION_SPEC_RETAIL_PRICE(
	Article_Seq_Num
) PARTITION BY (RANGE_N ( Location_Seq_Num BETWEEN
                        1 AND 1000 EACH 1,
                        NO RANGE OR UNKNOWN
                ));

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','LOCATION_SPEC_RETAIL_PRICE','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.LOCATION_SPEC_RETAIL_PRICE IS '$Revision: 28029 $ - $Date: 2019-04-24 12:55:30 +0200 (ons, 24 apr 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('${DB_ENV}TgtT', 'LOCATION_SPEC_RETAIL_PRICE', '${DB_ENV}CntlLoadReadyT',null,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
