/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: DISTRIBUTION_EVENT.btq 30660 2020-03-06 12:30:57Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2020-03-06 13:30:57 +0100 (fre, 06 mar 2020) $
# Last Revision    : $Revision: 30660 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/DISTRIBUTION_EVENT.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','DISTRIBUTION_EVENT','PRE',NULL,1)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.DISTRIBUTION_EVENT
(

	Distribution_Event_Pos	SMALLINT  NOT NULL ,
	Event_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Event_Dt_DD	DATE  FORMAT 'YYYY-MM-DD' NOT NULL ,
	Event_End_Dttm	TIMESTAMP  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' NOT NULL ,
	Event_End_Hour_DD	SMALLINT  NOT NULL ,
	Location_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Ordering_Location_Seq_Num	INTEGER  DEFAULT -1 ,
	Location_Zone_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Concept_Cd	CHAR(3)  DEFAULT '-1' NOT NULL ,
	Ordering_Party_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	User_Distribution_Id	VARCHAR(80)  DEFAULT '-1' ,
	Distribution_Event_Subtype_Cd	VARCHAR(50)  DEFAULT '-1' NOT NULL ,
	Article_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Measuring_Unit_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Base_UOM_Meas_Unit_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Event_Subtype_Cd	VARCHAR(50)  DEFAULT '-1' ,
	Event_Category_Cd	VARCHAR(50)  DEFAULT '-1' ,
	Exception_Cd	VARCHAR(50)  DEFAULT '-1' NOT NULL ,
	Actual_Qty	DECIMAL(18,4)   COMPRESS (0.0000),
	Actual_Base_UOM_Qty	DECIMAL(18,4)   COMPRESS (0.0000),
	Ordered_Qty	DECIMAL(18,4)   COMPRESS (0.0000),
	Ordered_Base_UOM_Qty	DECIMAL(18,4)   COMPRESS (0.0000),
	OpenJobRunId	INTEGER  ,
	CloseJobRunId	INTEGER  ,
	InsertDttm	TIMESTAMP(6)  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT CURRENT_TIMESTAMP NOT NULL ,
	CONSTRAINT PKDISTRIBUTION_EVENT PRIMARY KEY (Distribution_Event_Pos,Event_Seq_Num)
)
	PRIMARY INDEX PPIDISTRIBUTION_EVENT
	 (
			Distribution_Event_Pos,
			Event_Seq_Num
	 ) PARTITION BY (RANGE_N(Event_Dt_DD  BETWEEN 
Date '2017-01-01'
AND
(((ADD_MONTHS((CURRENT_DATE ),(2 )))- (EXTRACT(DAY FROM (ADD_MONTHS((CURRENT_DATE ),(2 ))))( TITLE 'Day')( TITLE 'Day')))- (((((ADD_MONTHS((CURRENT_DATE ),(2 )))- (EXTRACT(DAY FROM (ADD_MONTHS((CURRENT_DATE ),(2 ))))( TITLE 'Day')( TITLE 'Day'))(DATE))- (10106 (DATE))) MOD  7 )+ 1 ( TITLE 'DayOfWeek()')))+ 1 
  EACH INTERVAL '7' DAY)                
, RANGE_N ( Location_Seq_Num BETWEEN
                        1 AND 2500 EACH 1,
                        NO RANGE OR UNKNOWN 
                )
) ;

COMMENT ON TABLE DISTRIBUTION_EVENT IS 'A DISTRIBUTION EVENT is an event, action, or process that is performed on distribution pieces within a distribution center. 

The event is linked to the distribution pieces involved in the event, or, in the case of untraceable distribution pieces, a lin';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Distribution_Event_Subtype_Cd IS 'Uniquely identifies a distribution event type.';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Location_Zone_Seq_Num IS 'Surrogate key for LOCATION_ZONE + location    composite key';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Measuring_Unit_Seq_Num IS 'Surrogate key for DISTRIBUTION_EVENT';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Base_UOM_Meas_Unit_Seq_Num IS 'Surrogate key for DISTRIBUTION_EVENT';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Actual_Qty IS 'Measuring Unit Actual Picked Quantity ';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Actual_Base_UOM_Qty IS 'Actual Picked Quantity recalculated in Article Base Units';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Ordered_Qty IS 'Measuring Unit Ordered Quantity ';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Ordered_Base_UOM_Qty IS 'Measuring Unit Ordered Quantity recalculated in base units';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Article_Seq_Num IS 'Surrogate key for DISTRIBUTION_EVENT';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Ordering_Party_Seq_Num IS 'Surrogate key for DISTRIBUTION_EVENT';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Event_Dt_DD IS 'Partitioning Column, derived from Event_End_Dttm';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Event_End_Hour_DD IS 'Derived hour representation of the distribution event, based on Event_End_Dttm';

COMMENT ON COLUMN DISTRIBUTION_EVENT.OpenJobRunId IS 'Reference to the (load) process that created the row. This defines "Transaction Start Dttm".';

COMMENT ON COLUMN DISTRIBUTION_EVENT.CloseJobRunId IS 'Reference to the (load) process that logically deleted the row. This defines "Transaction End Dttm".';

COMMENT ON COLUMN DISTRIBUTION_EVENT.InsertDttm IS 'Reference to the (load) process that created the row. This defines "Transaction Start Dttm".';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Exception_Cd IS 'Uniquely identifies an exception.';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Concept_Cd IS 'Uniquely identifies a Concept';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Location_Seq_Num IS 'Surrogate key for DISTRIBUTION_EVENT';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Ordering_Location_Seq_Num IS 'Surrogate key for DISTRIBUTION_EVENT';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Event_End_Dttm IS 'The date and time an event ends.';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Event_Subtype_Cd IS 'Uniquely identifies an event type.';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Event_Category_Cd IS 'Uniquely identifies an event category.';

COMMENT ON COLUMN DISTRIBUTION_EVENT.Event_Seq_Num IS 'Surrogate key for DISTRIBUTION_EVENT';

COMMENT ON COLUMN DISTRIBUTION_EVENT.User_Distribution_Id IS 'Surrogate key for DISTRIBUTION_EVENT';

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','DISTRIBUTION_EVENT','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('${DB_ENV}TgtT','DISTRIBUTION_EVENT','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.DISTRIBUTION_EVENT IS '$Revision: 30660 $ - $Date: 2020-03-06 13:30:57 +0100 (fre, 06 mar 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

