/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ECOMMERCE_STORE.btq 29042 2019-09-25 06:22:23Z  $
# Last Changed By  : $Author: $
# Last Change Date : $Date: 2019-09-25 08:22:23 +0200 (ons, 25 sep 2019) $
# Last Revision    : $Revision: 29042 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/ECOMMERCE_STORE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ECOMMERCE_STORE','PRE',NULL,1)
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.ECOMMERCE_STORE
(

	Store_Seq_Num	INTEGER  DEFAULT -1 NOT NULL ,
	Valid_From_Dttm	DATE  FORMAT 'YYYY-MM-DD' NOT NULL ,
	Valid_To_Dttm	DATE  FORMAT 'YYYY-MM-DD' ,
	ECommerce_Store_Ind	BYTEINT  DEFAULT 0 ,
	Ecommerce_Start_Dt	DATE  FORMAT 'YYYY-MM-DD' ,
	Ecommerce_End_Dt	DATE  FORMAT 'YYYY-MM-DD' ,
	OpenJobRunId	INTEGER  ,
	CloseJobRunId	INTEGER  ,
	InsertDttm	TIMESTAMP(6)  FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' NOT NULL ,
	CONSTRAINT PKECOMMERCE_STORE PRIMARY KEY (Store_Seq_Num,Valid_From_Dttm)
);

COMMENT ON COLUMN ECOMMERCE_STORE.Store_Seq_Num IS 'Surrogate key for ECOMMERCE_STORE';

COMMENT ON COLUMN ECOMMERCE_STORE.Valid_From_Dttm IS 'The validity start period';

COMMENT ON COLUMN ECOMMERCE_STORE.Valid_To_Dttm IS 'The validity end period';

COMMENT ON COLUMN ECOMMERCE_STORE.OpenJobRunId IS 'Reference to the (load) process that created the row. This defines "Transaction Start Dttm".';

COMMENT ON COLUMN ECOMMERCE_STORE.CloseJobRunId IS 'Reference to the (load) process that logically deleted the row. This defines "Transaction End Dttm".';

COMMENT ON COLUMN ECOMMERCE_STORE.InsertDttm IS 'Reference to the (load) process that created the row. This defines "Transaction Start Dttm".';

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ECOMMERCE_STORE','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('${DB_ENV}TgtT','ECOMMERCE_STORE','${DB_ENV}CntlLoadReadyT',NULL,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.ECOMMERCE_STORE IS '$Revision: 29042 $ - $Date: 2019-09-25 08:22:23 +0200 (ons, 25 sep 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

