/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ARTICLE_RETAIL_PRICE.btq 28029 2019-04-24 10:55:30Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2019-04-24 12:55:30 +0200 (ons, 24 apr 2019) $
# Last Revision    : $Revision: 28029 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/ARTICLE_RETAIL_PRICE.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ARTICLE_RETAIL_PRICE','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.ARTICLE_RETAIL_PRICE
(
	Org_Party_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Distribution_Channel_Cd	CHAR(2) DEFAULT '-1' NOT NULL,
	Price_List_Cd	CHAR(2) DEFAULT '-1' NOT NULL,
	Retail_Price_UOM_Cd	CHAR(4) DEFAULT '-1' NOT NULL,
	Article_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Valid_From_Dt	DATE FORMAT 'YYYY-MM-DD' NOT NULL,
	Retail_Price_Doc_Version_Num	INTEGER NOT NULL,
	Retail_Price_Doc_Num	INTEGER NOT NULL,
	Retail_Price_Type_Cd	CHAR(3) DEFAULT '-1' NOT NULL,
	Purchasing_Org_Party_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Currency_Cd	CHAR(3) DEFAULT '-1' NOT NULL,
	Applied_Cost_Price_Amt	DECIMAL(9,2),
	Net_Retail_Price_Amt	DECIMAL(9,2),
	Final_Retail_Price_Amt	DECIMAL(9,2),
	Valid_To_Dt	DATE FORMAT 'YYYY-MM-DD',
	OpenJobRunId	INTEGER,
	CloseJobRunId	INTEGER,
	InsertDttm	TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT PKARTICLE_RETAIL_PRICE PRIMARY KEY (Org_Party_Seq_Num,Distribution_Channel_Cd,Price_List_Cd,Retail_Price_UOM_Cd,Article_Seq_Num,Valid_From_Dt,Retail_Price_Doc_Version_Num)
)
PRIMARY INDEX PPI1ARTICLE_RETAIL_PRICE(
	Article_Seq_Num
) PARTITION BY (CASE_N(
	Price_List_Cd = '-1',
	Price_List_Cd = '01',
	Price_List_Cd = '02',
	Price_List_Cd = '10',
	Price_List_Cd = '11',
	Price_List_Cd = '13',
	Price_List_Cd = '15',
	Price_List_Cd = '16',
	Price_List_Cd = '20',
	Price_List_Cd = '21',
	Price_List_Cd = '22',
	Price_List_Cd = '23',
	Price_List_Cd = '24',
	Price_List_Cd = '2A',
	Price_List_Cd = '2B',
	Price_List_Cd = '2C',
	Price_List_Cd = '2F',
	Price_List_Cd = '32',
	Price_List_Cd = '33',
	Price_List_Cd = '39',
	Price_List_Cd = '41',
	Price_List_Cd = '42',
	Price_List_Cd = '43',
	Price_List_Cd = '44',
	Price_List_Cd = '61',
	Price_List_Cd = '62',
	Price_List_Cd = '71',
	Price_List_Cd = '72',
	Price_List_Cd = '73',
	Price_List_Cd = '75',
	Price_List_Cd = 'AF',
	Price_List_Cd = 'MC',
	Price_List_Cd = 'MH',
	Price_List_Cd = 'MQ',
	Price_List_Cd = 'NE',
	Price_List_Cd = 'NG',
	Price_List_Cd = 'QK',
	Price_List_Cd = 'QL',
	NO CASE,
	UNKNOWN
));

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ARTICLE_RETAIL_PRICE','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.ARTICLE_RETAIL_PRICE IS '$Revision: 28029 $ - $Date: 2019-04-24 12:55:30 +0200 (ons, 24 apr 2019) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_CreLoadReady('${DB_ENV}TgtT', 'ARTICLE_RETAIL_PRICE', '${DB_ENV}CntlLoadReadyT',null,'D',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
