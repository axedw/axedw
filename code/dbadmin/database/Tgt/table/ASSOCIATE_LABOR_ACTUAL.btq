/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: ASSOCIATE_LABOR_ACTUAL.btq 30660 2020-03-06 12:30:57Z k9102939 $
# Last Changed By  : $Author: k9102939 $
# Last Change Date : $Date: 2020-03-06 13:30:57 +0100 (fre, 06 mar 2020) $
# Last Revision    : $Revision: 30660 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/table/ASSOCIATE_LABOR_ACTUAL.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
--ASSOCIATE_LABOR_ACTUAL
.SET MAXERROR 0;

DATABASE ${DB_ENV}TgtT;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ASSOCIATE_LABOR_ACTUAL','PRE',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE TABLE ${DB_ENV}TgtT.ASSOCIATE_LABOR_ACTUAL
(
	Associate_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Location_Seq_Num	INTEGER DEFAULT -1 NOT NULL,
	Actual_Labor_Dt	DATE FORMAT 'YYYY-MM-DD' NOT NULL,
	Actual_Labor_Start_Dttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT TIMESTAMP '0001-01-01 00:00:00.000000' NOT NULL,
	Actual_Labor_End_Dttm	TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT TIMESTAMP '9999-12-31 23:59:59.999999' NOT NULL,
	Actual_Labor_Minute_Qty	INTEGER,
	Actual_Non_Labor_Minute_Qty	INTEGER,
	Actual_OT_Labor_Minute_Qty	INTEGER,
	Actual_Labor_Cost_Amt	DECIMAL(18,4) DEFAULT -1,
	Actual_OT_Labor_Cost_Amt	DECIMAL(18,4) DEFAULT -1,
	Derived_Hourly_Labor_Cost_Amt	DECIMAL(18,4) DEFAULT -1,
	OpenJobRunId	INTEGER,
	CloseJobRunId	INTEGER,
	InsertDttm	TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT PKASSOCIATE_LABOR_ACTUAL PRIMARY KEY (Associate_Seq_Num,Location_Seq_Num,Actual_Labor_Dt),
	CONSTRAINT FK2ASSOCIATE_LABOR_ACTUAL FOREIGN KEY (Location_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.LOCATION (Location_Seq_Num),
	CONSTRAINT FK3ASSOCIATE_LABOR_ACTUAL FOREIGN KEY (Associate_Seq_Num) REFERENCES WITH NO CHECK OPTION ${DB_ENV}TgtT.ASSOCIATE (Associate_Seq_Num)
)
PRIMARY INDEX PPIASSOCIATE_LABOR_ACTUAL
	 (
			Associate_Seq_Num,
			Location_Seq_Num
	 ) PARTITION BY (RANGE_N ( Actual_Labor_Dt BETWEEN
    DATE '2014-12-01' AND (ADD_MONTHS((CURRENT_DATE ),(7 )))- (EXTRACT(DAY FROM (ADD_MONTHS((CURRENT_DATE ),(3 ))))( TITLE 'DAY')( TITLE 'DAY')) EACH INTERVAL '7' DAY,
    NO RANGE OR UNKNOWN )
                , RANGE_N ( Location_Seq_Num BETWEEN
                        1 AND 1000 EACH 1,
                        NO RANGE OR UNKNOWN
                )

) ;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}TgtT','ASSOCIATE_LABOR_ACTUAL','POST',NULL,1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}TgtT.ASSOCIATE_LABOR_ACTUAL IS '$Revision: 30660 $ - $Date: 2020-03-06 13:30:57 +0100 (fre, 06 mar 2020) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

