/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: FINANCIAL_TRAIT.btq 14310 2014-10-28 15:38:23Z k9107640 $
# Last Changed By  : $Author: k9107640 $
# Last Change Date : $Date: 2014-10-28 16:38:23 +0100 (tis, 28 okt 2014) $
# Last Revision    : $Revision: 14310 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/init/FINANCIAL_TRAIT.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

SELECT * FROM DBC.Tables WHERE DatabaseName = '${DB_ENV}TgtT' AND TableName = 'FINANCIAL_TRAIT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPINS

DATABASE ${DB_ENV}TgtT;

CREATE VOLATILE TABLE #Financial_Trait#V1
,NO LOG
AS (SELECT * FROM ${DB_ENV}TgtT.FINANCIAL_TRAIT)
WITH NO DATA
ON COMMIT PRESERVE ROWS
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (1,'Nettoomsättning kr','3',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (2,'Bruttovinst kr','3',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (3,'Omsättning bonuskunder kr','3',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (4,'EMV försäljning kr','3',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (5,'Timmar','1',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (6,'Personalkostnad kr','3',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (7,'Omsättning Linas matkasse','3',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (8,'Omsättning Middagsfrid','3',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (9,'Omsättning Linas matkasse','3',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (10,'Omsättningsandel PG/HG av tot %','2',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (11,'Aktivitetspåverkan % central','2',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (12,'Aktivitetspåverkan % lokal','2',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (13,'Artikelrabatt %','2',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (14,'Bruttovinst %','2',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (15,'Svinn känt %','2',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (16,'Bruttovinst Middagsfrid','3',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (17,'Bruttovinst Linas matkasse','3',-1,NULL);
INSERT INTO #Financial_Trait#V1 (Trait_Id,Trait_Desc,Trait_UOM_Cd,OpenJobRunID,CloseJobRunID) VALUES (-1,'Saknar beskrivning','-1',-1,NULL);


.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

MERGE INTO ${DB_ENV}TgtT.FINANCIAL_TRAIT
AS t1
USING (
	SELECT	
		s0.Trait_Id,
		s0.Trait_Desc,
		s0.Trait_UOM_Cd
	FROM #Financial_Trait#V1 AS s0
	LEFT OUTER JOIN ${DB_ENV}TgtT.FINANCIAL_TRAIT AS t0
	ON t0.Trait_Id = s0.Trait_Id
	WHERE	t0.Trait_Id IS NULL
	OR	COALESCE(TRIM(t0.Trait_Desc),'') IN ('UNKNOWN', 'Saknar beskrivning')
) AS s1
ON	t1.Trait_Id = s1.Trait_Id
WHEN MATCHED THEN UPDATE
SET	Trait_Desc = s1.Trait_Desc, Trait_UOM_Cd = s1.Trait_UOM_Cd
WHEN NOT MATCHED THEN INSERT
(	Trait_Id,
	Trait_Desc,
	Trait_UOM_Cd,
	OpenJobRunId
) VALUES (
	s1.Trait_Id,
	s1.Trait_Desc,
	s1.Trait_UOM_Cd,
	-1
);

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.LABEL SKIPINS
