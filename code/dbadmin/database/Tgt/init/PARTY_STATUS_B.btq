/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: PARTY_STATUS_B.btq 8492 2013-03-12 15:40:35Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2013-03-12 16:40:35 +0100 (tis, 12 mar 2013) $
# Last Revision    : $Revision: 8492 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Tgt/init/PARTY_STATUS_B.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
/*
** Add the initial status for selected partner parties
** This is only for an initial situation. An ETL process will load the rest later.
** SOS Barnbyar is redemption only (LOR) and the rest are both accrual (LOA) and redemption (LOR)
*/
SELECT * FROM DBC.Tables WHERE DatabaseName = '${DB_ENV}TgtT' AND TableName = 'PARTY_STATUS_B';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPINS

DATABASE ${DB_ENV}TgtT;

CREATE VOLATILE TABLE #PARTY_STATUS_B#V1
,NO LOG
AS (SELECT * FROM ${DB_ENV}TgtT.PARTY_STATUS_B Where Party_Status_Type_Cd in ('LOR', 'LOA'))
WITH NO DATA
ON COMMIT PRESERVE ROWS
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

INSERT INTO #PARTY_STATUS_B#V1
(
	Party_Seq_Num,
	Party_Status_Type_Cd,
	Valid_From_Dttm,
	Valid_To_Dttm,
	OpenJobRunId,
	CloseJobRunId
)
Select
	onb.Org_Party_Seq_Num,
	pst.Party_Status_Type_Cd,
	onb.Valid_From_Dttm,
	onb.Valid_To_Dttm,
	-1,
	null

From ${DB_ENV}TgtVOUT.ORGANIZATION_NAME_B as onb
Cross Join ${DB_ENV}TgtT.PARTY_STATUS_TYPE as pst

Where	onb.Org_Name = 'SOS Barnbyar Sverige'
and	onb.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'
and pst.Party_Status_Type_Cd in ('LOR')

UNION ALL

Select
	onb.Org_Party_Seq_Num,
	pst.Party_Status_Type_Cd,
	onb.Valid_From_Dttm,
	onb.Valid_To_Dttm,
	-1,
	null

From ${DB_ENV}TgtVOUT.ORGANIZATION_NAME_B as onb
Inner Join ${DB_ENV}TgtVOUT.BUSINESS_DETAILS_B as bdb
	On	onb.Org_Party_Seq_Num = bdb.Org_Party_Seq_Num
Cross Join ${DB_ENV}TgtT.PARTY_STATUS_TYPE as pst

Where	onb.Org_Name = 'PREEM AB'
and	onb.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'
and	bdb.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'
and bdb.Business_Account_Group_Cd = 'Z170'
and pst.Party_Status_Type_Cd in ('LOR', 'LOA')

UNION ALL

Select
	onb.Org_Party_Seq_Num,
	pst.Party_Status_Type_Cd,
	onb.Valid_From_Dttm,
	onb.Valid_To_Dttm,
	-1,
	null

From ${DB_ENV}TgtVOUT.ORGANIZATION_NAME_B as onb
Inner Join ${DB_ENV}TgtVOUT.BUSINESS_DETAILS_B as bdb
	On	onb.Org_Party_Seq_Num = bdb.Org_Party_Seq_Num
Cross Join ${DB_ENV}TgtT.PARTY_STATUS_TYPE as pst

Where	onb.Org_Name = 'ANUL EJ G CLUB ERIKS AB'
and	onb.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'
and	bdb.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'
and pst.Party_Status_Type_Cd in ('LOR', 'LOA')

UNION ALL

Select
	onb.Org_Party_Seq_Num,
	pst.Party_Status_Type_Cd,
	onb.Valid_From_Dttm,
	onb.Valid_To_Dttm,
	-1,
	null
From ${DB_ENV}TgtVOUT.ORGANIZATION_NAME_B as onb
Inner Join ${DB_ENV}TgtVOUT.BUSINESS_DETAILS_B as bdb
	On	onb.Org_Party_Seq_Num = bdb.Org_Party_Seq_Num
Cross Join ${DB_ENV}TgtT.PARTY_STATUS_TYPE as pst

Where	bdb.Corporate_Identity_num = 'SE516406092201' -- IKANO BANK AB (PUBL)
and	onb.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'
and	bdb.Valid_To_Dttm = timestamp '9999-12-31 23:59:59.999999'
and pst.Party_Status_Type_Cd in ('LOR', 'LOA')
;

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


	
MERGE INTO ${DB_ENV}TgtT.PARTY_STATUS_B
AS t1
USING (
	SELECT
		s0.Party_Seq_Num,
		s0.Party_Status_Type_Cd,
		s0.Valid_From_Dttm,
		s0.Valid_To_Dttm,
		s0.OpenJobRunId,
		s0.CloseJobRunId
	FROM #PARTY_STATUS_B#V1 AS s0
	LEFT OUTER JOIN ${DB_ENV}TgtT.PARTY_STATUS_B AS t0
	ON s0.Party_Seq_Num = t0.Party_Seq_Num
	and s0.Party_Status_Type_Cd = t0.Party_Status_Type_Cd
	WHERE	t0.Party_Status_Type_Cd IS NULL
) AS s1
ON t1.Party_Seq_Num = s1.Party_Seq_Num
and t1.Party_Status_Type_Cd = s1.Party_Status_Type_Cd

WHEN MATCHED THEN UPDATE
SET	
	Party_Status_Type_Cd = s1.Party_Status_Type_Cd
WHEN NOT MATCHED THEN INSERT
(
	Party_Seq_Num,
	Party_Status_Type_Cd,
	Valid_From_Dttm,
	Valid_To_Dttm,
	OpenJobRunId,
	CloseJobRunId
) VALUES (
	s1.Party_Seq_Num,
	s1.Party_Status_Type_Cd,
	s1.Valid_From_Dttm,
	s1.Valid_To_Dttm,
	s1.OpenJobRunId,
	s1.CloseJobRunId
);
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

.LABEL SKIPINS
