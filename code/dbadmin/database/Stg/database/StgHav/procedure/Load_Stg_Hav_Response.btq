/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_Stg_Hav_Response.btq 29611 2020-06-25 13:23:56Z  $
# Last Changed By  : $Author: a97884$
# Last Change Date : $Date: 2020-06-25 14:23:56 +0100  $
# Last Revision    : $Revision: 29611 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgHav/procedure/Load_Stg_Hav_Response.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Procedure for Reading Queue
# Project	: Traceability Hav
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2020-06-25 a97884  Initial version
# 2020-07-03 a97884  new column Event_Severity  VARCHAR(20)
# --------------------------------------------------------------------------
# Description
#       Response_Code:	    Code from Hav
#	    Integration_Id:     'SCI357b' 
#       Event_Status:       0 => OK  ,  1 => Error  
#
# Dependencies
#   StgHavT.Stg_Hav_Response
#   StgHavT.Stg_Hav_Response_Q
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}StgHavT.Load_Stg_Hav_Response(
      ASYNC_EXTERNAL_ID         VARCHAR(80)  ,
      Response_Code             VARCHAR(80),           
      Integration_Id		VARCHAR(20),
      Event_Status              Integer,
	  Event_Severity            VARCHAR(20),
      Event_Description         VARCHAR(2000)        
	)
BEGIN

    INSERT INTO ${DB_ENV}StgHavT.Stg_Hav_Response
        (	
	    ASYNC_EXTERNAL_ID   = :ASYNC_EXTERNAL_ID  ,
	    Response_Code       = :Response_Code,           
	    Integration_Id      = :Integration_Id,               
        Event_Status      	= :Event_Status,
		Event_Severity      = :Event_Severity ,
	    Event_Description 	= :Event_Description
          );

END;
