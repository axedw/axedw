/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: StgAxBO_DB.btq 20780 2017-01-10 15:00:33Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2017-01-10 16:00:33 +0100 (tis, 10 jan 2017) $
# Last Revision    : $Revision: 20780 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgAxBO/database/StgAxBO_DB.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/

-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgAxBO
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgAxBO';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgAxBO
FROM ${DB_ENV}Stg AS
   PERM = 50000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgAxBO AS
'Parent Database for source data from AxBO'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgAxBO TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgAxBOT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgAxBOT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgAxBOT
FROM ${DB_ENV}StgAxBO AS
   PERM = 50000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgAxBOT AS
'Database holding stagingtables from AxBO'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgAxBOT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgAxBOVIN
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgAxBOVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgAxBOVIN
FROM ${DB_ENV}StgAxBO AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgAxBOVIN AS
'Database holding views for loading stagingtables from AxBO'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgAxBOVIN TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}StgAxBOT TO ${DB_ENV}StgAxBOVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgAxBOVOUT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgAxBOVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgAxBOVOUT
FROM ${DB_ENV}StgAxBO AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgAxBOVOUT AS
'Database holding views for reading stagingtables from AxBO'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgAxBOVOUT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgAxBOT TO ${DB_ENV}StgAxBOVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
