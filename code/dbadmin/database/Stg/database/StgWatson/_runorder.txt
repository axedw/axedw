BTQRUN	code/dbadmin/database/Stg/database/StgWatson/database/StgWatson.btq

---------- Tables
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/table/S1_Watson_PickedPackages.btq
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/table/S1_Watson_PlacedGoods.btq
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/table/S1_Watson_Transport.btq
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/table/Stg_Watson_Pickdata.btq
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/table/Stg_Watson_PickdataQ.btq
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/table/S2_Watson_PickedPackages.btq
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/table/S2_Watson_PlacedGoods.btq
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/table/S2_Watson_Transport.btq

---------- Views
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/view/Stg_Watson_Pickdata_vc.btq

---------- Procedures
BTQCOMPILE	code/dbadmin/database/Stg/database/StgWatson/procedure/Watson_Pickdata_ReadQ.btq
BTQCOMPILE	code/dbadmin/database/Stg/database/StgWatson/procedure/Watson_Pickdata_PurgeDataFrmStg.btq

---------- Macros
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/macro/Load_Stg_Watson_Pickdata.btq

---------- Triggers
BTQRUN	code/dbadmin/database/Stg/database/StgWatson/trigger/Load_Stg_Watson_PickdataQ.btq

