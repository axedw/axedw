/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_Stg_SF_Employee.btq 26775 2019-01-22 14:04:30Z k9107640 $
# Last Changed By  : $Author: k9107640 $
# Last Change Date : $Date: 2019-01-22 15:04:30 +0100 (tis, 22 jan 2019) $
# Last Revision    : $Revision: 26775 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSF/procedure/Load_Stg_SF_Employee.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Sonic calls this procedure for insert in stg-table
# Project	: EDW2
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-03-31 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Sonic call this procedure for insert in stg-table
# Dependencies
#	${DB_ENV}StgSAPT.Stg_Quinyx_APIResponse
#	${DB_ENV}StgSAPT.Stg_Quinyx_APIResponseQ
#	
# --------------------------------------------------------------------------
*/


REPLACE PROCEDURE ${DB_ENV}StgSFT.Load_Stg_SF_CompoundEmployee
(	IN SequenceId	VARCHAR(100)
	,IN ContentType	VARCHAR(30)
	,IN SubContentType	VARCHAR(30)
	,IN TransactionTS	VARCHAR(26)
	,IN ExtractionTS	VARCHAR(26)
	,IN IEReceivedTS	VARCHAR(26)
	,IN IEDeliveryTS	VARCHAR(26)
	,IN SourceVersion	VARCHAR(10)
	,IN XmlData	CLOB(8000000) CHARACTER SET UNICODE
)
BEGIN

INSERT INTO ${DB_ENV}StgSFT.Stg_SF_CompoundEmployee
(	SequenceId
	,ContentType
	,SubContentType
	,TransactionTS
	,ExtractionTS
	,IEReceivedTS
	,IEDeliveryTS
	,SourceVersion
	,ArchiveKey
	,XmlData
) VALUES (
	:SequenceId
	,:ContentType
	,:SubContentType
	,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:ExtractionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:IEReceivedTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:IEDeliveryTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,:SourceVersion
	,(	TRIM(:SequenceId)
		|| '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 1 FOR 10)
		|| '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 12 FOR 15)
		)
	,clobreplerrc(TRANSLATE(:XmlData USING UNICODE_TO_LATIN WITH ERROR))
);

	INSERT INTO ${DB_ENV}StgSFT.Stg_SF_CompoundEmployeeQ
	(	SequenceId
		,ReferenceTS
	) VALUES (
		:SequenceId
		,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	);

END;
