/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: StgTILTID.btq 21893 2017-03-08 14:16:26Z a20841 $
# Last Changed By  : $Author: a20841 $
# Last Change Date : $Date: 2017-03-08 15:16:26 +0100 (ons, 08 mar 2017) $
# Last Revision    : $Revision: 21893 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgTILTID/database/StgTILTID.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgTILTID
-- ----------------------------------------------------------------------------

--
-- Dummy change
--

SELECT * FROM DBC.Databases WHERE DataBaseName = '${DB_ENV}StgTILTID';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgTILTID
FROM ${DB_ENV}Stg AS
   PERM = 10000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgTILTID AS
'Parent Database for source data from TILTID'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgTILTID TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgTILTIDT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DataBaseName = '${DB_ENV}StgTILTIDT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgTILTIDT
FROM ${DB_ENV}StgTILTID AS
   PERM = 10000000
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgTILTIDT AS
'Database holding stagingtables from TILTID'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgTILTIDT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgTILTIDVIN
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DataBaseName = '${DB_ENV}StgTILTIDVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgTILTIDVIN
FROM ${DB_ENV}StgTILTID AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgTILTIDVIN AS
'Database holding views for loading stagingtables from TILTID'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgTILTIDVIN TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}StgTILTIDT TO ${DB_ENV}StgTILTIDVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE


-- ----------------------------------------------------------------------------
-- Database: ${DB_ENV}StgTILTIDVOUT
-- ----------------------------------------------------------------------------
SELECT * FROM DBC.Databases WHERE DataBaseName = '${DB_ENV}StgTILTIDVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT > 0 THEN .GOTO SKIPCRE

CREATE DATABASE ${DB_ENV}StgTILTIDVOUT
FROM ${DB_ENV}StgTILTID AS
   PERM = 0
   NO FALLBACK
   NO BEFORE JOURNAL
   NO AFTER JOURNAL
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPCRE

COMMENT ON DATABASE ${DB_ENV}StgTILTIDVOUT AS
'Database holding views for reading stagingtables from TILTID'
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

GRANT ALL ON ${DB_ENV}StgTILTIDVOUT TO ${DB_ENV}dbadmin,DBADMIN,DBC WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgTILTIDT TO ${DB_ENV}StgTILTIDVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON Sys_Calendar TO ${DB_ENV}StgTILTIDVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
/* ===============================================================================
These grants are required to support usage of dynamically (runtime) created views  
within Informatica PowerCenter (push-down optimization)
=============================================================================== */
-- Read Access on MetaData for stage dbs (update when adding new sources)
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVOUT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}MetaDataVOUT TO ${DB_ENV}StgTILTIDT,${DB_ENV}StgTILTIDVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read & Delete Access on UtilT for stage dbs
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}UtilT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT,DELETE,INSERT,UPDATE ON ${DB_ENV}UtilT TO ${DB_ENV}StgTILTIDT,${DB_ENV}StgTILTIDVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}UtilT TO ${DB_ENV}StgTILTIDVOUT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgTILTID to Cntl
SELECT * FROM DBC.Databases WHERE DatabaseName IN ( '${DB_ENV}CntlCleanseT', '${DB_ENV}CntlCleanseVIN');
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT <> 2 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgTILTIDT TO ${DB_ENV}CntlCleanseT,${DB_ENV}CntlCleanseVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT SELECT ON ${DB_ENV}StgTILTIDVOUT TO ${DB_ENV}CntlCleanseT,${DB_ENV}CntlCleanseVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
GRANT DROP TABLE ON ${DB_ENV}StgTILTIDT TO ${DB_ENV}CntlCleanseT;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on StgTILTIDT to MetaDataVIN
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}MetaDataVIN';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}StgTILTIDT TO ${DB_ENV}MetaDataVIN WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT

-- Read Access on CNTLCleanseT to StgTILTIDT
SELECT * FROM DBC.Databases WHERE DatabaseName = '${DB_ENV}StgTILTIDT';
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.IF ACTIVITYCOUNT = 0 THEN .GOTO SKIPGRANT
GRANT SELECT ON ${DB_ENV}CntlCleanseT TO ${DB_ENV}StgTILTIDT WITH GRANT OPTION;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE
.LABEL SKIPGRANT
