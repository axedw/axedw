/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Load_Stg_Pricing.btq 4027 2012-06-25 09:52:25Z k9105194 $
# Last Changed By  : $Author: k9105194 $
# Last Change Date : $Date: 2012-06-25 11:52:25 +0200 (mån, 25 jun 2012) $
# Last Revision    : $Revision: 4027 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSAP/procedure/Load_Stg_Pricing.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: SAP PI calls this procedure for insert in stg-table
# Project	: EDW2
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-03-20 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	SAP PI calls this procedure for insert in stg-table
# Dependencies
#	${DB_ENV}StgSAPT.Stg_Pricing
#	${DB_ENV}StgSAPT.Stg_PricingQ
#	${DB_ENV}StgSAPT.Stg_Pricing_Full_Load
# --------------------------------------------------------------------------
*/
REPLACE PROCEDURE ${DB_ENV}StgSAPT.Load_Stg_Pricing
(	IN SequenceId	VARCHAR(40)
	,IN ContentType	VARCHAR(30)
	,IN SubContentType	VARCHAR(30)
	,IN TransactionTS	VARCHAR(26)
	,IN ExtractionTS	VARCHAR(26)
	,IN IEReceivedTS	VARCHAR(26)
	,IN IEDeliveryTS	VARCHAR(26)
	,IN SourceVersion	VARCHAR(10)
	,IN RecordCount	INTEGER
	,IN LoadIndicator	CHAR(1)
	,IN XmlData	CLOB(1000000000)
)
BEGIN

INSERT INTO ${DB_ENV}StgSAPT.Stg_Pricing
(	SequenceId
	,ContentType
	,SubContentType
	,TransactionTS
	,ExtractionTS
	,IEReceivedTS
	,IEDeliveryTS
	,SourceVersion
	,RecordCount
	,LoadIndicator
	,ArchiveKey
	,XmlData
) VALUES (
	:SequenceId
	,:ContentType
	,:SubContentType
	,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:ExtractionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:IEReceivedTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,CAST(:IEDeliveryTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	,:SourceVersion
	,:RecordCount
	,:LoadIndicator
	,(	TRIM(:SequenceId)
		|| '_' || TRIM(:LoadIndicator)
		|| '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 1 FOR 10)
		|| '_' || SUBSTRING(CAST(:TransactionTS AS VARCHAR(26)) FROM 12 FOR 15)
		)
	,:XmlData
);

IF LoadIndicator = 'D'
THEN
	INSERT INTO ${DB_ENV}StgSAPT.Stg_PricingQ
	(	SequenceId
		,ReferenceTS
	) VALUES (
		:SequenceId
		,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	);
ELSE
	INSERT INTO ${DB_ENV}StgSAPT.Stg_Pricing_Full_Load
	(	SequenceId
		,ReferenceTS
	) VALUES (
		:SequenceId
		,CAST(:TransactionTS AS TIMESTAMP(6) FORMAT 'YYYY-MM-DDBHH:MI:SS.S(6)')
	);
END IF;

END;
