/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SAPPricing_ReadQ.btq 7583 2012-11-20 05:35:33Z k9106435 $
# Last Changed By  : $Author: k9106435 $
# Last Change Date : $Date: 2012-11-20 06:35:33 +0100 (tis, 20 nov 2012) $
# Last Revision    : $Revision: 7583 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSAP/procedure/SAPPricing_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Procedure for Customer Reading Queue
# Project	: EDW
# Subproject:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-03-31 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Procedure for Customer Reading Queue
# Dependencies
#	${DB_ENV}StgSAPT.Stg_PricingQ
#	${DB_ENV}UtilT.SAP_Pricing_Load_Cache
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}StgSAPT.SAPPricing_ReadQ(IN WorkflowRunId VARCHAR(255))

BEGIN

DECLARE RecordCount INT; 					-- To keep frequency Limit
DECLARE Counter INT; 						-- To keep loop counter
DECLARE QCount INT; 						-- To keep count of records present in queue table

SET Counter = 1;							-- Set Counter to 1

SELECT RecordCount INTO :RecordCount FROM ${DB_ENV}StgSAPT.Stg_SAP_Load_Frequency WHERE SourceID = 53; -- Getting Frequency Limit

IF RecordCount = -1 THEN

BT;

INSERT INTO ${DB_ENV}UtilT.SAP_Pricing_Load_Cache (  -- Select all records from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)
SELECT
  SequenceId
 ,ReferenceTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_PricingQ
;DELETE FROM ${DB_ENV}StgSAPT.Stg_PricingQ;

ET;

ELSE

BT;

INSERT INTO ${DB_ENV}UtilT.SAP_Pricing_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)
SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_PricingQ
;

LOCK ROW FOR ACCESS 
SELECT COUNT(*) INTO :QCount FROM ${DB_ENV}StgSAPT.Stg_PricingQ; -- Get current row count of Queue table 

WHILE Counter < RecordCount AND QCount > 0 -- Loop until frequeny limit is reached or queue is emtpy

DO

INSERT INTO ${DB_ENV}UtilT.SAP_Pricing_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)

SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_PricingQ
;

SET Counter = Counter + 1; 
SET QCount = QCount - 1; -- Get current row count of Queue table 

END WHILE;  -- end loop

ET;

END IF;

END;
