/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: SAPPMRPromotion_ReadQ.btq 14711 2014-12-15 11:46:18Z a18249 $
# Last Changed By  : $Author: a18249 $
# Last Change Date : $Date: 2014-12-15 12:46:18 +0100 (må, 15 dec 2014) $
# Last Revision    : $Revision: 14711 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgSAP/procedure/SAPPMRPromotion_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Procedure for SAPPMRPromotion Reading Queue
# Project	: 
# Subproject:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2014-12-15           Initial version
# --------------------------------------------------------------------------
# Description
#	Procedure for SAPPMRPromotion Reading Queue
# Dependencies
#	
#	
# --------------------------------------------------------------------------
*/

REPLACE PROCEDURE ${DB_ENV}StgSAPT.SAPECCPromo_ReadQ(IN WorkflowRunId VARCHAR(255))

BEGIN

DECLARE RecordCount INT; 					-- To keep frequency Limit
DECLARE Counter INT; 						-- To keep loop counter
DECLARE QCount INT; 						-- To keep count of records present in queue table

SET Counter = 1;							-- Set Counter to 1

SELECT RecordCount INTO :RecordCount FROM ${DB_ENV}StgSAPT.Stg_SAP_Load_Frequency WHERE SourceID = 58; -- Getting Frequency Limit

IF COALESCE(RecordCount,-1) = -1 THEN
--IF RecordCount = -1 THEN

BT;

INSERT INTO ${DB_ENV}UtilT.SAP_ECC_Promotion_Load_Cache (  -- Select all records from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)
SELECT
  SequenceId
 ,ReferenceTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_ECC_PromotionQ
;DELETE FROM ${DB_ENV}StgSAPT.Stg_ECC_PromotionQ;

ET;

ELSE

BT;

INSERT INTO ${DB_ENV}UtilT.SAP_ECC_Promotion_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)
SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_ECC_PromotionQ
;

LOCK ROW FOR ACCESS 
SELECT COUNT(*) INTO :QCount FROM ${DB_ENV}StgSAPT.Stg_ECC_PromotionQ; -- Get current row count of Queue table 

WHILE Counter < RecordCount AND QCount > 0 -- Loop until frequeny limit is reached or queue is emtpy

DO

INSERT INTO ${DB_ENV}UtilT.SAP_ECC_Promotion_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)

SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
 ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgSAPT.Stg_ECC_PromotionQ
;

SET Counter = Counter + 1; 
SET QCount = QCount - 1; -- Get current row count of Queue table 

END WHILE;  -- end loop

ET;

END IF;

END;
