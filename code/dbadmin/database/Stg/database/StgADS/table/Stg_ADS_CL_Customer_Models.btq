/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: Stg_ADS_CL_Customer_Models.btq 15934 2015-04-03 07:01:58Z a43094 $
# Last Changed By  : $Author: a43094 $
# Last Change Date : $Date: 2015-04-03 09:01:58 +0200 (fre, 03 apr 2015) $
# Last Revision    : $Revision: 15934 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgADS/table/Stg_ADS_CL_Customer_Models.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
*/
.SET MAXERROR 0;

DATABASE ${DB_ENV}StgADST;

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}StgADST','Stg_ADS_CL_Customer_Models','PRE','V',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CREATE MULTISET TABLE ${DB_ENV}StgADST.Stg_ADS_CL_Customer_Models ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      Datum DATE ,
      Calendar_Month_Id INTEGER,
      Member_Account_Seq_Num INTEGER,
      Loyalty_Program_Seq_Num BYTEINT,
      RFM_Score DECIMAL(18,4),
      RFM_Lvl INTEGER,
      RFM_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      RFM_Model_ID INTEGER,
      Revenue_Score DECIMAL(18,4),
      Revenue_Lvl INTEGER,
      Revenue_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Revenue_Model_ID INTEGER,
      SoW_Score DECIMAL(18,4),
      Sow_Lvl INTEGER,
      SoW_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      SoW_Model_ID INTEGER,
      Kluster_Score DECIMAL(18,4),
      Kluster_Lvl INTEGER,
      Kluster_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Kluster_Model_ID INTEGER,
      Matris9_Score DECIMAL(18,4),
      Matris9_Lvl INTEGER,
      Matris9_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Matris9_Model_ID INTEGER,
      Top_Btm_100000_Score DECIMAL(18,4),
      Top_Btm_100000_Lvl INTEGER,
      Top_Btm_100000_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Top_Btm_Model_ID INTEGER,
      Specgrupp_Gluten_Score DECIMAL(18,4),
      Specgrupp_Gluten_Lvl INTEGER,
      Specgrupp_Gluten_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Specgrupp_Gluten_ID INTEGER,
      Specgrupp_Laktos_Score DECIMAL(18,4),
      Specgrupp_Laktos_Lvl INTEGER,
      Specgrupp_Laktos_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Specgrupp_Laktos_ID INTEGER,
      Specgrupp_EKO_Score DECIMAL(18,4),
      Specgrupp_EKO_Lvl INTEGER,
      Specgrupp_EKO_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Specgrupp_EKO_ID INTEGER,
      Specgrupp_Barn_Score DECIMAL(18,4),
      Specgrupp_Barn_Lvl INTEGER,
      Specgrupp_Barn_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Specgrupp_Barn_ID INTEGER,
      Specgrupp_Master_Score DECIMAL(18,4),
      Specgrupp_Master_Lvl INTEGER,
      Specgrupp_Master_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Specgrupp_Master_ID INTEGER,
      Top_Rank_Score DECIMAL(18,4),
      Top_Rank_Lvl INTEGER,
      Top_Rank_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Top_Rank_ID INTEGER,
      Grand_Amt_3000_3mths_Score DECIMAL(18,4),
      Grand_Amt_3000_3mths_Lvl INTEGER,
      Grand_Amt_3000_3mths_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Grand_Amt_3000_3mths_ID INTEGER,
      Sales_inc_Tax_1Mths_Score DECIMAL(18,4),
      Sales_inc_Tax_1Mths_Lvl INTEGER,
      Sales_inc_Tax_1Mths_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Sales_inc_Tax_1Mths_ID INTEGER,
      Recpts_1Mths_Score DECIMAL(18,4),
      Recpts_1Mths_Lvl INTEGER,
      Recpts_1Mths_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Recpts_1Mths_ID INTEGER,
      Avg_Pur_inc_Tax_1Mths_Score DECIMAL(18,4),
      Avg_Pur_inc_Tax_1Mths_Lvl INTEGER,
      Avg_Pur_inc_Tax_1Mths_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      Avg_Pur_inc_Tax_1Mths_ID INTEGER,
      BV_1Mths_Score DECIMAL(18,4),
      BV_1Mths_Lvl INTEGER,
      BV_1Mths_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      BV_1Mths_ID INTEGER,
      BV_pct_1Mths_Score DECIMAL(18,4),
      BV_pct_1Mths_Lvl INTEGER,
      BV_pct_1Mths_Name VARCHAR(60) CHARACTER SET LATIN NOT CASESPECIFIC,
      BV_pct_1Mths_ID INTEGER,
	  InsertDttm TIMESTAMP(6) FORMAT 'YYYY-MM-DDHH:MI:SS.S(6)', 	  
      SourceVersion VARCHAR(10) CHARACTER SET LATIN NOT CASESPECIFIC,
      TS TIMESTAMP(6) FORMAT 'YYYY-MM-DD HH:MI:SS.S(6)' DEFAULT CURRENT_TIMESTAMP NOT NULL,
      ArchiveKey VARCHAR(256) CHARACTER SET LATIN NOT CASESPECIFIC)
PRIMARY INDEX ( Member_Account_Seq_Num );

.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

CALL ${DB_ENV}dbadmin.DBA_NewTabDef('${DB_ENV}StgADST','Stg_ADS_CL_Customer_Models','POST','V',1)
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE

COMMENT ON ${DB_ENV}StgADST.Stg_ADS_CL_Customer_Models IS '$Revision: 15934 $ - $Date: 2015-04-03 09:01:58 +0200 (fre, 03 apr 2015) $ '
;
.IF ERRORCODE <> 0 THEN .EXIT ERRORCODE





