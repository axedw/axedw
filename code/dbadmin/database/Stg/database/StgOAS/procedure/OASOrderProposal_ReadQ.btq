/*
# ----------------------------------------------------------------------------
# SVN Infostamp             (DO NOT EDIT THE NEXT 9 LINES!)
# ----------------------------------------------------------------------------
# ID               : $Id: OASOrderProposal_ReadQ.btq 4394 2012-07-20 13:37:41Z k9105777 $
# Last Changed By  : $Author: k9105777 $
# Last Change Date : $Date: 2012-07-20 15:37:41 +0200 (fre, 20 jul 2012) $
# Last Revision    : $Revision: 4394 $
# Subversion URL   : $HeadURL: http://axprsv01.axfood.se/svn/axedw/trunk/code/dbadmin/database/Stg/database/StgOAS/procedure/OASOrderProposal_ReadQ.btq $
# --------------------------------------------------------------------------
# SVN Info END
# --------------------------------------------------------------------------
# Purpose	: Procedure for OrderProposal Reading Queue
# Project	: EDW2
# Subproject	:
# --------------------------------------------------------------------------
# Change History
# Date       Author    Description
# 2012-03-31 Teradata  Initial version
# --------------------------------------------------------------------------
# Description
#	Procedure for OrderProposal Reading Queue
# Dependencies
#	${DB_ENV}StgOAST.Stg_OAS_OrderProposalQ
#	${DB_ENV}StgOAST.OAS_OrderProposal_Load_Cache
# --------------------------------------------------------------------------
*/




REPLACE PROCEDURE ${DB_ENV}StgOAST.OASOrderProposal_ReadQ(IN WorkflowRunId VARCHAR(255))
BEGIN
DECLARE QCount INT; -- To keep count of records present in queue table


INSERT INTO ${DB_ENV}UtilT.OAS_OrderProposal_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)

SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
    ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgOAST.Stg_OAS_OrderProposalQ
;

LOCK ROW FOR ACCESS 
SELECT COUNT(*) INTO :QCount FROM ${DB_ENV}StgOAST.Stg_OAS_OrderProposalQ; -- Get current row count of Queue table 

BT;

WHILE QCount > 0 -- Loop until queue is emtpy

DO

INSERT INTO ${DB_ENV}UtilT.OAS_OrderProposal_Load_Cache (  -- Select and Consume top row from queue table and insert in cache table
  SequenceId
 ,ReferenceTS
 ,WorkflowRunId
 ,TS
)

SELECT AND CONSUME TOP 1
  SequenceId
 ,ReferenceTS
    ,:WorkflowRunId
 ,TS
FROM
 ${DB_ENV}StgOAST.Stg_OAS_OrderProposalQ
;

SET QCount = QCount - 1; -- Get current row count of Queue table 

END WHILE;  -- end loop

ET;

END;