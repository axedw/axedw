REPLACE VIEW SYS_MGMT.QryLogObjectsAllBoth
AS
SELECT    
QueryID,
'NTP' as SystemGeneration,
CAST(collecttimestamp AS DATE) AS logdate,
ProcID,
CollectTimeStamp,
ObjectDatabaseName,
ObjectTableName,
ObjectColumnName,
ObjectID,
ObjectNum,
ObjectType,
FreqofUse,
TypeOfUse   
FROM DBC.QryLogObjects
UNION ALL
SELECT    
QueryID,
'NTP' as SystemGeneration,
LogDate,
ProcID,
CollectTimeStamp,
ObjectDatabaseName,
ObjectTableName,
ObjectColumnName,
ObjectID,
ObjectNum,
ObjectType,
FreqofUse,
TypeOfUse   
FROM SYS_MGMT.QryLogObjects
UNION ALL
SELECT    
QueryID,
'OTP' as SystemGeneration,
LogDate,
ProcID,
CollectTimeStamp,
ObjectDatabaseName,
ObjectTableName,
ObjectColumnName,
ObjectID,
ObjectNum,
ObjectType,
FreqofUse,
TypeOfUse   
FROM SYS_MGMT_OTP.QryLogObjects
;