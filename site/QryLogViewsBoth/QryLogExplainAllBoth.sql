REPLACE VIEW SYS_MGMT.QryLogExplainAllBoth
AS
SELECT
     QueryID,
'NTP' as SystemGeneration,
     cast(collecttimestamp as date) as LogDate,
     ProcID,
     CollectTimeStamp,
     ExpRowNo,
     ExplainText
FROM DBC.QryLogExplain
UNION ALL
SELECT
     QueryID,
'NTP' as SystemGeneration,
     LogDate,
     ProcID,
     CollectTimeStamp,
     ExpRowNo,
     ExplainText
FROM SYS_MGMT.QryLogExplain
UNION ALL
SELECT
     QueryID,
'OTP' as SystemGeneration,
     LogDate,
     ProcID,
     CollectTimeStamp,
     ExpRowNo,
     ExplainText
FROM SYS_MGMT_OTP.QryLogExplain
;