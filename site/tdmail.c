#define SQL_TEXT Latin_Text
#include <sqltypes_td.h>
#include <stdio.h>
#include <stdlib.h>

/******************************************************************/
/* exec sendmail to send the mail. It returns -1 if it failed. */
/******************************************************************/
static int send_to_system(char *mail_msg, int from_addr)
{
	char c;
	char *mailcmd;
	FILE *mailhndl;

	/*********************************/
	/* sets up the send mail command */
	/*********************************/
	/* If the caller did not provide a from user then supply ""mailmgr" */
	if (from_addr == -1)
		mailcmd = "/usr/sbin/sendmail -oi -f mailmgr -oem -t";
	else
		mailcmd = "/usr/sbin/sendmail -oi -oem -t";

	/* Execute the send mail command */
	mailhndl = (FILE *) popen(mailcmd, "w");
	/* If the handle is not null then at least it took it */
	if (mailhndl == NULL)
		return -1;

	/* Pipe the message otherwise there is not going to be any mail */
	while ((c= *mail_msg++) != 0)
		putc(c,mailhndl);

	/* sent the whole message- close the pipe to let it know we are done */
	pclose(mailhndl);
	return 0;
}

/*********************************/
/* The External Stored Procedure */
/*********************************/
void tdmail(
	VARCHAR_LATIN *From, /* who it is from */
	VARCHAR_LATIN *To, /* For who Name@domain from */
	VARCHAR_LATIN *Subject, /* Need a subject */
	VARCHAR_LATIN *Message, /* What you want to say */
	int *i_from,
	int *i_to,
	int *i_subject,
	int *i_message,
	char sqlstate[6],
	SQL_TEXT extname[129],
	SQL_TEXT specific_name[129],
	SQL_TEXT error_message[257]
)
{
	char *mailbuf;
	int status;
	char *from_usr;
	char *subject_usr;
	char *message_usr;

	/* Format the email into standard form for mail. */
	/* If there is no "TO" address then it is not going anywhere. */
	/* There might me other problems. We probably should check for a */
	/* properly formated sender address of the form <name>@<domain>. */
	if (*i_to == -1) {
		strcpy((char *) sqlstate, "U0001");
		strcpy((char *) error_message, "There is no 'To' address");
		return;
	}

	/* If no "FROM" user, then use mailmgr. */
	if (*i_from == -1)
		from_usr = "mailmgr";
	else
		from_usr = (char *) From;

	/* No subject. Just substitute or make one up. */
	if (*i_subject == -1)
		subject_usr = "<no subject>";
	else
		subject_usr = (char *) Subject;

	/* No message. Just make one up. */
	if (*i_message == -1)
		message_usr = "<Empty Message>";
	else
		message_usr = (char *) Message;

	/* Need to allocate memory to hold formated message. */
	mailbuf = FNC_malloc(
		strlen(From)+
		strlen(To) +
		strlen(Subject)+
		strlen(Message)+ 1000
	);

	/* This will format it. The assumption is that the message */
	/* part is already formatted with returns and the like. */
	sprintf(
		mailbuf,
		"From: %s\nTo: %s\nSubject: %s\n\n%s\n\0",
		from_usr,
		To,
		subject_usr,
		message_usr
	);

	/* Call sendmail to send it off. */
	status = send_to_system(mailbuf, *i_from);
	FNC_free(mailbuf);
	/* Well we got an error back. At this point just send a warning. */
	if (status < 0)	{
		strcpy(sqlstate, "01H001");
		strcpy((char *) error_message, "Error: mail not sent");
	}
}
