For each folder do the following to create an R-Package (tdlob as example):
	# creat an installable tarball
	R CMD build tdlob
	# check the tarball and document the test
	R CMD check tdlob
	# install the tarball into R as a package
	R CMD INSTALL tdlob_1.0.tar.gz
